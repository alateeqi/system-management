<?php

namespace Modules\Account\Http\Controllers\Client;


use Illuminate\Support\Facades\DB;
use Modules\Account\Http\Requests\Client\UpdateAccount;
use Modules\Account\Http\Requests\Client\UpdatePassword;
use Modules\Account\Interfaces\Client\ClientAccountInterface;
use Modules\Auth\Http\Controllers\Client\Controller;


class ClientAccountController extends Controller
{
    public function updateAccount(UpdateAccount $request, ClientAccountInterface $interface)
    {
        try {
            DB::beginTransaction();
            $interface->updateAccount($request->validated());
            DB::commit();
            return $this->respondWithSuccess();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->respondWithError($exception->getMessage());
        }

    }

    public function updatePassword(UpdatePassword $request, ClientAccountInterface $interface)
    {
        try {
            DB::beginTransaction();
            $interface->updatePassword($request->validated());
            DB::commit();
            return $this->respondWithSuccess();
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->respondWithError($exception->getMessage());
        }

    }

}
