<?php

namespace Modules\Account\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccount extends FormRequest
{
    public function authorized()
    {
        return true;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $customer = request()->user();
        return [
            "name" => ['nullable','max:255'],
            "email" => ['nullable','email:rfc,dns','unique:customers,email,' . $customer->id]
        ];
    }
}
