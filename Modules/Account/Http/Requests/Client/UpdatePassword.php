<?php

namespace Modules\Account\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePassword extends FormRequest
{
    public function authorized()
    {
        return true;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            "password" => ['confirmed','min:6','required'],

        ];
    }
}
