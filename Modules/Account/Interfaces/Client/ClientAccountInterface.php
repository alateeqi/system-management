<?php

namespace Modules\Account\Interfaces\Client;

interface ClientAccountInterface
{
    public function updateAccount($data);

    public function updatePassword($data);
}
