<?php

namespace Modules\Account\Repositories\Client;

use Illuminate\Support\Facades\Hash;
use Modules\Account\Interfaces\Client\ClientAccountInterface;
use Modules\Customer\Entities\Customer;

class ClientAccountRepository implements ClientAccountInterface
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function updateAccount($data)
    {
        $this->customer->name = $data['name'];
        $this->customer->email = $data['email'];
        $this->customer->save();
        return $this->customer;
    }

    public function updatePassword($data)
    {
        $this->customer->password = Hash::make($data['password']);
        $this->customer->save();
        return $this->customer;
    }
}
