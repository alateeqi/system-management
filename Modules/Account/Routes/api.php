<?php



Route::middleware('auth:api')->group(function () {
    Route::post('update-account', 'Client\ClientAccountController@updateAccount');
    Route::post('update-password', 'Client\ClientAccountController@updatePassword');
});
