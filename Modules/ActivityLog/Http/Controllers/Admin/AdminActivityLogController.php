<?php

namespace Modules\ActivityLog\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\ActivityLog\Http\Requests\Admin\Filter;
use Modules\ActivityLog\Interfaces\Admin\AdminActivityLogInterface;
use Modules\User\Interfaces\Admin\AdminUserInterface;
use Spatie\Activitylog\Models\Activity;

class AdminActivityLogController extends Controller
{

    public function index(AdminUserInterface $interface, AdminActivityLogInterface $interface_logs)
    {
        $users = $interface->allUser();
        $activities = $interface_logs->index();
        return view('activitylog::admin.index', compact('users', 'activities'));
    }

    public function show(Filter $request, AdminActivityLogInterface $interface)
    {
        $activities = $interface->filter($request->validated());
        return view('activitylog::admin.partials.datatable', compact('activities'));
    }

}
