<?php

namespace Modules\ActivityLog\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Filter extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => ['nullable', 'numeric'],
            'date_from' => ['required', 'date'],
            'date_to' => ['nullable', 'date'],
        ];
    }
}
