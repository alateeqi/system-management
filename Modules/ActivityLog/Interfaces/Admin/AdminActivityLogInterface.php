<?php

namespace Modules\ActivityLog\Interfaces\Admin;

interface AdminActivityLogInterface
{
    public function filter($data, $paginate = 20);

    public function index($paginate = 20);
}
