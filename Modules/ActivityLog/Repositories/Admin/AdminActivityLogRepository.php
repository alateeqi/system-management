<?php

namespace Modules\ActivityLog\Repositories\Admin;

use Carbon\Carbon;
use Modules\ActivityLog\Interfaces\Admin\AdminActivityLogInterface;
use Modules\Auth\Entities\User;
use Spatie\Activitylog\Models\Activity;

class AdminActivityLogRepository implements AdminActivityLogInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index($paginate = 20)
    {
        $startDate = Carbon::parse(now())->format('Y-m-d 00:00:00');
        $endDate = Carbon::parse(now())->format('Y-m-d 23:59:59');
        return Activity::orderBy('id', 'desc')->whereBetween('created_at', [$startDate, $endDate])
            ->paginate($paginate);
    }
    public function filter($data,$paginate = 20)
    {
//        $activities = Activity::where('created_at', Carbon::parse($data['date'])->format('Y-m-d'));
//
//        if (isset($data['user_id']))
//            $activities = $activities->where('causer_id', $data['user_id']);
//
//        if ($data['type'] == 'group')
//            $activities =$activities->orderBy('created_at')
//                ->orderBy('id')
//                ->groupBy('description') ;
//        return $activities->paginate($paginate);
        $activities = Activity::orderBy('id', 'desc');
        if (isset($data['date_from'])) {

            $activities = $activities->where('created_at', '>=', Carbon::parse($data['date_from'])->format('Y-m-d 00:00:00'));
        }
        if (isset($data['date_to'])) {

            $activities = $activities->where('created_at', '<=', Carbon::parse($data['date_to'])->format('Y-m-d 23:59:59'));
        }


        if (($data['user_id']) !== "0")
            $activities = $activities->where('causer_id', $data['user_id']);


//        return $activities->paginate($paginate);
        return $activities->get();
    }

}
