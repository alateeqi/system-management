@extends('layouts.dashboard.app')
@section('content')



    <!-- Main content -->
    <section class="page-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-12">


                    <div class="card">
                        <div class="card-header">

                            Activity Logs

                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-group" method="GET"
                                  action="{{route('logs.filter')}}" id="form_filter_activity">

                                <div class="row">
                                    <div class="form-group col-4">
                                        <label>From</label>


                                        <input id="date_from" type="date" name="date_from"
                                               class="form-control"
                                                   value="{{!is_null(request('date_from'))?request('date_from'):now()->toDateString()}}">


                                    </div>

                                    <div class="form-group col-4">
                                        <label>To</label>

                                        <input id="date_to" type="date" name="date_to"
                                               class="form-control"
                                               value="{{!is_null(request('date_to'))?request('date_to'):now()->toDateString()}}">

                                    </div>


                                </div>

                                <div class="row">
                                    <div class="form-group col-4">
                                        <label> User :</label>
                                        <select class="form-control" name="user_id" id="user_id">
                                            <option value="0">All Users</option>
                                            @foreach($users as $index=>$user)
                                                <option
                                                    value="{{$index}}" {{$index==request('user_id')?'selected':''}}>{{$user}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>
                                <br/>
                                <div class="row">
                                    <div class="form-group col-4 ">
                                        <button type="submit" class="btn btn-primary  form-control">

                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div id="data-response">
                                @include('activitylog::admin.partials.datatable')
                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('js')



    <script>



        $('#form_filter_activity').on('submit', function (e) {
            e.preventDefault()
            $('#data-response').empty()

            $.get('{{route('logs.filter')}}?' + $('#form_filter_activity').serialize(), function (data, status) {
                $('#data-response').append(data)

            });
        })
    </script>
@endpush

