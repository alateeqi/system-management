<div class="row ">
    <div class="col-12 mt-3 ">
        <div class="card ">
            <div class="card-body">
                <div class="table-responsive">
                    <table  class="table  table-bordered">
                        <thead>
                        <tr>
                            <th>User</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activities
                        ->groupBy(['causer_id', function($item) { return $item->created_at->toDateTimeString(); }]) as $userActivities)
                            <tr>
                                <td width="150">
                                    <b>{{ $userActivities->first()->first()->causer->name }}</b>
                                    <div>
                                        First process At :
                                        <b>{{ $userActivities->min(function ($items){return $items->min('created_at');}) }}</b>
                                    </div>
                                    <div>
                                        Last process At :
                                        <b>{{ $userActivities->max(function ($items){return $items->max('created_at');}) }}</b>
                                    </div>
                                </td>
                                <td>
                                    <table class="table  table-bordered" style="width: 100%;">
                                        <tbody>
                                        @foreach($userActivities->sortByDesc(function($item) { return  $item->first()->created_at; }) as $userActivitiesDate)
                                            <tr>
                                                <td width="120px">
                                                    {{ $userActivitiesDate->first()->created_at->toDateTimeString() }}
                                                </td>
                                                <td>
                                                    @foreach($userActivitiesDate as $activity)
                                                        <h3 class="text-capitalize">{{ $activity->description }} </h3>
                                                        <div>
                                                            <b> ID </b>:{{ optional($activity->subject)->id }}
                                                        </div>
                                                        @if(strpos($activity->description,'updated') !== false)
                                                            @foreach(json_decode($activity->properties)->data as $attribute => $values)
                                                                <div>
                                                                    <span class="text-capitalize">{{ $attribute }}</span>
                                                                    : {!! $values->old ?? 'null'  !!}
                                                                    <strong class="text-capitalize"> to </strong> {!! $values->new  !!}
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                        <hr>
                                                    @endforeach
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
