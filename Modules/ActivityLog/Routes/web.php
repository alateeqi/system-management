<?php


use Illuminate\Support\Facades\Route;




Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            Route::get('/logs/filter', [\Modules\ActivityLog\Http\Controllers\Admin\AdminActivityLogController::class,'show'])->name('logs.filter');
            Route::get('/logs', [\Modules\ActivityLog\Http\Controllers\Admin\AdminActivityLogController::class,'index'])->name('logs.index');
            Route::get('/file/logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);


        });
    }
);
