<?php

namespace Modules\Address\Entities;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Modules\Customer\Entities\Customer;
use Modules\Taxonomy\Entities\Taxonomy;

class Address extends Model
{
    use SoftDeletes,RecordsActivity;

    protected $with = [
        'city'
    ];

    protected $fillable = [
        'customer_id',
        'city_id',
        'street',
        'block',
        'house',
        'avenue',
        'flat',
        'floor',
        'notes',
    ];

    protected static function boot()
    {
        parent::boot();
        if (Auth::guard('web')->check() && request()->user()) {
            self::savingActivity();

            static::creating(function ($model) {
                $model->user_id = request()->user()->id;
            });

            static::updating(function ($model) {
                $model->editor_id = request()->user()->id;
            });
        }else{
            static::creating(function ($model) {
                $model->user_id = 1;
            });

            static::updating(function ($model) {
                $model->editor_id = 1;
            });
        }
    }


    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function city()
    {
        return $this->belongsTo(Taxonomy::class);
    }

}
