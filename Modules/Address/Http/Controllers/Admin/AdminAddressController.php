<?php

namespace Modules\Address\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Address\Http\Requests\Admin\Store;
use Modules\Address\Interfaces\Admin\AdminAddressInterface;
use Modules\Address\Transformer\Admin\AdminAddressTransformer;

class AdminAddressController extends Controller
{
    public function findByPhone(AdminAddressInterface $interface, $phone)
    {
        return response()->json([
            'data' => $interface->addressByPhoneCustomer($phone)
        ]);
    }

    public function store(Store $request, AdminAddressInterface $interface)
    {
        try {
            DB::beginTransaction();

            $data = $interface->store($request->validated());
            DB::commit();

            return response()->json([
                'data' => (new AdminAddressTransformer())->transform($data)
            ]);
        } catch (\Exception $exception) {

            DB::rollBack();
            return response()->json([
                'data' => $exception->getMessage()
            ], 500);

        }
    }
}
