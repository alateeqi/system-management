<?php

namespace Modules\Address\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Address\Http\Requests\Client\Update;
use Modules\Address\Interfaces\Client\ClientAddressInterface;
use Modules\Auth\Http\Controllers\Client\Controller;

class ClientAddressController extends Controller
{
    public function updateAddresses(Update $request, ClientAddressInterface $interface)
    {

        try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();

            return $this->respondWithSuccess();
        } catch (\Exception $exception) {

            DB::rollBack();

            return $this->respondWithError($exception->getMessage());
        }

    }
}
