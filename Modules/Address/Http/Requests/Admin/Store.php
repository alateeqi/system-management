<?php

namespace Modules\Address\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            "customer_phone" => ['required', 'numeric'],
            "city_id" => ['required', 'numeric', 'exists:taxonomies,id'],
            "street" => ['nullable'],
            "block" => ['nullable'],
            "house" => ['nullable'],
            "avenue" => ['nullable'],
            "flat" => ['nullable', 'digits_between:1,500'],
            "floor" => ['nullable', 'digits_between:-20,150'],
            "notes" => ['nullable'],
        ];
    }
}
