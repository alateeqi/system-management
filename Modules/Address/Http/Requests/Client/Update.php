<?php

namespace Modules\Address\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'addresses'=>['required','array'],
            "addresses.*.city" => ['required'],
            "addresses.*.city.id" => ['required'],
            "addresses.*.street" => ['required'],
            "addresses.*.block" => ['required'],
            "addresses.*.house" => ['required'],
            "addresses.*.flat" => ['nullable', 'digits_between:1,500'],
            "addresses.*.floor" => ['nullable', 'digits_between:-20,150'],
        ];
    }
}

