<?php

namespace Modules\Address\Interfaces\Admin;

interface AdminAddressInterface
{
    public function addressByPhoneCustomer($phone);

    public function store($data);
}
