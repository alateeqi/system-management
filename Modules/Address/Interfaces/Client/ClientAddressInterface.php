<?php

namespace Modules\Address\Interfaces\Client;

interface ClientAddressInterface
{
    public function find($id);

    public function update($data);
}
