<?php

namespace Modules\Address\Repositories\Admin;

use Modules\Address\Entities\Address;
use Modules\Address\Interfaces\Admin\AdminAddressInterface;
use Modules\Auth\Entities\User;
use Modules\Customer\Repositories\Admin\AdminCustomerRepository;

class AdminAddressRepository implements AdminAddressInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function find($id)
    {
        return Address::find($id);
    }

    public function addressByPhoneCustomer($phone)
    {
        $customer = (new AdminCustomerRepository($this->user))->findByPhone($phone);
        return Address::where('customer_id', $customer->id)->get();
    }

    public function store($data)
    {
        $address = new Address();
        $customer = (new AdminCustomerRepository($this->user))->findByPhone($data['customer_phone']);
        if (is_null($customer))
            $customer = (new AdminCustomerRepository($this->user))->store(['phone' => $data['customer_phone']]);
        $address->customer_id = $customer->id;
        $address->city_id = $data['city_id'];
        if (isset($data['street']))
            $address->street = $data['street'];
        if (isset($data['block']))
            $address->block = $data['block'];
        if (isset($data['house']))
            $address->house = $data['house'];
        if (isset($data['avenue']))
            $address->avenue = $data['avenue'];
        if (isset($data['flat']))
            $address->flat = $data['flat'];
        if (isset($data['floor']))
            $address->floor = $data['floor'];
        if (isset($data['notes']))
            $address->notes = $data['notes'];

        $address->save();

        return $address;
    }
}
