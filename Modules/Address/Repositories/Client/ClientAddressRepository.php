<?php

namespace Modules\Address\Repositories\Client;

use Modules\Address\Entities\Address;
use Modules\Address\Interfaces\Client\ClientAddressInterface;
use Modules\Customer\Entities\Customer;

class ClientAddressRepository implements ClientAddressInterface
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function find($id)
    {
        return Address::find($id);
    }

    public function update($data)
    {
        foreach ($data['addresses'] as $requestedAddress) {
            if (!empty($requestedAddress['id'])) {
                $address = $this->find($requestedAddress['id']);
            } else
                $address = new Address();

            $address->customer_id = $this->customer['id'];
            $address->city_id = $requestedAddress['city']['id'];
            $address->street = $requestedAddress['street'];
            $address->block = $requestedAddress['block'];
            $address->house = $requestedAddress['house'];
            $address->avenue = $requestedAddress['avenue'];
            $address->flat = $requestedAddress['flat'];
            $address->floor = $requestedAddress['floor'];
            $address->notes = $requestedAddress['notes'];
            $address->save();
            $registeredAddresses[] = $address->id;
        }
        $this->customer->addresses()->whereNotIn('id', $registeredAddresses)->delete();
        return $address;
    }

    public function store($address)
    {
        $new_address = new Address();

        $new_address->customer_id = $this->customer->id;
        $new_address->city_id = $address['city']['id'];
        $new_address->street = $address['street'];
        $new_address->block = $address['block'];
        $new_address->house = $address['house'];
        $new_address->avenue = $address['avenue'];
        $new_address->flat = $address['flat'];
        $new_address->floor = $address['floor'];
        $new_address->notes = $address['notes'];
        $new_address->save();

        return $new_address;
    }

}
