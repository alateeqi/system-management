<?php

Route::middleware('auth:api')->group(function (){
    Route::post('update-addresses', 'Client\ClientAddressController@updateAddresses');
});
