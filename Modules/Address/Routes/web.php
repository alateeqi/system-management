<?php

use Illuminate\Support\Facades\Route;


Route::group(
    ['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {
            //address routes
            Route::get('/address-phone/{phone}', [\Modules\Address\Http\Controllers\Admin\AdminAddressController::class, 'findByPhone'])->name('admin.address.customer.phone');
            Route::post('/address', [\Modules\Address\Http\Controllers\Admin\AdminAddressController::class, 'store'])->name('admin.address.store');

        });
    }
);
