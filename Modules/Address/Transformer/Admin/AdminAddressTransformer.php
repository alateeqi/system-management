<?php

namespace Modules\Address\Transformer\Admin;

use League\Fractal\TransformerAbstract;
use Modules\Address\Entities\Address;
use Modules\Taxonomy\Repositories\Admin\City\AdminCityRepository;

class AdminAddressTransformer extends TransformerAbstract
{
    public function transform(Address $address)
    {
        return [
            'id' => $address->id,
            'city' => (new AdminCityRepository(auth()->user()))->find($address->city_id),
            'street' => $address->street,
            'floor' => $address->floor,
            'flat' => $address->flat,
            'avenue' => $address->avenue,
            'block' => $address->block,
            'house' => $address->house,

        ];
    }
}
