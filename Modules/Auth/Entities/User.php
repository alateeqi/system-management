<?php

namespace Modules\Auth\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Points\Entities\Point;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property mixed $working_time
 * @property mixed $bank_details
 * @property mixed $salary
 * @property string $birth_date
 * @property string $employment_date
 * @property string $role
 */
class User extends Authenticatable implements HasMedia
{
    use LaratrustUserTrait;
    use Notifiable, SoftDeletes, InteractsWithMedia, RecordsActivity, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'time_start',
        'bank_details',
        'salary',
        'birth_date',
        'time_end',
        'start_date_employment',
        'end_date_employment',
        'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }
        self::savingActivity();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('full')
            ->performOnCollections('images');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(450)
            ->performOnCollections('images');

        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(225)
            ->performOnCollections('images');
    }

    public function points()
    {
        return $this->belongsToMany(Point::class, 'users_points', 'user_id', 'point_id');
    }
}
