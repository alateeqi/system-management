<?php

namespace Modules\Auth\Http\Controllers\Client;


use App\Notifications\SendOTP;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Http\Requests\Client\Login;
use Modules\Auth\Http\Requests\Client\Register;
use Modules\Auth\Http\Requests\Client\Reset;
use Modules\Auth\Interfaces\Client\ClientAuthInterface;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Repositories\Guest\GuestCustomerRepository;
use Symfony\Component\HttpFoundation\Response;

class ClientAuthController extends Controller
{
    public function login(Login $request, ClientAuthInterface $interface)
    {
        $client = (new GuestCustomerRepository())->findByPhone($request['phone']);
        if (!$client) {
            return $this->register($request, $interface);
        }
        Auth::shouldUse('web-api');
        $credentials = ['phone' => $request['phone'], 'password' => $request['password']];
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('Personal Access Token', []);
            $token->token->expires_at = Carbon::now()->addYears(3);
            $token->token->save();

            return $this->setData([
                'token' => $token->accessToken,
                'tokenType' => 'Bearer',
                'expiresAt' => Carbon::parse($token->token->expires_at)->toISOString(),
                'user' => $user,
            ])->respondWithSuccess('Logged in Successfully!');
        }

        return $this->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->respondWithError(trans('auth.failed'));
    }

    public function register(Register $request, ClientAuthInterface $interface)
    {
        $client = (new GuestCustomerRepository())->findByPhone($request['phone']);
        if ($client) {
            return $this->reset($request, $interface);
        }

        $customer = $interface->register($request);

        return $this->respondWithSuccess('We have been sent your account information to: +965' . $customer->phone);
    }

    public function reset(Reset $request, ClientAuthInterface $interface)
    {
        $customer = (new GuestCustomerRepository())->findByPhone($request['phone']);

        if (!$customer) {
            return $this->respondValidationFails('This number is not exist, you can it register now!');
        } else {
            $interface->reset($customer);
            return $this->respondWithSuccess('We have been sent your account information to: +965' . $customer->phone);
        }
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        if ($user && $user->token()->delete()) {
            return $this->respondWithSuccess('You\'re logged out!');
        }

        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
            ->respondWithError();
    }
}
