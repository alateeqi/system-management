<?php

namespace Modules\Auth\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone' => ['required', 'regex:/[0-9]{8}/', 'unique:customers,phone']
        ];
    }
}
