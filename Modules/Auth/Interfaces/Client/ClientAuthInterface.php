<?php

namespace Modules\Auth\Interfaces\Client;

use Modules\Customer\Entities\Customer;

interface ClientAuthInterface
{

    public function register($data);



    public function reset(Customer $customer);


}
