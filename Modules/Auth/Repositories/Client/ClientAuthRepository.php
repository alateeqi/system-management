<?php

namespace Modules\Auth\Repositories\Client;

use App\Notifications\SendOTP;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Interfaces\Client\ClientAuthInterface;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Repositories\Guest\GuestCustomerRepository;

class ClientAuthRepository implements ClientAuthInterface
{

    public function register($data)
    {
        $password = rand(00000000, 99999999);
        $customer = Customer::create([
            'phone' => $data['phone'],
            'password' => Hash::make($password),
            'user_id' => 1
        ]);

        $customer->notify(new SendOTP($password, $data['phone']));
        return $customer;
    }

    public function reset(Customer $customer)
    {
        $password = rand(00000000, 99999999);
        $customer->password = Hash::make($password);
        $customer->save();

        $customer->notify(new SendOTP($password, $customer->phone));
    }
}
