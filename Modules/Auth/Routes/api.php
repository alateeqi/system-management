<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'throttle:15'], function () {
    Route::post('login', 'Client\ClientAuthController@login');
    Route::post('reset', 'Client\ClientAuthController@reset');
    Route::post('register', 'Client\ClientAuthController@register');
    Route::post('logout', 'Client\ClientAuthController@logout')->middleware('auth:api');
});
