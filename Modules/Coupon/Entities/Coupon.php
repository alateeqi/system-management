<?php

namespace Modules\Coupon\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Customer\Entities\Customer;
use Modules\Gift\Entities\Gift;

class Coupon extends Model
{
    use SoftDeletes;

    protected $casts = [
        'starts_at' => 'datetime',
        'ends_at' => 'datetime',
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'customer_coupon', 'coupon_id', 'customer_id');
    }

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }
}
