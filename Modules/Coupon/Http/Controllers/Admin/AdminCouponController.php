<?php

namespace Modules\Coupon\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Coupon\Entities\Coupon;

use Modules\Coupon\Http\Requests\Admin\Store;
use Modules\Coupon\Http\Requests\Admin\Update;
use Modules\Coupon\Interfaces\Admin\AdminCouponInterface;

class AdminCouponController extends Controller
{
    public function __construct(Coupon $Coupon)
    {
        $this->middleware(['permission:coupons-read'])->only('index');
        $this->middleware(['permission:coupons-create'])->only('create');
        $this->middleware(['permission:coupons-update'])->only('show');
        $this->middleware(['permission:coupons-delete'])->only('destroy');
    }
    public function index(AdminCouponInterface $interface)
    {
        $coupons = $interface->index();
        return view('coupon::admin.index', compact('coupons'));
    }

    public function create()
    {
        return view('coupon::admin.create');
    }

    public function show(AdminCouponInterface $interface, $id)
    {
        $coupon = $interface->find($id);
        return view('coupon::admin.edit', compact('coupon'));
    }

    public function store(Store $request, AdminCouponInterface $interface)
    {
        try {
            DB::beginTransaction();
            $coupon = $interface->store($request->validated());
            DB::commit();
            return response()->json([
                'message' => $coupon->name . ' created successfully'
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function update(Update $request, AdminCouponInterface $interface)
    {
        try {
            DB::beginTransaction();
            $coupon = $interface->update($request->validated());
            DB::commit();
            return response()->json([
                'message' => $coupon->name . ' updated successfully'
            ], 200);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function destroy(AdminCouponInterface $interface,$id)
    {

        try {
            DB::beginTransaction();
            $interface->delete($id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('admin.coupons.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->route('admin.coupons.index')->withErrors($exception->getMessage());
        }

    }
}
