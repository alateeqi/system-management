<?php

namespace Modules\Coupon\Http\Controllers\Client;


use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Coupon\Interfaces\Client\ClientCouponInterface;

class ClientCouponsController extends Controller
{
    public function coupons(ClientCouponInterface $interface)
    {
        return $interface->coupons();
    }
}
