<?php

namespace Modules\Coupon\Http\Controllers\Guest;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Coupon\Interfaces\Guest\GuestCouponInterface;


class GuestCouponController extends Controller
{
    public function coupon(GuestCouponInterface $interface, $coupon, $productPrice)
    {
        return response()->json($interface->coupon($coupon, $productPrice));
    }
}
