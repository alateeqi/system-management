<?php

namespace Modules\Coupon\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            "code" => ['required', 'unique:coupons,code'],
            "name" => ['required', 'string'],
            "value" => ['required', 'digits_between:1,500'],
            "type_discount" => ['required', 'numeric'],
            "customer_phones" => ['nullable', 'array'],
            "customer_phones.*" => ['nullable', 'numeric', 'exists:customers,phone'],
            "minimum_spend" => ['nullable', 'digits_between:1,500'],
            "usage_limit" => ['nullable', 'numeric'],
            "start_date" => ['required','date'],
            "end_date" => ['required','date'],

        ];
    }
}
