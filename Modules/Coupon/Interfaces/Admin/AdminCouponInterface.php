<?php

namespace Modules\Coupon\Interfaces\Admin;

interface AdminCouponInterface
{
    public function index($paginate = 20);

    public function find($id);

    public function store($data);

    public function update($data);

    public function delete($id);
}
