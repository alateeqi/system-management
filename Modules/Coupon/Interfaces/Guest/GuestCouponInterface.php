<?php

namespace Modules\Coupon\Interfaces\Guest;

interface GuestCouponInterface
{
    public function coupon($coupon, $productPrice);

}
