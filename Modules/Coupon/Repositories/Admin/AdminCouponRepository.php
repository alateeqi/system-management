<?php

namespace Modules\Coupon\Repositories\Admin;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Modules\Auth\Entities\User;
use Modules\Coupon\Entities\Coupon;
use Modules\Coupon\Interfaces\Admin\AdminCouponInterface;
use Modules\Customer\Repositories\Admin\AdminCustomerRepository;

class AdminCouponRepository implements AdminCouponInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index($paginate = 20)
    {
        $coupons = Cache::remember('coupons-page-' . request('page', 1), 60 * 60, function () use ($paginate) {
            return Coupon::orderBy('id', 'desc')->paginate($paginate);
        });
        return $coupons;
    }

    public function find($id)
    {
        return Coupon::find($id);
    }

    public function store($data)
    {


        $coupon = $this->set_values_object(new Coupon(), $data);
//        $coupon->discount_percentage = $data['type_discount'] == 2 ? $data['value'] : 0;
//        $coupon->discount = $data['type_discount'] == 1 ? $data['value'] : 0;
//        $coupon->use_times = isset($data['usage_limit']) ? $data['usage_limit'] : 0;
//        $coupon->condition = isset($data['minimum_spend']) ? $data['minimum_spend'] : 0;
//        $coupon->name = $data['name'];
//        $coupon->starts_at = Carbon::parse($data['start_date'])->format('Y-m-d 00:00:00');
//        $coupon->ends_at = Carbon::parse($data['end_date'])->format('Y-m-d 23:59:59');
//        $coupon->code = $data['code'];
        $this->clearCache();
        $coupon->save();


        if (isset($data['customer_phones'])) {
            $customersIds = [];

            foreach ($data['customer_phones'] as $customerPhone) {
                $customer = (new AdminCustomerRepository($this->user))->findByPhone($customerPhone);
//                if (!$customer) {
//                    $customer = new Customer(['phone' => $customerPhone]);
//                    $customer->save();
//                }
                $customersIds[] = $customer->id;
            }
            $coupon->customers()->sync($customersIds);
        }
        return $coupon;
    }

    public function update($data)
    {


        $coupon = $this->set_values_object($this->find($data['coupon_id']), $data);

        if (isset($data['customer_phones'])) {
            $customersIds = [];

            foreach ($data['customer_phones'] as $customerPhone) {
                $customer = (new AdminCustomerRepository($this->user))->findByPhone($customerPhone);
//                if (!$customer) {
//                    $customer = new Customer(['phone' => $customerPhone]);
//                    $customer->save();
//                }
                $customersIds[] = $customer->id;
            }
            $coupon->customers()->sync($customersIds);
        } else   $coupon->customers()->sync([]);
        $this->clearCache();
        return $coupon;
    }

    private function set_values_object(Coupon $coupon, $data)
    {

        $coupon->discount_percentage = $data['type_discount'] === "2" ? $data['value'] : 0;
        $coupon->discount = $data['type_discount'] === "1" ? $data['value'] : 0;
        $coupon->use_times = isset($data['usage_limit']) ? $data['usage_limit'] : 0;
        $coupon->condition = isset($data['minimum_spend']) ? $data['minimum_spend'] : 0;
        $coupon->name = $data['name'];
        $coupon->starts_at = Carbon::parse($data['start_date'])->format('Y-m-d 00:00:00');
        $coupon->ends_at = Carbon::parse($data['end_date'])->format('Y-m-d 23:59:59');
        $coupon->code = $data['code'];
        $coupon->save();
        return $coupon;
    }

    public function delete($id)
    {
        $coupon = $this->find($id);

        $coupon->delete();
        $this->clearCache();
        return true;
    }

    private function clearCache()
    {
        for ($i = 1; $i < 100; $i++) {
            $key = 'coupons-page-' . $i;
            if (Cache::has($key))
                Cache::forget($key);
        }
    }
}
