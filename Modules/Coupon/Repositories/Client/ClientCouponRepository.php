<?php

namespace Modules\Coupon\Repositories\Client;

use Modules\Coupon\Entities\Coupon;
use Modules\Coupon\Interfaces\Client\ClientCouponInterface;
use Modules\Customer\Entities\Customer;
use Carbon\Carbon;

class ClientCouponRepository implements ClientCouponInterface
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function coupons()
    {
        $usedCoupon = $this->customer->usedCoupons()->select('coupon_id')->get()->pluck('coupon_id')->toArray();

        return $this->customer->coupons()->where('ends_at', '>=', now())->whereNotIn('coupons.id', $usedCoupon)->get();
    }

    public static function couponsByGiftId($giftId)
    {
//        $gifts = Coupon::where('gift_id', $giftId)->where('use_times', '>', 0)->get();
        $gifts = Coupon::where('gift_id', $giftId)->get();

        return $gifts;
    }

    public function store($wonCoupon)
    {
        $coupon = new Coupon();
        $coupon->name = $wonCoupon->name;
        $coupon->discount_percentage = $wonCoupon->discount_percentage;
        $coupon->discount = $wonCoupon->discount;
        $coupon->condition = $wonCoupon->condition;
        $coupon->use_times = 1;
        $coupon->starts_at = Carbon::now()->format('Y-m-d 00:00:00');
        $coupon->ends_at = Carbon::now()->addWeek()->format('Y-m-d 23:59:59');


        $coupon->code = $this->unique_code(12);
        $coupon->save();
        $coupon->customers()->sync([$this->customer->id]);
        return $coupon;
    }

    private function unique_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
