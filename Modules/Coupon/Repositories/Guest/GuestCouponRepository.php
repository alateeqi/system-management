<?php

namespace Modules\Coupon\Repositories\Guest;

use Carbon\Carbon;
use Modules\Coupon\Entities\Coupon;
use Modules\Coupon\Interfaces\Guest\GuestCouponInterface;

class GuestCouponRepository implements GuestCouponInterface
{
    public function findByCode($code)
    {
        return Coupon::where([
            'code' => $code,
            'gift_id' => null
        ])->first();
    }

    public static function checkCoupon($coupon, $productPrice)
    {
        if (auth()->user()) {
            $userId = auth()->user()->id;
        } else {
            $userId = 0;
        }

        if ($coupon) {
            if ($productPrice <= $coupon->condition) {
                $couponErrorMessage = 'Selected products price must be bigger than' . ' ' . $coupon->condition . ' KWD';
            } elseif (Carbon::now() < Carbon::parse($coupon->starts_at) || Carbon::now() > Carbon::parse($coupon->ends_at)) {
                $couponErrorMessage = 'This coupon dates is not valid!';
            } elseif ($coupon->customers()->count() && !$coupon->customers()->find($userId)) {
                $couponErrorMessage = "You don't have this coupon!";
            } else {
                $count = GuestCouponUseRepository::countOfUseCoupon($userId, $coupon);

                if (!$coupon->customers()->count()) {
                    if ($count >= 1) {
                        $couponErrorMessage = "You already used this coupon!";
                    }
                } else {
                    if ($count >= $coupon->use_times) {
                        $couponErrorMessage = "You used this coupon " . $count . " times!";
                    }
                }
            }
        } else {
            $couponErrorMessage = 'This Coupon is not found!';
        }

        if (isset($couponErrorMessage)) {
            return [
                'status' => false,
                'message' => $couponErrorMessage
            ];
        } else {
            return [
                'status' => true,
                'message' => '',
                'coupon' => $coupon
            ];
        }
    }

    public function coupon($coupon, $productPrice)
    {

        $coupon = $this->findByCode($coupon);
        $checkCoupon = $this::checkCoupon($coupon, $productPrice);
        if ($checkCoupon['status'] === false) {
            return [
                'couponErrorMessage' => $checkCoupon['message'],
                'couponDiscount' => 0,
                'couponDiscountPercentage' => 0,
            ];
        } else {
            return [
                'couponErrorMessage' => null,
                'couponDiscount' => $coupon->discount,
                'couponDiscountPercentage' => $coupon->discount_percentage,
            ];
        }

    }
}
