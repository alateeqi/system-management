<?php

namespace Modules\Coupon\Repositories\Guest;

use Modules\Coupon\Entities\CouponUse;

class GuestCouponUseRepository
{
    public static function countOfUseCoupon($userId, $coupon)
    {
        return CouponUse::where([
            'customer_id' => $userId,
            'coupon_id' => $coupon->id
        ])->count();
    }

    public function store($order)
    {
        $couponUse = new CouponUse();
        $couponUse->customer_id = $order->customer_id;
        $couponUse->order_id = $order->id;
        $couponUse->coupon_id = $order->coupon_id;
        $couponUse->save();
        return $couponUse;
    }
}
