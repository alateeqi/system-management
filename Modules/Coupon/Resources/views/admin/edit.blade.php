@extends('layouts.dashboard.app')

@push('css')

    @if(app()->getLocale() == 'ar')
        <style>
            * {
                /*box-sizing: border-box*/
            }

            body {
                font-family: "Lato", sans-serif;
            }

            /* Style the tab */
            .tab {
                padding-right: 10px;
                float: right;
                /*border: 1px solid #ccc;*/
                background-color: #f1f1f1;
                width: 25%;
                height: 270px;
            }

            /* Style the buttons inside the tab */
            .tab button {
                display: block;
                background-color: inherit;
                color: black;
                padding: 25px 15px 25px 0px;
                width: 100%;
                border: none;
                outline: none;
                text-align: right;
                cursor: pointer;
                transition: 0.3s;
                font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current "tab button" class */
            .tab button.active {
                background-color: #fff;
            }

            /* Style the tab content */
            .tabcontent {
                float: right;
                padding: 0px 12px;
                /*border: 1px solid #ccc;*/
                width: 70%;
                /*border-left: none;*/

            }
        </style>
    @else
        <style>
            * {
                /*box-sizing: border-box*/
            }

            body {
                font-family: "Lato", sans-serif;
            }

            /* Style the tab */
            .tab {
                padding-left: 10px;
                float: left;
                /*border: 1px solid #ccc;*/
                background-color: #f1f1f1;
                width: 25%;
                height: 270px;
            }

            /* Style the buttons inside the tab */
            .tab button {
                display: block;
                background-color: inherit;
                color: black;
                padding: 25px 0px 25px 15px;
                width: 100%;
                border: none;
                outline: none;
                text-align: left;
                cursor: pointer;
                transition: 0.3s;
                font-size: 17px;
            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current "tab button" class */
            .tab button.active {
                background-color: #fff;
            }

            /* Style the tab content */
            .tabcontent {
                float: left;
                padding: 0px 12px;
                /*border: 1px solid #ccc;*/
                width: 70%;
                /*border-left: none;*/

            }
        </style>
    @endif

@endpush

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">

                <div class="card-body">
                    <h5 class="card-title">@lang('coupon::site.edit_coupon')</h5>
                    <hr/>

                    <div class="row mt-5">
                        <form id="form-coupon">
                            <input name="coupon_id" value="{{$coupon->id}}" type="hidden">
                            <div class="tab ">
                                <div>
                                    <h5 class="text-center m-1">@lang('coupon::site.coupon_information')</h5>
                                </div>

                                <hr class="m-0 p-0"/>
                                <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">
                                    @lang('coupon::site.general')

                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Paris')">
                                    @lang('coupon::site.usage_restrictions')
                                </button>
                                <button class="tablinks" onclick="openCity(event, 'Tokyo')">@lang('coupon::site.customers')</button>
                            </div>

                            <div id="London" class="tabcontent">
                                <h3 class="mt-3">
                                    @lang('coupon::site.general')
                                </h3>
                                <hr/>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.name')  <span class="text-danger"> *</span></h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="name" class="form-control" type="text" value="{{$coupon->name}}">
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> Code <span class="text-danger"> *</span></h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="code" class="form-control" type="text" value="{{$coupon->code}}">
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.discount_type')</h5>
                                    </div>

                                    <div class="col-6 form-group">
                                        <select class=" form-control" name="type_discount">
                                            <option value="1" {{$coupon->discount>0?'selected':''}}>Fixed</option>
                                            <option value="2" {{$coupon->discount_percentage>0?'selected':''}}>Percent
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.value') <span class="text-danger"> *</span></h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="value" class="form-control"
                                            value="{{$coupon->discount_percentage>0?$coupon->discount_percentage:$coupon->discount}}">
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.start_date')</h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="start_date" class="form-control" type="date"
                                        value="{{$coupon->starts_at->toDateString()}}">
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.end_date')</h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="end_date" class="form-control" type="date"
                                            value="{{$coupon->ends_at->toDateString()}}">
                                    </div>
                                </div>
                            </div>

                            <div id="Paris" class="tabcontent" style="display: none">
                                <h3 class="mt-3">
                                    @lang('coupon::site.usage_restrictions')
                                </h3>
                                <hr/>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.usage_limit') <span class="text-danger"> *</span></h5>
                                    </div>


                                    <div class="col-6 form-group">

                                        <input name="usage_limit" id="usage_limit" class="form-control" type="number"
                                        value="{{$coupon->use_times}}">
                                    </div>
                                </div>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.minimum_spend')</h5>
                                    </div>


                                    <div class="col-6 form-group">
                                        <input name="minimum_spend" class="form-control" type="number"
                                               value="{{$coupon->condition}}">
                                    </div>
                                </div>
                            </div>

                            <div id="Tokyo" class="tabcontent" style="display: none">
                                <h3 class="mt-3">
                                    @lang('coupon::site.customers')
                                </h3>
                                <hr/>
                                <div class="row m-3">
                                    <div class="col-1 form-group"></div>
                                    <div class="col-2 form-group">

                                        <h5 class=" text-left"> @lang('coupon::site.customers')</h5>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="form-label">@lang('coupon::site.search_on_phone')</label>

                                        <select id="phone-customer" multiple
                                                name="customer_phones[]">
                                        </select>
                                    </div>


                                </div>

                            </div>


                        </form>
                        <div class="justify-content-center d-flex">
                            <button class="btn btn-primary" type="button" id="submit-coupon">@lang('coupon::site.update')</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>
        $('#submit-coupon').on('click', function (e) {
            e.preventDefault()
            if ($('#usage_limit').val() === '')
                error_noti('ادخل عدد مرات الاستخدام');
            else
                $.post('{{route('admin.coupons.update')}}', $('#form-coupon').serialize(), function (data) {
                    success_noti(data.message);
                }).fail(function (error) {
                    error_noti(error.responseJSON.message);
                })
        })
        $(function () {
            $('#phone-customer').select2({
                theme: 'bootstrap4',

                data :  @json(old('customer_phones',$coupon->customers->pluck('phone'))).map(function (item) {
                    return {
                        id: item,
                        text: item,
                        selected:true
                    }
                }),
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                ajax: {
                    url: '{{ route('customers.search.phones-get') }}',
                    data: function (params) {
                        var search = {
                            query: params.term,
                            page: params.page || 1
                        }


                        return search;
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data.map(function (item) {
                                return {
                                    "id": item,
                                    "text": item
                                }
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },

                },
                transport: function (params, success, failure) {
                    var $request = $.ajax(params);

                    $request.then(success);
                    $request.fail(failure);

                    return $request;
                }
            });

        })
    </script>
    <script>
        function openCity(evt, cityName) {
            evt.preventDefault();
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>
@endpush
