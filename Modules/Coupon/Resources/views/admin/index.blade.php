@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-header">
                    <h3>Coupons</h3>
                </div>
                <div class="card-body">
                    <div class="d-lg-flex align-items-center mb-4 gap-3">
{{--                        <div class="position-relative">--}}
{{--                            <input type="text" class="form-control ps-5 radius-30" placeholder="@lang('coupon::site.search')" value="{{request()->search}}">--}}
{{--                            <span  class="position-absolute top-50 product-show translate-middle-y">--}}
{{--                                <i class="bx bx-search"></i></span>--}}

{{--                        </div>--}}
                        @if (auth()->user()->hasPermission('coupons-create'))
                            <div class="ms-auto"><a href="{{route('admin.coupons.create')}}"
                            class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class="bx bxs-plus-square"></i>@lang('coupon::site.add_new_coupon')</a></div>
                        @endif

                    </div>
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="table-light">
                            <tr>
                                <th>Code#</th>
                                <th>@lang('coupon::site.name') </th>

                                <th>@lang('coupon::site.use_Times')</th>
                                <th></th>
                                <th>@lang('coupon::site.discount_percentage')</th>
                                <th>@lang('coupon::site.condition')</th>
                                <th>@lang('coupon::site.start_date')</th>
                                <th>@lang('coupon::site.end_date')</th>
                                <th>@lang('coupon::site.actions')</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($coupons as $coupon)
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">

                                            <div class="ms-2">
                                                <h6 class="mb-0 font-14">{{$coupon->code}}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{$coupon->name}}</td>

                                    <td>
                                        <div
                                            class="badge rounded-pill text-success bg-light-success p-2 text-uppercase px-3">

                                            {{$coupon->use_times}}
                                        </div>
                                    </td>
                                    <td>{{$coupon->discount}}</td>
                                    <td>{{$coupon->discount_percentage}}</td>
                                    <td>{{$coupon->condition}} KWD</td>
                                    <td>{{$coupon->starts_at->toDateString()}}</td>
                                    <td>{{$coupon->ends_at->toDateString()}}</td>

                                    <td>
                                        <div class="d-flex order-actions">
                                            @if (auth()->user()->hasPermission('coupons-update'))
                                                <a href="{{route('admin.coupons.show',$coupon->id)}}" class=""><i
                                                    class="bx bxs-edit"></i></a>
                                            @endif
                                            @if (auth()->user()->hasPermission('coupons-delete'))
                                            <a href="#"  data-bs-toggle="modal"
                                               data-bs-target="#exampleVerticallycenteredModal{{ $coupon->id }}" class="ms-3"><i class="bx bxs-trash"></i></a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                <div class="modal fade"
                                     id="exampleVerticallycenteredModal{{ $coupon->id }}" tabindex="-1"
                                     aria-hidden="true">
                                    <form action="{{ route('admin.coupons.delete', $coupon->id) }}" method="POST"
                                          style="display:inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                                    <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">

                                                    @lang('taxonomy::site.confirm_the_deletion')

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">@lang('taxonomy::site.cancel')</button>
                                                    <button type="submit"
                                                            class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    {{ $coupons->links('vendor.pagination.bootstrap-4',['paginator'=>$coupons]) }}
                </div>

            </div>
        </div>
    </div>



@endsection
