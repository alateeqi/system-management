<?php

Route::middleware('auth:api')->group(function () {

    Route::get('coupons', 'Client\ClientCouponsController@coupons');

});
Route::get('coupon/{coupon}/{productPrice}', 'Guest\GuestCouponController@coupon');
