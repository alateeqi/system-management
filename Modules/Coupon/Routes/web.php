<?php

use Illuminate\Support\Facades\Route;
use Modules\Product\Http\Controllers\ProductController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //category routes
            Route::get('/coupons', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'index'])->name('admin.coupons.index');
            Route::get('/coupons/create', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'create'])->name('admin.coupons.create');
            Route::get('/coupons/show/{id}', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'show'])->name('admin.coupons.show');
            Route::post('/coupons', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'store'])->name('admin.coupons.store');
            Route::post('/coupons/update', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'update'])->name('admin.coupons.update');
            Route::delete('/coupons/delete/{id}', [\Modules\Coupon\Http\Controllers\Admin\AdminCouponController::class, 'destroy'])->name('admin.coupons.delete');
        });
    }
);
