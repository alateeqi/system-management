<?php

namespace Modules\Customer\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use Modules\Address\Entities\Address;
use Modules\Coupon\Entities\Coupon;
use Modules\Coupon\Entities\CouponUse;
use Modules\Gift\Entities\Gift;
use Modules\Gift\Entities\WonGift;
use Modules\Order\Entities\Order;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property int $user_id
 */
class Customer extends Authenticatable implements HasMedia
{
    use InteractsWithMedia, SoftDeletes, Notifiable, HasApiTokens, RecordsActivity;

    protected $with = [
        'addresses'
    ];

    protected $fillable = [
        'name',
        'phone',
        'email',
        'password',
        'status',
        'user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        if (Auth::guard('web')->check() && request()->user()) {
            self::savingActivity();

            static::creating(function ($model) {
                self::clearCache();
                $model->user_id = request()->user()->id;
            });

            static::updating(function ($model) {
                self::clearCache();
                $model->editor_id = request()->user()->id;
            });
        } else {
            static::creating(function ($model) {
                self::clearCache();
                $model->user_id = 1;
            });

            static::updating(function ($model) {
                self::clearCache();
                $model->editor_id = 1;
            });
        }
    }

    private function clearCache()
    {
        if (Cache::has('all_customers_phones'))
            Cache::forget('all_customers_phones');

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('full')
            ->performOnCollections('images');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(450)
            ->performOnCollections('images');

        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(225)
            ->performOnCollections('images');
    }
    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function completedOrders()
    {
        return $this->orders()->whereNotIn('status', [Order::STATUS_NAME['canceled'], Order::STATUS_NAME['pending']]);
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'customer_coupon', 'customer_id', 'coupon_id');
    }

    public function gifts()
    {
        return $this->belongsToMany(Gift::class, 'gift_customer', 'customer_id', 'gift_id');
    }

    public function wonGifts()
    {
        return $this->hasMany(WonGift::class);
    }

    public function usedCoupons()
    {
        return $this->hasMany(CouponUse::class);
    }

    public function routeNotificationForTwilio()
    {
        return '+965' . $this->phone;
    }
}
