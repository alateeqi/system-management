<?php

namespace Modules\Customer\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Http\Requests\ToggleStatusCustomer;
use Modules\Customer\Interfaces\Admin\AdminCustomerInterfaces;
use Modules\User\Http\Requests\Admin\Store;

class AdminCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function index(Request $request, AdminCustomerInterfaces $interface)
    {

        $customers = $interface->index($request);


        return view('customer::admin.index', compact('customers'));
    }


        public function toggleStatus (ToggleStatusCustomer $request , AdminCustomerInterfaces $interface){

        return response()->json([
            'data' =>$interface->toggleStatus($request->validated())
        ]);
    }


    public function searchByPhone(Request $request, AdminCustomerInterfaces $interface)
    {

        return response()->json([
            'data' => $interface->searchByPhone($request->input('query'))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('customer::admin.create');

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminCustomerInterfaces $interface)
    {

            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('recipes.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->route('recipes.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('customer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('customer::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        session()->flash('success',__('site.deleted_successfully'));
        return redirect()->route('customers.index');
    }
}
