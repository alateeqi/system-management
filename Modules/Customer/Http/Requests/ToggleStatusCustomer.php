<?php

namespace Modules\Customer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ToggleStatusCustomer extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        
        return [
            'customer_id' => ['required', 'numeric', 'exists:customers,id'],
        ];
    }
}
