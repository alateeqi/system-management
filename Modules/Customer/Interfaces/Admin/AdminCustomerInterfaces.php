<?php

namespace Modules\Customer\Interfaces\Admin;
use Illuminate\Http\Request;
interface AdminCustomerInterfaces

{
    public function index(Request $request);

    public function store($data);
    
    public function toggleStatus($data);

    public function all_phones();

    public function searchByPhone($phone);
}
