<?php

namespace Modules\Customer\Repositories\Admin;


use Illuminate\Support\Facades\Cache;
use Modules\Auth\Entities\User;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Interfaces\Admin\AdminCustomerInterfaces;
use Illuminate\Http\Request;

class AdminCustomerRepository implements AdminCustomerInterfaces
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function toggleStatus($data)
    {
        $this->clearCache();
        $model = Customer::find($data['customer_id']);
        $model->status = !$model->status;
        $model->save();

        return $model;

    }
    private function clearCache()
    {
        if (Cache::has('all_customers_phones'))
            Cache::forget('all_customers_phones');

    }

    public function index(Request $request)
    {
        $params = null;
        if (isset($request->search))
            $params .= '?search=' . $request->search;
        $customers = Customer::when($request->search, function ($query) use ($request) {
            return $query->where('phone', 'like', '%' . $request->search . '%');
        })->latest()->Paginate(50);

        $customers->setPath(url()->current() . $params);
        return $customers;
        // return Customer::paginate($paginate);

    }

    public function store($data)
    {
        $customer = new Customer();
        $customer->phone = $data['phone'];
        $customer->save();
        return $customer;
    }

    public function findByPhone($phone)
    {
        return Customer::where('phone', $phone)->first();
    }

    public function all_phones()
    {
        $phones = Cache::remember('all_customers_phones', 60 * 60, function () {
            return Customer::pluck('phone')->chunk(998, function ($all_phones) {
                return $all_phones;
            });
        });
        return $phones;
    }

    public function searchByPhone($phone)
    {
        return Customer::where('phone', 'LIKE', '%' . $phone . '%')->limit(20)->pluck('phone')->toArray();

    }


}
