<?php

namespace Modules\Customer\Repositories\Guest;

use Modules\Address\Repositories\Client\ClientAddressRepository;
use Modules\Customer\Entities\Customer;
use Modules\Product\Repositories\Guest\GuestProductRepository;

class GuestCustomerRepository
{
    public function find($id)
    {
        return Customer::findOrFail($id);
    }

    public function findByPhone($phone)
    {
        return Customer::where('phone', $phone)->first();
    }

    public function updateCustomerByOrder($order)
    {
        $requestedCustomer = $order['customer'];

        if (isset($requestedCustomer['id'])) {
            $customer = $this->find($requestedCustomer['id']);
            $customer->update($requestedCustomer);
        } elseif (isset($requestedCustomer['phone'])) {
            $customer = $this->findByPhone($requestedCustomer['phone']);
            if ($customer) {
                $customer->update($requestedCustomer);
            } else {
                $customer = new Customer($requestedCustomer);
            }
        } else {
            $customer = new Customer($requestedCustomer);
        }
        $customer->save();

        if ((new GuestProductRepository())->isPickUp($order['gate']['id'])) {
            return [
                'id' => $customer->id,
                'address' => null
            ];
        }

        $registeredAddresses = [];
        $selectedAddress = null;
        foreach ($requestedCustomer['addresses'] as $requestedAddress) {
            if (!empty($requestedAddress['id'])) {
                $address = (new ClientAddressRepository($customer))->find($requestedAddress['id']);
            } else
                $address = (new ClientAddressRepository($customer))->store($requestedAddress);

            $registeredAddresses[] = $address->id;
            if (isset($requestedAddress['selected']) && $requestedAddress['selected']) {
                $selectedAddress = $address;
            }
        }

        //$customer->addresses()->whereNotIn('id', $registeredAddresses)->delete();

        return [
            'id' => $customer->id,
            'address' => $selectedAddress
        ];
    }
}
