@extends('layouts.dashboard.app')
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('customer::site.add_customer')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('customers.index') }}">@lang('customer::site.customers')</li></a>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">

                <div class="col-xl-10 mx-auto">
                    {{-- <form action="{{ route('customers.store')}}" method="POST"> --}}
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 text-info"><i class="lni lni-users"></i></div>
                                        <h5 class="mb-0 text-info">@lang('customer::site.add_customer')</h5>
                                    </div>
                                    <hr/>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">@lang('customer::site.name')</label>
                                            <input class="form-control" name="name"id="name" rows="3" data-form-type="name"placeholder="@lang('customer::site.name')">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">@lang('customer::site.email')</label>
                                            <input type="email" class="form-control" name="email"id="email" rows="3" data-form-type="email"placeholder="@lang('customer::site.email')">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">@lang('customer::site.phone')</label>
                                            <input type="number" class="form-control" name="phone"id="phone" rows="3" data-form-type="phone"placeholder="@lang('customer::site.phone')">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit"class="btn btn-info">@lang('customer::site.add')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
