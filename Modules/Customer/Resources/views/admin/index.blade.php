@extends('layouts.dashboard.app')
@section('title')
    @lang('customer::site.customers')
@endsection
@section('css')
    <style>
        .add-order {
            background: rgb(5, 124, 21)
        }

    </style>
@endsection
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('customer::site.customers')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>

            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-22 text-primary pt-2"> <i class="lni lni-users"></i></div>

                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('customer::site.customers') <small></small></h5>

                    <form action="" method="GET">
                        <div class="row">

                            <div class="col-md-6">
                                <input type="search" name="search" class="form-control mt-2 mx-3  radius-30 form-control "
                                    placeholder="@lang('customer::site.search')" value="{{ request()->search }}">
                            </div>

                            {{-- <div class="col-md-6">

                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                    href="{{route('customers.create')}}">@lang('customer::site.add_new')<i class="lni lni-circle-plus mx-1"></i></a>


                            </div> --}}

                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($customers->count() > 0)
                        <div class="table-responsive">
                            <table id="example2" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">@lang('customer::site.name')</th>
                                        <th scope="col">@lang('customer::site.phone')</th>
                                        <th scope="col">@lang('customer::site.city')</th>
                                        <th scope="col">@lang('customer::site.email')</th>
                                        <th scope="col">@lang('customer::site.action')</th>
                                        <th scope="col">@lang('customer::site.status')</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $index => $customer)
                                        <tr>
                                            <th scope="row">{{ $index + 1 }}</th>

                                            <td>{{ $customer->name }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>{{ $customer->city }}</td>
                                            <td>{{ $customer->email }}</td>


                                            <td>
                                                @if (auth()->user()->hasPermission('users-delete'))
                                                    <a href="#" class="text-dark font-22 " data-bs-toggle="modal"
                                                        data-bs-target="#exampleVerticallycenteredModal{{ $customer->id }}"><i
                                                            class="bx bxs-trash " style="color: #ff6b6b;"></i></a>
                                                @else
                                                    <a href="#" class="text-dark font-22 " disabled><i
                                                            class="bx bxs-trash " style="color: #ff6b6b;"></i></a>
                                                @endif
                                            </td>

                                            <td class="font-30">
                                                <div class="form-check form-switch">
                                                        <input class=" form-check-input" id="flexSwitchCheckChecked" type="checkbox"
                                                        onchange="toggle_status_customer({{$customer->id}})" {{$customer->status === 1 ? 'checked= "" ' : ''}}>
                                                    </div>
                                            </td>

                                        </tr>

                                        <!-- users-delete -->


                                        <div class="modal fade" id="exampleVerticallycenteredModal{{ $customer->id }}"
                                            tabindex="-1" aria-hidden="true">
                                            <form action="{{ route('customers.delete', $customer->id) }}" method="POST"
                                                style="display:inline-block;">
                                                @csrf
                                                @method('DELETE')
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">@lang('customer::site.delete')</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @lang('customer::site.confirm_the_deletion')
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">@lang('customer::site.cancel')</button>
                                                            <button type="submit"
                                                                class="btn btn-primary">@lang('customer::site.delete')</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="mt-3 mb-3 mx-3">
                                {{ $customers->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        @else
                            <h2>@lang('customer::site.not_data_found')</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
