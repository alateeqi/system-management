<?php


use Illuminate\Support\Facades\Route;
use Modules\Customer\Http\Controllers\Admin\AdminCustomerController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            Route::post('/customers/toggle-status-customer/', [AdminCustomerController::class, 'toggleStatus'])->name('toggle-status-customer');


            Route::get('/customers', [AdminCustomerController::class,'index'])->name('customers.index');
            Route::get('/customers/create', [AdminCustomerController::class,'create'])->name('customers.create');
            // Route::post('/customers/store', [AdminCustomerController::class,'store'])->name('customers.store');
            Route::delete('/customers/delete/{customers}', [AdminCustomerController::class,'destroy'])->name('customers.delete');
            Route::post('/customers/search-phone', [AdminCustomerController::class,'searchByPhone'])->name('customers.search.phones');
            Route::get('/customers/search-phone-get', [AdminCustomerController::class,'searchByPhone'])->name('customers.search.phones-get');


        });
    }
    );
