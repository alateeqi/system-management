<?php

namespace Modules\Gift\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Customer\Entities\Customer;

class Gift extends Model
{
    use SoftDeletes;

    protected $casts = [
        'starts_at' => 'datetime',
        'ends_at' => 'datetime'
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class, 'gift_customer', 'gift_id', 'customer_id');
    }
}
