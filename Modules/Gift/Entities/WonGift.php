<?php

namespace Modules\Gift\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Coupon\Entities\Coupon;
use Modules\Customer\Entities\Customer;

class WonGift extends Model
{
    use HasFactory;

    protected $fillable = [
        'gift_id',
        'customer_id',
        'coupon_id',
        'coupon_template_id',
        'is_general_gift'
    ];

    public function gift()
    {
        return $this->belongsTo(Gift::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function couponTemplate()
    {
        return $this->belongsTo(Coupon::class, 'coupon_template_id');
    }
}
