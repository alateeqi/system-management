<?php

namespace Modules\Gift\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Gift\Http\Requests\Admin\Store;
use Modules\Gift\Http\Requests\Admin\Update;
use Modules\Gift\Interfaces\Admin\AdminGiftInterface;

class AdminGiftController extends Controller
{
    public function index(AdminGiftInterface $interface)
    {
        $gifts = $interface->index();
        return view('gift::admin.gifts.index', compact('gifts'));
    }

    public function create()
    {
        return view('gift::admin.gifts.create');
    }

    public function show(AdminGiftInterface $interface, $id)
    {
        $gift = $interface->show($id);
        return view('gift::admin.gifts.show', compact('gift'));
    }

    public function store(Store $request, AdminGiftInterface $interface)
    {
        try {
            DB::beginTransaction();
            $interface->store($request->validated());
            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('admin.gifts.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('admin.gifts.create')->withErrors($exception->getMessage());
        }
    }

    public function update(Update $request, AdminGiftInterface $interface)
    {
        try {
            DB::beginTransaction();
            $interface->update($request->validated());
            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('admin.gifts.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('admin.gifts.show', $request['gift_id'])->withErrors($exception->getMessage());
        }
    }
}
