<?php

namespace Modules\Gift\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Modules\Gift\Interfaces\Admin\AdminGiftWinnerInterface;

class AdminGiftWinnerController extends Controller
{
    public function index(AdminGiftWinnerInterface $interface)
    {
        $gifts = $interface->index();
        return view('gift::admin.winner.index', compact('gifts'));
    }

}
