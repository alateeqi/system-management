<?php

namespace Modules\Gift\Http\Controllers\Client;


use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Gift\Http\Requests\Client\Gift;
use Modules\Gift\Interfaces\Client\ClientGiftInterface;

class ClientGiftController extends Controller
{

    public function gift(Gift $request, ClientGiftInterface $interface)
    {
        if ($this->giftCheck($request, $interface)) {
            return $interface->gift($request);
        }

        return $this->respondNotFound();
    }

    public function giftCheck(Gift $request, ClientGiftInterface $interface)
    {
        return $interface->giftCheck($request);
    }

    public function gifts(ClientGiftInterface $interface)
    {
        return $interface->gifts();
    }
}
