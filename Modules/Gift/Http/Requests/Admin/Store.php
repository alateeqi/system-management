<?php

namespace Modules\Gift\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'customer_phones' => ['nullable','array'],
            'customer_phones.*' => ['numeric'],
            'title' => ['required','string'],
            'starts_at' => ['required','date'],
            'ends_at' => ['required','date'],
        ];
    }
}
