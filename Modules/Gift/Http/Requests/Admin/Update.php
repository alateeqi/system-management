<?php

namespace Modules\Gift\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gift_id' => ['required','numeric','exists:gifts,id'],
            'customer_phones' => ['nullable','array'],
            'customer_phones.*' => ['numeric'],
            'title' => ['required','string'],
            'starts_at' => ['required','date'],
            'ends_at' => ['required','date'],
        ];
    }
}
