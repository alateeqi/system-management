<?php

namespace Modules\Gift\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class Gift extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gift_id' => ['required']
        ];
    }
}
