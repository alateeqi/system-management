<?php

namespace Modules\Gift\Interfaces\Admin;

interface AdminGiftInterface
{
    public function index($paginate = 20);

    public function store($data);

    public function update($data);

    public function show($id);
}
