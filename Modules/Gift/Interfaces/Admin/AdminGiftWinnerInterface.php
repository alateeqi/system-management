<?php

namespace Modules\Gift\Interfaces\Admin;

interface AdminGiftWinnerInterface
{
    public function index($paginate = 20);
}
