<?php

namespace Modules\Gift\Interfaces\Client;

interface ClientGiftInterface
{
    public function gifts();

    public function gift($data);

    public function giftCheck($data);
}
