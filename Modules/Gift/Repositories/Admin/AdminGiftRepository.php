<?php

namespace Modules\Gift\Repositories\Admin;


use Carbon\Carbon;
use Modules\Auth\Entities\User;
use Modules\Customer\Repositories\Admin\AdminCustomerRepository;
use Modules\Gift\Entities\Gift;
use Modules\Gift\Entities\WonGift;
use Modules\Gift\Interfaces\Admin\AdminGiftInterface;

class AdminGiftRepository implements AdminGiftInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index($paginate = 20)
    {
        return Gift::paginate($paginate);
    }

    public function show($id)
    {
        return Gift::find($id);
    }

    public function store($data)
    {
        $gift = new Gift();
        $gift->title = $data['title'];
        $gift->ends_at = Carbon::parse($data['ends_at'])->format('Y-m-d 23:59:59');
        $gift->starts_at = Carbon::parse($data['starts_at'])->format('Y-m-d 00:00:00');
        $gift->save();

        if (isset($data['customer_phones'])) {
            $customersIds = [];

            foreach ($data['customer_phones'] as $customerPhone) {
                $customer = (new AdminCustomerRepository($this->user))->findByPhone($customerPhone);

                if (!$customer) {
                    (new AdminCustomerRepository($this->user))->$this->store(['phone' => $customerPhone]);

                }
                $customersIds[] = $customer->id;
            }
            $gift->customers()->sync($customersIds);
            return $gift;
        }
    }

    public function update($data)
    {
        $gift = $this->show($data['gift_id']);
        $gift->title = $data['title'];
        $gift->ends_at = Carbon::parse($data['ends_at'])->format('Y-m-d 23:59:59');
        $gift->starts_at = Carbon::parse($data['starts_at'])->format('Y-m-d 00:00:00');
        $gift->save();
        $customersIds = [];
        if (isset($data['customer_phones'])) {


            foreach ($data['customer_phones'] as $customerPhone) {
                $customer = (new AdminCustomerRepository($this->user))->findByPhone($customerPhone);

                if (!$customer) {
                    (new AdminCustomerRepository($this->user))->$this->store(['phone' => $customerPhone]);

                }
                $customersIds[] = $customer->id;
            }


        }
        $gift->customers()->sync($customersIds);
        return $gift;
    }
}
