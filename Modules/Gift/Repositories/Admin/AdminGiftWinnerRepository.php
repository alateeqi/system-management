<?php

namespace Modules\Gift\Repositories\Admin;

use Modules\Auth\Entities\User;
use Modules\Gift\Entities\WonGift;
use Modules\Gift\Interfaces\Admin\AdminGiftWinnerInterface;

class AdminGiftWinnerRepository implements AdminGiftWinnerInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index($paginate = 20)
    {
        return WonGift::with([
            'gift',
            'customer',
            'coupon',
            'couponTemplate',
        ])->select('won_gifts.*')->orderBy('id','desc')->paginate($paginate);
    }
}
