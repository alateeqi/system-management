<?php

namespace Modules\Gift\Repositories\Client;

use Carbon\Carbon;
use Modules\Coupon\Repositories\Client\ClientCouponRepository;
use Modules\Customer\Entities\Customer;
use Modules\Gift\Entities\Gift;
use Modules\Gift\Entities\WonGift;
use Modules\Gift\Interfaces\Client\ClientGiftInterface;


class ClientGiftRepository implements ClientGiftInterface
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function gift($data)
    {
        if ($data['gift_id'] === 'general') {
            $giftId = Gift::where('title', 'General Gifts')->firstOrFail()->id;
            $isGeneralGift = 1;
        } else {
            $giftId = $data['gift_id'];
            $isGeneralGift = 0;
        }

        $coupons = ClientCouponRepository::couponsByGiftId($giftId);
        $lot = [];
        $totalUseTime = 0;
        foreach ($coupons as $coupon) {
            $totalUseTime += $coupon->use_times;
            $lot[$totalUseTime] = $coupon;
        }
        $wonCoupon = $coupons->first();
        $rand = rand(0, $totalUseTime);
        foreach ($lot as $key => $coupon) {
            if ($key < $rand) {
                continue;
            } else {
                $wonCoupon = $coupon;
                break;
            }
        }


        if ($isGeneralGift) {
            $wonCoupon->use_times = $wonCoupon->use_times - 1;
            $wonCoupon->save();
        }
        ////////////
        $coupon = (new ClientCouponRepository($this->customer))->store($wonCoupon);
        ////////////

        $wonGift = (new ClientWonGiftRepository($this->customer))->store([
            'customer_id' => $this->customer->id,
            'coupon_id' => $coupon->id,
            'coupon_template_id' => $wonCoupon->id,
            'gift_id' => $giftId,
            'is_general_gift' => $isGeneralGift
        ]);
        ////////////

        if ($wonGift->id && $coupon->id) {

            return $coupon;
        }


    }

    public function giftCheck($data)
    {
        if ($data['gift_id'] === 'general') {
            $lastWon = $this->customer->wonGifts()->where('is_general_gift', true)->latest()->first();
            $lastDate = $lastWon->created_at ?? Carbon::createFromDate($year = 2021, $month = 03, $day = 01);

            return $this->customer->completedOrders()->where('created_at', '>', $lastDate)->count();
        }
        $wonGifts = $this->customer->wonGifts()->select('gift_id')->get()->pluck('gift_id')->toArray();

        $gift = $this->customer->gifts()->where('gifts.starts_at', '<', now())
            ->where('gifts.ends_at', '>', now())
            ->whereNotIn('gifts.id', $wonGifts)
            ->where('gifts.id', $data['gift_id'])->first();

        if ($gift) {
            return true;
        }

        $publicGifts = $this->publicGiftsByGiftId($data['gift_id'], $wonGifts);

        if ($publicGifts) {
            $won = (new ClientWonGiftRepository($this->customer))->wonGiftByGiftId($data['gift_id']);
            if ($won) {
                return false;
            }
            return true;
        }
        return false;
    }

    private function publicGiftsByGiftId($gift_id, $wonGifts)
    {
        return Gift::where('gifts.id', $gift_id)
            ->where('gifts.ends_at', '>', now())
            ->whereNotIn('gifts.id', $wonGifts)
            ->has('customers', '=', 0)
            ->get();
    }


    public function gifts()
    {
        $wonGifts = $this->customer->wonGifts()->select('gift_id')->get()->pluck('gift_id')->toArray();

        $myGifts = $this->customer->gifts()->where('ends_at', '>', now())->whereNotIn('gifts.id', $wonGifts)->get();

        $publicGifts = $this->publicGifts($wonGifts);

        $merged = $myGifts->merge($publicGifts);

        return $merged;
    }

    private function publicGifts($wonGifts)
    {
        return Gift::where('id', '!=', 1)
            ->where('ends_at', '>', now())
            ->whereNotIn('gifts.id', $wonGifts)
            ->has('customers', '=', 0)
            ->get();
    }
}
