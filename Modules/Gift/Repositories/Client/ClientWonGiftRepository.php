<?php

namespace Modules\Gift\Repositories\Client;

use Modules\Customer\Entities\Customer;
use Modules\Gift\Entities\WonGift;

class ClientWonGiftRepository
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function store($data)
    {
        return WonGift::create($data);
    }
    public function wonGiftByGiftId($gift_id)
    {
        return WonGift::where([
            'gift_id' => $gift_id,
            'customer_id' => $this->customer->id
        ])->first();
    }

}
