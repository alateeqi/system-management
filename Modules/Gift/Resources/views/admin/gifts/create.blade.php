@extends('layouts.dashboard.app')
@section('title')
    Gifts
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Gifts</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-4">
                    <h5 class="card-title">@lang('coupon::site.create_gifts')</h5>
                    <hr/>
                    <form class=" mt-4" action="{{ route('admin.gifts.store') }}" id="form-gift" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row col-md-12">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="inputTitle"
                                                       class="form-label"> @lang('coupon::site.name')</label>
                                                <input type="text" name="title" class="form-control"
                                                       value="{{old('title')}}" id="inputTitle"
                                                       placeholder="@lang('coupon::site.name') ">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label"> @lang('coupon::site.start_date')</label>
                                                <input type="text" name="starts_at" class="form-control date-datepicker"
                                                       value="{{old('starts_at')}}"    placeholder="@lang('coupon::site.start_date')">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label "> @lang('coupon::site.end_date')</label>
                                                <input type="text" name="ends_at" class="form-control date-datepicker"
                                                       value="{{old('ends_at')}}"   placeholder="@lang('coupon::site.end_date')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ">

                                        <div class="col-md-2 form-group">

                                            <h5 class=" text-left"> @lang('coupon::site.customers')</h5>
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label class="form-label">@lang('coupon::site.search_on_phone')</label>

                                            <select id="phone-customer" multiple
                                                    name="customer_phones[]">
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row ">


                                        <div class="col-md-6 col-sm-12">
                                            <br/>
                                            <button id="button-submit" type="submit" class="btn btn-primary w-100">@lang('site.save')
                                            </button>
                                        </div>
                                        <div class="col-md-3"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->

                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script>

        $(function () {
            $('#button-submit').on('click',function (e) {
                $(this).prop("disabled", true);
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
                document.getElementById('form-gift').submit()
            })

            $('#phone-customer').select2({
                theme: 'bootstrap4',

                data :  @json(old('customer_phones',[])).map(function (item) {
                    return {
                        id: item,
                        text: item,
                        selected:true
                    }
                }),
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                ajax: {
                    url: '{{ route('customers.search.phones-get') }}',
                    data: function (params) {
                        var search = {
                            query: params.term,
                            page: params.page || 1
                        }


                        return search;
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data.map(function (item) {
                                return {
                                    "id": item,
                                    "text": item
                                }
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },

                },
                transport: function (params, success, failure) {
                    var $request = $.ajax(params);

                    $request.then(success);
                    $request.fail(failure);

                    return $request;
                }
            });

        })
    </script>
@endpush
