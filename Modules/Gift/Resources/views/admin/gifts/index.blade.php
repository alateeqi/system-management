@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-header">
                    <h3>@lang('coupon::site.gifts')</h3>
                </div>
                <div class="card-body">
                    <div class="d-lg-flex align-items-center mb-4 gap-3">
                        {{-- <div class="position-relative"> --}}
                        {{-- <input type="text" class="form-control ps-5 radius-30" placeholder="@lang('coupon::site.search')" value="{{request()->search}}"> --}}
                        {{-- <span  class="position-absolute top-50 product-show translate-middle-y"> --}}
                        {{-- <i class="bx bx-search"></i></span> --}}

                        {{-- </div> --}}
                        @if (auth()->user()->hasPermission('coupons-create'))
                            <div class="ms-auto"><a href="{{ route('admin.gifts.create') }}"
                                    class="btn btn-primary radius-30 mt-2 mt-lg-0"><i class="bx bxs-plus-square"></i>
                                    @lang('coupon::site.create')
                                </a></div>
                        @endif

                    </div>
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="table-light">
                                <tr>

                                    <th>@lang('coupon::site.name')</th>
                                    <th> @lang('coupon::site.start_date')</th>
                                    <th>@lang('coupon::site.end_date')</th>
                                    <th>@lang('coupon::site.actions')</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($gifts as $gift)
                                    <tr>

                                        <td>{{ $gift->title }}</td>

                                        <td>
                                            {{ $gift->starts_at }}
                                        </td>
                                        <td>{{ $gift->ends_at }}</td>


                                        <td>
                                            <div class="d-flex order-actions">
                                                @if (auth()->user()->hasPermission('coupons-update'))
                                                    <a href="{{ route('admin.gifts.show', $gift->id) }}"
                                                        class=""><i class="bx bxs-edit"></i></a>
                                                @endif
                                                {{-- @if (auth()->user()->hasPermission('coupons-delete')) --}}
                                                {{-- <a href="javascript:;" class="ms-3"><i class="bx bxs-trash"></i></a> --}}
                                                {{-- @endif --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br />
                    {{ $gifts->links('vendor.pagination.bootstrap-4', ['paginator' => $gifts]) }}
                </div>

            </div>
        </div>
    </div>
@endsection
