@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-header">
                    <h3>Gift Winners</h3>
                </div>
                <div class="card-body">
                    <div class="d-lg-flex align-items-center mb-4 gap-3">
{{--                        <div class="position-relative">--}}
{{--                            <input type="text" class="form-control ps-5 radius-30" placeholder="@lang('coupon::site.search')" value="{{request()->search}}">--}}
{{--                            <span  class="position-absolute top-50 product-show translate-middle-y">--}}
{{--                                <i class="bx bx-search"></i></span>--}}

{{--                        </div>--}}

                    </div>
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="table-light">
                            <tr>

                                <th>@lang('coupon::site.start_date') </th>
                                <th> Customer</th>
                                <th>Is General Gift</th>
                                <th>Gift</th>
                                <th>Coupon</th>
                                <th>Template Coupon</th>
                                <th>@lang('coupon::site.actions')</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($gifts as $gift)

                                <tr>

                                    <td>{{$gift->created_at->toDateString()}}</td>

                                    <td>
                                        <div
                                            class="badge rounded-pill text-success bg-light-success p-2 text-uppercase px-3">

                                            {{$gift->customer->phone}}
                                        </div>
                                    </td>
                                    <td>{{$gift->is_general_gift}}</td>
                                    <td>{{$gift->gift->title}}</td>
                                    <td>{{$gift->coupon->name}} </td>
                                    <td>{{is_null($gift->couponTemplate)?'':$gift->couponTemplate->name}}</td>

                                    <td>
                                        <div class="d-flex order-actions">
{{--                                            @if (auth()->user()->hasPermission('coupons-update'))--}}
{{--                                                <a href="{{route('admin.coupons.show',$coupon->id)}}" class=""><i--}}
{{--                                                        class="bx bxs-edit"></i></a>--}}
{{--                                            @endif--}}
{{--                                            @if (auth()->user()->hasPermission('coupons-delete'))--}}
{{--                                                <a href="javascript:;" class="ms-3"><i class="bx bxs-trash"></i></a>--}}
{{--                                            @endif--}}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    {{ $gifts->links('vendor.pagination.bootstrap-4',['paginator'=>$gifts]) }}
                </div>

            </div>
        </div>
    </div>



@endsection
