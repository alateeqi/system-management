<?php

Route::middleware('auth:api')->group(function () {

    Route::post('gift', 'Client\ClientGiftController@gift');
    Route::get('gifts', 'Client\ClientGiftController@gifts');
    Route::get('gift-check', 'Client\ClientGiftController@giftCheck');

});
