<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //orders routes
            Route::get('/gifts', [\Modules\Gift\Http\Controllers\Admin\AdminGiftController::class, 'index'])->name('admin.gifts.index');
            Route::get('/gifts-winners', [\Modules\Gift\Http\Controllers\Admin\AdminGiftWinnerController::class, 'index'])->name('admin.gifts-winners.index');
            Route::get('/gifts/show/{id}', [\Modules\Gift\Http\Controllers\Admin\AdminGiftController::class, 'show'])->name('admin.gifts.show');
            Route::get('/gifts/create', [\Modules\Gift\Http\Controllers\Admin\AdminGiftController::class, 'create'])->name('admin.gifts.create');
            Route::post('/gifts', [\Modules\Gift\Http\Controllers\Admin\AdminGiftController::class, 'store'])->name('admin.gifts.store');
            Route::post('/gifts/update', [\Modules\Gift\Http\Controllers\Admin\AdminGiftController::class, 'update'])->name('admin.gifts.update');
//            Route::post('/orders/filter', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'filter'])->name('admin.orders.filter');

        });
    }
);
