<?php

namespace Modules\Home\Http\Controllers\Guest;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Product\Interfaces\Guest\GuestProductInterface;
use Modules\Taxonomy\Interfaces\Guest\Category\GuestCategoryInterface;
use Modules\Taxonomy\Interfaces\Guest\Slider\GuestSliderInterface;

class GuestHomeController extends Controller
{
    public function home(GuestCategoryInterface $category_interface, GuestSliderInterface $slider_interface, GuestProductInterface $product_interface)
    {
        return $this->respond([
            'sliders' => $slider_interface->sliders(),
            'popularProducts' => $product_interface->popularProducts(),
            'homeProducts' => $product_interface->homeProducts(),
            'categories' => $category_interface->categories(),
        ]);
    }
}
