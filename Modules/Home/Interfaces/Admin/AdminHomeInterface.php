<?php

namespace Modules\Home\Interfaces\Admin;

interface AdminHomeInterface
{
    public function statistics();
}
