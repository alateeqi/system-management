<?php

namespace Modules\Home\Repositories\Admin;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Modules\Auth\Entities\User;
use Modules\Customer\Entities\Customer;
use Modules\Home\Interfaces\Admin\AdminHomeInterface;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderProduct;

class AdminHomeRepository implements AdminHomeInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function statistics()
    {
        $orders_today = Order::whereBetween('requested_time', [
            Carbon::now()->format('Y-m-d 00:00:00'),
            Carbon::now()->format('Y-m-d 23:59:59')
        ])->where('status', '!=', Order::STATUS_NAME['canceled']);

        $customer = Customer::whereBetween('created_at', [
            Carbon::now()->format('Y-m-d 00:00:00'),
            Carbon::now()->format('Y-m-d 23:59:59')
        ])->get();
        $monthlySales = Cache::remember('monthly_sales', 60 * 60, function () {
            return Order::where('status', '!=', Order::STATUS_NAME['canceled'])->whereBetween('requested_time', [now()->startOfMonth(), now()->endOfMonth()])->sum('total_price');
        });

        $last_7_days_orders = Cache::remember('weekly_sales', 60 * 60, function () {
            return Order::whereBetween('requested_time', [
                Carbon::now()->addDays(-7)->format('Y-m-d 00:00:00'),
                Carbon::now()->format('Y-m-d 23:59:59')
            ])->orderBy('requested_time')->where('status', '!=', Order::STATUS_NAME['canceled'])->get();
        });
        $data['date_7_sales'] = [];
        $data['total_7_sales'] = [];

        foreach (
            $last_7_days_orders->groupBy(function ($order) {
                return Carbon::parse($order->requested_time)->format('Y-m-d');
            }) as $date => $sales
        ) {
            array_push($data['date_7_sales'], $date);
            array_push($data['total_7_sales'], $sales->sum('total_price'));

        }
        $data['best_sales_product_last_7_days'] = Cache::remember('best_sales_product_last_7_days', 60 * 60, function () {
            return OrderProduct::with([
                'product',
                'order'
            ])->whereHas('order', function ($q) {
                $q->whereBetween('requested_time',
                    [
                        Carbon::now()->addDays(-7)->format('Y-m-d 00:00:00'),
                        Carbon::now()->format('Y-m-d 23:59:59')
                    ])
                    ->where('status', '!=', Order::STATUS_NAME['canceled']);
            })->get()->groupBy(['product_id', 'order_id'])->sortByDesc(function ($item) {
                return $item->count();
            })->take(4);
        });
        $todaySales=$orders_today->sum('total_price');
        return [
            'countOfOrdersToday' => count($orders_today->get()),
            'countOfWebsiteOrderCount' => ($orders_today->where('gate_id', 680)->count()),
            'customerToday' => $customer,
            'todaySales' => $todaySales,
            'monthlySales' => $monthlySales,
            'dates_last_7_days_orders' => $data['date_7_sales'],
            'total_last_7_days_orders' => $data['total_7_sales'],
            'bestProducts' => $data['best_sales_product_last_7_days'],
        ];
    }
}
