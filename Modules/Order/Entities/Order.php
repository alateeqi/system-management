<?php

namespace Modules\Order\Entities;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Modules\Address\Entities\Address;
use Modules\Auth\Entities\User;
use Modules\Coupon\Entities\Coupon;
use Modules\Customer\Entities\Customer;
use Modules\Taxonomy\Entities\Taxonomy;


class Order extends Model
{
    use RecordsActivity, SoftDeletes;

    const STATUSES = [
        0 => [
            'class' => 'bg-primary',
            'name' => 'new'
        ],
        1 => [
            'class' => 'bg-success',
            'name' => 'delivered'
        ],
        2 => [
            'class' => 'bg-info',
            'name' => 'proved'
        ],
        3 => [
            'class' => 'bg-warning',
            'name' => 'updated'
        ],
        4 => [
            'class' => 'bg-danger',
            'name' => 'canceled'
        ],
        5 => [
            'class' => 'bg-black',
            'name' => 'pending'
        ],
        6 => [
            'class' => 'bg-warning',
            'name' => 'underway'
        ],
    ];

    const STATUS_NAME = [
        'new' => 0,
        'delivered' => 1,
        'proved' => 2,
        'updated' => 3,
        'canceled' => 4,
        'pending' => 5,
        'underway' => 6,
    ];

    protected $with = [
        'products',
        'address',
        'customer',
        'paymentMethod',
        'gate',
    ];

    protected $fillable = [
        'customer_id',
        'address_id',
        'city_id',
        'payment_method_id',
        'gate_id',
        'requested_time',
        'notes',
        'discount_percentage',
        'discount',
        'delivery_price',
        'products_price',
        'total_price',
        'status',
        'canceled_due',
        'receipt',
        'coupon_id',
    ];

    protected $appends = [
        'status_name',
        'status_class',
        'statuses',
        'invoice_url'
    ];

    protected $casts = [
        'requested_time' => 'datetime'
    ];

    private function clearCache()
    {
        if (Cache::has('monthly_sales'))
            Cache::forget('monthly_sales');
        if (Cache::has('weekly_sales'))
            Cache::forget('weekly_sales');
        if (Cache::has('best_sales_product_last_7_days'))
            Cache::forget('best_sales_product_last_7_days');

    }

    protected static function boot()
    {
        parent::boot();
        if (Auth::guard('web')->check() && request()->user()) {
            self::savingActivity();

            static::creating(function ($model) {
                $model->clearCache();
                $model->user_id = request()->user()->id;
            });

            static::updating(function ($model) {
                $model->clearCache();
                $model->editor_id = request()->user()->id;
            });
        } else {
            static::creating(function ($model) {
                $model->clearCache();
                $model->user_id = 1;
            });

            static::updating(function ($model) {
                $model->clearCache();
                $model->editor_id = 1;
            });
        }

        static::addGlobalScope('impending', function (Builder $builder) {
            $builder->where('orders.status', '!=', Order::STATUS_NAME['pending']);
        });

    }



    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class)->withTrashed();
    }

    public function city()
    {
        return $this->belongsTo(Taxonomy::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(Taxonomy::class, 'payment_method_id');
    }

    public function gate()
    {
        return $this->belongsTo(Taxonomy::class, 'gate_id');
    }

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function getStatusNameAttribute()
    {

        return trans('order::strings.orderStatus' . $this->status);
    }

    public function getStatusClassAttribute()
    {
        return self::STATUSES[$this->status]['class'];
    }

    public function getInvoiceUrlAttribute()
    {
//        return   public_path('storage/invoices/'  . $this->id . '.pdf');
//        return url('invoices/invoice' . $this->id . '.pdf');
        return  'https://bakednco-media.s3.eu-central-1.amazonaws.com/invoices/invoice' . $this->id . '.pdf';
    }

    public function getStatusesAttribute()
    {
        return self::STATUS_NAME;
    }
}
