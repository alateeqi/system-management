<?php

namespace Modules\Order\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
   use RecordsActivity;

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }

        self::savingActivity();
    }
}
