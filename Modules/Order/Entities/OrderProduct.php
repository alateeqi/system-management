<?php

namespace Modules\Order\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Price;
use Modules\Product\Entities\Product;

class OrderProduct extends Model
{
    use RecordsActivity;

    protected $with = [
        'product',
        'priceList'
    ];

    protected $fillable = [
        'order_id',
        'product_id',
        'price_id',
        'price',
        'quantity'
    ];

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }

        self::savingActivity();
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function priceList()
    {
        return $this->belongsTo(Price::class, 'price_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
