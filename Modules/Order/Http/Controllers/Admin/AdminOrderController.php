<?php

namespace Modules\Order\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Customer\Interfaces\Admin\AdminCustomerInterfaces;
use Modules\Order\Http\Requests\Admin\Filter;
use Modules\Order\Http\Requests\Admin\PaymentMethod;
use Modules\Order\Http\Requests\Admin\SmsInvoice;
use Modules\Order\Http\Requests\Admin\Status;
use Modules\Order\Http\Requests\Admin\Store;
use Modules\Order\Http\Requests\Admin\Update;
use Modules\Order\Interfaces\AdminOrderInterface;
use Modules\Taxonomy\Interfaces\Admin\Category\AdminCategoryInterface;
use Modules\Taxonomy\Interfaces\Admin\Gate\AdminGateInterface;
use Modules\Taxonomy\Interfaces\Admin\Payment\AdminPaymentsInterface;

class AdminOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:orders-read'])->only('index');
        $this->middleware(['permission:orders-create'])->only('create');
        $this->middleware(['permission:orders-update'])->only('edit');
        $this->middleware(['permission:orders-delete'])->only('destroy');
    }

    public function index(AdminOrderInterface $interface)
    {
        $data = $interface->index();
        $canceledDues = $data['canceledDues'];
        $paymentMethods = $data['paymentMethods'];
        $gates = $data['gates'];
        $orders = $data['orders'];

        return view('order::admin.index', compact('canceledDues', 'paymentMethods', 'gates', 'orders'));
    }

    public function filter(Filter $request, AdminOrderInterface $interface)
    {
        $data = $interface->filter($request->validated());
        $orders = $data['orders'];
        $paymentMethods = $data['paymentMethods'];
        $canceledDues = $data['canceledDues'];
        return view('order::admin.partials.orders', compact('orders', 'paymentMethods', 'canceledDues'));
    }

    public function show(AdminOrderInterface $interface, $id)
    {
        $printed =false;
        $order = $interface->show($id)['order'];
        if (Storage::disk('s3')->exists('/invoices/invoice' . $order->id . '.pdf')) {
            $printed=true;
            return view('order::admin.summary', compact('order','printed'));
        } else {
            $interface->printOrder($order->id);
            return view('order::admin.summary', compact('order','printed'));
        }


    }

    public function edit(AdminOrderInterface $interface, AdminCategoryInterface $category_interface, AdminGateInterface $gate_interface, AdminPaymentsInterface $payments_interface, $id)
    {
        $order = $interface->show($id);

        $categories = $category_interface->allCategoriesWithProducts();
        $payment_methods = $payments_interface->activePaymentMethods();
        $gates = $gate_interface->active_gates();
        return view('order::admin.edit', compact('order', 'categories', 'gates', 'payment_methods'));
    }

    public function create(AdminCategoryInterface $interface, AdminGateInterface $gate_interface, AdminPaymentsInterface $payments_interface)
    {
        $categories = $interface->allCategoriesWithProducts();


        $payment_methods = $payments_interface->activePaymentMethods();
        $gates = $gate_interface->active_gates();
        return view('order::admin.create', compact('categories', 'gates', 'payment_methods'));
    }

    public function store(Store $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $data = $interface->store($request->validated());
            DB::commit();

            if (isset($data['content'])) {
                return response()->json([
                    'data' => $data['order'],
                    'message' => $data['message'],
                    'content' => $data['content']
                ]);
            }
            return response()->json([
                'data' => $data
            ], 201);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function websiteOrders(AdminOrderInterface $interface)
    {
        $data = $interface->unreadWebsiteOrders();
        $orders = $data['unreadWebsiteOrders'];
        $canceledDues = $data['canceledDues'];
        return view('order::admin.website', compact('orders', 'canceledDues'));
    }

    public function update(Update $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->update($request->validated());
            DB::commit();
            if (isset($data['content'])) {
                return response()->json([
                    'data' => $data['order'],
                    'message' => $data['message'],
                    'content' => $data['content']
                ]);
            }
            return response()->json([
                'data' => $order
            ], 200);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function delivery(Status $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changeStatus($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function new(Status $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changeStatus($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function cancel(Status $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changeStatus($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function proved(Status $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changeStatus($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function receipt_no(Status $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changeStatus($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function changePaymentMethod(PaymentMethod $request, AdminOrderInterface $interface)
    {
        try {
            DB::beginTransaction();
            $order = $interface->changePaymentMethod($request->validated());
            DB::commit();
            return response()->json([
                'data' => $order
            ]);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

    }

    public function sms(SmsInvoice $request, AdminOrderInterface $interface)
    {
        $data = $interface->sendInvoiceSms($request->validated());
        if (is_string($data)) {
            return response()->json([
                'data' => $interface->sendInvoiceSms($request->validated())
            ], 200);
        } else  return response()->json([
            'message' => 'fail'
        ], 500);

    }

    public function all_phones(AdminCustomerInterfaces $interface)
    {
        return response()->json([
            'data' => $interface->all_phones()
        ]);
    }

}
