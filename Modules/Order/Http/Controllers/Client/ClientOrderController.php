<?php

namespace Modules\Order\Http\Controllers\Client;

use Illuminate\Http\Request;
use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Order\Interfaces\Client\ClientOrderInterface;

class ClientOrderController extends Controller
{
    public function orders(ClientOrderInterface $interface)
    {
        return $interface->orders();
    }

    public function show(ClientOrderInterface $interface, $orderId)
    {
        return $interface->show($orderId);
    }
}
