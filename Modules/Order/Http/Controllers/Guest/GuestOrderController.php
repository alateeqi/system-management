<?php

namespace Modules\Order\Http\Controllers\Guest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Order\Http\Requests\Guest\Check;
use Modules\Order\Http\Requests\Guest\MyFatouraCheck;
use Modules\Order\Http\Requests\Guest\Store;
use Modules\Order\Interfaces\Guest\GuestOrderInterface;
use Modules\Taxonomy\Repositories\Guest\Gate\GuestGateRepository;

class GuestOrderController extends Controller
{
    public function store(Store $request, GuestOrderInterface $interface)
    {
        try {

            DB::beginTransaction();
            $data = $interface->store($request->validated());

            if (!is_null($data['errors'])) {
                return $this->respond([
                    'errors' => $data
                ], 500);
            } else {
                DB::commit();
                return $this->respond([
                    'url' => $data['url'],
                    'errors' => $data['errors']
                ]);
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->respond([
                'errors' => $exception->getMessage()
            ]);
        }
    }

//    public function orderCheck(Check $request, GuestOrderInterface $interface, $paymentId)
    public function orderCheck(Request $request, GuestOrderInterface $interface, $paymentId)
    {
        $data = $interface->orderCheck($paymentId, $request);
        return $this->respond($data);
    }
//    public function myFaturaOrderCheck(MyFatouraCheck $request, GuestOrderInterface $interface)
//    {
//       return $interface->myFaturaOrderCheck($request->validated());
//    }
    public function myFaturaOrderCheck(Request $request, GuestOrderInterface $interface)
    {
        return $interface->myFaturaOrderCheck($request);
    }
}
