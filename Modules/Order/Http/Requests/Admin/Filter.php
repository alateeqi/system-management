<?php

namespace Modules\Order\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Filter extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'startDate' => ['required','date'],
            'endDate' =>  ['required','date'],
            'statuses' => ['nullable'],
            'payments' =>  ['nullable'],
            'gates' =>  ['nullable'],//test
            'query' =>[ 'nullable'],
            'phone_number'=>['nullable','numeric','exists:customers,phone']
        ];
    }
}
