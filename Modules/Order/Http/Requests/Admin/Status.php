<?php

namespace Modules\Order\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Order\Entities\Order;

class Status extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'order_id' => ['required', 'numeric', 'exists:orders,id'],
            'status' => ['required'],
            'message' => ['nullable'],
            'receipt_no' => ['nullable']
        ];
    }
}
