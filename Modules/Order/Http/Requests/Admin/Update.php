<?php

namespace Modules\Order\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'order_id' => ['required', 'numeric', 'exists:orders,id'],
            'products_ids' => ['required', 'array'],
            'products_ids.*' => ['numeric', 'exists:prices,id'],
            'products_qty' => ['required', 'array'],
            'products_qty.*' => ['numeric'],
            'customer_phone' => ['required', 'numeric'],
            'address_id' => ['nullable', 'numeric', 'exists:addresses,id'],
            'city_id' => ['nullable', 'numeric', 'exists:taxonomies,id'],
            'requested_date' => ['required'],
            'name' => ['nullable'],
            'note' => ['nullable'],
            'discount_fixed' => ['nullable'],
            'discount_percentage' => ['nullable'],
            'gate_id' => ['required', 'numeric'],
            'payment_method_id' => ['required', 'numeric'],
            'printable' => ['required', 'boolean']
        ];
    }
}
