<?php

namespace Modules\Order\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class Check extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'free_order' => ['nullable'],
            'order_id' =>  ['nullable']
        ];
    }
}
