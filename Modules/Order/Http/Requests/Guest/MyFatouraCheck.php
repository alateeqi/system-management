<?php

namespace Modules\Order\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class MyFatouraCheck extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'EventType' => ['nullable'],
            'Data' =>  ['nullable'],
            'Data.UserDefinedField' =>  ['nullable'],
        ];
    }
}
