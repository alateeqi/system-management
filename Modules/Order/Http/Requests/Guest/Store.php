<?php

namespace Modules\Order\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'order' => ['required'],
            'order.customer' =>  ['required'],
            'order.products' => ['required'],
            'order.payment_method' =>  ['nullable'],
            'order.gate' =>  ['nullable'],//test
            'order.notes' =>[ 'nullable'],
            'order.requested_time' => ['nullable','date'],
            'order.customer.phone'=>['required','regex:/[0-9]{8}/']
        ];
    }
}
