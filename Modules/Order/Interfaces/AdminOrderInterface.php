<?php

namespace Modules\Order\Interfaces;

interface AdminOrderInterface
{
    public function index($paginate = 15);

    public function find($id);

    public function store($data);

    public function update($data);

    public function show($id);
    public function printOrder($id);

    public function filter($data, $paginate = 15);

    public function changePaymentMethod($data);

    public function changeStatus($data);

    public function sendInvoiceSms($data);

    public function unreadWebsiteOrders();

}
