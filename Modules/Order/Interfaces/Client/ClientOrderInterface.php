<?php

namespace Modules\Order\Interfaces\Client;

interface ClientOrderInterface
{
    public function orders();

    public function show($orderId);
}
