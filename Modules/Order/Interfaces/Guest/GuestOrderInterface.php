<?php

namespace Modules\Order\Interfaces\Guest;

use Illuminate\Http\Request;

interface GuestOrderInterface
{
    public function store($data);

//    public function orderCheck($request, $paymentId);
    public function orderCheck($paymentId, Request $request);

//    public function myFaturaOrderCheck($data);
    public function myFaturaOrderCheck(Request $request);
}
