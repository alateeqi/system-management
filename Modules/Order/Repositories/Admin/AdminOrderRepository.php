<?php

namespace Modules\Order\Repositories\Admin;

use App\Notifications\SendInvoiceUrlSms;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Modules\Address\Repositories\Admin\AdminAddressRepository;
use Modules\Auth\Entities\User;
use Modules\Customer\Repositories\Admin\AdminCustomerRepository;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderProduct;
use Modules\Order\Interfaces\AdminOrderInterface;
use Modules\Order\Repositories\OrderRepository;
use Modules\Points\Repositories\Admin\AdminOccasionRepository;
use Modules\Product\Repositories\Admin\Price\AdminPriceRepository;
use Modules\Taxonomy\Repositories\Admin\DeliveryPrice\DeliveryPriceRepository;
use Modules\Taxonomy\Repositories\Admin\Gate\AdminGateRepository;
use Modules\Taxonomy\Repositories\Admin\Options\AdminOptionRepository;
use Modules\Taxonomy\Repositories\Admin\Payment\AdminPaymentRepository;

class AdminOrderRepository implements AdminOrderInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function index($paginate = 15)
    {
        $canceledDues = Order::distinct()
            ->select('canceled_due')
            ->whereNotNull('canceled_due')
            ->get()
            ->pluck('canceled_due')
            ->toArray();
        $paymentMethods = AdminPaymentRepository::activePaymentMethods();
        $gates = AdminGateRepository::active_gates();
        $startDate = Carbon::parse(now())->format('Y-m-d 00:00:00');
        $endDate = Carbon::parse(now())->format('Y-m-d 23:59:59');
        $orders = Order::withoutGlobalScope('impending')->whereBetween('requested_time', [$startDate, $endDate])->orderBy('requested_time')->get();

        return [
            'canceledDues' => $canceledDues,
            'paymentMethods' => $paymentMethods,
            'gates' => $gates,
            'orders' => $orders,
        ];
    }

    public function show($id)
    {
        $productsArray = [];
        $order = $this->find($id);
//        foreach ($order->products as $product) {
//            $productsArray[$product->product_id]["quantityIndex$product->price_id"] = [
//                'price' => $product->price,
//                'quantity' => $product->quantity,
//                'product_name' => $product->product->title,
//                'unite_name' => $product->priceList->unite_name,
//            ];
//        }

        foreach ($order->products as $product) {

            $productsArray[] = [
                'product_id' => $product->product_id,
                'price_id' => $product->price_id,
                'product_title' => $product->product->title,
                'product_image' => $product->product->image,
                'price' => $product->price,
                'qty' => $product->quantity,
                'unite_name' => $product->priceList->unite_name,
            ];
        }
        $orderItem = $order;
        if (!$this->isPickUp($order->gate_id)) {
//            $order->customer->addresses->where('id', $order->address->id)->first()->selected = true;
            $order = $order->toArray();
            $order['address']['selected'] = $order['address']['id'];
        } else {
            $order = $order->toArray();
            $order['address'] = 0;
        }


        return [
            'id' => $order['id'],
            'address_id' => ($order['address']) > 0 ? $order['address']['id'] : 0,
            'products' => $productsArray,
            'customer_phone' => $order['customer']['phone'],
            'customer_addresses' => $order['customer']['addresses'],
            'payment_method_id' => $order['payment_method_id'],
            'gate_id' => $order['gate_id'],
            'requested_time' => $order['requested_time'],
            'discount_percentage' => $order['discount_percentage'],
            'discount' => $order['discount'],
            'invoice_url' => $order['invoice_url'],
            'notes' => $order['notes'],
            'status' => $order['status'],
            'order' => $orderItem
        ];
    }

    public function isPickUp($gateId)
    {
        return in_array($gateId, [681]);
    }

    public function find($id)
    {
//        return  Order::findOrFail($id);
        return Order::withoutGlobalScope('impending')->find($id);
    }

    public function store($data)
    {

        $address = null;
        $customer = (new AdminCustomerRepository($this->user))->findByPhone($data['customer_phone']);
        if (is_null($customer))
            $customer = (new AdminCustomerRepository($this->user))->store(['phone' => $data['customer_phone']]);
        if (isset($data['address_id']))
            $address = (new AdminAddressRepository($this->user))->find($data['address_id']);
        $productsData = $this->productsData($data, $address);
        $data['products_price'] = $productsData['productsPrice'];
        $order_data = [
            'customer_id' => $customer->id,
            'address_id' => $data['address_id'] ?? null,
            'city_id' => $data['city_id'] ?? null,
            'payment_method_id' => $data['payment_method_id'],
            'gate_id' => $data['gate_id'],
            'requested_time' => Carbon::parse($data['requested_date'] ?? now())->format('Y-m-d H:i:s'),
            'notes' => $data['note'] ?? '',
            'discount_percentage' => floatval($data['discount_percentage'] ?? 0),
            'discount' => floatval($data['discount_fixed'] ?? 0),
            'delivery_price' => ($this->isPickUp($data['gate_id'])) ? 0 : $this->getDeliveryPrice($data, $address),
            'total_price' => $productsData['totalPrice'],
            'products_price' => $productsData['productsPrice'],
            'status' => 0
        ];


        $order = new Order($order_data);
        $order->save();
        $this->storeProducts($order, $productsData['products']);
        $this->addPointsIsActive();
        if (isset($data['printable']) && $data['printable'] == true) {
            return [
                'order' => $order,
                'message' => 'hasPdf',
                'content' => $this->printOrder($order->id)
            ];
        }
        $this->printOrder($order->id);
        return $order;
    }

    public function update($data)
    {
        $address = null;
        $order = $this->find($data['order_id']);
        $customer = (new AdminCustomerRepository($this->user))->findByPhone($data['customer_phone']);
        if (is_null($customer))
            $customer = (new AdminCustomerRepository($this->user))->store(['phone' => $data['customer_phone']]);
        if (isset($data['address_id']))
            $address = (new AdminAddressRepository($this->user))->find($data['address_id']);
        $productsData = $this->productsData($data, $address);
        $data['products_price'] = $productsData['productsPrice'];
        $order_data = [
            'customer_id' => $customer->id,
            'address_id' => $data['address_id'] ?? null,
            'city_id' => $data['city_id'] ?? null,
            'payment_method_id' => $data['payment_method_id'],
            'gate_id' => $data['gate_id'],
            'requested_time' => Carbon::parse($data['requested_date'] ?? now())->format('Y-m-d H:i:s'),
            'notes' => $data['note'] ?? '',
            'discount_percentage' => floatval($data['discount_percentage'] ?? 0),
            'discount' => floatval($data['discount_fixed'] ?? 0),
            'delivery_price' => ($this->isPickUp($data['gate_id'])) ? 0 : $this->getDeliveryPrice($data, $address),
            'total_price' => $productsData['totalPrice'],
            'products_price' => $productsData['productsPrice'],
            'status' => Order::STATUS_NAME['updated']
        ];

        $order->update($order_data);

        $order->save();

        $this->storeProducts($order, $productsData['products']);
        if (isset($data['printable']) && $data['printable'] == true) {
            return [
                'order' => $order,
                'message' => 'hasPdf',
                'content' => $this->printOrder($order->id)
            ];
        }

        return $order;
    }

    public function storeProducts(Order $order, $products)
    {

        $order->products()->delete();

        foreach ($products as $product) {

            foreach ($product as $id => $data) {

                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $id;
                $orderProduct->price_id = $data['price_id'];
                $orderProduct->price = $data['price'];
                $orderProduct->quantity = $data['quantity'];
                $orderProduct->save();

            }
        }
    }

    public function productsData($order, $address = null)
    {

        $total = 0;
        $productsPrice = 0;
        $products = [];
        for ($i = 0; $i < count($order['products_ids']); $i++) {
            $price = (new AdminPriceRepository($this->user))->find($order['products_ids'][$i]);
            $price_product = $order['products_qty'][$i] * $price->price;
            $products[] = [
                $price->product_id => [
                    'price' => $price_product,
                    'price_id' => $price->id,
                    'quantity' => $order['products_qty'][$i],
                ]
            ];
            $productsPrice += $price_product;
            $total += $price_product;
        }
        if ($percentage = floatval($order['discount_percentage'] ?? 0)) {
            $total -= $total * ($percentage / 100);
        }
        $total -= floatval($order['discount_fixed'] ?? 0);


        // if payment method is not Pick up
        if (!is_null($address))
            if (!$this->isPickUp($order['gate_id'])) {
                $total += $this->getDeliveryPrice([
                    'products_price' => $productsPrice,
                    'gate_id' => $order['gate_id'],

                ], $address);
            }

        if ($total < 0) {
            $total = 0;
        }

        return [
            'totalPrice' => $total,
            'productsPrice' => $productsPrice,
            'products' => $products,
        ];
    }

    private function getDeliveryPrice($data, $address)
    {
        $generalDeliveryPrice = $this->getGeneralDeliveryPrice($data);
        if ($generalDeliveryPrice === null) {
            Log::info("order city excerpt " . $address->city->excerpt, []);

            return floatval($address->city->excerpt);
        } else {
            return $generalDeliveryPrice;
        }
    }

    private function getGeneralDeliveryPrice($data)
    {
        $websiteDeliveryPrice = DeliveryPriceRepository::websiteDeliveryPrice();
        $systemDeliveryPrice = DeliveryPriceRepository::systemDeliveryPrice();
        $isSunOrMon = date('D') == 'Sun' || date('D') == 'Mon';

        if ($websiteDeliveryPrice->status !== 1 && $systemDeliveryPrice->status !== 1 && !$isSunOrMon) {
            return null;
        }

        if ($data['products_price'] < 7) {
            return 2;
        }

        if ($this->isWebsiteOrder($data['gate_id'])) {
            if ($websiteDeliveryPrice->status === 1) {
                return floatval($websiteDeliveryPrice->excerpt);
            }
        } else {
            if ($systemDeliveryPrice->status === 1) {
                return floatval($systemDeliveryPrice->excerpt);
            }
        }

        if ($isSunOrMon) {
            return floatval(optional(DeliveryPriceRepository::sunOrMonDeliveryPrice())->excerpt) ?? 1;
        }

        return null;
    }

    private function isWebsiteOrder($gate_id)
    {
        return in_array($gate_id, [680]);
    }

    public function filter($data, $paginate = 15)
    {
        $startDate = Carbon::parse($data['startDate'])->format('Y-m-d 00:00:00');
        $endDate = Carbon::parse($data['endDate'])->format('Y-m-d 23:59:59');
        $orders = Order::withoutGlobalScope('impending')
            ->whereBetween('requested_time', [$startDate, $endDate])->orderBy('id');

        if (isset($data['statuses'])) {
            $orders = $orders->whereIn('status', $data['statuses']);
        }
        if (isset($data['phone_number'])) {
            $orders = $orders->where('customer_id', (new AdminCustomerRepository($this->user))->findByPhone($data['phone_number'])->id);
        }

        if (isset($data['payments'])) {
            $orders = $orders->whereIn('payment_method_id', $data['payments']);
        }

        if (isset($data['gates'])) {
            $orders = $orders->whereIn('gate_id', $data['gates']);
        }

        if (isset($data['query'])) {
            $keyword = $data['query'];
            $orders = $orders->where(function ($query) use ($keyword) {
                $query->where('id', 'LIKE', '%' . $keyword . '%')->orWhereHas('customer',
                    function ($query2) use ($keyword) {
                        $query2->where('phone', 'LIKE', '%' . $keyword . '%');
                    });
            });
        }
        $paymentMethods = AdminPaymentRepository::activePaymentMethods();
        $canceledDues = Order::distinct()
            ->select('canceled_due')
            ->whereNotNull('canceled_due')
            ->get()
            ->pluck('canceled_due')
            ->toArray();
        return [
            'orders' => $orders->get(),
            'paymentMethods' => $paymentMethods,
            'canceledDues' => $canceledDues
        ];
    }

    public function changePaymentMethod($data)
    {
        $oredr = $this->find($data['order_id']);
        $oredr->payment_method_id = ($oredr->payment_method_id === 120) ? 119 : 120;
        $oredr->save();

        return $oredr;
    }

    public function changeStatus($data)
    {
        $order = $this->find($data['order_id']);
        $order->status = $data['status'];
        if (isset($data['message']))
            $order->canceled_due = $data['message'];
        if (isset($data['receipt_no']))
            $order->receipt = $data['receipt_no'];
        if ($data['status'] != Order::STATUS_NAME['canceled']) {
            $order->canceled_due = null;
        }
        if ($data['status'] != Order::STATUS_NAME['proved']) {
            $order->receipt = null;
        }
        if ($data['status'] === Order::STATUS_NAME['new']) {
            $order->confirmed_at = now();
        }
        if ($data['status'] === Order::STATUS_NAME['delivered']) {
            $order->delivered_at = now();
        }
        $order->save();

        return $order;
    }

    public function printOrder($orderID)
    {

        $order = $this->find($orderID);
        //local host
//         $fileName = public_path('storage/invoices/' . $order->id . '.pdf');


        $pdf = PDF::loadView('order::admin.print', ['order' => $order])->setOptions(["logOutputFile" => ini_get('upload_tmp_dir') . '/log.htm']);

        //server
//        $fileName = 'invoices/invoice' . $order->id . '.pdf';
//        $order->addMediaFromBase64($pdf)->toMediaCollection('images');
//        $order->addMediaFromUrl($imageUrl)->toMediaCollection('images');

        Storage::disk('s3')
            ->put(
                'invoices/invoice' . $order->id . '.pdf',
                $pdf->output()
            );

//        $order->addMedia($pdf->output())->toMediaCollection('invoices');
//        $pdf->setOptions(["logOutputFile" => ini_get('upload_tmp_dir') . '/log.htm'])->save($fileName);

        return 'https://bakednco-media.s3.eu-central-1.amazonaws.com/invoices/invoice' . $order->id . '.pdf';
//        return url($fileName);
    }

    public function sendInvoiceSms($data)
    {

        $order = $this->find($data['order_id']);
        if ($order->total_price) {
            $langExtension = App::getLocale() != 'en' ? 'ar/' : '';
            $invoiceURL = (new OrderRepository())->createInvoiceUrl(json_encode((object)[
                "NotificationOption" => "ALL",
                "CustomerName" => $order->customer->phone . " - Website",
                "DisplayCurrencyIso" => "KWD",
                "MobileCountryCode" => "+965",
                "CustomerMobile" => $order->customer->phone,
                "CustomerEmail" => $order->customer->email ?? 'info@backednco.com',
                "InvoiceValue" => $order->total_price,
                "CallBackUrl" => "https://bakednco.com/" . $langExtension . "order-check/?status=success",
                "ErrorUrl" => "https://bakednco.com/" . $langExtension . "order-check/?status=error",
                "Language" => "en",
                "UserDefinedField" => json_encode((object)[
                    "OrderId" => $order->id
                ]),
            ]), 'v2/SendPayment')->InvoiceURL;
            $order->customer->notify(new SendInvoiceUrlSms($invoiceURL, $order->customer->phone));
            return $invoiceURL;
        }
        return false;
    }

    public function unreadWebsiteOrders()
    {

        $canceledDues = Order::distinct()
            ->select('canceled_due')
            ->whereNotNull('canceled_due')
            ->get()
            ->pluck('canceled_due')
            ->toArray();
        $unreadWebsiteOrders = Order::where([
            'gate_id' => 680,
            'status' => Order::STATUS_NAME['underway']
        ])->get();
        return [
            'unreadWebsiteOrders' => $unreadWebsiteOrders,
            'canceledDues' => $canceledDues
        ];
    }

    public function getCountOrderByTodayForUser()
    {
        return Order::whereBetween('created_at', [Carbon::parse(today())->format('Y-m-d 00:00:00'), Carbon::parse(today())->format('Y-m-d 23:59:59')])
            ->where('user_id', $this->user->id)->count();
    }

    public function addPointsIsActive()
    {
        // check from point is active or not
        if (AdminOptionRepository::checkOption(738)) {
            $point = (new AdminOccasionRepository($this->user));
            //check if there is occasion by today or not
            $occasion = AdminOccasionRepository::getOccasionByToday();
            if (!is_null($occasion)) {
                if (!$occasion->status)
                    return;


                //check from count order by today for user adding the order
                if ($this->getCountOrderByTodayForUser() >= $occasion->min_order) {
                    //add point to table points
                    $point->addPointsToUser($occasion->id, $occasion->points, true);
                } else {
                    return;
                }

            } else {

                // check from today is end or middle week
                $object = AdminOptionRepository::pointOfWeek();
                $object_point = null;
                if (!is_null($object)) {
                    if (AdminOptionRepository::isEndWeek()) {
                        if (date('D') == 'Sat') {
                            $object_point = $point->storeMiddleEndWeek([
                                'name' => 'عطلة نهاية الأسبوع ' . ' ' . Carbon::today()->format('Y-m-d'),
                                'points' => $object->translate('ar')->excerpt,
                                'min_order' => $object->translate('ar')->content,
                                'from' => Carbon::today()->format('Y-m-d 00:00:00'),
                                'to' => Carbon::today()->format('Y-m-d 23:59:59'),
                            ]);
                        } else if (date('D') == 'Fri')
                            $object_point = $point->storeMiddleEndWeek([
                                'name' => 'عطلة نهاية الأسبوع ' . ' ' . Carbon::today()->format('Y-m-d'),
                                'points' => $object->translate('ar')->excerpt,
                                'min_order' => $object->translate('ar')->content,
                                'from' => Carbon::today()->format('Y-m-d 00:00:00'),
                                'to' => Carbon::tomorrow()->format('Y-m-d 23:59:59'),
                            ]);
                    } else {
                        $object_point = $point->storeMiddleEndWeek([
                            'name' => 'وسط الأسبوع ' . ' ' . Carbon::today()->format('Y-m-d'),
                            'points' => $object->translate('ar')->excerpt,
                            'min_order' => $object->translate('ar')->content,
                            'from' => Carbon::today()->format('Y-m-d 00:00:00'),
                            'to' => now()->endOfWeek(5)->subDay()->format('Y-m-d 23:59:59'),
                        ]);
                    }

                    //check from count order by today for user adding the order
                    if ($this->getCountOrderByTodayForUser() >= $object->translate('ar')->content) {
                        //add point to table points
                        $point->addPointsToUser($object_point->id, $object->translate('ar')->excerpt, false);
                    } else {
                        return;
                    }
                } else return;
            }

        } else return;

    }

}
