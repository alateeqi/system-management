<?php

namespace Modules\Order\Repositories\Client;

use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;
use Modules\Order\Interfaces\Client\ClientOrderInterface;

class ClientOrderRepository implements ClientOrderInterface
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function orders()
    {
        return Order::where('customer_id', $this->customer->id)->orderByDesc('id')->get();
    }

    public function show($orderId)
    {
        return Order::findOrFail($orderId);
    }
}
