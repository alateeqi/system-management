<?php

namespace Modules\Order\Repositories\Guest;

use App\Notifications\SendMessageSms;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Modules\Coupon\Repositories\Guest\GuestCouponUseRepository;
use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;
use Modules\Order\Interfaces\Guest\GuestOrderInterface;
use Modules\Order\Repositories\OrderRepository;
use Modules\Taxonomy\Repositories\Guest\Bell\GuestBellRepository;
use Modules\Taxonomy\Repositories\Guest\Gate\GuestGateRepository;
use Modules\Taxonomy\Repositories\Guest\PaymentMethod\GuestPaymentMethodRepository;

class GuestOrderRepository implements GuestOrderInterface
{
    public function store($data)
    {
        $orderArr = $data['order'];
        if (empty($data['order']['customer']['address'])) {
            $newAddress = $data['order']['address'];
            $newAddress['selected'] = true;
            if (!empty($orderArr['customer'])) {
                foreach ($orderArr['customer']['addresses'] as $index => $customerAddress) {
                    if (empty($customerAddress['id'])) {
                        unset($orderArr['customer']['addresses'][$index]);
                    }
                }
            }
            $orderArr['customer']['addresses'][] = $newAddress;
        }

        if (!empty($data['order']['phone'])) {
            $phone = $data['order']['phone'];
            $orderArr['customer']['phone'] = $phone;
        }

        if ($data['order']['laterTime']) {
            $orderArr['requested_time'] = $data['order']['requestedDate'] . ' ' . $data['order']['requestedTime'];
            $orderArr['requested_time'] = Carbon::parse($orderArr['requested_time'])->format('Y-m-d H:i:s');
        } else {
            $orderArr['requested_time'] = Carbon::now()->format('Y-m-d H:i:s');
        }
        $orderArr['payment_method'] = GuestPaymentMethodRepository::Bank();

        $orderArr['gate'] = GuestGateRepository::website();

        $orderArr['gate_id'] = $orderArr['gate']['id'];
        $orderArr['fromWebsite'] = true;
        $data['order'] = $orderArr;


        $order = (new OrderRepository())->storeFunctions($data);

        if (empty($order->id)) {
            return [
                'errors' => $order->original['errors'],
                'url' => null,
            ];
        }
        $order->status = Order::STATUS_NAME['pending'];
        $order->save();
        \Log::info('$order->total_price:' . $order->total_price);
        \Log::info('$order->id' . $order->id);
        $langExtension = App::getLocale() != 'en' ? 'ar/' : '';
        if ($order->total_price == 0) {


            return [
                'errors' => null,
                'url' => "https://bakednco.com/" . $langExtension . "order-check?paymentId=0&free_order=1&order_id=" . $order->id
            ];
        }
        //Auth::login($order->customer);

        $invoiceURL = (new OrderRepository())->createInvoiceUrl(json_encode((object)[
            "NotificationOption" => "ALL",
            "CustomerName" => $order->customer->phone . " - Website",
            "DisplayCurrencyIso" => "KWD",
            "MobileCountryCode" => "+965",
            "CustomerMobile" => $order->customer->phone,
            "CustomerEmail" => $order->customer->email ?? 'info@backednco.com',
            "InvoiceValue" => $order->total_price,
            "CallBackUrl" => "https://bakednco.com/" . $langExtension . "order-check/?status=success",
            "ErrorUrl" => "https://bakednco.com/" . $langExtension . "order-check/?status=error",
            "Language" => "en",
            "UserDefinedField" => json_encode((object)[
                "OrderId" => $order->id
            ]),
        ]), 'v2/SendPayment');


        return [
            'errors' => null,
            'url' => $invoiceURL->InvoiceURL
        ];
    }


//    public function orderCheck($request, $paymentId)
    public function orderCheck($paymentId, Request $request)
    {
//        if (isset($request['free_order']) && isset($request['order_id'])) {
        if ($request->has('free_order') && $request->has('order_id')) {
            $data = (object)['InvoiceStatus' => 'Paid'];
//            $order = Order::withoutGlobalScope('impending')
//                ->withTrashed()
//                ->where('total_price', 0)
//                ->findOrFail($request['order_id']);
            $order = Order::withoutGlobalScope('impending')->withTrashed()->where('total_price',
                0)->findOrFail($request->input('order_id'));
        } else {

            $data = (new OrderRepository())->createInvoiceUrl(json_encode((object)[
                'key' => $paymentId,
                'KeyType' => 'paymentId',
            ]), 'v2/GetPaymentStatus');
            $userDefinedField = json_decode($data->UserDefinedField);
            $orderId = $userDefinedField->OrderId;
            $order = Order::withoutGlobalScope('impending')->withTrashed()->findOrFail($orderId);
        }

        if ($data->InvoiceStatus === 'Paid') {
            if ($order->status === Order::STATUS_NAME['pending']) {
                $order->status = Order::STATUS_NAME['underway'];
                $order->save();

//                $message = '# ' . $order->id . ' \n ';
//                $message .= 'KWD ' . number_format($order->total_price, 1);
//                foreach ($order->products as $product) {
//                    $message .= ' \n ' . $product->product->title . '  - QTY ' . $product->quantity . (!is_null($product->priceList) ? $product->priceList->unite_name : '') . ' KWD ' . number_format($product->price, 1);
//                }
//                $message = ' \n ' . ' Delivering At:  ' . $order->requested_time->toDateString() . ' \n ';
//                $order->customer->notify(new SendMessageSms($message, $order->customer->phone));
                if ($order->coupon_id) {
                    $couponUse = (new GuestCouponUseRepository())->store($order);

                }

                // todo make events for this
                $bell = GuestBellRepository::onBell();
            }

            return [
                'status' => true,
                'orderId' => $order->id
            ];
        } else {

            return [
                'status' => false,
                'myfatura' => $data
            ];
        }
    }

//    public function myFaturaOrderCheck($data)
//    {
//        if ($data['EventType'] === 1) {
//            $data = $data['Data'];
//            Log::info('MyFaturaCheck $data', ['data' => $data]);
//            if ($data['TransactionStatus'] === 'SUCCESS') {
//                $UserDefinedField = json_decode($data['UserDefinedField'], 1);
//                if ($UserDefinedField) {
//                    $orderId = $UserDefinedField['OrderId'];
//                    $order = Order::withoutGlobalScope('impending')->withTrashed()->findOrFail($orderId);
//                    if ($order->status === Order::STATUS_NAME['pending']) {
//                        $order->status = Order::STATUS_NAME['underway'];
//                        $order->save();
//
//                        if ($order->coupon_id) {
//                            $couponUse = (new  GuestCouponUseRepository())->store($order);
//                        }
//
//                        //  event
//                        GuestBellRepository::onBell();
//
//                    }
//                }
//            }
//        }
//    }
    public function myfaturaOrderCheck(Request $request)
    {
        if ($request->input('EventType') === 1) {
            $data = $request->input('Data');
            Log::info('MyFaturaCheck $data', ['data' => $data]);
            if ($data['TransactionStatus'] === 'SUCCESS') {
                $UserDefinedField = json_decode($data['UserDefinedField'], 1);
                if ($UserDefinedField) {
                    $orderId = $UserDefinedField['OrderId'];
                    $order = Order::withoutGlobalScope('impending')->withTrashed()->findOrFail($orderId);
                    if ($order->status === Order::STATUS_NAME['pending']) {
                        $order->status = Order::STATUS_NAME['underway'];
                        $order->save();

                        if ($order->coupon_id) {
                            $couponUse = (new  GuestCouponUseRepository())->store($order);
                        }

                        // todo make events for this
                        GuestBellRepository::onBell();
                    }
                }
            }
        }
    }
}
