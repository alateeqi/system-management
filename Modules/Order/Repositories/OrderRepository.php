<?php

namespace Modules\Order\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Modules\Coupon\Repositories\Guest\GuestCouponRepository;
use Modules\Customer\Repositories\Guest\GuestCustomerRepository;
use Modules\Order\Entities\Order;
use Modules\Product\Repositories\Guest\GuestProductRepository;

class OrderRepository
{

    public function storeFunctions($request)
    {
        $coupon_id = null;
        if (isset($request['order']['couponCode'])) {
            $coupon = (new GuestCouponRepository())->findByCode($request['order']['couponCode']);
            $productsData = (new GuestProductRepository())->productsData($request['order']);
            $checkCoupon = GuestCouponRepository::checkCoupon($coupon, $productsData['productsPrice']);
            \Log::info('checkCoupon: ' . json_encode($checkCoupon));

            if ($checkCoupon['status'] == true) {

//                $request['order'] = (['order' => $request['order'] + ['discount_percentage' => $coupon->discount_percentage]]);
//                $request['order'] = (['order' => $request['order'] + ['discount' => $coupon->discount]]);
//
                $request['order']['discount_percentage'] =   $coupon->discount_percentage;
                $request['order']['discount'] =  $coupon->discount;
                $coupon_id = $coupon->id;

            }
        }

        $customer = (new GuestCustomerRepository())->updateCustomerByOrder($request['order']);
        $productsData = (new GuestProductRepository())->productsData($request['order']);
        $customOrderObject = [
            'gate_id' => $request['order']['gate']['id'],
            'products_price' => $productsData['productsPrice'],
            'address' => [
                'city' => [
                    'excerpt' => $customer['address']['city']['excerpt']
                ]
            ]
        ];
        $data = [
            'customer_id' => $customer['id'],
            'payment_method_id' => $request['order']['payment_method']['id'],
            'gate_id' => $request['order']['gate']['id'],
            'requested_time' => Carbon::parse($request['order']['requested_time'] ?? now())->format('Y-m-d H:i:s'),
            'notes' => $request['order']['notes'] ?? '',
            'discount_percentage' => floatval($request['order']['discount_percentage'] ?? 0),
            'discount' => floatval($request['order']['discount'] ?? 0),
            'delivery_price' => ((new GuestProductRepository())->isPickUp($request['order']['gate']['id'])) ? 0 : (new GuestProductRepository())->getDeliveryPrice($customOrderObject),
            'total_price' => $productsData['totalPrice'],
            'products_price' => $productsData['productsPrice']
        ];
        if ($coupon_id) {
            $data['coupon_id'] = $coupon_id;
        }

        if (!(new GuestProductRepository())->isPickUp($request['order']['gate']['id'])) {
            $data['address_id'] = $customer['address']->id;
            $data['city_id'] = $customer['address']->city_id;
            $data['delivery_price'] = (new GuestProductRepository())->getDeliveryPrice($customOrderObject);
        } else {
            $data['address_id'] = null;
            $data['city_id'] = null;
        }

        if ($data['notes'] === 'test') {
            $data['notes'] = json_encode($request->order) . ' + ' . json_encode($data);
        }

        $order = new Order($data);
        $order->save();
        (new GuestProductRepository())->storeProducts($order, $productsData['products']);
        return $order;
    }

    public function createInvoiceUrl($body, $url)
    {
//live
        $token = "yf_XhWjxTuCA5_uj3zZ5wwXQpFXhX0vtwexdhDBBd7HRo7HH3F-Ux2C5MV3H33arM-Kkkh3gLiI6Jea2JO2BS1Cs6q6Xw1D3-Ro6Vnj5lIktfleqtDOBV8RNMjBzLkYhIxCVl6DOQOyHFztiYLAGdQxOxLoTpBcty7pMd5DZ5PxomroUyd6O9xd1X9DETjhvSz6HcsWSaJZ1dZQGSeA-4lDBgDqq7-WEXVjPuhAMnmOqTFt8kEUDxxMnf5to2Tef9932jkbVAVsmczcPWyqP8ssRBFXjq2t6lDjEBsdIRqLrCIyUwfSmEPZZqOt2LMYQ7KOkUCZnhoJzm_dpojzN1mVjbO8DQwPv_rdkdc0H0YHEsU5hMNdGvQnbzCooh3ZX0kPnE5vKNhlAYHNmoUqUKgZSVQ4BwhlHrrnCbE7a6fYBxIdR3k6btZOxXLKo7Zr9LOzLQwAzLou2gXfUwyhVfIiqVwTEkL_8Hs_NHhB89AWKx9MOusg7TfOyxdFzzpTymvVmBq8ZCOOF4FIEWt_bcGXULaROesaHrFf5p3Mw4BA2JGZ9bNU-_A2uuiqus83GriLXWO2mT0KICc2pG9mPnfl1wknriMhtoMDdM5rlzfMGqOX1-hCXv49X1qxQh43rAbXbdDcuAP_9AWs1gDPLojwDB_SwutjOkGW5JZ5tSUnHfgpV";
        $basURL = "https://api.myfatoorah.com/";
//test
//        $token = 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL';
//        $basURL ='https://apitest.myfatoorah.com/';

        //            CURLOPT_URL => "$basURL/v2/SendPayment",

        $curl = curl_init();
        curl_setopt_array($curl, [

            CURLOPT_URL => $basURL . $url,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => ["Authorization: Bearer $token", "Content-Type: application/json"],

        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        $data = json_decode($response)->Data;
        if ($err) {
            Log::info("cURL Error payment order#:" . $err, []);
        }

        return $data;
    }
}
