<?php

return [

    'Warring' => 'Warring!',
    'doYouWantToClose' => 'Are you sure you want to close the Window? All the unsaved data will be lost.',
    'DoYouWantToDeleteThisRecord' => 'هل انت متأكد من حذف السجل؟؟',
    'orderStatus0' => 'جديد',
    'orderStatus1' => 'تم التسليم',
    'orderStatus2' => 'تم تثبيته',
    'orderStatus3' => 'معدل',
    'orderStatus4' => 'ملغى',
    'orderStatus5' => 'التأكد من الدفع',
    'orderStatus6' => 'قيد التحضير',
];
