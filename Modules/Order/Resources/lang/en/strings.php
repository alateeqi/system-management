<?php

return [

    'Warring' => 'Warring!',
    'doYouWantToClose' => 'Are you sure you want to close the Window? All the unsaved data will be lost.',
    'DoYouWantToDeleteThisRecord' => 'Are you sure you want to delete this record?',
    'orderStatus0' => 'New',
    'orderStatus1' => 'Delivered',
    'orderStatus2' => 'Proved',
    'orderStatus3' => 'Updated',
    'orderStatus4' => 'Canceled',
    'orderStatus5' => 'Pending',
    'orderStatus6' => 'Underway',
];
