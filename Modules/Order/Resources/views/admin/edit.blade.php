@extends('layouts.dashboard.app')
@push('css')
    <style>
        @media (max-width: 767.98px) {
            .small-box {
                text-align: center;
            }


        }

        @media screen and (max-width: 640px) {
            .sw > .nav {

                -webkit-box-orient: vertical !important;
                -webkit-box-direction: normal !important;
                flex-direction: inherit !important;
                -webkit-box-flex: 1;
                flex: 1 auto;
            }
        }

        @media screen and (max-width: 640px) {
            #stepper-nav {

                z-index: 1;
                background: #f4f6f9;
                position: fixed;
                bottom: 55px;
                right: 0;
                left: 0;
                padding: 1px 0;
            }

            #stepper-nav-buttons {

                z-index: 1;
                background: #f4f6f9;
                position: fixed;
                bottom: 35px;
                right: 0;
                left: 0;
                padding: 1px 0;
            }

            #product-tags {

                z-index: 2;
                background: #f4f6f9;
                position: fixed;
                bottom: 140px;
                right: 0;
                left: 0;
                padding: 1px 0;
            }
        }

        .product-box {
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            color: #ffffff;
            margin-bottom: 1em;
        }

        .product-box .product-overlay {
            background: rgba(0, 0, 0, 0.5);
            margin: 0 !important;
            border: 5px solid transparent;
            min-height: 150px;
        }


        .product-selected {
            border-color: #00CDFBFF !important;
        }


        .product-box .inner {
            min-height: 150px;
        }

    </style>
@endpush
@section('content')

    <div class=" page-wrapper">
        <div class="page-content">
            <div style="display: none">
                <div class="row" id="cart">
                    <div class="card p-5">
                        <div class="card-header">
                            <h2>
                                Basket
                            </h2>
                        </div>
                        <div class="card-body" id="basket"></div>
                        <div class="card-footer" id="basket-footer">
                            <div class="d-flex flex-row mb-4 pb-2">
                                <div class="flex-fill">
                                    <h6>
                                        Items Total
                                    </h6>
                                    <br/>
                                    <h4 id="basket-total-items"></h4>
                                </div>
                                <div class="flex-fill">
                                    <h6>
                                        Qty Total
                                    </h6>
                                    <br/>
                                    <h4 id="basket-total-qty"></h4>
                                </div>
                                <div class="flex-fill">
                                    <h6>
                                        Price Total
                                    </h6>
                                    <br/>
                                    <h4 id="basket-total-price"></h4>
                                </div>
                                <div class="flex-fill">
                                    <h6>
                                        Delivery Price
                                    </h6>
                                    <br/>
                                    <h4 id="basket-total-delivery-price"></h4>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-12 mx-auto">


                    <br/>
                    <!-- SmartWizard html -->
                    <div id="smartwizard">
                        <div class="row row-cols-auto g-3 " id="product-tags"></div>
                        <ul id="stepper-nav" class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#step-1">
                                    <strong>Products</strong>


                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#step-2">
                                    <strong>Address</strong>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#step-3"> <strong>Payment </strong>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#step-4">
                                    <strong>Cart</strong>
                                </a>
                            </li>


                        </ul>
                        <ul id="stepper-nav-buttons" class="list-inline mb-0">
                            <li class="list-inline-item">
                                <button class="d-flex justify-content-start btn btn-secondary" id="prev-btn"
                                        type="button"> <
                                </button>
                            </li>
                            <li class="list-inline-item">
                                <button class="d-flex justify-content-end btn btn-secondary" id="next-btn"
                                        type="button"> >
                                </button>
                            </li>
                        </ul>


                        <div class="tab-content">
                            <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                                @include('order::admin.partials.wizard_steps.step_1')

                            </div>

                            <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                                @include('order::admin.partials.wizard_steps.step_2')
                            </div>
                            <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                                @include('order::admin.partials.wizard_steps.step_3')
                            </div>
                            <div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">
                                @include('order::admin.partials.wizard_steps.step_4')
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


    </div>
    </div>

@endsection
@push('js')
    <script src="{{URL::asset('assets/plugins/smart-wizard/js/jquery.smartWizard.min.js')}}"></script>
    <script>
        window.array_ids_product = [];
        window.array_qty_product = [];
        window.gate_id = 0;
        window.order_id = @json($order['id']);
        window.payment_id = 0;
        window.address_id = 0;
        var quantity_basket = 1;

        function getOrder() {
            @for($i=0;$i<count($order['products']);$i++)
            products_selected('{{$order['products'][$i]['product_id'].$order['products'][$i]['price_id']}}', '{{$order['products'][$i]['product_id']}}', '{{$order['products'][$i]['product_title']}}', '{{$order['products'][$i]['product_image']}}', '{{$order['products'][$i]['unite_name']}}', '{{$order['products'][$i]['price']}}', '{{$order['products'][$i]['price_id']}}', '{{$order['products'][$i]['qty']}}');
            qty_plus('{{$order['products'][$i]['product_id'].$order['products'][$i]['price_id']}}', '{{$order['products'][$i]['price']}}')

            $('#date-time').val('{{Carbon\Carbon::parse($order['requested_time'])->toDateTimeString()}}')
            $('#order-note').val('{{$order['notes']}}')
            $('#discount-percentage').val('{{$order['discount_percentage']}}')
            $('#discount-fixed').val('{{$order['discount']}}')
            $('#order-customer-phone').text('(+965) ' + $('#phone-customer').val())

            $('#payment{{$order['payment_method_id']}}').prop('checked', true)
            $('#gate{{$order['gate_id']}}').prop('checked', true)
            $('#addresses').empty()
            @for ($j = 0; $j < count($order['customer_addresses']); $j++)
            $('#addresses').append(
                '<div class="col-12 col-md-6 address-content" id="address{{$order['customer_addresses'][$j]['id']+$j}}" ' +
                'style="margin: 0 !important;' +
                'border: 5px solid transparent;">' +
                '<div class="card  card card-default">' +
                '<div class="card-header">' +
                '<div class="row">' +
                '<div class="col-6">' +
                '<h5 class="card-title"> Address {{$j+1}}</h5>' +
                '</div>' +
                '<div class="col-6">' +
                '<button type="button" class="btn btn-sm mx-1 float-right btn-secondary" onclick="select_address_box({{$order['customer_addresses'][$j]['id']+$j}}  , {{$order['customer_addresses'][$j]['city']['options']['no']}}, {{$order['customer_addresses'][$j]['id']}} )">' +
                'Select this address' +
                '</button>' +
                '</div>' +
                '</div>' +
                '</div> ' +
                '<div class="card-body">' +
                '<div class="row">' +
                '<div class="col-md-8">' +
                '<div class="form-group">' +
                '<label>City</label>' +
                '<input type="text" class="form-control city" value="{{$order['customer_addresses'][$j]['city']['title']}}">' +
                '</div>' +
                '</div>' +

                '<div class="col-md-4"><label>Delivery Price</label> ' +
                '<div class="font-weight-bold">{{$order['customer_addresses'][$j]['city']['options']['no'] }} KWD</div>' +
                '</div>' +

                ' <div class="col-md-6">' +
                '<div class="form-group"><label>Street</label> ' +
                '<input type="text" class="form-control street" value="{{$order['customer_addresses'][$j]['street'] }}"></div></div>' +
                ' <div class="col-md-3"><div class="form-group"><label>Block</label> <input type="text" class="form-control block"  value="{{$order['customer_addresses'][$j]['block'] }}"></div></div> ' +
                '<div class="col-md-3 col-6"><div class="form-group"><label>House</label> <input type="text" class="form-control house" value="{{$order['customer_addresses'][$j]['house'] }}"></div></div>' +
                ' <div class="col-md-4 col-6"><div class="form-group"><label>Avenue</label> <input type="text" class="form-control avenue" value="{{$order['customer_addresses'][$j]['avenue'] }}"></div></div>' +
                ' <div class="col-md-4 col-6"><div class="form-group"><label>Flat</label> <input type="text" class="form-control flat" value="{{$order['customer_addresses'][$j]['flat'] }}"></div></div>' +
                ' <div class="col-md-4 col-6">' +
                '<div class="form-group"><label>Floor</label> <input type="text" class="form-control floor"  value="{{$order['customer_addresses'][$j]['floor'] }}></div></div>' +
                '</div></div></div></div></div>'
            );
            @if ($order['customer_addresses'][$j]['id'] == $order['address_id'])
            select_address_box({{$order['customer_addresses'][$j]['id']+$j}}, {{$order['customer_addresses'][$j]['city']['options']['no']}}, {{$order['customer_addresses'][$j]['id']}});
            @endif

            @endfor

            @endfor

        }

        function products_selected(id, product_id, product_title, product_image, price_unite, price, price_id, qty) {

            $(`#product-seleccted${id}`).addClass('product-selected')
            $(`#product-father-selected${product_id}`).addClass('product-selected')

            $('#basket').fadeIn()
            if ($(`#basket-product${id}`).length) {
                return;
            } else {
                $('#product-tags').append(
                    '<div class="col">' +
                    ' <span  class="badge bg-primary  " id="tag-' + id + '"> ' + product_title + '  (<span  class="badge bg-black " id="tag-qty-' + id + '">1 </span> - ' + price_unite + ' ) <button   onclick="remove_item_from_basket(' + id + ',' + product_id + ',' + price_id + ')" class="badge btn-danger ml-2">X</button> </span>' +
                    ' </div>')
                $('#basket').append(
                    '<div id="basket-product' + id + '"  class="basket-content" style="height:auto">' +
                    '<div class="d-flex flex-row " data-product-id="' + price_id + '">' +
                    ' <div class="flex-fill">' +
                    ' <h5 class="bold">' + product_title + ' (' + price_unite + ') ' +
                    '<button type="button" id="basket-remove-item' + id + '" class="btn btn-danger" onclick="remove_item_from_basket(' + id + ',' + product_id + ',' + price_id + ')"><i class="bx bxs-trash me-0" ></i>' +
                    ' </button>' +
                    '</h5>' +
                    ' <h5 class="mb-3">' +
                    '<strong id="price-item-basket' + id + '">' + price + '</strong> KWD' +
                    ' </h5>' +
                    /////
                    ' <div class="input-group mb-3" style="width:18%">' +
                    '<span class="input-group-btn">' +
                    '<button type="button" id="quantity-basket-left-minus' + id + '" class="quantity-left-minus  btn btn-danger btn-number" data-type="minus" data-field="" onclick="qty_minis(' + id + ',' + price + ')">' +
                    '   <span class="bx bx-minus"></span>' +
                    '  </button>' +
                    ' </span>' +
                    '  <input type="text" disabled id="quantity-basket' + id + '" name="quantity" class="form-control input-number text-lg-center" value="' + (Number(qty) - 1) + '" min="1" max="100">' +
                    ' <span class="input-group-btn">' +
                    ' <button type="button" id="quantity-basket-right-plus' + id + '" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="" onclick="qty_plus(' + id + ',' + price + ')">' +
                    '<span class="bx bx-plus"></span>' +
                    '  </button>' +
                    ' </span>' +
                    '  </div>' +
                    /////
                    ' </div>' +
                    /////////////////////////////////

                    '<div class="col-3  product-box" style=" background-image:url(' + product_image + ');  ">' +

                    ' <div class="small-box product-overlay"  >' +
                    '<div class="inner"></div>' +
                    '</div>' +
                    '</div>' +
                    ////////////////////////////////////////////////////

                    // ' <img class="align-self-center img-fluid rounded " src="' + product_image + '" > ' +


                    '</div>' + ' <hr/>' + '</div>'
                )
            }


            calculate_basket(id)

        }

        $(document).ready(function () {

            // $('#basket').hide()
            // Step show event
            $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {


                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
                if (stepPosition === 'first') {
                    $("#prev-btn").addClass('disabled');
                } else if (stepPosition === 'last') {
                    $("#next-btn").addClass('disabled');
                } else {
                    $("#prev-btn").removeClass('disabled');
                    $("#next-btn").removeClass('disabled');
                }


                invoice_page();
            });
            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'dots',
                transition: {
                    animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                },
                toolbarSettings: {
                    toolbarPosition: 'both', // both bottom
                    // toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

            $("#show-all").on("click", function () {
                $('.cat-hide').show()

            });
            $("#prev-btn").on("click", function () {
                // Navigate previous
                $('#smartwizard').smartWizard("prev");
                return true;
            });
            $("#next-btn").on("click", function () {
                // Navigate next

                $('#smartwizard').smartWizard("next");

                return true;
            });
            // Demo Button Events
            $("#got_to_step").on("change", function () {

                var step_index = $(this).val() - 1;
                $('#smartwizard').smartWizard("goToStep", step_index);
                return true;
            });
            $("#is_justified").on("click", function () {
                // Change Justify
                var options = {
                    justified: $(this).prop("checked")
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });
            $("#animation").on("change", function () {
                // Change theme
                var options = {
                    transition: {
                        animation: $(this).val()
                    },
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });
            $("#theme_selector").on("change", function () {
                // Change theme
                var options = {
                    theme: $(this).val()
                };
                $('#smartwizard').smartWizard("setOptions", options);
                return true;
            });

            {{--$.get("{{route('admin.orders.customer.phones')}}", function (data, status) {--}}
            {{--    for (var i = 0; i < data.data.length; i++) {--}}
            {{--        for (var j = 0; j < data.data[i].length; j++) {--}}

            {{--            --}}{{--if ({{$order['customer_phone']}} == data.data[i][j])--}}
            {{--            --}}{{--    $('#phone-customer').append(--}}
            {{--            --}}{{--        '<option value="' + data.data[i][j] + '" id="' + data.data[i][j] + '" selected>' + data.data[i][j] + '</option>'--}}
            {{--            --}}{{--    );--}}
            {{--            // else--}}
            {{--                $('#phone-customer').append(--}}
            {{--                '<option value="' + data.data[i][j] + '" id="' + data.data[i][j] + '">' + data.data[i][j] + '</option>'--}}
            {{--            );--}}
            {{--        }--}}


            {{--    }--}}
            {{--    $('#phone-customer').append(--}}
            {{--        '<option value="{{$order['customer_phone']}}" id="{{$order['customer_phone']}}" selected>{{$order['customer_phone']}}</option>'--}}
            {{--    );--}}
            {{--    $('#phone-customer option[value={{$order['customer_phone']}}]').attr('selected','selected');--}}
            {{--});--}}

            $.get("{{route('admin.cities.all_cities')}}", function (data, status) {
                for (var i = 0; i < data.data.length; i++) {
                    $('#city-address').append(
                        '<option value="' + data.data[i].id + '" id="' + data.data[i].id + '">' + data.data[i].title + '</option>'
                    );
                }
            });
            $('#phone-searchable').on('click', function (e) {
                e.stopPropagation();
                $('#phone-customer').show()
                // $('#phone-customer option').each(function () {
                //     if ($(this).is(':selected'))
                //
                // });
            });

            $('body').click(function () {
                $('#phone-customer').hide();

                if (!$('#phone-customer').is(':empty'))
                    $('#phone-searchable').val($('#phone-customer').val())


            });

            $('#phone-customer').on('change', function (e) {

                $('#phone-searchable').val($(this).val())
                $(this).hide()

                $('#addresses').empty()

                $.get('{{url("/admin/address-phone/")}}/' + $(this).val(), function (data, status) {

                    for (var i = 0; i < data.data.length; i++) {

                        $('#addresses').append(
                            '<div class="col-12 col-md-6 address-content" id="address' + data.data[i].id + i + '" ' +
                            'style="margin: 0 !important;' +
                            'border: 5px solid transparent;">' +
                            '<div class="card  card card-default">' +
                            '<div class="card-header">' +
                            '<div class="row">' +
                            '<div class="col-6">' +
                            '<h5 class="card-title"> Address ' + (i + 1) + '</h5>' +
                            '</div>' +
                            '<div class="col-6">' +
                            '<button type="button" class="btn btn-sm mx-1 float-right btn-secondary" onclick="select_address_box(' + data.data[i].id + i + ' ,' + data.data[i].city.options.no + ',' + data.data[i].id + ')">' +
                            'Select this address' +
                            '</button>' +
                            '</div>' +
                            '</div>' +
                            '</div> ' +
                            '<div class="card-body">' +
                            '<div class="row">' +
                            '<div class="col-md-8">' +
                            '<div class="form-group">' +
                            '<label>City</label>' +
                            '<input type="text" class="form-control city" value="' + data.data[i].city.title + '">' +
                            '</div>' +
                            '</div>' +

                            '<div class="col-md-4"><label>Delivery Price</label> ' +
                            '<div class="font-weight-bold">' + data.data[i].city.options.no + ' KWD</div>' +
                            '</div>' +

                            ' <div class="col-md-6">' +
                            '<div class="form-group"><label>Street</label> ' +
                            '<input type="text" class="form-control street" value="' + data.data[i].street + '"></div></div>' +
                            ' <div class="col-md-3"><div class="form-group"><label>Block</label> <input type="text" class="form-control block"  value="' + data.data[i].block + '"></div></div> ' +
                            '<div class="col-md-3 col-6"><div class="form-group"><label>House</label> <input type="text" class="form-control house" value="' + data.data[i].house + '"></div></div>' +
                            ' <div class="col-md-4 col-6"><div class="form-group"><label>Avenue</label> <input type="text" class="form-control avenue" value="' + data.data[i].avenue + '"></div></div>' +
                            ' <div class="col-md-4 col-6"><div class="form-group"><label>Flat</label> <input type="text" class="form-control flat" value="' + data.data[i].flat + '"></div></div>' +
                            ' <div class="col-md-4 col-6">' +
                            '<div class="form-group"><label>Floor</label> <input type="text" class="form-control floor"  value="' + data.data[i].floor + '></div></div>' +
                            '</div></div></div></div></div>'
                        );
                    }
                    $('.tab-content').css({'height': 'auto'})
                    invoice_page();
                });


            })
            getOrder();

            var typingTimer;                //timer identifier
            var doneTypingInterval = 500;  //time in ms, 5 seconds for example

            $('#phone-customer').append('<option value="{{$order['customer_phone']}}">{{$order['customer_phone']}}</option>');

            $('#phone-customer option[value={{$order['customer_phone']}}]').attr('selected', 'selected');
            $('#phone-searchable').val({{$order['customer_phone']}})
            $('#phone-customer').attr('size', 6)
            var $input = $('#phone-searchable');
            //on keyup, start the countdown
            $input.on('keyup', function () {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });

            //on keydown, clear the countdown
            $input.on('keydown', function () {
                clearTimeout(typingTimer);
            });


        });

        function doneTyping() {
            var value = $("#phone-searchable").val();
            $.post("{{route('customers.search.phones')}}", {
                query: value
            }).done(function (data) {
                var value_selected = $('#phone-customer').val()
                $('#phone-customer').empty()
                // $('#addresses').empty()
                $('#phone-customer').append('<option value="' + value_selected + '">' + value_selected + '</option>');
                $('#phone-customer option[value=' + value_selected + ']').attr('selected', 'selected');
                for (var i = 0; i < data.data.length; i++) {
                    if (value_selected !== data.data[i])
                        $('#phone-customer').append(
                            '<option value="' + data.data[i] + '">' + data.data[i] + '</option>'
                        );
                }
                if (data.data.length > 0) {
                    $('#phone-customer').attr('size', 6)
                    $('#phone-customer').show()
                } else {

                    $('#phone-customer').hide()
                    $('#phone-customer').empty()
                    $('#addresses').empty()
                }
            }).fail(function (error) {
                error_noti(error.responseJSON.message);
            });
        }

        function calculate_basket(id) {

            var qty = 0;
            var items = $("#basket").find(".basket-content").length;

            var price = 0;
            var prc = 0;
            var qt = 0;
            $("#basket").find('input:text,strong').each(function () {

                if ($(this).val() !== '') {
                    qt = Number($(this).val());
                    qty += qt;
                    price += qt * prc;

                }

                if ($(this).text() !== '')
                    prc = Number($(this).text());


            });
            $('#basket-total-qty').html(qty)
            $('#basket-total-items').html(items)
            if ($('#basket-total-delivery-price span').text() !== '' || $('#basket-total-delivery-price span').text() !== 'undefind')
                $('#basket-total-price').html('<span>' + (price + Number($('#basket-total-delivery-price span').text())) + '</span> KWD')
            else
                $('#basket-total-price').html('<span>' + price + '</span> KWD')

            invoice_page();

        }

        function select_address_box(id, price, address_id) {
            window.address_id = address_id;
            $('.address-content').removeClass('product-selected');

            $('#address' + id).addClass('product-selected')

            $('#basket-total-delivery-price').html('<span>' + price + '</span> KWD')

            calculate_basket(0);
        }

        function add_address() {
            if ($('#addresses-form').is(':visible'))
                $('#addresses-form').fadeOut()
            else
                $('#addresses-form').fadeIn()
            $('.tab-content').css({'height': 'auto'})

            invoice_page();
        }

        function submit_address() {
            if ($('#phone-searchable').val().length !== 8) {
                error_noti('رقم الهاتف يجب ان يكون 8 ارقام حصرا');
                return;
            }
            if ($('#phone-searchable').val() !== $('#phone-customer').val())
                $('#phone-customer').append('<option value="' + $('#phone-searchable').val() + '" selected>' + $('#phone-searchable').val() + '</option>')

            $.post("{{route('admin.address.store')}}", {
                customer_phone: $('#phone-customer').val(),
                city_id: $('#city-address').val(),
                floor: $('#address-floor').val(),
                flat: $('#address-flat').val(),
                street: $('#address-street').val(),
                block: $('#address-block').val(),
                house: $('#address-house').val(),
                notes: $('#address-note').val(),
                avenue: $('#address-avenue').val(),
            }).done(function (data) {

                $('#addresses').append(
                    '<div class="col-12 col-md-6 address-content" id="address' + data.data.id + '" ' +
                    'style="margin: 0 !important;' +
                    'border: 5px solid transparent;">' +
                    '<div class="card  card card-default">' +
                    '<div class="card-header">' +
                    '<div class="row">' +
                    '<div class="col-6">' +
                    '<h5 class="card-title"> Address ' + (1) + '</h5>' +
                    '</div>' +
                    '<div class="col-6">' +
                    '<button type="button" class="btn btn-sm mx-1 float-right btn-secondary" onclick="select_address_box(' + data.data.id + ',' + data.data.city.options.no + ')">' +
                    'Select this address' +
                    '</button>' +
                    '</div>' +
                    '</div>' +
                    '</div> ' +
                    '<div class="card-body">' +
                    '<div class="row">' +
                    '<div class="col-md-8">' +
                    '<div class="form-group">' +
                    '<label>City</label>' +
                    '<input type="text" class="form-control city" value="' + data.data.city.title + '">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-4"><label>Delivery Price</label> ' +
                    '<div class="font-weight-bold">' + data.data.city.options.no + ' KWD</div>' +
                    '</div>' +
                    ' <div class="col-md-6">' +
                    '<div class="form-group"><label>Street</label> ' +
                    '<input type="text" class="form-control street" value="' + data.data.street + '"></div></div>' +
                    ' <div class="col-md-3"><div class="form-group"><label>Block</label> <input type="text" class="form-control block"  value="' + data.data.block + '"></div></div> ' +
                    '<div class="col-md-3 col-6"><div class="form-group"><label>House</label> <input type="text" class="form-control house" value="' + data.data.house + '"></div></div>' +
                    ' <div class="col-md-4 col-6"><div class="form-group"><label>Avenue</label> <input type="text" class="form-control avenue" value="' + data.data.avenue + '"></div></div>' +
                    ' <div class="col-md-4 col-6"><div class="form-group"><label>Flat</label> <input type="text" class="form-control flat" value="' + data.data.flat + '"></div></div>' +
                    ' <div class="col-md-4 col-6">' +
                    '<div class="form-group"><label>Floor</label> <input type="text" class="form-control floor"  value="' + data.data.floor + '></div></div>' +
                    '</div></div></div></div>'
                );

                success_noti('تم إضافة العنوان');
                select_address_box(data.data.id, data.data.city.options.no)
            }).fail(function (error) {
                error_noti(error);
            });
        }


        function invoice_page() {
            window.array_ids_product = []
            window.array_qty_product = []

            $('#order-products').empty()
            $('#order-address-city').text($('#addresses .product-selected .city').val())
            $('#order-address-street').text($('#addresses .product-selected .street').val() + ',')
            $('#order-address-block').text($('#addresses .product-selected .block').val() + ',')
            $('#order-address-house').text($('#addresses .product-selected .house').val() + ',')
            $('#order-address-avenue').text($('#addresses .product-selected .avenue').val() + ',')
            $('#order-address-flat').text($('#addresses .product-selected .flat').val())
            $('#order-address-floor').text($('#addresses .product-selected .floor').val())
            $('#order-requested-date').text($('#date-time').val())
            $('#order-customer-name').text($('#name-customer').val())
            $('#order-customer-phone').text('(+965) ' + $('#phone-customer').val())
            $('#order-notice').text($('#order-note').val())
            $('#order-discount-percentage').text($('#discount-percentage').val())
            $('#order-discount').text($('#discount-fixed').val())
            $('#order-sum-discount').text(Number($('#order-discount').text()))
            $('#order-sum-discount-percentage').text((Number($('#basket-total-price span').text()) * Number($('#order-discount-percentage').text())) / 100)
            $('#order-delivery-rate').text($('#basket-total-delivery-price span').text())
            $('#order-grand-total').text(Number($('#order-subtotal').text()) - Number($('#order-sum-discount-percentage').text()) - Number($('#order-sum-discount').text()))
            $('#order-subtotal').text($('#basket-total-price span').text())

            $(".radio-payment").each(function (index) {

                if ($(this).is(':checked')) {

                    window.payment_id = Number($(this).val());

                    $('#order-payment-method').text($(this).next().text())
                }

            });

            $(".radio-gate").each(function (index) {
                if ($(this).is(':checked')) {

                    window.gate_id = Number($(this).val());

                    $('#order-gate').text($(this).next().text())
                }
            });

            $(".basket-content .flex-row").each(function (index) {
                window.array_ids_product.push($(this).attr('data-product-id'))

                $('#order-products').append(
                    '<tr>' +
                    `<td class="no">` +
                    `<span >#${index + 1}</span>` +
                    '</td>' +
                    // `<td class="">` +
                    // `<div style='height: 100px;width: 100px; background-repeat: no-repeat; background-position: center; background-size: cover;margin-bottom: 3px;background-image:${this.children[1].style["background-image"]}'></div>` +
                    // '</td>' +

                    '  <td class="text-left ">' +
                    ' <h3>' + $(this).text() + ' </h3>' +
                    '</td>' +
                    '<td class="unit"> ' + $(this.children[0]).find('strong').text() + ' KWD    </td>' +
                    '  <td >' + this.children[0].children[2].children[1].value + '</td>' +

                    '<td class="total">  ' + (this.children[0].children[2].children[1].value * $(this.children[0]).find('strong').text()) + ' KWD</td>' +
                    '</tr>'
                )
                $('.tab-content').css({'height': 'auto !important'})
            });
            $(".basket-content input").each(function (index) {
                window.array_qty_product.push($(this).val())

            });

        }

        function submitOrder(print = false) {

            if ($('#phone-searchable').val() !== $('#phone-customer').val())
                $('#phone-customer').append('<option value="' + $('#phone-searchable').val() + '" selected>' + $('#phone-searchable').val() + '</option>')

            if (window.payment_id === 0) {

                error_noti('الرجاء اختيار طريقة الدفع  ');
                return;
            }
            if (window.gate_id === 0) {
                error_noti('الرجاء اختيار بوابة الدفع  ');
                return;
            }
            if (window.array_ids_product.length === 0) {
                error_noti('الرجاء اختيار منتجات');
                return;
            }
            if ($('#date-time').val() === '') {
                error_noti('لم يتم اختيار تاريخ تسليم الطلب');
                return;
            }
            if ($('#phone-customer').val() === '') {
                error_noti('الرجاء اختيار رقم هاتف العميل ');
                return;
            }
            if (window.gate_id === 681)
                var payload = {
                    order_id: window.order_id,
                    products_ids: window.array_ids_product,
                    products_qty: window.array_qty_product,
                    requested_date: $('#date-time').val(),
                    customer_phone: $('#phone-customer').val(),
                    note: $('#order-note').val(),
                    name: $('#name-customer').val(),
                    discount_fixed: $('#discount-fixed').val(),
                    discount_percentage: $('#discount-percentage').val(),
                    gate_id: window.gate_id,
                    payment_method_id: window.payment_id,
                    printable: print ? 1 : 0
                }
            else var payload = {
                order_id: window.order_id,
                products_ids: window.array_ids_product,
                products_qty: window.array_qty_product,
                requested_date: $('#date-time').val(),
                customer_phone: $('#phone-customer').val(),
                city_id: $('#city-address').val(),
                address_id: window.address_id,
                note: $('#order-note').val(),
                name: $('#name-customer').val(),
                discount_fixed: $('#discount-fixed').val(),
                discount_percentage: $('#discount-percentage').val(),
                gate_id: window.gate_id,
                payment_method_id: window.payment_id,
                printable: print ? 1 : 0
            }


            $.post("{{route('admin.orders.update')}}", payload).done(function (response) {


                success_noti('تم تعديل الطلب');
                if (response.message === 'hasPdf') {
                    window.open(response.content, '_blank');
                }
            }).fail(function (error) {

                error_noti(error.responseJSON.message);
            });
        }

        function groupByCategory(id) {
            $('.cat-hide').hide()
            $('.tab-content').css({'height': 'auto'})
            $('#category' + id).show()
        }

        function select_product(id, product_id, product_title, product_image, price_unite, price, price_id, price_object) {
            if ($(`#product-seleccted${id}`).hasClass('product-selected')) {
                $(`#product-father-selected${product_id}`).removeClass('product-selected');
                $(`#product-seleccted${id}`).removeClass('product-selected');


                if ($(`#basket-product${id}`).length) {
                    $(`#basket-product${id}`).remove()
                }
                if ($('#tag-' + id).length)
                    $(`#tag-${id}`).remove()

                $('#basket .basket-content .flex-row').each(function (index) {

                    if (Number($(this).attr('data-product-id')) === price_object.product_id) {
                        $(`#product-father-selected${product_id}`).addClass('product-selected')
                    }
                })


            } else {
                $(`#product-seleccted${id}`).addClass('product-selected')
                $(`#product-father-selected${product_id}`).addClass('product-selected')

                $('#basket').fadeIn()
                if ($(`#basket-product${id}`).length) {
                    return;
                } else {
                    $('#product-tags').append(
                        '<div class="col">' +
                        ' <span  class="badge bg-primary  " id="tag-' + id + '"> ' + product_title + '  (<span  class="badge bg-black " id="tag-qty-' + id + '">1 </span> - ' + price_unite + ' ) <button   onclick="remove_item_from_basket(' + id + ',' + product_id + ',' + price_id + ')" class="badge btn-danger ml-2">X</button> </span>' +
                        ' </div>')

                    $('#basket').append(
                        '<div id="basket-product' + id + '"  class="basket-content" style="height:auto">' +
                        '<div class="d-flex flex-row " data-product-id="' + price_id + '">' +
                        ' <div class="flex-fill">' +
                        ' <h5 class="bold">' + product_title + ' (' + price_unite + ') ' +
                        '<button type="button" id="basket-remove-item' + id + '" class="btn btn-danger" onclick="remove_item_from_basket(' + id + ',' + product_id + ',' + price_id + ')"><i class="bx bxs-trash me-0" ></i>' +
                        ' </button>' +
                        '</h5>' +
                        ' <h5 class="mb-3">' +
                        '<strong id="price-item-basket' + id + '">' + price + '</strong> KWD' +
                        ' </h5>' +
                        /////
                        ' <div class="input-group mb-3" style="width:18%">' +
                        '<span class="input-group-btn">' +
                        '<button type="button" id="quantity-basket-left-minus' + id + '" class="quantity-left-minus  btn btn-danger btn-number" data-type="minus" data-field="" onclick="qty_minis(' + id + ',' + price + ')">' +
                        '   <span class="bx bx-minus"></span>' +
                        '  </button>' +
                        ' </span>' +
                        '  <input type="text" disabled id="quantity-basket' + id + '" name="quantity" class="form-control input-number text-lg-center" value="1" min="1" max="100">' +
                        ' <span class="input-group-btn">' +
                        ' <button type="button" id="quantity-basket-right-plus' + id + '" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="" onclick="qty_plus(' + id + ',' + price + ')">' +
                        '<span class="bx bx-plus"></span>' +
                        '  </button>' +
                        ' </span>' +
                        '  </div>' +
                        /////
                        ' </div>' +
                        /////////////////////////////////

                        '<div class="col-3  product-box" style=" background-image:url(' + product_image + ');  ">' +

                        ' <div class="small-box product-overlay"  >' +
                        '<div class="inner"></div>' +
                        '</div>' +
                        '</div>' +
                        ////////////////////////////////////////////////////

                        // ' <img class="align-self-center img-fluid rounded " src="' + product_image + '" > ' +


                        '</div>' + ' <hr/>' + '</div>'
                    )
                }


            }

            calculate_basket(id)

        }

        function qty_plus(id, price) {

            quantity_basket = parseInt($('#quantity-basket' + id).val());
            // If is not undefined

            $('#quantity-basket' + id).val(quantity_basket + 1);

            $('#quantity' + id).val(quantity_basket + 1);
            $('#price-item-basket' + id).text((quantity_basket + 1) * Number(price));

            calculate_basket(id)
        }

        function qty_minis(id, price) {
            quantity_basket = parseInt($('#quantity-basket' + id).val());

            if (quantity_basket > 1) {
                $('#quantity-basket' + id).val(quantity_basket - 1);
                $('#price-item-basket' + id).text((quantity_basket - 1) * Number(price));
                $('#quantity' + id).val(quantity_basket - 1);
            }
            calculate_basket(id)
        }

        function remove_item_from_basket(id, product_id, price_id) {
            $('#product-seleccted' + id).removeClass('product-selected');
            $(`#product-father-selected${product_id}`).removeClass('product-selected');
            $('#basket-product' + id).remove()
            $('#tag-' + id).remove()
            calculate_basket(id)
        }


    </script>
@endpush
