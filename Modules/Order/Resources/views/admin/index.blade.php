@extends('layouts.dashboard.app')
@section('title')
    Orders
@endsection
@push('css')
    <style>
        .inputGroup label {
            border-radius: 10px;
            height: auto !important;
            line-height: inherit !important;
            font-weight: inherit !important;
            padding: 12px 30px;
            width: 100%;
            display: block;
            text-align: left;
            color: #3c454c;
            cursor: pointer;
            position: relative;
            z-index: 2;
            -webkit-transition: color 200ms ease-in;
            transition: color 200ms ease-in;
            overflow: hidden;
        }

        .inputGroup input {
            left: auto !important;
            opacity: 1 !important;
            width: 32px;
            height: 32px;
            -webkit-box-ordinal-group: 2;
            order: 1;
            z-index: 2;
            position: absolute;
            right: 30px;
            top: 50%;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            cursor: pointer;
            visibility: hidden;
        }
    </style>
@endpush
@section('content')
    <div class=" page-wrapper ">
        <div class="card-body">
            <h5 class="card-title">Orders</h5>
            <hr>

            <div class="row">
                @include('order::admin.partials.form_filter')
            </div>
            <hr/>
            <div class="row" id="result-filter">

                @include('order::admin.partials.orders')
            </div>

        </div>
    </div>

@endsection
@push('js')
    <script>
        $(function () {

            $('#phone-customer').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
                ajax: {
                    url: '{{ route('customers.search.phones-get') }}',
                    delay: 250,
                    allowClear: true,

                    data: function (params) {
                        var search = {
                            query: params.term,
                            page: params.page || 1
                        }


                        return search;
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.data.map(function (item) {
                                return {
                                    "id": item,
                                    "text": item
                                }
                            }),
                            pagination: {
                                more: true
                            }
                        };
                    },
                    cache: true

                },
                transport: function (params, success, failure) {
                    var $request = $.ajax(params);

                    $request.then(success);
                    $request.fail(failure);

                    return $request;
                },
                templateResult: formatRepo,

            });
        })


        function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }
            if ($('#phone-customer').val() === repo.text)
                $('#phone-customer').val(null).trigger('change');

            var $container = $(
                "<div class='select2-result-repository clearfix'>" +

                "<div class='select2-result-repository__forks'> </div>" +

                "</div>"
            );
            $container.find(".select2-result-repository__forks").append(repo.id );

            return $container;
        }
        function whatsAppContact(phone) {

            window.location = 'https://api.whatsapp.com/send?phone=965' + phone;
        }

        function filter(e) {
            e.preventDefault()
            $.post("{{route('admin.orders.filter')}}", $('#form-filter-data').serialize()).done(function (data) {

                $('#result-filter').empty();
                $('#result-filter').append(data);

            });
        }

        function setPaymentMethod(id) {
            $.post("{{route('admin.orders.changePaymentMethod')}}", {
                order_id: id,
            }).done(function (data) {
                success_noti('تم التعديل على طريقة الدفع');
                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);

            });
        }

        function canceled(id, status, message, rason) {


            $.post("{{route('admin.orders.cancel')}}", {
                order_id: id,
                status: status,
                message: rason,
            }, function (data) {
                success_noti(message);

                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)

                $('#btns-status-ep' + id).empty()
                $('#btns-status' + id).empty()
                $('#bar-status' + id).empty()


            }).fail(function (error) {
                error_noti(error);
            });
        }

        function receipt(id, status, message, receipt_no) {

            $.post("{{route('admin.orders.receipt_no')}}", {
                order_id: id,
                status: status,
                receipt_no: receipt_no,
            }, function (data) {
                success_noti(message);

                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)
                $('#proved' + id).hide()
                $('#cancel' + id).hide()
                $('#edit' + id).hide()
                $('#canceled-modal' + id).hide()
                $('#nav-link-proved' + id).addClass(' active ')
                $('#nav-link-proved' + id).removeClass('inactive')
            }).fail(function (error) {
                error_noti(error);
            });
        }

        function delivered(id, status, message, proveds_status) {

            $.post("{{route('admin.orders.delivery')}}", {
                order_id: id,
                status: status
            }).done(function (data) {
                success_noti(message);
                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)
                $('#delivered' + id).hide()
                $('#btns-status' + id).append(
                    '   <button type="button"' +
                    ' data-bs-toggle="modal"' +
                    ' data-bs-target="#receiptVerticallycenteredModal' + id + '"' +
                    ' class="btn btn-info "' +
                    ' id="receipt-modal' + id + '">' +
                    '  Receipt no' +
                    ' </button>' +
                    '<div class="border-start h-25"></div>' +
                    '   <button type="button" class="btn btn-info " id="proved' + id + '">' +
                    '  Proved' +
                    ' </button>' +
                    '<div class="border-start h-25"></div>'
                );
                $('#proved' + id).on('click', function () {
                    proved(id, proveds_status, 'تمت العملية بنجاح')

                });

                $('#nav-link-delivered' + id).addClass(' active ')
                $('#nav-link-delivered' + id).removeClass('inactive')
                $('#nav-link-underway' + id).removeClass(' inactive ')
                $('#nav-link-underway' + id).addClass(' active ')

            });
        }
        function newAndUpdate(id, status, message) {

            $.post("{{route('admin.orders.new_update')}}", {
                order_id: id,
                status: status
            }).done(function (data) {
                success_noti(message);
                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)
                $('#confirm' + id).hide()
                $('#btns-status' + id).append(
                    '   <button type="button"' +
                    ' data-bs-toggle="modal"' +
                    ' data-bs-target="#receiptVerticallycenteredModal' + id + '"' +
                    ' class="btn btn-info "' +
                    ' id="receipt-modal' + id + '">' +
                    '  Receipt no' +
                    ' </button>' +
                    '<div class="border-start h-25"></div>' +
                    '   <button type="button" class="btn btn-info " id="proved' + id + '">' +
                    '  Proved' +
                    ' </button>' +
                    '<div class="border-start h-25"></div>'
                );
                // $('#confirm' + id).on('click', function () {
                //     proved(id, proveds_status, 'تمت العملية بنجاح')
                //
                // });
                $('#nav-link-new-update' + id).removeClass('inactive')
                $('#nav-link-underway' + id).removeClass(' inactive ')
                $('#nav-link-new-update' + id).addClass(' active ')
                $('#nav-link-underway' + id).addClass(' active ')

            });

        }
        function proved(id, status, message) {

            $.post("{{route('admin.orders.proved')}}", {
                order_id: id,
                status: status
            }).done(function (data) {
                success_noti(message);
                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)
                $('#proved' + id).hide()
                $('#cancel' + id).hide()
                $('#edit' + id).hide()
                $('#canceled-modal' + id).hide()
                $('#nav-link-proved' + id).addClass(' active ')
                $('#nav-link-proved' + id).removeClass('inactive')


            });
        }

        function show(id) {

            $('#exampleSummary' + id + 'body').empty()
            $('#exampleSummary' + id + 'body').html(
                '<div class="spinner-border " role="status"> <span class="visually-hidden text-center">Loading...</span></div>' +
                'plaese waite .......'
            )

            $.get('{{url('/admin/orders/show/')}}/' + id,
                function (data, status) {
                    $('#exampleSummary' + id + 'body').html(data)

                });


        }

        function edit(id) {

            window.location = '{{url('/admin/orders/edit/')}}/' + id + '#step-4';

        }

        function sms(id, message) {

            $.post("{{route('admin.orders.sms')}}", {
                order_id: id,

            }).done(function (data) {
                success_noti(message);
            });
        }

    </script>
@endpush

