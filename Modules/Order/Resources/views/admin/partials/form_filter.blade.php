<div class="accordion" id="form-filter">
    <div class=" accordion-item">
        <h2 class="accordion-header" id="headingOne">

            <div class="accordion-button collapsed" data-bs-toggle="collapse"
                 data-bs-target="#collapseOne" aria-expanded="false"
                 aria-controls="collapseOne">
                <div class="container-fluid">
                    <div class="row text-muted">

                        <div class="col-4 ">
                            <h3>Filter</h3>
                        </div>

                    </div>
                </div>
            </div>
        </h2>
        <div id="collapseOne" class="accordion-collapse collapse"
             aria-labelledby="headingOne"
             data-bs-parent="#form-filter" style="">
            <div class="accordion-body">
                <div class="card p-3 ">
                    <form class="form-group" autocomplete="off" method="POST" id="form-filter-data">

                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <h5>From</h5>
                                <input id="date_from" type="text"
                                       class="form-control date-datepicker"
                                       name="startDate"
                                       value="{{old('startDate',now()->toDateString())}}">
                            </div>

                            <div class="col-md-4 ">
                                <h5>To</h5>
                                <input id="date_to" type="text" name="endDate"
                                       class="form-control date-datepicker"
                                       value="{{old('endDate',now()->toDateString())}}">
                            </div>

                            <div class="col-md-4 col-sm-12">
                                <h5 >Search on Phone</h5>
{{--                                <input class="form-control" id="phone-searchable"--}}
{{--                                       type="number"--}}
{{--                                       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"--}}
{{--                                       maxlength="8"--}}
{{--                                       minlength="8"--}}
{{--                                       min="0"--}}
{{--                                       autocomplete="off"--}}
{{--                                       name="phone_number"--}}
{{--                                       placeholder="phone number">--}}

                                <select id="phone-customer"
                                        name="phone_number">

                                </select>
{{--                                <select class="form-control" id="phone-customer"  style="display: none" >--}}

{{--                                </select>--}}


                            </div>
                        </div>

                        <br/>
                        <div class="row">
                            <h5>Status</h5>
                            <br/>
                            <br/>
                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesNew" type="checkbox"
                                           class="material"
                                           value="0"
                                           checked>
                                    <label for="statusesNew" class="material"> New</label>
                                </div>

                            </div>
                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesUpdated"
                                           type="checkbox"
                                           class="material"
                                           checked
                                           value="3">
                                    <label for="statusesUpdated" class="material">
                                        Updated</label>
                                </div>

                            </div>
                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesDelivered"
                                           type="checkbox"
                                           class="material"
                                           checked
                                           value="1">
                                    <label for="statusesDelivered" class="material">
                                        Delivered</label>
                                </div>

                            </div>

                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesProved" type="checkbox"
                                           class="material"
                                           checked
                                           value="2">
                                    <label for="statusesProved" class="material"> Proved</label>
                                </div>

                            </div>
                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesUnderway" type="checkbox"
                                           class="material"
                                           checked
                                           value="6">
                                    <label for="statusesUnderway" class="material"> Underway</label>
                                </div>

                            </div>
                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesPending" type="checkbox"
                                           class="material"

                                           value="5">
                                    <label for="statusesPending" class="material"> Pending</label>
                                </div>

                            </div>

                            <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                <div class="checked">
                                    <input name="statuses[]" id="statusesCanceled"
                                           type="checkbox"
                                           class="material"

                                           value="4">
                                    <label for="statusesCanceled" class="material">
                                        Canceled</label>
                                </div>

                            </div>
{{--                            @for ($i = 0; $i < count(\Modules\Order\Entities\Order::STATUSES); $i++)--}}

{{--                                <div class="inputGroup col-md-2 col-sm-2 col-xs-2">--}}
{{--                                    <div class="checked">--}}
{{--                                        <input class="form-check-input" name="statuses[]" type="checkbox"--}}
{{--                                               id="{{\Modules\Order\Entities\Order::STATUSES[$i]['name']}}"--}}
{{--                                               value="{{$i}}">--}}
{{--                                        <label class="form-check-label "--}}
{{--                                               for="{{\Modules\Order\Entities\Order::STATUSES[$i]['name']}}">{{\Modules\Order\Entities\Order::STATUSES[$i]['name']}}</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endfor--}}
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col">
                                <h5>Payment Methods</h5>
                                <br/>
                                <br/>
                                @for ($i = 0; $i < count($paymentMethods); $i++)
                                    <div class="col">
                                    <div class="inputGroup col-md-4 col-sm-4 col-xs-4">
                                        <div class="checked">
                                            <input name="payments[]" id="payment{{$paymentMethods[$i]['title']}}"
                                                   type="checkbox"
                                                   class="material"
                                                   checked
                                                   value="{{$paymentMethods[$i]['id']}}">
                                            <label for="payment{{$paymentMethods[$i]['title']}}" class="material"> {{$paymentMethods[$i]['title']}}</label>
                                        </div>

                                    </div>
                                    </div>
{{--                                    <div class="col">--}}
{{--                                        <div class="form-check">--}}
{{--                                            <input class="form-check-input" type="checkbox"--}}
{{--                                                   name="payments[]"--}}
{{--                                                   id="payment{{$paymentMethods[$i]['title']}}"--}}
{{--                                                   value="{{$paymentMethods[$i]['id']}}">--}}
{{--                                            <label class="form-check-label"--}}
{{--                                                   for="payment{{$paymentMethods[$i]['title']}}">{{$paymentMethods[$i]['title']}}</label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                @endfor
                            </div>
                            <div class="col">
                                <h5>Gates</h5>
                                <br/>
                                <br/>
                                @for ($i = 0; $i < count($gates); $i++)
                                    <div class="col">
                                        <div class="inputGroup col-md-5 col-sm-5 col-xs-5">
                                            <div class="checked">
                                                <input name="gates[]" id="gate{{$gates[$i]['title']}}"
                                                       type="checkbox"
                                                       class="material"
                                                       checked
                                                       value="{{$gates[$i]['id']}}">
                                                <label for="gate{{$gates[$i]['title']}}" class="material"> {{$gates[$i]['title']}}</label>
                                            </div>

                                        </div>
                                    </div>
{{--                                    <div class="col">--}}
{{--                                        <div class="form-check">--}}
{{--                                            <input class="form-check-input" type="checkbox"--}}
{{--                                                   name="gates[]"--}}
{{--                                                   id="gate{{$gates[$i]['title']}}"--}}
{{--                                                   value="{{$gates[$i]['id']}}">--}}
{{--                                            <label class="form-check-label"--}}
{{--                                                   for="gate{{$gates[$i]['title']}}">{{$gates[$i]['title']}}</label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                @endfor
                            </div>
                        </div>

                        <hr/>
                        <div class="col-4 ">
                            <br/>
                            <button class="btn btn-primary mt-2 form-control" id="submit-form-filter" onclick="filter(event)">
                                Submit
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
