@push('css')
    <style>
        @media screen and (max-width: 640px) {
            .sw>.nav {

                -webkit-box-orient: horizontal !important;
                -webkit-box-direction: normal !important;
                flex-direction: row !important;
                -webkit-box-flex: 1;
                flex: 1 auto;
            }
        }

    </style>
@endpush

<div class="mb-2 col-md-6 ">
    <div class="col-md-12 accordion" id="accordionExample{{ $order->id }}">
        <div class=" accordion-item">
            <h2 class="accordion-header" id="headingOne{{ $order->id }}">

                <div class="accordion-button collapsed" data-bs-toggle="collapse"
                    data-bs-target="#collapseOne{{ $order->id }}" aria-expanded="false"
                    aria-controls="collapseOne{{ $order->id }}">
                    <div class="container-fluid">
                        <div class="row text-muted">
                            <div class="col-4 ">

                                <i class="lni lni-phone"></i>
                                <a class="" id="{{ $order->customer->phone }}"
                                    onclick="whatsAppContact('{{ $order->customer->phone }}')"
                                    href="https://api.whatsapp.com/send?phone=965{{ $order->customer->phone }}"
                                    title="965{{ $order->customer->phone }}"> {{ $order->customer->phone }}</a>
                            </div>
                            <div class="col-4 ">
                                <i class="lni lni-map-marker"></i>
                                {{ isset($order->address) ? $order->address->city->title : '' }}
                            </div>
                            <div class="col-2 "></div>
                            <div class="col-2 ">
                                <span class="pr-2"><i class="fa fa-clock"></i></span>
                                <span class="badge {{ $order->status_class }}"
                                    id="status-title{{ $order->id }}">{{ $order->status_name }}</span>
                                @if ($order->gate->id == 680)
                                    <p class="text-primary">
                                        {{$order->gate->title}}
                                    </p>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </h2>
            <div id="collapseOne{{ $order->id }}" class="accordion-collapse collapse"
                aria-labelledby="headingOne{{ $order->id }}" data-bs-parent="#accordionExample{{ $order->id }}"
                style="">
                <div class="accordion-body">


                    <div class="row d-flex justify-content-center align-items-center">

                        <div class=" card-stepper">
                            <div class=" p-1">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div>
                                        <p class="text-muted mb-2"> Order ID <span
                                                class="fw-bold text-body">#{{ $order->id }}</span>
                                        </p>
                                        <p class="text-muted mb-0"> Place On <span
                                                class="fw-bold text-body">{{ $order->created_at->toDateString() }}
                                                -{{ date('h:i A', strtotime($order->created_at)) }}</span>
                                        </p>
                                    </div>
                                    <div>
                                        <h3 class="mb-0"><a
                                                href="#">{{ number_format($order->total_price, 1) }}
                                                KWD</a></h3>
                                    </div>
                                </div>
                            </div>
                            <div class=" p-4">
                                @foreach ($order->products as $product)
                                    <div class="d-flex flex-row mb-4 pb-2">
                                        <div class="flex-fill">
                                            <h5 class="bold">{{ $product->product->title }}</h5>
                                            <p class="text-muted"> QTY: {{ $product->quantity }}
                                                ({{ !is_null($product->priceList) ? $product->priceList->unite_name : '' }})
                                            </p>
                                            <h4 class="mb-3">
                                                KWD {{ number_format($product->price, 1) }}

                                            </h4>

                                        </div>
                                        <div>
                                            <img class="align-self-center img-fluid"
                                                src="{{ $product->product->getFirstMediaUrl('images', 'thumbnail') }} "
                                                width="150">
                                        </div>
                                    </div>
                                    <hr />
                                @endforeach

                                <div id="bar-status{{ $order->id }}">

                                    @if ($order->status !== \Modules\Order\Entities\Order::STATUS_NAME['canceled'])
                                        <p class=" text-black">
                                            Delivery Price:
                                            <span class="text-body">
                                                <strong>
                                                    {{ $order->delivery_price . ' ' }} KWD
                                                </strong>
                                            </span>
                                        </p>
                                        <p class="text-muted">
                                            Delivering At:
                                            <b class="text-body " style="font-size: 20px">
                                                {{ $order->requested_time->toDateString() }} -
                                                {{ date('h:i A', strtotime($order->requested_time)) }}
                                            </b>
                                        </p>

                                        <div id="smartwizard" class="sw sw-theme-dots sw-justified">
                                            <ul id="stepper-nav" class="nav">

                                                <li class="nav-item">
                                                    <a id="nav-link-underway{{ $order->id }}"
                                                        class="nav-link inactive {{ $order->status == \Modules\Order\Entities\Order::STATUS_NAME['updated'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['new'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['underway'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['proved']? 'active': '' }} "
                                                        href="#step-1">
                                                        <strong>Underway</strong>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link inactive {{ $order->status == \Modules\Order\Entities\Order::STATUS_NAME['new'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['updated'] ||($order->status == \Modules\Order\Entities\Order::STATUS_NAME['underway'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['proved'])? 'active': '' }} "
                                                        href="#step-2">
                                                        <strong>New&Updated</strong>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a id="nav-link-delivered{{ $order->id }}"
                                                        class="nav-link inactive {{ $order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'] ||$order->status == \Modules\Order\Entities\Order::STATUS_NAME['proved']? 'active': '' }}"
                                                        href="#step-3">
                                                        <strong>Delivered</strong>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a id="nav-link-proved{{ $order->id }}"
                                                        class="nav-link inactive  {{ $order->status == \Modules\Order\Entities\Order::STATUS_NAME['proved'] ? 'active' : '' }}"
                                                        onclick="proved({{ $order->id }}, '{{ $order->statuses['proved'] }}', 'تمت العملية بنجاح')"
                                                        href="#step-4">
                                                        <strong>Proved</strong>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    @endif
                                </div>

                                <div class=" p-4">
                                    @if ($order->status== \Modules\Order\Entities\Order::STATUS_NAME['pending'])
                                    <div class="d-flex justify-content-between"
                                         id="btns-status{{$order->id}}">


                                            <button type="button" class="btn btn-success me-0"
                                                    id="confirm{{$order->id}}"
                                                    onclick=" newAndUpdate({{$order->id}}, '{{$order->statuses['new'] }}', 'تم تأكيد  الطلب', '{{$order->statuses['proved'] }}')">
                                                Confirm
                                            </button>
                                            <div class="border-start h-25"></div>



                                        @if (auth()->user()->hasPermission('orders-delete'))
                                            <button type="button" class="btn btn-danger"
                                                    id="canceled-modal{{ $order->id }}"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#exampleVerticallycenteredModal{{ $order->id }}">
                                                Canceled
                                            </button>

                                            <div class="border-start h-25"></div>
                                        @endif


                                    </div>
                                    @else
                                    <div class="d-flex justify-content-around">
                                        <button type="button" id="sms{{ $order->id }}" class="btn badge bg-warning "
                                            onclick="sms({{ $order->id }}, 'تم ارسال الفاتورة للعميل بنجاح')"
                                            style="font-size: 13px">
                                            Sms
                                        </button>
                                        <div class="border-start h-100"></div>
                                        <button type="button" class="btn badge bg-primary "
                                                data-bs-target="#exampleSummary{{ $order->id }}"
                                                data-bs-toggle="modal"
                                                onclick="show({{ $order->id }})"
                                                style="font-size: 13px">
                                            Summary
                                        </button>
                                        <div class="border-start h-100"></div>
                                        <span id="btns-status-ep{{ $order->id }}">
                                            @if ($order->status == \Modules\Order\Entities\Order::STATUS_NAME['new'] || $order->status == \Modules\Order\Entities\Order::STATUS_NAME['updated'])
                                            @if (auth()->user()->hasPermission('orders-update'))
                                                <button type="button" id="edit{{ $order->id }}"
                                                    class="btn badge bg-info " style="font-size: 13px"
                                                    onclick="edit('{{ $order->id }}')">
                                                    Edit
                                                </button>
                                                @endif
                                                <div class="border-start h-25"></div>
                                            @endif
                                            @if ($order->status !== \Modules\Order\Entities\Order::STATUS_NAME['canceled'] && $order->status !== \Modules\Order\Entities\Order::STATUS_NAME['pending'] && $order->status !== \Modules\Order\Entities\Order::STATUS_NAME['underway'])
                                                {{-- <button type="button" --}}
                                                {{-- class="btn badge bg-success " --}}
                                                {{-- style="font-size: 13px"> --}}
                                                {{-- Transaction --}}
                                                {{-- </button> --}}
                                                {{-- <div class="border-start h-25"></div> --}}
                                                <button type="button" class="btn badge bg-info"
                                                    id="payment-method-{{ $order->id }}"
                                                    data-id="{{ $order->id }}"
                                                    onclick="setPaymentMethod({{ $order->id }})"
                                                    style="font-size: 13px">
                                                    Set Payment Method to
                                                    {{ $order->payment_method_id === 120
                                                        ? collect($paymentMethods)->first(function ($q) {
                                                            return $q->id == 119;
                                                        })->title
                                                        : collect($paymentMethods)->first(function ($q) {
                                                            return $q->id == 120;
                                                        })->title }}
                                                </button>
                                            @endif

                                        </span>
                                    </div>
                                    <hr />
                                    <div class="d-flex justify-content-between" id="btns-status{{ $order->id }}">
                                        @if ($order->status == \Modules\Order\Entities\Order::STATUS_NAME['new'] || $order->status == \Modules\Order\Entities\Order::STATUS_NAME['updated'])
                                            <button type="button" class="btn btn-success me-0"
                                                id="delivered{{ $order->id }}"
                                                onclick=" delivered({{ $order->id }}, '{{ $order->statuses['delivered'] }}', 'تم تسليم الطلب', '{{ $order->statuses['proved'] }}')">
                                                Delivered
                                            </button>
                                            <div class="border-start h-25"></div>
                                        @endif
                                        @if ($order->status == \Modules\Order\Entities\Order::STATUS_NAME['new'] || $order->status == \Modules\Order\Entities\Order::STATUS_NAME['updated'] || $order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'])
                                        @if (auth()->user()->hasPermission('orders-delete'))
                                            <button type="button" class="btn btn-danger"
                                                id="canceled-modal{{ $order->id }}" data-bs-toggle="modal"
                                                data-bs-target="#exampleVerticallycenteredModal{{ $order->id }}">
                                                Canceled
                                            </button>
                                            @endif
                                            <div class="border-start h-25"></div>
                                        @endif
                                        @if ($order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'] || $order->status == \Modules\Order\Entities\Order::STATUS_NAME['proved'])
                                            <button type="button" class="btn btn-info " data-bs-toggle="modal"
                                                data-bs-target="#receiptVerticallycenteredModal{{ $order->id }}"
                                                id="receipt-modal{{ $order->id }}">
                                                Receipt no
                                            </button>
                                            <div class="border-start h-25"></div>
                                        @endif
                                        @if ($order->status == \Modules\Order\Entities\Order::STATUS_NAME['delivered'])
                                            <button type="button" class="btn btn-info "
                                                id="proved{{ $order->id }}">
                                                Proved
                                            </button>
                                            <div class="border-start h-25"></div>
                                        @endif


                                    </div>
                                    @endif
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleVerticallycenteredModal{{ $order->id }}" tabindex="-1" style="display: none;"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    سبب الحذف
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @foreach ($canceledDues as $index => $reason)
                    <div class="form-check">
                        <input name="reason" id="reason{{ $index }}{{ $order->id }}" type="radio"
                            onchange=" $('#message_reason{{ $order->id }}').val('{{ $reason }}');"
                            class="form-check-input" value="{{ $reason }}">
                        <label for="reason{{ $index }}{{ $order->id }}" class="form-check-label">

                            <strong>
                                {{ $reason }}
                            </strong>
                        </label>

                    </div>
                @endforeach
                <div class="form-check">
                    <input name="reason" id="other_reason{{ $order->id }}" type="radio"
                        onchange="$('#message_reason{{ $order->id }}').val('')" class="form-check-input" checked
                        value="other">
                    <label for="other_reason{{ $order->id }}" class="form-check-label">

                        <strong>
                            Other
                        </strong>
                    </label>

                </div>
            </div>
            <div class="form-group m-2">

                <input name="message" id="message_reason{{ $order->id }}" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-danger" id="cancel{{ $order->id }}"
                    onclick="canceled({{ $order->id }}, '{{ $order->statuses['canceled'] }}', 'تم حذف الطلب', $('#message_reason{{ $order->id }}').val())">
                    إلغاء الطلب
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="receiptVerticallycenteredModal{{ $order->id }}" tabindex="-1" style="display: none;"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Receipt No
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group m-2">
                    <input name="receipt_no" id="receipt_no{{ $order->id }}" class="form-control">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-success" id="receipt{{ $order->id }}"
                    onclick="receipt({{ $order->id }}, '{{ $order->statuses['proved'] }}', 'تمت العملية بنجاح', $('#receipt_no{{ $order->id }}').val())">
                    حفظ
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleSummary{{ $order->id }}" tabindex="-1" style="display: none;"
     aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="exampleSummary{{ $order->id }}body">


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Close
                </button>

            </div>
        </div>
    </div>
</div>
