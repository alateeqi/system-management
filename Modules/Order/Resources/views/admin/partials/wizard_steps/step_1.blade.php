<a href="#" class="mb-2 mr-1 p-2 badge  bg-secondary"
   id="show-all">
    Show All
</a>
@foreach($categories as $category)

    <a href="#" class="mb-2 mr-1 p-2 badge  bg-secondary"
       id="show-category{{$category->id}}" onclick="groupByCategory({{$category->id}})">
        {{$category->title}}
    </a>
@endforeach
<hr/>


@foreach($categories as $category)

    <div id="category{{$category->id}}" class="cat-hide">
        <div class="card  p-3">
            {{$category->title}}
            <hr/>
            <div class="container-fluid ">
                <div class="row ">
                    @foreach ($category->products as $index => $product)

                        <div class="modal fade" id="productsModal{{$product->id}}" tabindex="-1"
                             aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">{{$product->title}}</h5>
                                        <button type="button" class="btn btn-danger " data-bs-dismiss="modal" aria-label="Close">X</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            @foreach($product->prices as $price)
                                                <div class="col-lg-6 col-md-6 col-6 col-xs-6 border p-2 "
                                                     id="product-box-selected{{$product->id}}{{$price->id}}">
                                                    <div class="product-box"
                                                         id="product-box-{{$product->id}}{{$price->id}}"
                                                         style="background-image: url('{{$product->getFirstMediaUrl('images', 'thumbnail')}}');">
                                                        <div
                                                            id="product-seleccted{{$product->id}}{{$price->id}}"
                                                            class="small-box product-overlay"
                                                            onclick="select_product({{$product->id}}{{$price->id}},{{$product->id}},'{{$product->title}}','{{$product->getFirstMediaUrl('images', 'thumbnail')}}','{{$price->unite_name}}',{{$price->price}},{{$price->id}},{{json_encode($price)}})">
                                                            <div class="inner">
                                                                <h6 class="product-title  text-white">
                                                                    {{$product->title}}
                                                                </h6>
                                                                <h5 class="product-title  text-white">

                                                                    ({{$price->unite_name}})
                                                                </h5>
                                                                <br/>
                                                                <br/>
                                                                ({{$price->price}}) KWD
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-group input-spinner">
                                                        <button class="btn btn-success" type="button"  onclick="qty_plus({{$product->id}}{{$price->id}},{{$price->price}})" id="quantity-right-plus{{$product->id}}{{$price->id}}"> + </button>
                                                        <input type="text" class="form-control text-center"  disabled id="quantity{{$product->id}}{{$price->id}}" value="1">
                                                        <button class="btn btn-danger"  onclick="qty_minis({{$product->id}}{{$price->id}},{{$price->price}})" type="button" id="quantity-left-minus{{$product->id}}{{$price->id}}"> − </button>
                                                    </div>


                                                </div>
                                            @endforeach
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div data-bs-toggle="modal"
                             class="col-lg-3 col-md-3 col-6 col-xs-6"
                             data-bs-target="#productsModal{{$product->id}}">
                            <div class="product-box"
                                 style="background-image: url('{{$product->getFirstMediaUrl('images', 'thumbnail')}}');">
                                <div class="small-box product-overlay" id="product-father-selected{{$product->id}}">
                                    <div class="inner">
                                        <h4 class="product-title  text-white">
                                            {{$product->title}}
                                        </h4>
                                    </div>
                                </div>
                            </div>

                        </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endforeach
{{--                        @push('js')--}}
{{--                            <script>--}}

{{--                                var quantity_basket = 1;--}}
{{--                                $('#product-seleccted{{$product->id}}{{$price->id}}').on('click', function (e) {--}}

{{--                                    e.preventDefault();--}}
{{--                                    if ($('#product-seleccted{{$product->id}}{{$price->id}}').hasClass('product-selected')) {--}}
{{--                                        $('#product-seleccted{{$product->id}}{{$price->id}}').removeClass('product-selected');--}}
{{--                                        if ($('#basket-product{{$product->id}}{{$price->id}}').length) {--}}
{{--                                            $('#basket-product{{$product->id}}{{$price->id}}').remove()--}}
{{--                                        }--}}

{{--                                    }--}}
{{--                                    else {--}}
{{--                                        $('#product-seleccted{{$product->id}}{{$price->id}}').addClass('product-selected')--}}

{{--                                        $('#basket').fadeIn()--}}
{{--                                        if ($('#basket-product{{$product->id}}{{$price->id}}').length) {--}}
{{--                                            return;--}}
{{--                                        } else--}}
{{--                                            $('#basket').append(--}}
{{--                                                '<div id="basket-product{{$product->id}}{{$price->id}}"  class="basket-content" style="height:auto">' +--}}
{{--                                                '<div class="d-flex flex-row  " data-product-id="{{$product->id}}">' +--}}
{{--                                                ' <div class="flex-fill">' +--}}
{{--                                                ' <h5 class="bold">{{$product->title}} ({{$price->unite_name}}) ' +--}}
{{--                                                '<button type="button" id="basket-remove-item{{$product->id}}{{$price->id}}" class="btn btn-danger"><i class="bx bxs-trash me-0"></i>' +--}}
{{--                                                ' </button>' +--}}
{{--                                                '</h5>' +--}}
{{--                                                ' <h5 class="mb-3">' +--}}
{{--                                                '<strong id="price-item-basket{{$product->id}}{{$price->id}}">{{$price->price }}</strong> KWD' +--}}
{{--                                                ' </h5>' +--}}
{{--                                                /////--}}
{{--                                                ' <div class="input-group mb-3" style="width:18%">' +--}}
{{--                                                '<span class="input-group-btn">' +--}}
{{--                                                '<button type="button" id="quantity-basket-left-minus{{$product->id}}{{$price->id}}" class="quantity-left-minus  btn btn-danger btn-number" data-type="minus" data-field="">' +--}}
{{--                                                '   <span class="bx bx-minus"></span>' +--}}
{{--                                                '  </button>' +--}}
{{--                                                ' </span>' +--}}
{{--                                                '  <input type="text" disabled id="quantity-basket{{$product->id}}{{$price->id}}" name="quantity" class="form-control input-number text-lg-center" value="1" min="1" max="100">' +--}}
{{--                                                ' <span class="input-group-btn">' +--}}
{{--                                                ' <button type="button" id="quantity-basket-right-plus{{$product->id}}{{$price->id}}" class="quantity-right-plus btn btn-success btn-number" data-type="plus" data-field="">' +--}}
{{--                                                '<span class="bx bx-plus"></span>' +--}}
{{--                                                '  </button>' +--}}
{{--                                                ' </span>' +--}}
{{--                                                '  </div>' +--}}
{{--                                                /////--}}
{{--                                                ' </div>' +--}}
{{--                                                /////////////////////////////////--}}

{{--                                                '<div class="col-3 product-box" style="height: 170px; background-image:url(' + '{{ $product->getFirstMediaUrl('images', 'thumbnail')}}' + ');  background-repeat: no-repeat; background-position: center; background-size: cover;margin-bottom: 3px;">' +--}}

{{--                                                ' <div class="small-box product-overlay" style=" height: 170px;margin: 0 !important; border: 5px solid transparent;">' +--}}

{{--                                                '</div>' +--}}
{{--                                                '</div>' +--}}
{{--                                                ////////////////////////////////////////////////////--}}

{{--                                                ' <img class="align-self-center rounded " src="{{$product->getFirstMediaUrl('images', 'thumbnail')}}" style="height:50%"> ' +--}}


{{--                                                    '</div>' + ' <hr/>' + '</div>')--}}
{{--                                        $('#quantity-basket-right-plus{{$product->id}}{{$price->id}}').on('click', function (e) {--}}

{{--                                            e.preventDefault();--}}
{{--                                            // Get the field name--}}
{{--                                            quantity_basket = parseInt($('#quantity-basket{{$product->id}}{{$price->id}}').val());--}}
{{--                                            // If is not undefined--}}

{{--                                            $('#quantity-basket{{$product->id}}{{$price->id}}').val(quantity_basket + 1);--}}


{{--                                            calculate_basket({{$product->id}}{{$price->id}})--}}

{{--                                        });--}}
{{--                                        $('#quantity-basket-left-minus{{$product->id}}{{$price->id}}').on('click', function (e) {--}}
{{--                                            // Stop acting like a button--}}
{{--                                            e.preventDefault();--}}
{{--                                            // Get the field name--}}
{{--                                            quantity_basket = parseInt($('#quantity-basket{{$product->id}}{{$price->id}}').val());--}}

{{--                                            // If is not undefined--}}

{{--                                            // Increment--}}
{{--                                            if (quantity_basket > 1) {--}}
{{--                                                $('#quantity-basket{{$product->id}}{{$price->id}}').val(quantity_basket - 1);--}}
{{--                                            }--}}
{{--                                            calculate_basket({{$product->id}}{{$price->id}})--}}
{{--                                        });--}}
{{--                                        $('#basket-remove-item{{$product->id}}{{$price->id}}').on('click', function (e) {--}}
{{--                                            e.preventDefault();--}}
{{--                                            $('#product-seleccted{{$product->id}}{{$price->id}}').removeClass('product-selected');--}}
{{--                                            $('#basket-product{{$product->id}}{{$price->id}}').remove()--}}
{{--                                            calculate_basket({{$product->id}}{{$price->id}})--}}
{{--                                        })--}}
{{--                                    }--}}

{{--                                    calculate_basket({{$product->id}}{{$price->id}})--}}

{{--                                });--}}


{{--                            </script>--}}
{{--                        @endpush--}}
