<div class="card">
    <div class="card-header">
       Address & Request Date  & Note
    </div>
    <div class="card-body">
        <div class="row">
            <div class="mb-3 select2-sm col-6">
                <label class="form-label">Search on Phone</label>
                <input class="form-control" id="phone-searchable"
                       type="number"
                       oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                       maxlength="8"
                       minlength="8"
                       min="0"
                       name="new-phone"
                       placeholder="phone number">

                    <select class="form-control" id="phone-customer"  style="display: none" >
{{--                        <option>Select Phone Number</option>--}}
                    </select>

{{--                <ul class="list-group" id="phone-customer">--}}
{{--                    <li class="list-group-item">First item</li>--}}
{{--                    <li class="list-group-item">Second item</li>--}}
{{--                    <li class="list-group-item">Third item</li>--}}
{{--                    <li class="list-group-item">Fourth</li>--}}
{{--                </ul>--}}
{{--                <select class="single-select " id="phone-customer" >--}}
{{--                    <option>Search By Phone</option>--}}
{{--                </select>--}}
            </div>
{{--            <div class="mb-3 select2-sm col-6">--}}
{{--                <label class="form-label">Customer Name</label>--}}
{{--                <input type="text" id="name-customer" name="name" class="form-control">--}}
{{--            </div> --}}
            <div class="mb-3 select2-sm col-6">
                <label class="form-label">Requested Time </label>
                <input class="result form-control" type="text" id="date-time" placeholder="Requested Time"
                       data-dtp="dtp_iiXqD">
            </div>
        </div>

        <div class="row">
            <div class="col-6 ">

                <div class="input-group input-group-sm mb-3">

                    <textarea class="form-control" placeholder="NOTE..." id="order-note" rows="3"></textarea>

                </div>
            </div>
{{--            <div class="col-6 ">--}}
{{--                <div class="mb-3">--}}
{{--                    <label class="form-label">Requested Time </label>--}}
{{--                    <input class="result form-control" type="text" id="date-time" placeholder="Requested Time"--}}
{{--                           data-dtp="dtp_iiXqD">--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <button class="btn btn-primary" onclick="add_address()">
            Add Address
        </button>
    </div>
    <br/>
</div>


<div class="row">
    <div class="col-12 col-md-6 address-content" id="addresses-form"
         style="display: none; margin: 0 !important;border: 5px solid transparent;">
        <div class="card  card card-default">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h5 class="card-title"> Address </h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">

                            <div class="mb-3 select2-sm col-12">
                                <label class="form-label">Search on City</label>
                                <select class="single-select" id="city-address">

                                </select>
                            </div>

                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group"><label>Street</label>
                            <input type="text" id="address-street" class="form-control" value=""></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group"><label>Block</label> <input type="text" id="address-block"
                                                                            class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="form-group"><label>House</label> <input type="text" id="address-house"
                                                                            class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="form-group"><label>Avenue</label> <input type="text" id="address-avenue"
                                                                             class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="form-group"><label>Flat</label> <input type="text" id="address-flat"
                                                                           class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="form-group"><label>Floor</label> <input type="text" id="address-floor"
                                                                            class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-md-12 col-12">
                        <div class="form-group"><label>Note</label> <input type="text" id="address-note"
                                                                            class="form-control" value="">
                        </div>
                    </div>
                </div>
                <button type="button" class="mt-3 btn btn-success" onclick="submit_address()" id="address-submit">
                    Select This Address
                </button>
            </div>

        </div>

    </div>
</div>
<div class="row" id="addresses">

</div>

