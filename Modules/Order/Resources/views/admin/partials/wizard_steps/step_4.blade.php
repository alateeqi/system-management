<div class="card">
    <div class="card-body">
        <div id="invoice">
            <div class="toolbar hidden-print">
                <div class="text-end">
{{--                    <button type="button" onclick=" $('.invoice').printThis();" class="btn btn-dark">--}}
{{--                        <i class="lni lni-printer"></i> Print--}}
{{--                    </button>--}}
                    <button type="button" class="btn btn-outline-primary " onclick="submitOrder()"><i class="fadeIn animated bx bx-save"></i> Submit</button>
                    <button type="button" class="btn btn-primary" onclick="submitOrder(true)"><i class="lni lni-printer"></i> Submit&Print</button>
                </div>
                <hr/>
            </div>
            <div class="invoice overflow-auto">
                <div style="min-width: 600px">
                    <header>
                        <div class="row">
                            <div class="col">
                                <a href="javascript:;">
                                    <img src="{{asset('/assets/images/logo-icon.png')}}" width="80" alt=""/>
                                </a>
                            </div>
                            <div class="col company-details">
                                <h2 class="name">
                                    <a target="_blank" href="javascript:;">
                                        Baked&CO
                                    </a>
                                </h2>
                                <div>(+965) 55231033</div>
                                <div>info@bakednco.com</div>
                            </div>
                        </div>
                    </header>
                    <main>
                        <div class="row contacts">
                            <div class="col invoice-to">
                                <div class="text-gray-light">INVOICE TO:</div>
                                <h2 class="to" id="order-customer-name"></h2>

                                <div class="address">

                                    <strong>City: </strong><span id="order-address-city"></span><br>
                                    <strong>Street: </strong><span id="order-address-street"></span>
                                    <strong>Block: </strong><span id="order-address-block"></span><br>
                                    <strong>House: </strong><span id="order-address-house"></span>
                                    <strong>Avenue: </strong><span id="order-address-avenue"></span>
                                    <strong>Flat: </strong><span id="order-address-flat"></span>
                                </div>
                                <div class="email"><a href="" id="order-customer-phone"></a>
                                </div>


                            </div>
                            <div class="col invoice-details">
                                <div class="date">Requested Time: <strong id="order-requested-date"></strong></div>
                                <div class="date">Payment Method: <strong id="order-payment-method"></strong></div>
                                <div class="date">Gate Method: <strong id="order-gate"></strong></div>
                            </div>
                        </div>

                        <table>
                            <thead>
                            <tr>
                                <th>#</th>

                                <th class="text-left">Products</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">QTY</th>
                                <th class="text-right">TOTAL</th>
                            </tr>
                            </thead>
                            <tbody id="order-products">


                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Delivery Rate</td>
                                <td><span id="order-delivery-rate">0</span> KWD</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">SUBTOTAL</td>
                                <td><span id="order-subtotal">0</span>KWD</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Discount <span id="order-discount-percentage">0</span>%</td>
                                <td> <span id="order-sum-discount-percentage">0</span> KWD</td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">Discount <span id="order-discount">0</span>KWD</td>
                                <td> <span id="order-sum-discount">0</span> KWD</td>
                            </tr>

                            <tr>
                                <td colspan="2"></td>
                                <td colspan="2">GRAND TOTAL</td>
                                <td><span id="order-grand-total"></span> KWD </td>
                            </tr>
                            </tfoot>
                        </table>

                        <div class="notices">
                            <div>NOTICE:</div>
                            <div class="notice">
                                <span id="order-notice"></span>
                            </div>
                        </div>
                        <div class="thanks">Thank you For Your Request!</div>
                    </main>
                    {{--                    <footer>Invoice was created on a computer and is valid without the signature and seal.</footer>--}}
                </div>
                <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->

            </div>
        </div>
    </div>
</div>
