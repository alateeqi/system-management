@if ($printed)
    @php

        function getFullAddress($address)
        {
            if ( ! $address->city) {
                return '';
            }
            $content = '<small><span>City: </span>' . $address->city->title;
            $content .= ', <span>Street: </span>' . $address->street;
            $content .= ', <span>Block: </span>' . $address->block;
            $content .= ', <span>House: </span>' . $address->house;
            $content .= ', <span>Avenue: </span>' . $address->avenue;
            $content .= ', <span>Flat: </span>' . $address->flat;
            $content .= ', <span>Floor: </span>' . $address->floor;
            $content .= ', <span>Note: </span>' . $address->notes . '</small>';

            return $content;
        }

        function productPrice($order)
        {
            $i = 0;
            foreach ($order->products as $product) {
                $i += floatval($product->price);
            }

            return $i;
        }

        function deliveryPrice($order)
        {
            return floatval($order->delivery_price);
        }

        function totalPrice($order)
        {
            $i = productPrice($order);
            $i += deliveryPrice($order);

            $i -= $order->discount ? floatval($order->discount) : 0;
            if ($order->discount_percentage) {
                $percentageAmount = $i * (floatval($order->discount_percentage) / 100);
                $i -= $percentageAmount;
            }

            return $i;
        }

    @endphp
@endif

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style type="text/css">
        .font-weight-bold {
            font-weight: bold;
        }

        table {
            margin-bottom: 20px;
            width: 100%;
            border-collapse: collapse;
        }

        table td, table th {
            border-top: 1px solid #ddd;
            padding: 10px 5px;
        }

        .text-center {
            text-align: center
        }

        .text-right {
            text-align: right
        }

        .text-left {
            text-align: left
        }

        .product-container table td, .product-container table th {
            padding-top: 2px !important;
            padding-bottom: 2px !important;
        }

        .product-container table tr:nth-child(odd) td {
            background-color: #ddd !important;
        }

        .page-container {
            font-size: x-small;
        }

        .invoice-container {
            height: 13cm !important;
            overflow: hidden;
        }

        body {
            font-family: DejaVu Sans;
        }
    </style>
</head>

<body>

<div class=" w-100" id="form-card">
    <div class="card-header">
        <h3 class="card-title">Order #{{ $order->id }}</h3>
        <span
            class="text-right p-1 d-inline-block rounded text-white {{ \Modules\Order\Entities\Order::STATUSES[$order['status']]['class'] }}">{{ trans("order::strings.orderStatus".$order['status']) }}</span>
    </div>
    <div class="card-body invoice-body">
        <div class="invoice p-3 mb-3">
            <div class="invoice-details">
                <!-- title row -->
                <div class="row">
                    <div class="col-4 my-auto">
                        <img src="/assets/images/logo-img.jpg" width="150" alt="logo">
                    </div>
                    <div class="col-4 text-center my-auto">
                        <div>+965 55231033</div>
                    </div>
                    <div class="col-4 my-auto">
                        <div class="float-right">
                            <div class="text-center">
                                <div class="font-weight-bold">
                                    {{ $order->requested_time}}
                                </div>
                                {{--                                        <div class="font-weight-bold"--}}
                                {{--                                             v-html="order.requested_time ? order.orequested_time : 'Now'"></div>--}}

                            </div>
                        </div>

                    </div>
                    <!-- /.col -->
                </div>
                <!-- Table row -->
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <tbody>
                                <tr>
                                    <td style="width: 25%">
                                        <div><b>Phone:</b> {{  $order->customer->phone }}</div>
                                        <div><b>Order:</b> #{{  $order->id }}</div>
                                    </td>
                                    <td><b>Address:</b>
                                        @if($order->address)
                                            <span>{!!   getFullAddress($order->address) !!}</span>
                                        @endif


                                    </td>
                                    <td style="width: 25%"><b>Notes:</b> {{ $order->notes }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- Table row -->
                <div class="row pt-1 pb-1">
                    <div class="col-12 table-responsive product-container">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($order->products as $product)
                                <tr>
                                    <td>{{ $product->product->title }}</td>
                                    <td>{{ $product->quantity }} {{ $product->priceList->unite_name }}</td>
                                    <td>{{ $product->price }} KWD</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                    <!-- /.col -->
                    <div class="col-12">

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td style="width:33%">
                                        <div>
                                            <b>Payment Method:</b>
                                            <span>{{ $order->paymentMethod->title }}</span>
                                        </div>
                                        <div>
                                            <b>Gate:</b>
                                            <span>{{ $order->gate->title }}</span>
                                        </div>
                                    </td>
                                    <td style="width:33%"><b>Products Price:</b> <span>{{ productPrice($order) }}
								KWD</span>
                                    </td>
                                    <td style="width:33%"><b>Delivery Price:</b><span>{{ deliveryPrice($order) }}
								KWD</span>
                                    </td>
                                </tr>
                                <tr>
                                    @if($order->discount)
                                        <td style="width:33%"><b>Discount:</b> <span>{{ $order->discount }} KWD</span>
                                        </td>
                                    @endif
                                    @if($order->discount)
                                        <td style="width:33%"><b>Discount %:</b> <span>{{ $order->discount_percentage }}
									%</span>
                                        </td>
                                    @endif
                                    <td style="width:33%"><b>Total Price:</b> <span>{{ totalPrice($order) }} KWD</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-12">
                    <a target="_blank"
                       href="https://bakednco-media.s3.eu-central-1.amazonaws.com/invoices/invoice{{$order->id}}.pdf"
                       class="btn btn-primary ">
                        <i class="fas fa-print"></i>
                        print
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>


