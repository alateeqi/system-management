@extends('layouts.dashboard.app')

@push('css')
    <style>
        .inputGroup label {
            border-radius: 10px;
            height: auto !important;
            line-height: inherit !important;
            font-weight: inherit !important;
            padding: 12px 30px;
            width: 100%;
            display: block;
            text-align: left;
            color: #3c454c;
            cursor: pointer;
            position: relative;
            z-index: 2;
            -webkit-transition: color 200ms ease-in;
            transition: color 200ms ease-in;
            overflow: hidden;
        }

        .inputGroup input {
            left: auto !important;
            opacity: 1 !important;
            width: 32px;
            height: 32px;
            -webkit-box-ordinal-group: 2;
            order: 1;
            z-index: 2;
            position: absolute;
            right: 30px;
            top: 50%;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            cursor: pointer;
            visibility: hidden;
        }
    </style>
@endpush
@section('content')
    <div class=" page-wrapper ">
        <div class="card-body">
            <h5 class="card-title">Orders</h5>


            <hr/>
            <div class="row">
                @foreach($orders as $order)
                    <div class="mb-2 col-md-6 ">
                        <div class="col-md-12 accordion" id="accordionExample{{$order->id}}">
                            <div class=" accordion-item">
                                <h2 class="accordion-header" id="headingOne{{$order->id}}">

                                    <div class="accordion-button collapsed" data-bs-toggle="collapse"
                                         data-bs-target="#collapseOne{{$order->id}}" aria-expanded="false"
                                         aria-controls="collapseOne{{$order->id}}">
                                        <div class="container-fluid">
                                            <div class="row text-muted">
                                                <div class="col-4 ">

                                                    <i class="lni lni-phone"></i>
                                                    <a class="" id="{{$order->customer->phone}}"
                                                       onclick="whatsAppContact('{{$order->customer->phone}}')"
                                                       href="https://api.whatsapp.com/send?phone=965{{$order->customer->phone}}"
                                                       title="965{{$order->customer->phone}}"> {{$order->customer->phone}}</a>
                                                </div>
                                                <div class="col-4 ">
                                                    <i class="lni lni-map-marker"></i>
                                                    {{isset($order->address)?$order->address->city->title:''}}
                                                </div>
                                                <div class="col-2 "></div>
                                                <div class="col-2 ">
                                                    <span class="pr-2"><i class="fa fa-clock"></i></span>
                                                    <span
                                                        class="badge {{$order->status_class}}"
                                                        id="status-title{{$order->id}}">{{$order->status_name}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </h2>
                                <div id="collapseOne{{$order->id}}" class="accordion-collapse collapse"
                                     aria-labelledby="headingOne{{$order->id}}"
                                     data-bs-parent="#accordionExample{{$order->id}}" style="">
                                    <div class="accordion-body">
                                        <section class=" gradient-custom-2">

                                            <div class="row d-flex justify-content-center align-items-center">

                                                <div class="card card-stepper">
                                                    <div class="card-header p-4">
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <div>
                                                                <p class="text-muted mb-2"> Order ID <span
                                                                        class="fw-bold text-body">#{{$order->id}}</span>
                                                                </p>
                                                                <p class="text-muted mb-0"> Place On <span
                                                                        class="fw-bold text-body">{{$order->created_at->toDateString()}} -{{ date('h:i A', strtotime($order->created_at))}}</span>
                                                                </p>
                                                            </div>
                                                            <div>
                                                                <h3 class="mb-0"><a
                                                                        href="#">{{number_format($order->total_price,1)}}
                                                                        KWD</a></h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body p-4">
                                                        @foreach($order->products as $product)

                                                            <div class="d-flex flex-row mb-4 pb-2">
                                                                <div class="flex-fill">
                                                                    <h5 class="bold">{{$product->product->title}}</h5>
                                                                    <p class="text-muted"> QTY: {{ $product->quantity }}
                                                                        ( {{!is_null($product->priceList)?$product->priceList->unite_name:'' }}
                                                                        )</p>
                                                                    <h4 class="mb-3">
                                                                        KWD {{number_format($product->price,1)}}

                                                                    </h4>

                                                                </div>
                                                                <div>
                                                                    <img class="align-self-center img-fluid"
                                                                         src="{{$product->product->getFirstMediaUrl('images', 'thumbnail')}} "
                                                                         width="150">
                                                                </div>
                                                            </div>
                                                            <hr/>
                                                        @endforeach

                                                        <div id="bar-status{{$order->id}}">
                                                            <p class=" text-black">
                                                                Delivery Price:
                                                                <span class="text-body">
                                                                      <strong>
                                                                          {{$order->delivery_price  .' '}} KWD
                                                                      </strong>
                                                                </span>
                                                            </p>
                                                            <p class="text-muted">
                                                                Delivering At:
                                                                <span class="text-body">
                                                                       {{$order->requested_time->toDateString() }} - {{ date('h:i A', strtotime($order->requested_time))}}
                                                                </span>
                                                            </p>

                                                            {{--                                                            <div id="smartwizard"--}}
                                                            {{--                                                                 class="sw sw-theme-dots sw-justified">--}}
                                                            {{--                                                                <ul class="nav">--}}
                                                            {{--                                                                    <li class="nav-item">--}}
                                                            {{--                                                                        <a id="nav-link-underway{{$order->id}}"--}}
                                                            {{--                                                                           class="nav-link inactive {{($order->status ==\Modules\Order\Entities\Order::STATUS_NAME['underway'])?'active':''}} "--}}
                                                            {{--                                                                           href="#step-1">--}}
                                                            {{--                                                                            <strong>Underway</strong>--}}
                                                            {{--                                                                        </a>--}}
                                                            {{--                                                                    </li>--}}
                                                            {{--                                                                    <li class="nav-item">--}}
                                                            {{--                                                                        <a class="nav-link inactive "--}}
                                                            {{--                                                                           id="nav-link-new-update{{$order->id}}"--}}
                                                            {{--                                                                           href="#step-2">--}}
                                                            {{--                                                                            <strong>New&Updated</strong>--}}
                                                            {{--                                                                        </a>--}}
                                                            {{--                                                                    </li>--}}

                                                            {{--                                                                    <li class="nav-item">--}}
                                                            {{--                                                                        <a id="nav-link-delivered{{$order->id}}"--}}
                                                            {{--                                                                           class="nav-link inactive"--}}
                                                            {{--                                                                           href="#step-3">--}}
                                                            {{--                                                                            <strong>Delivered</strong>--}}
                                                            {{--                                                                        </a>--}}
                                                            {{--                                                                    </li>--}}
                                                            {{--                                                                    <li class="nav-item">--}}
                                                            {{--                                                                        <a id="nav-link-proved{{$order->id}}"--}}
                                                            {{--                                                                           class="nav-link inactive "--}}
                                                            {{--                                                                           href="#step-4">--}}
                                                            {{--                                                                            <strong>Proved</strong>--}}
                                                            {{--                                                                        </a>--}}
                                                            {{--                                                                    </li>--}}
                                                            {{--                                                                </ul>--}}

                                                            {{--                                                            </div>--}}


                                                        </div>
                                                        <div class="card-footer p-4">

                                                            {{--                                                            <div class="d-flex justify-content-around">--}}

                                                            {{--                                                                <div class="border-start h-100"></div>--}}
                                                            {{--                                                                <button type="button" class="btn badge bg-primary "--}}
                                                            {{--                                                                        style="font-size: 13px">--}}
                                                            {{--                                                                    Summary--}}
                                                            {{--                                                                </button>--}}
                                                            {{--                                                                <div class="border-start h-100"></div>--}}

                                                            {{--                                                            </div>--}}
                                                            {{--                                                            <hr/>--}}
                                                            <div class="d-flex justify-content-between"
                                                                 id="btns-status{{$order->id}}">
                                                                @if ($order->status== \Modules\Order\Entities\Order::STATUS_NAME['underway'])

                                                                    <button type="button" class="btn btn-success me-0"
                                                                            id="delivered{{$order->id}}"
                                                                            onclick=" newAndUpdate({{$order->id}}, '{{$order->statuses['new'] }}', 'تم تأكيد  الطلب', '{{$order->statuses['proved'] }}')">
                                                                        New
                                                                    </button>
                                                                    <div class="border-start h-25"></div>


                                                                @endif
                                                                @if (auth()->user()->hasPermission('orders-delete'))
                                                                    <button type="button" class="btn btn-danger"
                                                                            id="canceled-modal{{ $order->id }}"
                                                                            data-bs-toggle="modal"
                                                                            data-bs-target="#exampleVerticallycenteredModal{{ $order->id }}">
                                                                        Canceled
                                                                    </button>

                                                                    <div class="border-start h-25"></div>
                                                                @endif


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade"
                         id="receiptVerticallycenteredModal{{$order->id}}"
                         tabindex="-1" style="display: none;"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">
                                        Receipt No
                                    </h5>
                                    <button type="button"
                                            class="btn-close"
                                            data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group m-2">
                                        <input name="receipt_no" id="receipt_no{{$order->id}}" class="form-control">
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button"
                                            class="btn btn-secondary"
                                            data-bs-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="button"
                                            class="btn btn-success"
                                            id="receipt{{$order->id}}">
                                        حفظ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="exampleVerticallycenteredModal{{ $order->id }}" tabindex="-1"
                         style="display: none;"
                         aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">
                                        سبب الحذف
                                    </h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    @foreach ($canceledDues as $index => $reason)
                                        <div class="form-check">
                                            <input name="reason" id="reason{{ $index }}{{ $order->id }}" type="radio"
                                                   onchange=" $('#message_reason{{ $order->id }}').val('{{ $reason }}');"
                                                   class="form-check-input" value="{{ $reason }}">
                                            <label for="reason{{ $index }}{{ $order->id }}" class="form-check-label">

                                                <strong>
                                                    {{ $reason }}
                                                </strong>
                                            </label>

                                        </div>
                                    @endforeach
                                    <div class="form-check">
                                        <input name="reason" id="other_reason{{ $order->id }}" type="radio"
                                               onchange="$('#message_reason{{ $order->id }}').val('')"
                                               class="form-check-input" checked
                                               value="other">
                                        <label for="other_reason{{ $order->id }}" class="form-check-label">

                                            <strong>
                                                Other
                                            </strong>
                                        </label>

                                    </div>
                                </div>
                                <div class="form-group m-2">

                                    <input name="message" id="message_reason{{ $order->id }}" class="form-control">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                                        Close
                                    </button>
                                    <button type="button" class="btn btn-danger" id="cancel{{ $order->id }}"
                                            onclick="canceled({{ $order->id }}, '{{ $order->statuses['canceled'] }}', 'تم حذف الطلب', $('#message_reason{{ $order->id }}').val(),'{{$order->customer->phone}}')">
                                        إلغاء الطلب
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

        </div>
    </div>

@endsection
@push('js')
    <script>
        $(document).ready(function () {

            clearInterval(window.interval);
            window.interval = null;
        })

        function whatsAppContact(phone) {
            window.location = 'https://api.whatsapp.com/send?phone=965' + phone;
        }


        function receipt(id, status, message, receipt_no) {


            $.post("{{route('admin.orders.receipt_no')}}", {
                order_id: id,
                status: status,
                receipt_no: receipt_no,
            }, function (data) {
                success_noti(message);

                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)
                $('#proved' + id).hide()
                $('#cancel' + id).hide()
                $('#edit' + id).hide()
                $('#canceled-modal' + id).hide()
                $('#nav-link-proved' + id).addClass(' active ')
                $('#nav-link-proved' + id).removeClass('inactive')
            }).fail(function (error) {
                error_noti(error);
            });
        }


        function show(id) {

            window.location = '{{url('/admin/orders/show/')}}/' + id;

        }

        function newAndUpdate(id, status, message) {

            $.post("{{route('admin.orders.new_update')}}", {
                order_id: id,
                status: status
            }).done(function (data) {
                success_noti(message);
                // $('#payment-method-' + id).empty();
                // $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                // $('#status-title' + id).empty();
                // $('#status-title' + id).append(data.data.status_name);
                // $('#status-title' + id).removeClass()
                // $('#status-title' + id).addClass('badge ' + data.data.status_class)
                // $('#delivered' + id).hide()
                // $('#btns-status' + id).append(
                //     '   <button type="button"' +
                //     ' data-bs-toggle="modal"' +
                //     ' data-bs-target="#receiptVerticallycenteredModal' + id + '"' +
                //     ' class="btn btn-info "' +
                //     ' id="receipt-modal' + id + '">' +
                //     '  Receipt no' +
                //     ' </button>' +
                //     '<div class="border-start h-25"></div>' +
                //     '   <button type="button" class="btn btn-info " id="proved' + id + '">' +
                //     '  Proved' +
                //     ' </button>' +
                //     '<div class="border-start h-25"></div>'
                // );
                // $('#proved' + id).on('click', function () {
                //     proved(id, proveds_status, 'تمت العملية بنجاح')
                //
                // });

                $('#nav-link-new-update' + id).addClass(' active ')
                $('#nav-link-new-update' + id).removeClass('inactive')
                $('#nav-link-underway' + id).removeClass(' inactive ')
                $('#nav-link-underway' + id).addClass(' active ')

            });

        }

        function canceled(id, status, message, rason, phone) {


            $.post("{{route('admin.orders.cancel')}}", {
                order_id: id,
                status: status,
                message: rason,
            }, function (data) {
                success_noti(message);

                $('#payment-method-' + id).empty();
                $('#payment-method-' + id).append('set payment method to' + data.data.payment_method.title);
                $('#status-title' + id).empty();
                $('#status-title' + id).append(data.data.status_name);
                $('#status-title' + id).removeClass()
                $('#status-title' + id).addClass('badge ' + data.data.status_class)

                $('#btns-status-ep' + id).empty()
                $('#btns-status' + id).empty()
                $('#bar-status' + id).empty()

                whatsAppContact(phone);
            }).fail(function (error) {
                error_noti(error);
            });
        }
    </script>
@endpush

