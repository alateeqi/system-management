<?php
Route::middleware('auth:api')->group(function () {

    Route::get('orders', 'Client\ClientOrderController@orders');
    Route::get('orders/{orderId}', 'Client\ClientOrderController@show');

});

Route::post('order', 'Guest\GuestOrderController@store');
Route::get('order-check/{paymentId}', 'Guest\GuestOrderController@orderCheck');
Route::post('myfatura-order-check', 'Guest\GuestOrderController@myFaturaOrderCheck');
