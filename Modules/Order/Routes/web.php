<?php

use Illuminate\Support\Facades\Route;
use Modules\Product\Http\Controllers\ProductController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //orders routes
            Route::get('/orders', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'index'])->name('admin.orders.index');
            Route::get('/orders/show/{id}', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'show'])->name('admin.orders.show');
            Route::get('/orders/edit/{id}', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'edit'])->name('admin.orders.edit');
            Route::get('/orders/create', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'create'])->name('admin.orders.create');
            Route::post('/orders', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'store'])->name('admin.orders.store');
            Route::post('/orders/update', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'update'])->name('admin.orders.update');
            Route::post('/orders/filter', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'filter'])->name('admin.orders.filter');
            Route::post('orders/change-payment-method', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'changePaymentMethod'])->name('admin.orders.changePaymentMethod');
            Route::post('orders/delivery', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'delivery'])->name('admin.orders.delivery');
            Route::post('orders/new-update', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'new'])->name('admin.orders.new_update');
            Route::post('orders/proved', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'proved'])->name('admin.orders.proved');
            Route::post('orders/cancel', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'cancel'])->name('admin.orders.cancel');
            Route::post('orders/receipt-no', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'receipt_no'])->name('admin.orders.receipt_no');
            Route::post('orders/sms', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'sms'])->name('admin.orders.sms');
            Route::get('orders/customer-phones', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'all_phones'])->name('admin.orders.customer.phones');
            Route::get('orders/website', [\Modules\Order\Http\Controllers\Admin\AdminOrderController::class, 'websiteOrders'])->name('admin.orders.website');
        });
    }
);
