<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamp('from')->nullable();
            $table->timestamp('to')->nullable();
            $table->integer('min_order');
            $table->integer('points');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });

        Schema::create('users_points', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('point_id')->unsigned()->index();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->integer('points');
            $table->boolean('is_occasion')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
        Schema::dropIfExists('users_points');
    }
}
