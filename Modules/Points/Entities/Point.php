<?php

namespace Modules\Points\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;


class Point extends Model
{
    protected $table = 'points';

    protected $fillable = [
        'name',
        'from',
        'to',
        'min_order',
        'points',
        'status',
    ];


    public function users()
    {
        return $this->belongsToMany(User::class, 'users_points', 'point_id', 'user_id');
    }
}
