<?php

namespace Modules\Points\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\User;


class UserPoint extends Model
{
    protected $table = 'users_points';

    protected $fillable = [
        'point_id',
        'user_id',
        'points',
        'is_occasion',

    ];
    protected $with = [
        'user',
        'point',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function point()
    {
        return $this->belongsTo(Point::class);
    }
}
