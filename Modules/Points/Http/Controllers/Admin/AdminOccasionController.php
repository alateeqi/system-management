<?php

namespace Modules\Points\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Points\Http\Requests\Admin\Store;
use Modules\Points\Http\Requests\Admin\ToggleStatus;
use Modules\Points\Http\Requests\Admin\Update;
use Modules\Points\Interfaces\Admin\AdminOccasionInterface;

class AdminOccasionController extends Controller
{
    public function index(AdminOccasionInterface $interface)
    {
        $points = $interface->index();
        return view('points::admin.index', compact('points'));
    }

    public function create()
    {
        return view('points::admin.create');
    }

    public function show(AdminOccasionInterface $interface, $id)
    {
        $point = $interface->find($id);
        return view('points::admin.show', compact('point'));
    }

    public function pointsUsers(AdminOccasionInterface $interface)
    {
        $points = $interface->getPointOfUsers();
//        dd($points);
        return view('points::admin.users', compact('points'));
    }

    public function store(Store $request, AdminOccasionInterface $interface)
    {
        try {
            DB::beginTransaction();
            $data = $interface->store($request->validated());
            DB::commit();

            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('admin.points.index');

        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('admin.points.create')->withErrors($exception->getMessage());
        }
    }

    public function update(Update $request, AdminOccasionInterface $interface)
    {
        try {
            DB::beginTransaction();
            $interface->update($request->validated());
            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('admin.points.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->route('admin.show', $request['occasion_id'])->withErrors($exception->getMessage());
        }
    }

    public function toggleStatus(ToggleStatus $request, AdminOccasionInterface $interface)
    {
        return response()->json([
            'data' => $interface->toggleStatus($request->validated())
        ]);
    }
}
