<?php

namespace Modules\Points\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ToggleStatus extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'occasion_id' => ['required', 'numeric', 'exists:points,id'],
        ];
    }
}
