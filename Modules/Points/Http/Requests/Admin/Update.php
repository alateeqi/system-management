<?php

namespace Modules\Points\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'occasion_id' => ['required', 'numeric', 'exists:points,id'],
            'name' => ['required', 'string'],
            'points' => ['numeric'],
            'min_order' => ['required', 'numeric'],
            'from' => ['required', 'date'],
            'to' => ['required', 'date'],
//            'status' => ['required', 'boolean'],
        ];
    }
}
