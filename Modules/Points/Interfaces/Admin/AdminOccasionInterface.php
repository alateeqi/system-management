<?php

namespace Modules\Points\Interfaces\Admin;

interface AdminOccasionInterface
{
    public function index($paginate = 20);

    public function find($id);

    public function store($data);

    public function update($data);

    public function toggleStatus($data);

    public function getPointOfUsers();

}
