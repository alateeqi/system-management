<?php

namespace Modules\Points\Repositories\Admin;

use Carbon\Carbon;
use Modules\Auth\Entities\User;
use Modules\Points\Entities\Point;
use Modules\Points\Entities\UserPoint;
use Modules\Points\Interfaces\Admin\AdminOccasionInterface;

class AdminOccasionRepository implements AdminOccasionInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    public function index($paginate = 20)
    {
        return Point::paginate($paginate);
    }

    public function find($id)
    {
        return Point::find($id);
    }


    public function store($data)
    {
        $point = Point::create([
            'points' => $data['points'],
            'from' => Carbon::parse($data['from'])->format('Y-m-d 00:00:00'),
            'to' => Carbon::parse($data['to'])->format('Y-m-d 23:59:59'),
            'min_order' => $data['min_order'],
            'name' => $data['name'],
            'status' => 1,
        ]);
        return $point;
    }

    public function storeMiddleEndWeek($data)
    {
        $point = Point::create([
            'points' => $data['points'],
            'from' => $data['from'],
            'to' => $data['to'],
            'min_order' => $data['min_order'],
            'name' => $data['name'],
            'status' => 1,
        ]);
        return $point;
    }


    public function update($data)
    {
        $point = $this->find($data['occasion_id']);
        $point->update([
            'points' => $data['points'],
            'from' => Carbon::parse($data['from'])->format('Y-m-d 00:00:00'),
            'to' => Carbon::parse($data['to'])->format('Y-m-d 23:59:59'),
            'min_order' => $data['min_order'],
            'name' => $data['name'],
            'status' => $point->status,
        ]);
        return $point;
    }

    public function toggleStatus($data)
    {
        $model = $this->find($data['occasion_id']);
        $model->status = !$model->status;
        $model->save();
        return $model;
    }

    public static function getOccasionByToday()
    {
        return Point::where('from', '<=', today()->toDateTimeString())
            ->where('to', '>=', Carbon::parse(today())->format('Y-m-d 23:59:59'))
            ->first();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public function addPointsToUser($id, $points, $isOccasion)
    {
        $point_user = UserPoint::create([
            'points' => $points,
            'point_id' => $id,
            'user_id' => $this->user->id,
            'is_occasion' => $isOccasion
        ]);
        return $point_user;
    }

    public function getPointOfUsers()
    {
        return UserPoint::select('point_id')->groupBy('point_id')->get();
    }
}
