<?php

return [
    'name'=>'الاسم ',
    'start_at'=>'تاريخ البدء ',
    'end_at'=>'تاريخ الانتهاء ',
    'points'=>'النقاط',
    'min_orders'=>'الحد الادنى للطلبات',
    'status'=>'الحالة',
    'add_new_occasion'=>'اضافة مناسبة جديدة',
    'edit_occasion'=>'تعديل المناسبة',
    'point_of_order'=>'نقاط الطلب',
    'add'=>'حفظ',
    'users'=>'المستخدمين',
    'occasion'=>'المناسبة',
    'user_name'=>'اسم المستخدم',
    'point_count'=>'عدد النقاط',
    'occasion_name'=>'اسم المناسبة',
    'users_points'=>'نقاط المستخدمين',

];