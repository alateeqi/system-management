<?php

return [
    'name'=>'name',
    'start_at'=>'Start At ',
    'end_at'=>'End At',
    'points'=>'Points',
    'min_orders'=>'Min Orders',
    'status'=>'Status',
    'add_new_occasion'=>'Add a new occasion',
    'edit_occasion'=>'Edit a occasion',
    'point_of_order'=>'Point Of Order',
    'add'=>'add',
    'users'=>'users',
    'occasion'=>'Occasion',
    'user_name'=>'user_name',
    'point_count'=>'Point Count',
    'occasion_name'=>'Occasion Name',
    'users_points'=>'Users Points',
];