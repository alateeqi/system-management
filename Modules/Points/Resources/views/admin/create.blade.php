@extends('layouts.dashboard.app')
@section('title')
    Points & Target
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">المناسبات والعروض</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-4">
                    <h5 class="card-title">@lang('points::site.add_new_occasion')</h5>
                    <hr/>
                    <form class=" mt-4" action="{{ route('admin.points.store') }}" id="form-point" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row col-md-12">
                                        <div class="col-md-12">
                                            <div class="mb-3">
                                                <label for="inputTitle"
                                                       class="form-label"> @lang('points::site.name')</label>
                                                <input type="text" name="name" class="form-control"
                                                       value="{{ old('name') }}"
                                                       placeholder="@lang('points::site.name')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label"> @lang('points::site.point_of_order')</label>
                                                <input type="number" name="points" value="{{ old('points') }}"
                                                       class="form-control "
                                                       placeholder="@lang('points::site.point_of_order')">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label "> @lang('points::site.min_orders')</label>
                                                <input type="number" value="{{ old('min_order') }}" name="min_order"
                                                       class="form-control "
                                                       placeholder="@lang('points::site.min_orders')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('points::site.start_at')</label>
                                                <input type="text" name="from" value="{{ old('from') }}"
                                                       class="form-control date-datepicker"
                                                       placeholder="@lang('points::site.start_at')">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="form-label "> @lang('points::site.end_at')</label>
                                                <input type="text" value="{{ old('to') }}" name="to"
                                                       class="form-control date-datepicker"
                                                       placeholder="@lang('points::site.end_at')">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ">


                                        <div class="col-md-6 col-sm-12">
                                            <br/>
                                            <button id="button-submit" type="submit" class="btn btn-primary w-100">@lang('site.save')
                                            </button>
                                        </div>
                                        <div class="col-md-3"></div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--end row-->

                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')
    <script>
        $('#button-submit').on('click',function (e) {
            $(this).prop("disabled", true);
            // add spinner to button
            $(this).html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            document.getElementById('form-point').submit()
        })
    </script>
@endpush
