@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-header">
                    <h3>المناسبات والعروض</h3>
                </div>
                <div class="card-body">
                    <div class="d-lg-flex align-items-center mb-4 gap-3">
                        {{-- <div class="position-relative"> --}}
                        {{-- <input type="text" class="form-control ps-5 radius-30" placeholder="@lang('coupon::site.search')" value="{{request()->search}}"> --}}
                        {{-- <span  class="position-absolute top-50 product-show translate-middle-y"> --}}
                        {{-- <i class="bx bx-search"></i></span> --}}

                        {{-- </div> --}}
                        {{-- @if (auth()->user()->hasPermission('points-create')) --}}
                        <div class="ms-auto">
                            <a href="{{ route('admin.points.create') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i
                                    class="bx bxs-plus-square"></i>
                                إضافة مناسبة
                            </a>
                        </div>
                        {{-- @endif --}}

                    </div>
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="table-light">
                                <tr>

                                    <th>@lang('points::site.name')</th>
                                    <th>@lang('points::site.start_at')</th>
                                    <th>@lang('points::site.end_at')</th>
                                    <th>@lang('points::site.points')</th>
                                    <th>@lang('points::site.min_orders')</th>
                                    <th>@lang('points::site.status')</th>
                                    <th>@lang('coupon::site.actions')</th>



                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($points as $point)
                                    <tr>

                                        <td>{{ $point->name }}</td>

                                        <td>
                                            {{ Carbon\Carbon::parse($point->from)->toDateString() }}
                                        </td>
                                        <td>{{ Carbon\Carbon::parse($point->to)->toDateString() }}</td>
                                        <td>{{ $point->points }}</td>
                                        <td>{{ $point->min_order }}</td>
                                        <td>
                                            <div class="form-check form-switch col">
                                                <input class=" form-check-input"
                                                    id="flexSwitchCheckChecked{{ $point->id }}" type="checkbox"
                                                    onchange="point_toggle_status({{ $point->id }})"
                                                    {{ $point->status === 1 ? 'checked ' : '' }}>

                                            </div>
                                        </td>


                                        <td>
                                            <div class="d-flex order-actions">
                                                {{-- @if (auth()->user()->hasPermission('points-update')) --}}
                                                <a href="{{ route('admin.points.show', $point->id) }}"
                                                    class=""><i class="bx bxs-edit"></i></a>
                                                {{-- @endif --}}
                                                {{-- @if (auth()->user()->hasPermission('coupons-delete')) --}}
                                                {{-- <a href="javascript:;" class="ms-3"><i class="bx bxs-trash"></i></a> --}}
                                                {{-- @endif --}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br />
                    {{ $points->links('vendor.pagination.bootstrap-4', ['paginator' => $points]) }}
                </div>

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function point_toggle_status(id) {
            $.ajax({
                method: "POST",
                url: "{{ route('points.toggle-status') }}",
                data: {
                    occasion_id: id
                },
                success: function(data) {
                    success_noti('تم التعديل بنجاح')
                }
            });
        }
    </script>
@endpush
