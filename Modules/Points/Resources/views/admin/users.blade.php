@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <div class="card">
                <div class="card-header">
                    <h3>
                        @lang('points::site.users_points')
                    </h3>
                </div>
                <div class="card-body">
                    <div class="d-lg-flex align-items-center mb-4 gap-3">
                        {{-- <div class="position-relative"> --}}
                        {{-- <input type="text" class="form-control ps-5 radius-30" placeholder="@lang('coupon::site.search')" value="{{request()->search}}"> --}}
                        {{-- <span  class="position-absolute top-50 product-show translate-middle-y"> --}}
                        {{-- <i class="bx bx-search"></i></span> --}}

                        {{-- </div> --}}
                        {{-- @if (auth()->user()->hasPermission('points-create')) --}}
                        <div class="ms-auto">
                            <a href="{{ route('admin.points.create') }}" class="btn btn-primary radius-30 mt-2 mt-lg-0"><i
                                    class="bx bxs-plus-square"></i>
                                @lang('points::site.add_new_occasion')
                            </a>
                        </div>
                        {{-- @endif --}}

                    </div>

                    <div class="table-responsive">
                        @foreach ($points as $point)
                            <table class="table  table-bordered">
                                <thead class="table-light">
                                    <tr>
                                        <th>@lang('points::site.users')</th>
                                        <th>@lang('points::site.occasion')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <table class="table  table-bordered" style="width: 100%;">
                                                <thead class="table-light">
                                                    <tr>
                                                        <th>@lang('points::site.user_name')</th>
                                                        <th>@lang('points::site.point_count')</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if (isset($point->point->users))
                                                        @foreach ($point->point->users->groupBy('user_id') as $user)
                                                            @foreach ($user->groupBy('id') as $sub_user)
                                                                <tr>
                                                                    <td>
                                                                        {{ $sub_user[0]->name }}
                                                                    </td>
                                                                    <td>

                                                                        <h4>
                                                                            {{ $sub_user->count() * $point->point->points }}
                                                                        </h4>
                                                                    </td>

                                                                </tr>
                                                            @endforeach
                                                        @endforeach
                                                    @endif

                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="table  table-bordered" style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @lang('points::site.occasion_name')
                                                        </td>
                                                        <td>
                                                            {{ $point->point->name }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            stars at
                                                        </td>
                                                        <td>
                                                            {{ Carbon\Carbon::parse($point->point->from)->toDateString() }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @lang('points::site.start_at')
                                                        </td>
                                                        <td>
                                                            {{ Carbon\Carbon::parse($point->point->to)->toDateString() }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @lang('points::site.points')
                                                        </td>
                                                        <td>
                                                            {{ $point->point->points }}
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            @lang('points::site.min_orders')
                                                        </td>
                                                        <td>
                                                            {{ $point->point->min_order }}
                                                        </td>

                                                    </tr>


                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        @endforeach
                    </div>


                    <br />
                    {{-- {{ $points->links('vendor.pagination.bootstrap-4',['paginator'=>$points]) }} --}}
                </div>

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function point_toggle_status(id) {
            $.ajax({
                method: "POST",
                url: "{{ route('points.toggle-status') }}",
                data: {
                    occasion_id: id
                },
                success: function(data) {
                    success_noti('تم التعديل بنجاح')
                }
            });
        }
    </script>
@endpush
