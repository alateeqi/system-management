<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //orders routes
            Route::get('/points', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'index'])->name('admin.points.index');
            Route::get('/points/show/{id}', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'show'])->name('admin.points.show');
            Route::get('/points/users', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'pointsUsers'])->name('admin.points.users');
            Route::get('/points/create', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'create'])->name('admin.points.create');
            Route::post('/points', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'store'])->name('admin.points.store');
            Route::post('/points/update', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'update'])->name('admin.points.update');
            Route::post('/points/toggle-status', [\Modules\Points\Http\Controllers\Admin\AdminOccasionController::class, 'toggleStatus'])->name('points.toggle-status');

        });
    }
);
