<?php

namespace Modules\Product\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{
    use RecordsActivity,SoftDeletes;

    protected $fillable = [
        'product_id',
        'min_unite',
        'max_unite',
        'step',
        'price',
        'unite_name',
        'unite_description'
    ];

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }
        self::savingActivity();
        static::creating(function ($model) {
            $model->user_id = request()->user()->id;
        });

        static::updating(function ($model) {
            $model->editor_id = request()->user()->id;
        });
    }
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
}
