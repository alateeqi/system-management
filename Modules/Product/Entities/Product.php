<?php

namespace Modules\Product\Entities;

use App\Traits\RecordsActivity;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Taxonomy\Entities\Taxonomy;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


class Product extends Model implements HasMedia
{
    use Translatable, InteractsWithMedia, RecordsActivity, SoftDeletes;

    protected $translatedAttributes = ['title', 'excerpt', 'content', 'slug'];

    protected $with = [
        'prices'
    ];

    protected $appends = [
        'image',
        'fullimage',
        'mediumimage',
    ];

    protected $fillable = [
        'title',
        'excerpt',
        'content',
        'slug',
        'status',
        'user_id',
        'preparation'
    ];

    protected static function boot()
    {
        parent::boot();

        if (auth()->guest()) {
            return true;
        }
        self::savingActivity();

        static::creating(function ($model) {
            $model->user_id = request()->user()->id;
        });

        static::updating(function ($model) {
            $model->editor_id = request()->user()->id;
        });
    }

    public function registerMediaCollections(Media $media = null): void
    {
        $this->addMediaCollection('images');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('full')
            ->performOnCollections('images');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(450)
            ->performOnCollections('images');

        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(225)
            ->performOnCollections('images');
    }

    public function categories()
    {
        return $this->belongsToMany(Taxonomy::class, 'product_category', 'product_id', 'category_id');
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'thumbnail');
    }

    public function getFullimageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'full');
    }

    public function getMediumimageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'medium');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }


}
