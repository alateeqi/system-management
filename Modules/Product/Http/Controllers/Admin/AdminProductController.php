<?php

namespace Modules\Product\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\Admin\Filter;
use Modules\Product\Http\Requests\Admin\Store;
use Modules\Product\Http\Requests\Admin\ToggleStatus;
use Modules\Product\Http\Requests\Admin\Update;
use Modules\Product\Interfaces\Admin\AdminProductInterface;
use Modules\Taxonomy\Interfaces\Admin\Category\AdminCategoryInterface;


class AdminProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:products-read'])->only('index');
        $this->middleware(['permission:products-create'])->only('create');
        $this->middleware(['permission:products-update'])->only('edit');
        $this->middleware(['permission:products-delete'])->only('destroy');
    }

    public function index(AdminProductInterface $interface, AdminCategoryInterface $category_interface)
    {
        $products = $interface->index();
        $categories = collect($category_interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });
        return view('product::admin.index', compact('products', 'categories'));
    }

    public function setting(AdminProductInterface $interface, AdminCategoryInterface $category_interface)
    {
        $products_setting = $interface->indexSetting();
        $categories = collect($category_interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });
        return view('product::admin.setting', compact('products_setting', 'categories'));
    }

    public function filter(Filter $request, AdminProductInterface $interface, AdminCategoryInterface $category_interface)
    {

        $products = $interface->filter($request->validated());

        $categories = collect($category_interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });

        if ($request['index-view'] == '1') {
            return view('product::admin.index', compact('products', 'categories'));
        }
        else{
            $products_setting =$products;
            return view('product::admin.setting', compact('products_setting', 'categories'));
        }

//        return redirect()->to(redirect()->back()->getTargetUrl())->with(['products' => $products, 'categories' => $categories]);

    }

    public function create(AdminCategoryInterface $interface)
    {

        $categories = collect($interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });

        return view('product::admin.create', compact('categories'));
    }

    public function show(AdminCategoryInterface $interface_category, AdminProductInterface $interface, $id)
    {
        $categories = collect($interface_category->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });
        $product = $interface->find($id);

        return view('product::admin.show', compact('categories', 'product'));
    }


    public function store(Store $request, AdminProductInterface $interface)
    {


        try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();

            // ->withSuccess('Success');
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('products.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->route('products.create')->withErrors($exception->getMessage());
        }

    }

    public function update(Update $request, AdminProductInterface $interface)
    {


        try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('products.index');

        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }

    }

    public function toggleStatus(ToggleStatus $request, AdminProductInterface $interface)
    {
        return response()->json([
            'data' => $interface->toggleStatus($request->validated())
        ]);
    }

    public function togglePopular(ToggleStatus $request, AdminProductInterface $interface)
    {
        return response()->json([
            'data' => $interface->togglePopular($request->validated())
        ]);
    }

    public function toggleHome(ToggleStatus $request, AdminProductInterface $interface)
    {
        return response()->json([
            'data' => $interface->toggleHome($request->validated())
        ]);
    }

    public function delete(Product $product, AdminProductInterface $interface)
    {

        try {
            DB::beginTransaction();

            $interface->delete($product);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('products.index');

        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
