<?php

namespace Modules\Product\Http\Controllers\Guest;


use Modules\Product\Http\Requests\Guest\Search;
use Modules\Product\Interfaces\Guest\GuestProductInterface;

class GuestProductController
{
    public function index(GuestProductInterface $interface, $slug)
    {
        return $interface->index($slug);
    }

    public function search(Search $request, GuestProductInterface $interface)
    {
        return $interface->search($request->validated());
    }
    public function generalDeliveryPrice(GuestProductInterface $interface)
    {
        return $interface->getGeneralDeliveryPrice([
            'products_price' => 10,
            'gate_id' => 680
        ]);
    }

}
