<?php

namespace Modules\Product\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Filter extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'category_id' => ['nullable', 'numeric'],
            'query' => ['nullable', 'string'],
            'index-view' => ['required','numeric'],

        ];
    }
}
