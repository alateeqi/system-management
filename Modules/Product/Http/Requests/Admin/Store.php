<?php

namespace Modules\Product\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Taxonomy\Repositories\Admin\Category\AdminCategoryRepository;


class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'category_id' => ['required', 'exists:taxonomies,id'],
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
//            'content-ar' => ['required', 'max:255'],
//            'content-en' => ['required', 'max:255'],
//            'excerpt-ar' => ['required', 'max:255'],
//            'excerpt-en' => ['required', 'max:255'],
            'preparation' => 'required|between:0,99.99',
//            'home' => ['nullable'],
//            'popular' => ['nullable'],
            'selling_by_pieces' => ['nullable'],
            "price" => ['required', 'between:0.5,1000'],
//            'categories' => ['required'],
//            'categories.*' => ['in:' . AdminCategoryRepository::categoriesIDs()],
            'images' => ['required', 'array'],
            'images.*' => ['string'],
            'prices' => ['nullable', 'array'],
//            "prices.*.min_unite" => 'required|between:0.5,1000',
//            "prices.*.max_unite" => 'required|between:0.5,1000',
//            "prices.*.step" => 'required|between:0.5,1000',
            "prices.*.price" => 'nullable|between:0.5,1000',
            "prices.*.unite_name" => 'nullable',
            "unite_name" => 'nullable',
//            "prices.*.unite_description" => 'nullable',

        ];
    }
}
