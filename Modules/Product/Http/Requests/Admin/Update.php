<?php

namespace Modules\Product\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'product_id' => ['required', 'exists:products,id'],
            'category_id' => ['required', 'exists:taxonomies,id'],
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
            'preparation' => 'required|between:0,99.99',
//            'home' => ['nullable'],
//            'popular' => ['nullable'],

            'images_old_deleted' => ['nullable', 'array'],
            'images_old_deleted.*' => ['nullable', 'numeric', 'exists:media,id'],

            'images' => ['nullable', 'array'],
            'images.*' => ['nullable', 'string'],

            'prices' => ['required', 'array'],
            "prices.*.price" => 'required|between:0.5,1000',
            "prices.*.unite_name" => 'nullable',

        ];
    }
}

