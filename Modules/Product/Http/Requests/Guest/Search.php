<?php

namespace Modules\Product\Http\Requests\Guest;

use Illuminate\Foundation\Http\FormRequest;

class Search extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'query' => ['required', 'max:255'],
        ];
    }
}
