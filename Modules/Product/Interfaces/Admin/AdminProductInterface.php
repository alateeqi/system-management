<?php

namespace Modules\Product\Interfaces\Admin;

use Modules\Product\Entities\Product;

interface AdminProductInterface
{
    public function index($paginate = 20);

    public function indexSetting($paginate = 20);

    public static function allProduct();

    public function filter($data);

    public function find($id);

    public function store($data);

    public function update($data);

    public function toggleStatus($data);

    public function togglePopular($data);

    public function toggleHome($data);

    public function delete(Product $product);

}
