<?php

namespace Modules\Product\Interfaces\Admin\Price;

use Modules\Product\Entities\Product;

interface AdminPriceInterface
{
    public function store(Product $product, $data);

    public function find($id);
}
