<?php

namespace Modules\Product\Interfaces\Guest;

interface GuestProductInterface
{

    public function index($slug);

    public function search($data);

    public function popularProducts();

    public function homeProducts();

    public function getGeneralDeliveryPrice($order);
}
