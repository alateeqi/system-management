<?php

namespace Modules\Product\Interfaces;

interface ProductsInterface

{

    public function index ( $paginate = 15);

    public function store($data);
    
}
