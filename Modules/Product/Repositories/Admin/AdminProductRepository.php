<?php

namespace Modules\Product\Repositories\Admin;

use App\Helper\ImageHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Auth\Entities\User;
use Modules\Product\Entities\Price;
use Modules\Product\Entities\Product;
use Modules\Product\Interfaces\Admin\AdminProductInterface;
use Modules\Product\Repositories\Admin\Price\AdminPriceRepository;
use phpDocumentor\Reflection\Types\True_;

class AdminProductRepository implements AdminProductInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function allProduct()
    {
        return Product::all();
    }

    public function index($paginate = 20)
    {
        $products = Cache::remember('products-page-' . request('page', 1), 60 * 60, function () {
            return Product::orderBy('id', 'desc')->paginate();
        });
//        return Product::orderBy('id', 'desc')->paginate($paginate);
//        return Price::orderBy('id', 'desc')->paginate($paginate);
        return $products;
    }

    public function indexSetting($paginate = 20)
    {
        $products = Cache::remember('products-setting-page-' . request('page', 1), 60 * 60, function () {
            return Product::orderBy('id', 'desc')->paginate();
        });

        return $products;
    }

    public function filter($data)
    {
        $products = Product::orderBy('id', 'desc');
        $params = null;

        if (isset($data['query'])) {
            $params .= '?query=' . $data['query'];
            $products = $products->whereHas('translations', function ($query) use ($data) {
                $query->where('title', 'like', '%' . $data['query'] . '%');
            });
        }
        if (isset($data['category_id']) && $data['category_id'] != 0) {
            if ($params != null)
                $params .= '&category_id=' . $data['category_id'];
            else $params .= '?category_id=' . $data['category_id'];
            $products = $products->whereHas('categories', function ($q) use ($data) {
                return $q->where('category_id', $data['category_id']);
            });
        }
        $products = $products->paginate(20);
        $products->setPath(url()->current() . $params);
        return $products;
    }

    public function find($id)
    {
        return Product::find($id);
    }

    public static function findStatic($id)
    {
        return Product::find($id);
    }

    private function clearCache()
    {
        if (Cache::has('all_categories_with_products'))
            Cache::forget('all_categories_with_products');
        for ($i = 1; $i < 100; $i++) {
            $key = 'products-page-' . $i;
            if (Cache::has($key))
                Cache::forget($key);
        }
    }

    public function store($data)
    {

        $this->clearCache();
        $new_product = new Product();
        $new_product->preparation = $data['preparation'];
        $new_product->home = 0;
//        if (isset($data['popular']))
//            $new_product->popular = 1;
//        else
        $new_product->popular = 0;


        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_product->translateOrNew($key)->title = $data['title-' . $key];
//            $new_product->translateOrNew($key)->excerpt = $data['excerpt-' . $key];
//            $new_product->translateOrNew($key)->content = $data['content-' . $key];

        }
        $new_product->save();

        $new_product->categories()->sync($data['category_id']);
        if (!isset($data['prices']) && isset($data['selling_by_pieces'])) {

            $data['prices'][] = ['price' => $data['price'],
                'unite_name' => 'unite'];
            (new AdminPriceRepository($this->user))->store($new_product, $data['prices']);


        } else if (isset($data['prices']) && !isset($data['selling_by_pieces'])) {

            $data['prices'][] = ['price' => $data['price'],
                'unite_name' => $data['unite_name']];
            (new AdminPriceRepository($this->user))->store($new_product, $data['prices']);
        } else if (!isset($data['prices']) && !isset($data['selling_by_pieces'])) {
            $data['prices'][] = [
                'price' => $data['price'],
                'unite_name' => $data['unite_name']];
            (new AdminPriceRepository($this->user))->store($new_product, $data['prices']);

        }


        foreach ($data['images'] as $file) {

            $new_product->addMediaFromBase64($file)->toMediaCollection('images');
        }
        $new_product->save();
        return $new_product;
    }

    public function update($data)
    {

        $this->clearCache();
        $product = $this->find($data['product_id']);
        $product->preparation = $data['preparation'];
        $product->home = 0;
//        if (isset($data['popular']))
//            $product->popular = 1;
//        else
        $product->popular = 0;


        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $product->translateOrNew($key)->title = $data['title-' . $key];
//            $product->translateOrNew($key)->excerpt = $data['excerpt-' . $key];
//            $product->translateOrNew($key)->content = $data['content-' . $key];

        }
        $product->save();

        $product->categories()->sync($data['category_id']);

        if (isset($data['images_old_deleted'])) {
            for ($i = 0; $i < count($data['images_old_deleted']); $i++) {
                $product->deleteMedia($data['images_old_deleted'][$i]);
            }
        }
        (new AdminPriceRepository($this->user))->update($product, $data['prices']);

        if (isset($data['images'])) {
            foreach ($data['images'] as $file) {

                $product->addMediaFromBase64($file)->toMediaCollection('images');
            }
            $product->save();
        }

        return $product;
    }

    public function toggleStatus($data)
    {

        $model = $this->find($data['product_id']);

        $model->status = !$model->status;
        $model->save();
        $this->clearCache();
        return $model;
    }

    public function togglePopular($data)
    {

        $model = $this->find($data['product_id']);
        if ($model->popular == 0) {
            $model->popular = 1;
        } else
            $model->popular = 0;
        $model->save();
        $this->clearCache();
        return $model;
    }

    public function toggleHome($data)
    {

        $model = $this->find($data['product_id']);
        if ($model->home == 0) {
            $model->home = 1;
        } else
            $model->home = 0;
        $model->save();
        $this->clearCache();
        return $model;
    }

    public function delete(Product $product)
    {
        $product->delete();
        $this->clearCache();
    }


}
