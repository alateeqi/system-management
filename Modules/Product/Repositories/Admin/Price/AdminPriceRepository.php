<?php

namespace Modules\Product\Repositories\Admin\Price;

use Modules\Auth\Entities\User;
use Modules\Product\Entities\Price;
use Modules\Product\Entities\Product;
use Modules\Product\Interfaces\Admin\Price\AdminPriceInterface;

class AdminPriceRepository implements AdminPriceInterface
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function find($id)
    {
        return Price::find($id);
    }

    public function all_prices()
    {
        return Price::all();

    }

    public function store(Product $product, $prices)
    {

        $registeredPrices = [];
        for ($i = 0; $i < count($prices); $i++) {
            $price = new Price();

            $price->product_id = $product->id;
            $price->min_unite = 1;
            $price->max_unite = 100;
            $price->step = 1;
            $price->price = $prices[$i]['price'];
            $price->unite_name = $prices[$i]['unite_name'];
            $price->unite_description = '';
            $price->save();
            $registeredPrices[] = $price->id;
        }
//        foreach ($prices as $price=>$unite_name) {
//
//            $price_obj = new Price();
//
//            $price_obj->product_id = $product->id;
//            $price_obj->min_unite = 0;
//            $price_obj->max_unite = 100;
//            $price_obj->step = 1;
//            $price_obj->price = $price;
//            $price_obj->unite_name = $requestedPrice['unite_name'];
//            $price_obj->unite_description = '';
//            $price_obj->save();
//            $registeredPrices[] = $price_obj->id;
//        }
        foreach ($product->prices()->whereNotIn('id', $registeredPrices)->get() as $deletedPrice) {
            $this->destroy($deletedPrice);
        }

    }

    public function update(Product $product, $prices)
    {

        $registeredPrices = [];
        foreach ($prices as $price ) {

            if (isset($price['id'])) {
                $price_obj = $this->find($price['id']);
            } else $price_obj = new Price();

            $price_obj->product_id = $product->id;
            $price_obj->min_unite = 1;
            $price_obj->max_unite = 100;
            $price_obj->step = 1;
            $price_obj->price = $price['price'];
            $price_obj->unite_name = $price['unite_name'];
            $price_obj->unite_description = '';
            $price_obj->save();
            $registeredPrices[] = $price_obj->id;
        }

//        for ($i = 0; $i < count($prices); $i++) {
//
//            if (isset($prices[$i]['id'])) {
//                $price = $this->find($prices[$i]['id']);
//            } else $price = new Price();
//            dd($prices[$i]);
//            $price->product_id = $product->id;
//            $price->min_unite = 0;
//            $price->max_unite = 100;
//            $price->step = 1;
//            $price->price = $prices[$i]['price'];
//            $price->unite_name = $prices[$i]['unite_name'];
//            $price->unite_description = '';
//            $price->save();
//            $registeredPrices[] = $price->id;
//        }


        foreach ($product->prices()->whereNotIn('id', $registeredPrices)->get() as $deletedPrice) {
            $this->destroy($deletedPrice);
        }

    }

    private function destroy(Price $price)
    {
        $price->deleted_at = now();
        $price->save();
    }
}
