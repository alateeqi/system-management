<?php

namespace Modules\Product\Repositories\Guest;

use Illuminate\Support\Facades\Log;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderProduct;
use Modules\Product\Entities\Product;
use Modules\Product\Interfaces\Guest\GuestProductInterface;
use Modules\Taxonomy\Repositories\Guest\DeliveryPrice\GuestDeliveryPriceRepository;

class GuestProductRepository implements GuestProductInterface
{
    public function index($slug)
    {
        $product = Product::whereHas('translations', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->firstOrFail();

        $similar = $product->categories()->first()->products()
            ->whereRaw('products.id != ' . $product->id)
            ->inRandomOrder()
            ->limit(10)
            ->get();

        return [
            'similar' => $similar,
            'product' => $product
        ];
    }

    public function search($data)
    {
        $products = Product::limit(8)->inRandomOrder();
        if ($data['query']) {
            $products = $products->whereHas('translations', function ($query) use ($data) {
                $query->where('title', 'like', '%' . $data['query'] . '%');
            });
        }

        return $products->get();
    }

    public function popularProducts()
    {
        return Product::where('popular', '>', 0)->orderBy('popular', 'asc')->get();
    }

    public function homeProducts()
    {
        return Product::where('home', '>', 0)->orderBy('home', 'asc')->get();
    }

    public function productsAllCategories()
    {
        return Product::inRandomOrder()
            ->limit(10)
            ->get();
    }

    public function find($id)
    {
        return Product::findOrFail($id);
    }

    public function productsData($order)
    {
        $total = 0;
        $productsPrice = 0;
        $products = [];
        foreach ($order['products'] as $id => $quantity) {
            $product = $this->find($id);
            foreach ($quantity as $quantityIndex => $quantityValue) {
                $quantityIndex = str_replace('quantityIndex', '', $quantityIndex);
                $price = $quantityValue['quantity'] * $product->prices->find($quantityIndex)->price;
                $products[] = [
                    $id => [
                        'price' => $price,
                        'price_id' => $quantityIndex,
                        'quantity' => $quantityValue['quantity'],
                    ]
                ];
                $productsPrice += $price;
                $total += $price;
            }
        }

        if ($percentage = floatval($order['discount_percentage'] ?? 0)) {
            $total -= $total * ($percentage / 100);
        }

        $total -= floatval($order['discount'] ?? 0);

        // if payment method is not Pick up
        if (!$this->isPickUp($order['gate']['id'])) {
            $total += $this->getDeliveryPrice([
                'products_price' => $productsPrice,
                'gate_id' => $order['gate']['id'],
                'address' => [
                    'city' => [
                        'excerpt' => $order['address']['city']['excerpt']
                    ]
                ]
            ]);
        }

        if ($total < 0) {
            $total = 0;
        }

        return [
            'totalPrice' => $total,
            'productsPrice' => $productsPrice,
            'products' => $products,
        ];
    }


    public function getDeliveryPrice($order)
    {
        $generalDeliveryPrice = $this->getGeneralDeliveryPrice($order);
        if ($generalDeliveryPrice === null) {
            Log::info("order city excerpt " . $order['address']['city']['excerpt'], []);

            return floatval($order['address']['city']['excerpt']);
        } else {
            return $generalDeliveryPrice;
        }
    }

    public function getGeneralDeliveryPrice($order)
    {
        $websiteDeliveryPrice = GuestDeliveryPriceRepository::websiteDeliveryPrice();
        $systemDeliveryPrice = GuestDeliveryPriceRepository::systemDeliveryPrice();
        $isSunOrMon = (date('D') == 'Sun' || date('D') == 'Mon') && GuestDeliveryPriceRepository::isSunOrMon() != null;

        if ($websiteDeliveryPrice->status !== 1 && $systemDeliveryPrice->status !== 1 && !$isSunOrMon) {
            return null;
        }

        if ($order['products_price'] < 7) {
            return 2;
        }

        if ($this->isWebsiteOrder($order['gate_id'])) {
            if ($websiteDeliveryPrice->status === 1) {
                return floatval($websiteDeliveryPrice->excerpt);
            }
        } else {
            if ($systemDeliveryPrice->status === 1) {
                return floatval($systemDeliveryPrice->excerpt);
            }
        }

        if ($isSunOrMon) {
            return floatval(optional(GuestDeliveryPriceRepository::isSunOrMon())->excerpt) ?? 1;
        }

        return null;
    }

    public function storeProducts(Order $order, $products)
    {
        $order->products()->delete();
        foreach ($products as $product) {
            foreach ($product as $id => $data) {
                $orderProduct = new OrderProduct();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $id;
                $orderProduct->price_id = $data['price_id'];
                $orderProduct->price = $data['price'];
                $orderProduct->quantity = $data['quantity'];
                $orderProduct->save();
            }
        }
    }

    public function isPickUp($gateId)
    {
        return in_array($gateId, [681]);
    }

    public function isWebsiteOrder($gateId)
    {
        return in_array($gateId, [680]);
    }
}
