<?php

namespace Modules\Product\Repositories;

use Modules\Product\Entities\Product;
use Modules\Auth\Entities\User;
use Modules\product\Interfaces\ProductsInterface;


class ProductRepository implements ProductsInterface  
{
    private $user;
    public function __construct(User $user)
    {
    $this->user=$user;   
    }
public function index($paginate = 15 ){

        // if ($slug != 'all') {

        //     $category = Taxonomy::categories()
                // id 570 for Carrot Co
                // id 573 for Plates
        //                         ->whereNotIn('id', [570, 573])
        //                         ->with([
        //                             'products' => function ($p) {
        //                                 $p->where('status', '=', 1);
        //                             }
        //                         ])
        //                         ->whereHas('translations',
        //                             function ($query) use ($slug) {
        //                                 $query->where('slug', $slug);
        //                             })->firstOrFail();
        // } else {
        //     $category = (object)[
        //         'title' => 'All Categories',
        //         'products' => Product::inRandomOrder()
        //                             ->limit(10)
        //                             ->get()
        //     ];
        // }
        
        $products = Product::all()
            // id 570 for Carrot Co
            // id 573 for Plates 
            ->whereNotIn('id', [570, 573])->active()->orderBy('order')->paginate($paginate);
            
            return  $products;

}

    public function store($data){

    }  
}
