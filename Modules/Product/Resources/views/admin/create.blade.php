@extends('layouts.dashboard.app')
@section('title')
    @lang('product::site.products')
@endsection

@section('content')

    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('product::site.products')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-4">
                    <h5 class="card-title">Add New Product</h5>
                    <hr/>
                    <form class=" mt-4" action="{{route('products.store')}}" id="form-product" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputProductTitle" class="form-label">Product Title
                                                    AR</label>
                                                <input type="text" name="title-ar" class="form-control"
                                                       id="inputProductTitle"
                                                       placeholder="Enter product title Arabic">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputProductTitleEN" class="form-label">Product Title
                                                    EN</label>
                                                <input type="text" class="form-control" name="title-en"
                                                       id="inputProductTitleEN"
                                                       placeholder="Enter product title English">
                                            </div>
                                        </div>
                                    </div>
                                    {{--                                    <div class="row col-12">--}}
                                    {{--                                        <div class="col-6">--}}
                                    {{--                                            <div class="mb-3">--}}
                                    {{--                                                <label for="inputProductContentAR" class="form-label">Product Content--}}
                                    {{--                                                    AR</label>--}}
                                    {{--                                                <textarea class="form-control"--}}
                                    {{--                                                          name="content-ar"--}}
                                    {{--                                                          id="inputProductContentAR"--}}
                                    {{--                                                          rows="3"--}}
                                    {{--                                                          placeholder="Enter product content Arabic"></textarea>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-6">--}}
                                    {{--                                            <div class="mb-3">--}}
                                    {{--                                                <label for="inputProductContentEN" class="form-label">Product Content--}}
                                    {{--                                                    EN</label>--}}
                                    {{--                                                <textarea class="form-control"--}}
                                    {{--                                                          name="content-en"--}}
                                    {{--                                                          id="inputProductContentEN"--}}
                                    {{--                                                          rows="3"--}}
                                    {{--                                                          placeholder="Enter product content English"></textarea>--}}

                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="row col-12">--}}
                                    {{--                                        <div class="col-6">--}}
                                    {{--                                            <div class="mb-3">--}}
                                    {{--                                                <label for="inputProductExcerpt" class="form-label">Product Excerpt--}}
                                    {{--                                                    AR</label>--}}
                                    {{--                                                <input type="text" name="excerpt-ar" class="form-control"--}}
                                    {{--                                                       id="inputProductExcerpt"--}}
                                    {{--                                                       placeholder="Enter product Excerpt Arabic">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-6">--}}
                                    {{--                                            <div class="mb-3">--}}
                                    {{--                                                <label for="inputProductExcerptEN" class="form-label">Product Excerpt--}}
                                    {{--                                                    EN</label>--}}
                                    {{--                                                <input type="text" class="form-control" name="excerpt-en"--}}
                                    {{--                                                       id="inputProductExcerptEN"--}}
                                    {{--                                                       placeholder="Enter product Excerpt English">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    <div class="row col-12">

                                                                                <div class="col-6">
                                                                                    <div class="mb-3">
                                                                                        <label for="inputProductPreparation " class="form-label">Product
                                                                                            Preparation</label>
                                                                                        <input type="number" name="preparation" class="form-control"
                                                                                               id="inputProductPreparation"
                                                                                               placeholder="Enter product preparation">
                                                                                    </div>
                                                                                </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputPrice" class="form-label">Price</label>
                                                <input type="number"
                                                       class="form-control"
                                                       name="price"
                                                       id="inputPrice"
                                                       placeholder="Price">

                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
{{--                                                <label for="inputProductHome" class="form-label">Product Home</label>--}}
{{--                                                <input type="number" class="form-control" name="home"--}}
{{--                                                       id="inputProductHome"--}}
{{--                                                       placeholder="Enter product Home ">--}}
                                                <label class="form-label">Category</label>
                                                <select class="single-select"
                                                        name="category_id"
                                                        data-placeholder="Choose anything"
                                                >
                                                    @for($category =0;$category<count($categories);$category++)
                                                        <option
                                                            value="{{$categories[$category]['id']}}">{{$categories[$category]['title']}}</option>
                                                    @endfor

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="inputProductDescription" class="form-label">Product Images</label>
                                        <input id="image-uploadify"
                                               type="file"
                                               accept="image/*"
                                               multiple>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row g-3">

{{--                                        <div class="col-md-12">--}}

{{--                                            <input type="checkbox"--}}
{{--                                                   name="popular"--}}
{{--                                                   id="inputPopular"--}}
{{--                                                   placeholder="Popular">--}}
{{--                                            <label for="inputPopular" class="form-label">Popular</label>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-12">--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Category</label>--}}
{{--                                                <select class="single-select"--}}
{{--                                                        name="category_id"--}}
{{--                                                        data-placeholder="Choose anything"--}}
{{--                                                >--}}
{{--                                                    @for($category =0;$category<count($categories);$category++)--}}
{{--                                                        <option--}}
{{--                                                            value="{{$categories[$category]['id']}}">{{$categories[$category]['title']}}</option>--}}
{{--                                                    @endfor--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-md-6">

                                            <input type="checkbox"
                                                   name="selling_by_pieces"
                                                   id="selling_by_pieces"

                                                   checked>
                                            <label for="selling_by_pieces" class="form-label">selling by unite</label>
                                        </div>
                                        <div class="col-md-6" id="selling_by_pieces_div" style="display: none">
                                            <label for="selling_by_pieces" class="form-label">selling by </label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="unite_name"
                                                   placeholder="unite name">

                                        </div>


                                        @include('product::admin.partials.price_form')
                                        <div class="hidden-input-images"></div>
                                        <div class="col-12">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-primary" id="submit-form">@lang('site.save')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end row-->
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
@push('js')
    <script>
        $('#submit-form').on('click', function (e) {
            e.preventDefault();
            $(this).prop("disabled", true);
            // add spinner to button
            $(this).html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $('.imageuploadify-container img').each(function () {
                $('.hidden-input-images').append(
                    '<input type="hidden" name="images[]" value="' + $(this).attr('src') + '">'
                )
            });
            if ($('#selling_by_pieces_div').is(':visible')) {
                if ($('#selling_by_pieces_div input').val() === '') {
                    error_noti('ادخل اسم المبيع')
                } else document.getElementById('form-product').submit();
            } else document.getElementById('form-product').submit();
        })

        $('#selling_by_pieces').on('change', function (e) {
            e.preventDefault();

            if ($('#selling_by_pieces_div').is(':visible')) {
                $('#selling_by_pieces').val('false')
                $('#selling_by_pieces_div').fadeOut()
                $('#add-price').fadeOut()
            } else {
                $('#selling_by_pieces').val('true')
                $('#selling_by_pieces_div').fadeIn()
                $('#add-price').fadeIn()
            }
        })


    </script>
@endpush

