@extends('layouts.dashboard.app')
@section('title')
    @lang('product::site.products')
@endsection
@push('css')
    <style>
        .product-box {
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            color: #ffffff;
            margin-bottom: 1em;
        }

        .product-box .product-overlay {
            background: rgba(0, 0, 0, 0.2);
            margin: 0 !important;
            border: 1px solid transparent;
            min-height: 160px;
        }
    </style>
@endpush
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('product::site.products')</div>

                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <form action="{{route('products.filter')}}" class="form-group" method="GET">
                    <div class="card-title d-flex align-items-start">

                        <input type="hidden" name="index-view" value="1">
                        <div class="col-md-3 m-2">
                            <input type="search" name="query"
                                   class="form-control mt-2 mx-3  radius-30"
                                   value="{{request('query')}}"
                                   placeholder="@lang('product::site.search')">
                        </div>


                        <div class="col-md-4 m-3">
                            <select class="single-select "
                                    name="category_id"

                                    data-placeholder="Choose anything">
                                <option value="0"> Select Category</option>
                                @for($category =0;$category<count($categories);$category++)
                                    <option
                                        value="{{$categories[$category]['id']}}" {{$categories[$category]['id'] == request('category_id')?'selected':''}}>{{$categories[$category]['title']}}</option>
                                @endfor

                            </select>
                        </div>


                        <div class="col-md-2 m-2">

                            <button type="submit"
                                    class="p-2 btn btn-primary  btn-sm  mt-2 mx-2  radius-20 "
                                    href="#">Search
                            </button>

                        </div>
                        <div class="col-md-4 m-2">
                            @if (auth()->user()->hasPermission('products-create'))
                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                   href="{{ route('products.create') }}">@lang('product::site.add_new')<i
                                        class="lni lni-circle-plus mx-1"></i></a>
                            @else
                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30 disabled"
                                   href="#">@lang('product::site.add_new')<i class="lni lni-circle-plus mx-1 "></i></a>

                            @endif
                        </div>


                        {{-- end of form --}}
                    </div>
                </form>

            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 row-cols-xl-4 row-cols-xxl-5 product-grid">
                @foreach ($products as $index => $product)
                    <div class="col">
                        <div class="card">
                            <div class="card-body">

                                <a href="{{route('products.show',$product->id)}}">
                                    <div class="product-box"
                                         style="background-image: url('{{$product->getFirstMediaUrl('images', 'thumbnail')}}');">
                                        <div class="small-box product-overlay"
                                             id="product-father-selected{{$product->id}}">
                                            <div class="inner">
                                                <h5 class="product-title  text-white">
                                                    {{$product->title}}
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <h6 class="card-title cursor-pointer">{{$product->prices()->first()->price }}
                                        KWD</h6>
                                    <div class="row">
                                        <a href="#" class="text-dark col-7" data-bs-toggle="modal"
                                           data-bs-target="#exampleVerticallycenteredModal{{ $product->id }}"> <i
                                                class="bx bxs-trash"
                                                style="color: #ff6b6b;"></i>@lang('taxonomy::site.delete')</a>
                                        <div class="form-check form-switch col">
                                            <input class=" form-check-input"
                                                   id="flexSwitchCheckChecked{{ $product->id }}"
                                                   type="checkbox" onchange="product_toggle_status({{ $product->id }})"
                                                {{ $product->status === 1 ? 'checked ' : '' }}>
                                            <label id="label-toggle{{$product->id}}" class="form-label"
                                                   for="flexSwitchCheckChecked{{ $product->id }}">{{ $product->status === 1 ? 'OFF ' : 'ON' }}</label>
                                        </div>
                                    </div>


                                </a>


                            </div>
                        </div>
                    </div>
                    <div class="modal fade"
                         id="exampleVerticallycenteredModal{{ $product->id }}" tabindex="-1"
                         aria-hidden="true">
                        <form action="{{ route('products.delete', $product->id) }}" method="POST"
                              style="display:inline-block;">
                            @csrf
                            @method('DELETE')
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                        <button type="button" class="btn-close"
                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">

                                        @lang('taxonomy::site.confirm_the_deletion')
                                        <input type="hidden" name="" value="{{ $product->id }}"
                                               id="">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">@lang('taxonomy::site.cancel')</button>
                                        <button type="submit"
                                                class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endforeach
                <br/>

            </div>
            {{ $products->links('vendor.pagination.bootstrap-4',['paginator'=>$products]) }}
        </div>

    </div>
@endsection
@push('js')
    <script>
        function product_toggle_status(id) {
            $.ajax({
                method: "POST",
                url: "{{route('products.toggle-status')}}",
                data: {
                    product_id: id
                },
                success: function (one, two, three) {
                    if ($('#label-toggle' + id).text() == 'ON')
                        $('#label-toggle' + id).text('OFF')
                    else $('#label-toggle' + id).text('ON')
                    success_noti('تم التعديل بنجاح')
                }
            });
        }


    </script>
@endpush
