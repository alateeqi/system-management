<div class="col-12">
    <div class="row" id="prices">

    </div>
    <div class="row">
        <div class="col-12">
            <button class="btn btn-primary" type="button" id="add-price" >Add Size
                <i class="lni lni-circle-plus mx-1"></i>
            </button>
        </div>

    </div>
</div>
@push('js')
    <script>
        $(document).ready(function (e) {

            arr_price =
            @json($product->prices)

            // if (arr_price.length > 1) {
                // $('#selling_by_pieces').prop('checked', false)
                // $('#selling_by_pieces_div').fadeIn()
                for (var i = 0; i < arr_price.length; i++) {
                    $('#prices').append(
                        '<div class="col-12" id="price-item' + i + '">' +
                        '<button type="button" class="btn btn-danger btn-sm float-right" id="remove-price' + i + '">X</button>' +
                        '<div class="card card card-default w-100">' +
                        '<div class="card-header">' +
                        '<h3 class="card-title"> Size ' + (i + 1) + '</h3>' +
                        '  </div>' +
                        '   <div class="card-body">' +
                        '   <div class="row">' +
                        '    <div class="col-6">' +
                        ' <div data-input-name="price' + i + '" class="form-group"><label for="price' + i + '">Price</label>' +
                        '  <div class="input-group"><input type="number" name="prices[' + i + '][price]" id="price' + i + '" placeholder="Price" value="' + arr_price[i].price + '" class="form-control "></div>' +
                        ' <input type="hidden" name="prices[' + i + '][id]" id="id-price' + i + '"  value="'+arr_price[i].id+'"  ">' +
                        ' </div>' +
                        ' </div>' +
                        ' <div class="col-6">' +
                        ' <div data-input-name="unite_name' + i + '" class="form-group"><label for="unite_name' + i + '">Unite Name</label>' +
                        ' <div class="input-group"><input type="text" name="prices[' + i + '][unite_name]" id="unite_name' + i + '" placeholder="Unite Name" value="' + arr_price[i].unite_name + '" class="form-control "></div>' +
                        ' </div>' +
                        '  </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        ' </div>');
                    $('#remove-price' + i).on('click', function (e) {
                        e.preventDefault()
                        // arr_price.splice(i, 1);

                        var div = this.parentElement;
                        // div.style.display = "none";
                        div.remove();

                    });
                }

            // } else if (arr_price[0].unite_name !== 'unite') {
            //     $('#selling_by_pieces').prop('checked', false)
            //     $('#selling_by_pieces_div').fadeIn()
            //     $('#unite_name').val(arr_price[0].unite_name)
            //
            // }

            $('#add-price').on('click', function (e) {
                e.preventDefault()

                var count = arr_price.length;
                arr_price.push({
                    id: count
                });

                for (var i = count; i < arr_price.length; i++) {
                    $('#prices').append(
                        '<div class="col-12" id="price-item' + i + '">' +
                        '<button type="button" class="btn btn-danger btn-sm float-right" id="remove-price' + i + '">X</button>' +
                        '<div class="card card card-default w-100">' +
                        '<div class="card-header">' +
                        '<h3 class="card-title"> Size ' + (i + 1) + '</h3>' +
                        '  </div>' +
                        '   <div class="card-body">' +
                        '   <div class="row">' +
                        '    <div class="col-6">' +
                        ' <div data-input-name="price' + i + '" class="form-group"><label for="price' + i + '">Price</label>' +
                        '  <div class="input-group"><input type="number" name="prices[' + i + '][price]" id="price' + i + '" placeholder="Price" value="" class="form-control "></div>' +
                        ' </div>' +
                        ' </div>' +
                        ' <div class="col-6">' +
                        ' <div data-input-name="unite_name' + i + '" class="form-group"><label for="unite_name' + i + '">Unite Name</label>' +
                        ' <div class="input-group"><input type="text" name="prices[' + i + '][unite_name]" id="unite_name' + i + '" placeholder="Unite Name" value="" class="form-control "></div>' +
                        ' </div>' +
                        '  </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        ' </div>');
                    $('#remove-price' + i).on('click', function (e) {
                        e.preventDefault()
                        // arr_price.splice(i, 1);

                        var div = this.parentElement;
                        // div.style.display = "none";
                        div.remove();

                    });
                }
            });

        })

    </script>


@endpush
