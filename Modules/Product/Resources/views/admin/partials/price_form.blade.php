<div class="col-12">
    <div class="row" id="prices">

    </div>
    <div class="row">
        <div class="col-12">
            <button class="btn btn-primary" type="button" id="add-price" style="display: none">Add Size
                <i class="lni lni-circle-plus mx-1"></i>
            </button>
        </div>

    </div>
</div>
@push('js')
    <script>

        arr_price = [];


        $('#add-price').on('click', function (e) {
            e.preventDefault()
            var count = arr_price.length;
            arr_price.push({
                id: count
            });
            for (var i = count; i < arr_price.length; i++) {
                $('#prices').append(
                    '<div class="col-12" id="price-item' + i + '">' +
                    '<button type="button" class="btn btn-danger btn-sm float-right" id="remove-price' + i + '">X</button>' +
                    '<div class="card card card-default w-100">' +
                    '<div class="card-header">' +

                    '<h3 class="card-title"> Size ' + (i + 1) + '</h3>' +

                    '  </div>' +
                    '   <div class="card-body">' +
                    '   <div class="row">' +
                    // '  <div class="col-lg-4">' +
                    // '<div data-input-name="min_unite' + i + '" class="form-group"><label for="min_unite' + i + '">Min Unite</label><div class="input-group">' +
                    // '<input type="number" name="prices['+ i +'][min_unite]" id="min_unite' + i + '" placeholder="Min Unite" value="" class="form-control ">' +
                    // '</div>' +
                    // '</div>' +
                    // ' </div>' +
                    // ' <div class="col-lg-4">' +
                    // ' <div data-input-name="max_unite' + i + '" class="form-group">' +
                    // '<label for="max_unite' + i + '">Max Unite</label><div class="input-group">' +
                    // '<input type="number" name="prices['+ i +'][max_unite]" id="max_unite' + i + '" placeholder="Max Unite" value="" class="form-control "></div></div>' +
                    // '</div>' +
                    // '   <div class="col-lg-4">' +
                    // ' <div data-input-name="step' + i + '" class="form-group"><label for="step' + i + '">Step</label>' +
                    // ' <div class="input-group"><input type="text" name="prices['+ i +'][step]" id="step' + i + '" placeholder="Step" value="" class="form-control "></div></div>' +
                    // '</div>' +
                    '    <div class="col-6">' +
                    ' <div data-input-name="price' + i + '" class="form-group"><label for="price' + i + '">Price</label>' +
                    '  <div class="input-group"><input type="number" name="prices['+ i +'][price]" id="price' + i + '" placeholder="Price" value="" class="form-control "></div>' +
                    ' </div>' +
                    ' </div>' +
                    ' <div class="col-6">' +
                    ' <div data-input-name="unite_name' + i + '" class="form-group"><label for="unite_name' + i + '">Unite Name</label>' +
                    ' <div class="input-group"><input type="text" name="prices['+ i +'][unite_name]" id="unite_name' + i + '" placeholder="Unite Name" value="" class="form-control "></div>' +
                    ' </div>' +
                    '  </div>' +
                    // ' <div class="col-lg-12">' +
                    // ' <div data-input-name="prices['+ i +'][unite_description]" class="form-group"><label for="unite_description' + i + '">Unite Description</label>' +
                    // ' <textarea name="prices['+ i +'][unite_description]" id="unite_description' + i + '" cols="30" rows="3" placeholder="Unite Description" class="form-control "></textarea></div>' +
                    // ' </div>' +
                    '</div>' +
                    ' </div>' +
                    '  </div>' +

                    ' </div>');
                $('#remove-price' + i).on('click', function (e) {
                    e.preventDefault()
                    // arr_price.splice(i, 1);

                    var div = this.parentElement;
                    // div.style.display = "none";
                    div.remove();

                });
            }
        });

    </script>


@endpush
