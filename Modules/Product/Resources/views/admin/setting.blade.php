@extends('layouts.dashboard.app')
@section('title')
    @lang('product::site.products')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('product::site.products')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-30 mx-2 mt-2 text-primary"><i class="lni lni-image"></i></div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('product::site.products') <small></small></h5>
                    <form action="{{route('products.filter')}}" method="GET">
                        <input type="hidden" name="index-view" value="0">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="search" name="query"
                                       class="form-control mt-2 mx-3  radius-30 form-control "
                                       placeholder="@lang('product::site.search')" value="{{request('query')}}">
                            </div>


                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($products_setting->count() > 0)
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">@lang('taxonomy::site.name')</th>
                                    <th scope="col">@lang('taxonomy::site.image')</th>
                                    <th scope="col">Popular</th>
                                    <th scope="col">Home</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products_setting as $index => $product)
                                    <tr>
                                        <th scope="row">{{ $index + 1 }}</th>
                                        <td>{{ $product->title }}</td>
                                        <td><img class="w-25"
                                                 src="{{ $product->getFirstMediaUrl('images', 'thumbnail') }}"></td>


                                        <td class="font-30">
                                            @if (auth()->user()->hasPermission('website-update'))

                                                <div class="form-check form-switch">
                                                    <input class=" form-check-input" id="flexSwitchCheckCheckedpopular"
                                                           type="checkbox"
                                                           onchange="product_toggle_popular({{$product->id}})" {{$product->popular >= 1 ? 'checked= "" ' : ''}}>
                                                </div>
                                            @else
                                                <div class="form-check form-switch">
                                                    <a class=" form-check-input disabled"
                                                       id="flexSwitchCheckCheckedpopular"
                                                       type="checkbox"></a>
                                                </div>
                                            @endif
                                        </td>
                                        <td class="font-30">
                                            @if (auth()->user()->hasPermission('website-update'))

                                                <div class="form-check form-switch">
                                                    <input class=" form-check-input" id="flexSwitchCheckCheckedhome"
                                                           type="checkbox"
                                                           onchange="product_toggle_home({{$product->id}})" {{($product->home >= 1  ) ? 'checked= "" ' : ''}}>
                                                </div>
                                            @else
                                                <div class="form-check form-switch">
                                                    <a class=" form-check-input disabled"
                                                       id="flexSwitchCheckCheckedhome"
                                                       type="checkbox"></a>
                                                </div>
                                            @endif
                                        </td>

                                    </tr>


                                @endforeach
                                </tbody>
                            </table>
                            <div class="mt-3 mb-3 mx-3">
                                {{ $products_setting->links('vendor.pagination.bootstrap-4') }}

                            </div>
                            @else
                                <h2>@lang('taxonomy::site.not_data_found')</h2>
                            @endif

                        </div>
                </div>
            </div>
        </div>


        @endsection
        @push('js')
            <script>

                function product_toggle_home(id) {
                    $.ajax({
                        method: "POST",
                        url: "{{route('products.toggle-home')}}",
                        data: {
                            product_id: id
                        },
                        success: function (one, two, three) {

                            success_noti('تم التعديل بنجاح')
                        }
                    });
                }

                function product_toggle_popular(id) {
                    $.ajax({
                        method: "POST",
                        url: "{{route('products.toggle-popular')}}",
                        data: {
                            product_id: id
                        },
                        success: function (one, two, three) {

                            success_noti('تم التعديل بنجاح')
                        }
                    });
                }
            </script>
    @endpush
