@extends('layouts.dashboard.app')
@section('title')
    @lang('product::site.products')
@endsection
@push('css')
    <style>
        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show {
            width: 100px;
            height: 100px;
            position: relative;
            overflow: hidden;
            margin-bottom: 1em;
            float: left;
            border-radius: 12px;
            box-shadow: 0 0 4px 0 #888;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show button.btn-danger {
            position: absolute;
            top: 3px;
            right: 3px;
            width: 20px;
            height: 20px;
            border-radius: 15px;
            font-size: 10px;
            line-height: 1.42;
            padding: 2px 0;
            text-align: center;
            z-index: 3;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show .imageuploadify-details-show {
            position: absolute;
            top: 0;
            padding-top: 20px;
            width: 100%;
            height: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            background: rgba(255, 255, 255, .5);
            z-index: 2;
            opacity: 1;
        }

    </style>
@endpush
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('product::site.products')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-4">
                    <h5 class="card-title">Update New Product</h5>
                    <hr />
                    <form class=" mt-4" action="{{ route('products.update') }}" id="form-product" method="POST">
                        @csrf
                        <input name="product_id" value="{{ $product->id }}" type="hidden">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputProductTitle" class="form-label">Product Title
                                                    AR</label>
                                                <input type="text" name="title-ar" class="form-control"
                                                    value="{{ $product->translateOrNew('ar')->title }}"
                                                    id="inputProductTitle" placeholder="Enter product title Arabic">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputProductTitleEN" class="form-label">Product Title
                                                    EN</label>
                                                <input type="text" class="form-control" name="title-en"
                                                    value="{{ $product->translateOrNew('en')->title }}"
                                                    id="inputProductTitleEN" placeholder="Enter product title English">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col-12">

                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputProductPreparation " class="form-label">Product
                                                    Preparation</label>
                                                <input type="number" name="preparation" class="form-control"value="{{ $product->preparation }}" id="inputProductPreparation"placeholder="Enter product preparation">
                                            </div>
                                        </div>
                                        {{-- <div class="col-6"> --}}
                                        {{-- <div class="mb-3"> --}}
                                        {{-- <label for="inputPrice" class="form-label">Price</label> --}}
                                        {{-- <input type="number" --}}
                                        {{-- class="form-control" --}}
                                        {{-- name="price" --}}
                                        {{-- id="inputPrice" --}}
                                        {{-- value="{{$product->prices[0]['price']}}" --}}
                                        {{-- placeholder="Price"> --}}

                                        {{-- </div> --}}
                                        {{-- </div> --}}
                                        <div class="col-6">
                                            <div class="mb-3">
{{--                                                <label for="inputProductHome" class="form-label">Product Home</label>--}}
{{--                                                <input type="number" class="form-control" name="home"--}}
{{--                                                    id="inputProductHome" placeholder="Enter product Home ">--}}
                                                <label class="form-label">Category</label>
                                                <select class="single-select" name="category_id"
                                                        data-placeholder="Choose anything">
                                                    @for ($category = 0; $category < count($categories); $category++)
                                                        <option value="{{ $categories[$category]['id'] }}"
                                                            {{ $product->categories[0]->id === $categories[$category]['id'] ? 'selected' : '' }}>
                                                            {{ $categories[$category]['title'] }}</option>
                                                    @endfor

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="imageuploadify-show row  g-2 justify-content-center mt-3">
                                        <div class="imageuploadify-images-list-show text-center">
                                            <span class="mb-3 imageuploadify-message-show "> Product Old Images</span>
                                            <br />
                                            <div id="old_images">
                                                @php($image_ids = [])
                                                @foreach ($product->getMedia('images') as $image)
                                                    @php(array_push($image_ids, $image->id))
                                                    <div class="col imageuploadify-container-show" style="margin: 3px;"
                                                        id="image_old{{ $image->id }}">

                                                        <button type="button" class="btn btn-danger bx bx-x"
                                                            onclick="remove_old_image('{{ $image->id }}')"></button>
                                                        <div class="imageuploadify-details--show">
                                                            <img src="{{ $image->getUrl('full') }}"
                                                                data-id="{{ $image->id }}" width="100">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="mb-3">
                                        <label for="inputProductDescription" class="form-label">Product Images</label>
                                        <input id="image-uploadify-show" type="file" accept="image/*" multiple>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="border border-3 p-4 rounded">
                                    <div class="row g-3">

{{--                                        <div class="col-md-12">--}}

{{--                                            <input type="checkbox" name="popular" id="inputPopular" placeholder="Popular">--}}
{{--                                            <label for="inputPopular" class="form-label">Popular</label>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-12">--}}
{{--                                            <div class="mb-3">--}}
{{--                                                <label class="form-label">Categories</label>--}}
{{--                                                <select class="single-select" name="category_id"--}}
{{--                                                    data-placeholder="Choose anything">--}}
{{--                                                    @for ($category = 0; $category < count($categories); $category++)--}}
{{--                                                        <option value="{{ $categories[$category]['id'] }}"--}}
{{--                                                            {{ $product->categories[0]->id === $categories[$category]['id'] ? 'selected' : '' }}>--}}
{{--                                                            {{ $categories[$category]['title'] }}</option>--}}
{{--                                                    @endfor--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        {{-- <div class="col-md-6"> --}}

                                        {{-- <input type="checkbox" --}}
                                        {{-- name="selling_by_pieces" --}}
                                        {{-- id="selling_by_pieces" --}}

                                        {{-- checked> --}}
                                        {{-- <label for="selling_by_pieces" class="form-label">selling by unite</label> --}}
                                        {{-- </div> --}}
                                        {{-- <div class="col-md-6" id="selling_by_pieces_div" style="display: none"> --}}
                                        {{-- <label for="selling_by_pieces" class="form-label">selling by </label> --}}
                                        {{-- <input type="text" --}}
                                        {{-- class="form-control" --}}
                                        {{-- name="unite_name" --}}
                                        {{-- id="unite_name" --}}
                                        {{-- placeholder="unite name"> --}}

                                        {{-- </div> --}}


                                        @include(
                                            'product::admin.partials.edit_price_form'
                                        )
                                        <div class="hidden-input-images"></div>
                                        <div class="col-12">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-primary" id="submit-form">@lang('site.save')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
@push('js')
    <script>
        var images_list = [];
        $(document).ready(function() {
            $('#image-uploadify-show').imageuploadify();
            {{-- @foreach ($product->getMedia('images') as $image) --}}
            {{-- images_list.push('{{$image->id}}') --}}
            {{-- $('.imageuploadify-images-list').append(' <div class="imageuploadify-container" style="margin-left: 3px;">' + --}}
            {{-- '  <button type="button" class="btn btn-danger bx bx-x"></button>' + --}}
            {{-- ' <div class="imageuploadify-details">' + --}}
            {{-- '<span>phillips (3).png</span>' + --}}
            {{-- '<span>image/png</span>' + --}}
            {{-- '<span>14303</span>' + --}}
            {{-- '</div>' + --}}
            {{-- ' <img src="{{$image->getUrl('full')}}">' + --}}
            {{-- '</div>') --}}
            {{-- @endforeach --}}


            $('#submit-form').on('click', function(e) {
                e.preventDefault();
                $(this).prop("disabled", true);
                // add spinner to button
                $(this).html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
                );
                $('.imageuploadify-container img').each(function() {
                    $('.hidden-input-images').append(
                        '<input type="hidden" name="images[]" value="' + $(this).attr('src') +'">'
                    )
                });
                for (i = 0; i < images_list.length; i++) {
                    $('#form-product').append('<input type="hidden" name="images_old_deleted[]" value="' + images_list[i] + '">')
                }


                // if ($('#selling_by_pieces_div').is(':visible')) {
                //     if ($('#selling_by_pieces_div input').val() === '') {
                //         error_noti('ادخل اسم المبيع')
                //     } else document.getElementById('form-product').submit();
                // } else document.getElementById('form-product').submit();
                document.getElementById('form-product').submit();
            })

            $('#selling_by_pieces').on('change', function(e) {
                e.preventDefault();

                if ($('#selling_by_pieces_div').is(':visible')) {
                    $('#selling_by_pieces').val('false')
                    $('#selling_by_pieces_div').fadeOut()
                    $('#add-price').fadeOut()
                } else {
                    $('#selling_by_pieces').val('true')
                    $('#selling_by_pieces_div').fadeIn()
                    $('#add-price').fadeIn()
                }
            })

        })

        function remove_old_image(id) {
            $('#image_old' + id).remove();
            images_list.push(id)

        }
    </script>
@endpush
