<?php

Route::get('products/{slug}', 'Guest\GuestProductController@index');
Route::get('search', 'Guest\GuestProductController@search');
Route::get('get-general-delivery-price', 'Guest\GuestProductController@generalDeliveryPrice');
