<?php


use Illuminate\Support\Facades\Route;
use Modules\Product\Http\Controllers\ProductController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //category routes
            Route::get('/products', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'index'])->name('products.index');
            Route::get('/products/filter', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'filter'])->name('products.filter');
            Route::get('/products/create', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'create'])->name('products.create');
            Route::get('/products/show/{id}', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'show'])->name('products.show');
            Route::get('/products/setting', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'setting'])->name('products.setting');
            Route::post('/products', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'store'])->name('products.store');
            Route::post('/products/update', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'update'])->name('products.update');
            Route::post('/products/toggle-status', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'toggleStatus'])->name('products.toggle-status');
            Route::post('/products/toggle-home', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'toggleHome'])->name('products.toggle-home');
            Route::post('/products/toggle-popular', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'togglePopular'])->name('products.toggle-popular');
            Route::delete('/products/delete/{product}', [\Modules\Product\Http\Controllers\Admin\AdminProductController::class, 'delete'])->name('products.delete');
        });
    }
);
