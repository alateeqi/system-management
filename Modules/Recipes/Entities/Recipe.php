<?php

namespace Modules\Recipes\Entities;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Recipe extends Model implements HasMedia
{
    use SoftDeletes, RecordsActivity,InteractsWithMedia, RecordsActivity;
    
    
    
    protected $casts = [
        'excerpt' => 'array',
        'content' => 'array',
        'preparation' => 'array'
    ];
    
    protected $fillable = [
        'title'
    ];

    

    protected $appends = [
        'image',
        'fullimage',
        'mediumimage',
    ];


    protected static function boot()
    {
        parent::boot();

        if (auth()->guest()) {
            return true;
        }

        static::creating(function ($model) {
            $model->user_id = request()->user()->id;
        });

        self::savingActivity();
    }


    public function registerMediaCollections(Media $media = null): void
    {
        $this->addMediaCollection('images');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('full')
            ->performOnCollections('images');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(450)
            ->performOnCollections('images');

        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(225)
            ->performOnCollections('images');
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'thumbnail');
    }

    public function getFullimageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'full');
    }

    public function getMediumimageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'medium');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
