<?php

namespace Modules\Recipes\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Recipes\Entities\Recipe;
use Modules\Recipes\Http\Requests\Admin\Store;
use Modules\Recipes\Http\Requests\Admin\Update;
use Modules\Recipes\Interfaces\Admin\AdminRecipesInterface;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class AdminRecipesController extends Controller
{
        public function __construct(Recipe $recipe)
    {
        $this->middleware(['permission:recipes-read'])->only('index');
        $this->middleware(['permission:recipes-create'])->only('create');
        $this->middleware(['permission:recipes-update'])->only('edit');
        $this->middleware(['permission:recipes-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request,AdminRecipesInterface $interface)
    {
        
        // $recipes = Recipe::when($request->search, function($query) use ($request) {
        //     return $query->where('title','like', '%' . $request->search . '%' );
        // })->latest()->Paginate();


        // return view('recipes::admin.index',compact('recipes'));
        
        $recipes = $interface->index($request);

        return view('recipes::admin.index',compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('recipes::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminRecipesInterface $interface)
    {
        

            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('recipes.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('recipes.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Recipe $recipe)
    {
        
        $recipes= $recipe;
        $photos= $recipe->getMedia('images');
        
        return view('recipes::admin.show' ,compact('recipes','photos'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminRecipesInterface $interface, $id )
    {
        // $recipes = $recipe;
        $recipes = $interface->find($id);
        // $recipe->getMedia('model_type')->toMediaCollection('images');		
            return view('recipes::admin.edit',compact('recipes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(AdminRecipesInterface $interface, Update $request)
    {
        try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            
            // ->withSuccess('Success')
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('recipes.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Recipe $recipe){
        
        $recipe->delete();
        session()->flash('success',__('site.deleted_successfully'));
        return redirect()->route('recipes.index');
    }

    }

