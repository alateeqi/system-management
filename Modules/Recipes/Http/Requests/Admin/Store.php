<?php

namespace Modules\Recipes\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Recipes\Interfaces\Admin\AdminRecipesInterface;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'=>['nullable'],
            'title' => [''],
            'excerpt.*' => ['required'],
            'content.*' => ['required'],
            'preparation.*'=>['required'],
            'images' => ['required', 'array'],
            'images.*' => ['string'],




        ];
    }
}
