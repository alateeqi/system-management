<?php

namespace Modules\Recipes\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Recipes\Interfaces\Admin\AdminRecipesInterface;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'recipe_id'=>['required'],
            'title' => [''],
            'excerpt.*' => ['required'],
            'content.*' => ['required'],
            'preparation.*'=>['required'],
            
            'images_old_deleted' => ['nullable', 'array'],
            'images_old_deleted.*' => ['nullable', 'numeric', 'exists:media,id'],

            // 'video' => ['nullable'],
            'images' => ['nullable', 'array'],
            'images.*' => ['nullable', 'string'],




        ];
    }
}
