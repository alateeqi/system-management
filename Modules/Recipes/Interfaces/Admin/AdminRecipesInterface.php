<?php

namespace Modules\Recipes\Interfaces\Admin;

use Modules\Recipes\Entities\Recipe;
use Illuminate\Http\Request;
interface AdminRecipesInterface

{
    public function index(Request $request);

    public function store($data);
    public function update($data);
    public function find($id);



    // public static function medieIDs();

    // public function show($id);
    
    // public function destroy(Recipe $recipe);

    
}
