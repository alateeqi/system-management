<?php

namespace Modules\Recipes\Repositories\Admin;
use Modules\Auth\Entities\User;
use Illuminate\Http\Request;
use Modules\Recipes\Entities\Recipe;
use Modules\Recipes\Interfaces\Admin\AdminRecipesInterface;

class AdminRecipesRepository implements AdminRecipesInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {

        $recipes = Recipe::when($request->search, function($query) use ($request) {
            return $query->where('title','like', '%' . $request->search . '%' );
        })->latest()->Paginate();
        return $recipes;
    }

    public function store($data)
    {
        $new_recipe = new Recipe();
        $new_recipe-> user_id = $data['user_id'];
        $new_recipe-> title = $data['title'];
        $new_recipe-> excerpt = $data['excerpt'];
        $new_recipe-> content = $data['content'];
        $new_recipe-> preparation = $data['preparation'];
        // dd($new_recipe);

        $new_recipe->save();


        $new_recipe->addMedia(request()->file('video'))->toMediaCollection('videos');

        foreach ($data['images'] as $file) {
            $new_recipe->addMediaFromBase64($file)->toMediaCollection('images');
        }
        return $new_recipe;
    }

    public function find($id)
    {
        return Recipe::find($id);
    }

    public function update($data){

        $recipe = $this->find($data['recipe_id']);
        $recipe-> title = $data['title'];
        $recipe-> excerpt = $data['excerpt'];
        $recipe-> content = $data['content'];
        $recipe-> preparation = $data['preparation'];
        // dd($new_recipe);

        $recipe->save();
        if (isset($data['video'])) {
            foreach ($data['video'] as $file) {
                $recipe->addMedia(request()->file('video'))->toMediaCollection('videos');
            }
        }
        

        if (isset($data['images_old_deleted'])) {
            for ($i = 0; $i < count($data['images_old_deleted']); $i++) {
                $recipe->deleteMedia($data['images_old_deleted'][$i]);
            }
        }

        
        if (isset($data['images'])) {
            foreach ($data['images'] as $file) {
                $recipe->addMediaFromBase64($file)->toMediaCollection('images');
            }
        }
        return $recipe;
    }


    public function show($id){

    
    }
    // public function destroy(Recipe $recipe){
        
    //     $recipe->delete();   
    // }

}
