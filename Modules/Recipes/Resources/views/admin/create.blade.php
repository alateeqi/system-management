@extends('layouts.dashboard.app')
@section('title')
    @lang('recipes::site.add_recipes')
@endsection
@push('css')
    <style>
        .icon-font{
        font-size: 2px !important;
        }
        .form-font {
            font-size: 17px;
        }

    </style>
@endpush
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('recipes::site.add_recipes')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-3">
                    <h5 class="card-title">@lang('recipes::site.add_recipes')</h5>
                    <hr />
                    <form class="mt-4" action="{{ route('recipes.store') }}" id="form-recipe" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="border border-3 p-2 rounded">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-3">
                                                <label for="inputRecipestTitle"
                                                    class="form-label">@lang('recipes::site.recipes_name')</label>
                                                <input type="text" name="title" class="form-control form-font"
                                                    id="inputRecipestTitle" placeholder="أدخل اسم الوصفة">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="{{Auth::id();}}" name="user_id">
                                    <div class="row">
                                        <h6>المقادير</h6>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="inputRecipesName" class="form-label"> الاسم</label>
                                                <input type="text"  class="form-control form-font"
                                                    id="inputRecipesName" placeholder="ادخل الاسم">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="inputRecipesQuantity" class="form-label">الكمية</label>
                                                <input type="text" name="preparation" class="form-control form-font"
                                                    id="inputRecipesQuantity" placeholder="ادخل الكمية">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="d-grid">
                                            <button id="add-name-quantity" type="button"
                                                class="btn btn-success form-font"><i
                                                    class="lni lni-circle-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <label for="inputRecipesPreparation" class="form-label m-2">طريقة
                                                    التحضير</label>
                                            <div class="mb-3 input-group">
                                                <input type="text" name="preparation" class="form-control form-font"
                                                    id="inputRecipesPreparation" placeholder="ادخل طريقة التحضير">
                                                    <button id="add-preparation" type="button" class="btn btn-success form-font"><i class="lni lni-circle-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-2">
                                        <div class="d-grid">

                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="formFileMultiple" class="form-label">فيديو الوصفة</label>
                                        <input id="video" type="file" class="form-control video" name="video" accept="video" rules="mimes:mp4|max:1024">
                                    </div>

                                    <div class="mb-3">
                                        <label for="inputProductDescription" class="form-label">صور </label>
                                        <input  id="image-uploadify" type="file" accept="image/*" multiple>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="border border-3 p-2 rounded table-responsive">
                                    <div class="row g-3">
                                        <div class="col-md-12">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                    <button class="accordion-button collapsed hover-btn-one" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseOne"
                                                        aria-expanded="true" aria-controls="collapseOne" id="collapseOne-btn">
                                                        المقادير
                                                    </button>
                                                </h2>
                                                <div id="collapseOne" class="accordion-collapse collapse table-responsive"
                                                    aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <table class="table table-hover table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th>@lang('recipes::site.name')</th>
                                                                    <th>@lang('recipes::site.quantiy')</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="ingredients-list" id="ingredients-list-id">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingTwo">
                                                    <button class="accordion-button collapsed hover-btn-two" type="button"
                                                        data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo" id="collapseTwo-btn">
                                                        طريقة التحضير
                                                    </button>
                                                </h2>
                                                <div id="collapseTwo" class="accordion-collapse collapse table-responsive"
                                                    aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <table class="table table-hover table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th>@lang('recipes::site.name')</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="preparation-list" id="preparation-list-id">
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hidden-input-images"></div>
                                        <div class="col-12">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-primary" id="submit-form">@lang('site.save')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('js')

{{--
<script>

        const submitform = document.getElementById("submitform");
        const video  = document.getElementById("video");
        submitform.addEvenrListener("click",uploadCanvasVideo);
        function uploadCanvasVideo(){
            const base64 =video.toDataURL().split(",")[1];
            const body ={
                "generated_at" : new Date().toISOString(),
                "mp4":base64
            };

        }
    </script>  --}}


    <script>
        $(function() {
            //add recipes
            $('#add-name-quantity').on('click', function(e) {

                e.preventDefault();

                $('#collapseOne').addClass('show')
                $('#collapseTwo').removeClass('show')
                $('.hover-btn-one').addClass('bg-warning')
                $('.hover-btn-two').removeClass('bg-warning')


                var nameinput = document.getElementById('inputRecipesName').value;
                var quantityinput = document.getElementById('inputRecipesQuantity').value;
                var html =
                    `<tr>
                        <td>${nameinput}  <input type="hidden"  name= "excerpt[]" value="${nameinput}"> </td>
                        <td >${quantityinput} <input type="hidden"  name= "content[]" value="${quantityinput}"> </td>
                        <td><button id=""type="button" class=" btn p-0 btn-sm btn-outline-danger  delete-btn"><i class="fadeIn animated bx bx-trash"></i></button></td>
                    </tr>`;
                $('.ingredients-list').append(html);

                // input value
                $('#inputRecipesName').val('');
                $('#inputRecipesQuantity').val('');
            });

            // remove tr
            $('#ingredients-list-id').on('click', '.delete-btn', function() {
                $(this).closest('tr').remove();
            })

            //add preparation
            $('#add-preparation').on('click', function() {

                $('#collapseTwo').addClass('show')
                $('#collapseOne').removeClass('show')
                $('.hover-btn-two').addClass('bg-warning')
                $('.hover-btn-one').removeClass('bg-warning')


                var preparationinput = document.getElementById('inputRecipesPreparation').value;

                var html =
                    `<tr>
                        <td>${preparationinput}  <input type="hidden"  name="preparation[]" value="${preparationinput}"> </td>
                        <td><button type="button"class="btn p-0 btn-sm btn-outline-danger p-0 delete-btn-preparation"><i class="fadeIn animated bx bx-trash"></i></button></td>
                    </tr>`;

                $('.preparation-list').append(html);

                $('#inputRecipesPreparation').val('');
            });

            $('#collapseOne-btn').on('click', function() {
                $('.hover-btn-one').addClass('bg-warning')
                $('.hover-btn-two').removeClass('bg-warning')
            })

            $('#collapseTwo-btn').on('click', function() {
                $('.hover-btn-two').addClass('bg-warning')
                $('.hover-btn-one').removeClass('bg-warning')
            })

            $('#preparation-list-id').on('click', '.delete-btn-preparation', function() {
                $(this).closest('tr').remove()
            })

        }); //end of document ready
    </script>

        <script>
        $('#submit-form').on('click',function (e) {
            e.preventDefault();
            $(this).prop("disabled", true);
            // add spinner to button
            $(this).html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $('.imageuploadify-container img').each(function(){
                $('.hidden-input-images').append(
                    '<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'
                )
            });

            document.getElementById('form-recipe').submit();
        })


    </script>
@endpush
