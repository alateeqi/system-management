@extends('layouts.dashboard.app')
@section('title')
    @lang('recipes::site.recipes')
@endsection
@push('css')
@endpush

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('recipes::site.recipes')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-22 text-primary m-2"><i class="lni lni-service"></i></div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('recipes::site.recipes')<small></small></h5>
                    <form action="{{route('recipes.index')}}" method="GET">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="search" name="search" class="form-control mt-2 mx-3  radius-30 form-control "
                                    placeholder="@lang('taxonomy::site.search')" value="{{request()->search}}">
                            </div>
                            <div class="col-md-6">
                                @if (auth()->user()->hasPermission('recipes-create'))
                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                    href="{{ route('recipes.create') }}">@lang('recipes::site.add_recipes')<i
                                    class="lni lni-circle-plus mx-1"></i></a>
                                        @else
                                            <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30 disabled"
                                    href="#">@lang('product::site.add_new')<i class="lni lni-circle-plus mx-1 "></i></a>
                                    @endif
                            </div>
                            
                        </div>
                    </form>
                    {{-- end of form --}}
                </div>
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-4 row-cols-xl-4">
                    @if ($recipes->count() > 0)
                        @foreach ($recipes as $recipe)
                            <div class="col">
                                <div class="card">
                                    <a href="recipes/show/{{ $recipe->id }}"><img
                                            src="{{ $recipe->getFirstMediaUrl('images', 'thumbnail') }}"
                                            class="card-img-top" alt="..."></a>
                                    <div class="card-body">
                                        <a href="">
                                            <h6 class="card-title">{{ $recipe->title }}</h6>
                                        </a>
                                        @if (auth()->user()->hasPermission('recipes-update'))
                                        <a class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                            data-bs-toggle="dropdown"
                                            aria-expanded="false">@lang('taxonomy::site.action')</a>
                                            @else
                                            <a class="btn btn-primary dropdown-toggle btn-sm disabled"
                                                    href="#">@lang('user::site.action')</a>
                                            @endif

                                        
                                        <ul class="dropdown-menu" style="">

                                            @if (auth()->user()->hasPermission('recipes-update'))
                                            <li class="dropdown-item font-18 text-primary"><i class="lni lni-cog"></i><a
                                                    class="text-dark" href="{{route('recipes.edit',$recipe->id)}}">@lang('taxonomy::site.edit')</a>
                                            </li>
                                            @endif

                                            @if (auth()->user()->hasPermission('recipes-delete'))
                                            <li class="dropdown-item font-18"><i class="bx bxs-trash"
                                                    style="color: #ff6b6b;"></i>
                                                <a href="#" class="text-dark " data-bs-toggle="modal"
                                                    data-bs-target="#exampleVerticallycenteredModal{{$recipe->id}}">@lang('taxonomy::site.delete')</a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- recipes-delete -->
                            <div class="modal fade" id="exampleVerticallycenteredModal{{$recipe->id}}" tabindex="-1"aria-hidden="true">
                                <form action="{{ route('recipes.delete', $recipe->id) }}" method="POST"
                                    style="display:inline-block;">
                                    @csrf

                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" name="" id="" value="{{$recipe->id}}">
                                                @lang('taxonomy::site.confirm_the_deletion')
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">@lang('taxonomy::site.cancel')</button>
                                                <button type="submit"
                                                    class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                </div>
                <div class="mt-3 mb-3 mx-3">
						{{$recipes = DB::table('recipes')->simplePaginate(7);}}						
                </div>
            @else
                <h2>@lang('taxonomy::site.not_data_found')</h2>
                @endif
            </div>
        </div>
    </div>
@endsection
