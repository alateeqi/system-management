@extends('layouts.dashboard.app')
@section('title')
    @lang('recipes::site.recipes')
@endsection
@push('css')
    <style>
        #outer {
            width: 100%;
            border-radius: 20px;
            overflow: hidden;
            position: relative;
        }

    </style>
@endpush
@section('content')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">Applications</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <a class="mx-2" href="{{ route('recipes.index') }}">
                                <li class="breadcrumb-item active " aria-current="page"> @lang('recipes::site.recipes')</li>
                            </a>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="row">
                <div class="col-12 col-lg-7 ">
                    <div class="card shadow-lg  h-75 " style=" border-radius: 20px;">
                        <div class="card-body table-responsive chat-conten m-1 ">
                            {{-- <div class="d-grid"> <a href="javascript:;" class="btn btn-primary">+ Add File</a>
                            </div> --}}
                            <h5 class="my-3"><i class="lni lni-radio-button"></i> المقادير</h5>
                            <table class="table table-hover ">
                                <thead>
                                    <tr>
                                        
                                        <td>#</td>
                                        <td>@lang('taxonomy::site.name')</td>
                                        <td>@lang('recipes::site.quantiy')</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < count($recipes->excerpt) && count($recipes->content); $i++)
                                        <tr>
                                            <th>{{$i+1}}</th>
                                            <th>{{ $recipes->excerpt[$i] }}</th>
                                            <th>{{ $recipes->content[$i] }}</th>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <div class="card-body table-responsive chat-conten  m-0">
                            {{-- <div class="d-grid"> <a href="javascript:;" class="btn btn-primary">+ Add File</a>
                            </div> --}}
                            <h5 class="my-3"><i class="fadeIn animated bx bx-book-reader"></i> طريقة التحضير </h5>
                            <table class="table table-hover ">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>الخطوات</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for ($i = 0; $i < count($recipes->preparation); $i++)
                                        <tr>
                                            <th>{{$i+1}}</th>
                                            <th>{{ $recipes->preparation[$i] }}</th>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-lg-5 ">
                    <div class="card shadow-lg " id="outer">
                        <div class="card-body">
                            <div class="row row-cols-1 row-cols-md-1 row-cols-lg-2 row-cols-xl-2 ">
                                <div class="col-12 w-100 h-50">
                                    <div class="card w-100 " id="outer">
                                        {{-- <iframe src="{{ $recipes->getFirstMediaUrl('videos')}}" frameborder="0"></iframe> --}}
                                        <video width="320" height="320" controls=""
                                            src="{{ $recipes->getFirstMediaUrl('videos') }}"
                                            class="bs-card-video "></video>
                                        <div class="card-body p-1">
                                            <div class="card mx-0 w-100 p-0 " id="outer">
                                                <div id="carouselExampleDark" class=" carousel carousel-dark slide mx-0"
                                                    data-bs-ride="carousel">
                                                    <div class="carousel-inner">
                                                        
                                                        <div class="carousel-item active">
                                                            <img src="{{ $recipes->getFirstMediaUrl('images') }}"class="d-block w-100" alt="...">
                                                        </div>
                                                        
                                                        @foreach ($photos as $photo)
                                                        <div class="carousel-item ">
                                                            <img src="{{ $photo->getUrl('full') }}"class="d-block w-100" alt="...">
                                                        </div>
                                                        @endforeach
                                                        
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carouselExampleDark"
                                                        role="button" data-bs-slide="prev"> <span
                                                            class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="visually-hidden">Previous</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carouselExampleDark"
                                                        role="button" data-bs-slide="next"> <span
                                                            class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="visually-hidden">Next</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-12  mx-0">
                                    <div class="card h-100">
                                        <video width="320" height="320" controls=""
                                            src="{{ $recipes->getFirstMediaUrl('videos') }}"
                                            class="bs-card-video "></video>
                                        <div class="card-body">
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div>

            </div>
        </div>

        <!--end row-->
    </div>


    <!--end page wrapper -->

    @push('js')
        <script>
            new PerfectScrollbar('.chat-content');
        </script>
    @endpush
@endsection
