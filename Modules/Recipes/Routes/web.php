<?php


use Illuminate\Support\Facades\Route;

use Modules\Recipes\Http\Controllers\AdminRecipesController;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            //Recipes routes index 
            Route::get('/recipes', [AdminRecipesController::class,'index'])->name('recipes.index');
            Route::get('/recipes/create', [AdminRecipesController::class,'create'])->name('recipes.create');
            Route::post('/recipes/store', [AdminRecipesController::class,'store'])->name('recipes.store');
            Route::get('/recipes/show/{recipe}', [AdminRecipesController::class,'show'])->name('recipes.show');
            Route::get('/recipes/edit/{recipe}', [AdminRecipesController::class,'edit'])->name('recipes.edit');
            Route::post('/recipes/delete/{recipe}', [AdminRecipesController::class,'destroy'])->name('recipes.delete');
            Route::post('/recipes/update', [AdminRecipesController::class,'update'])->name('recipes.update');
        });
    }
    );
