<?php

namespace Modules\Record\Entities;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Transaction\Entities\Transaction;


class Record extends Model
{
    use SoftDeletes, RecordsActivity;

    protected $fillable = [
        'type_id',
        'title',
        'date'
    ];

    protected $with = [
        'type',
        'transactions'
    ];

    protected static function boot()
    {
        parent::boot();

        if (auth()->guest()) {
            return true;
        }

        static::creating(function ($model) {
            $model->user_id = request()->user()->id;
        });

        self::savingActivity();
    }

    public function type(){
        return $this->belongsTo(Taxonomy::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
}

