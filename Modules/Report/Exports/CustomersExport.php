<?php

namespace Modules\Report\Exports;


use Modules\Customer\Entities\Customer;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;

class CustomersExport implements FromView
{
    protected $startDate, $endDate, $statuses;

    public function __construct($startDate, $endDate, $statuses)
    {

        $this->startDate = Carbon::parse($startDate);
        $this->endDate = Carbon::parse($endDate);
        $this->statuses = $statuses;
    }


    public function data()
    {
//        $customers = Customer::get();
      return   Customer::orderBy('id','desc')->paginate(900);

     }
    /**
     * @return view
     */
    public function view(): view
    {
       $customers =$this->data();

        return view('report::admin.export.datatables.customers', compact('customers'));
    }
}
