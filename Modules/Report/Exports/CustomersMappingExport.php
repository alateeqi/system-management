<?php

namespace Modules\Report\Exports;


use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Modules\Customer\Entities\Customer;


class CustomersMappingExport implements FromView

{

    protected $startDate, $endDate, $statuses;

    public function __construct($startDate, $endDate, $statuses)
    {

        $this->startDate = Carbon::parse($startDate);
        $this->endDate = Carbon::parse($endDate);
        $this->statuses = $statuses;
    }

    public function data()
    {

        return Customer::orderBy('id', 'desc')->with('addresses')
            ->whereHas('addresses')
            ->orderBy('id', 'desc')
            ->paginate(500);

    }

    /**
     * @return view
     */
    public function view(): view
    {
        $customers = $this->data();

        return view('report::admin.export.datatables.customers-map', compact('customers'));
    }


}
