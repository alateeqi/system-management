<?php

namespace Modules\Report\Exports;

use Modules\Order\Entities\OrderProduct;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class ProductSalesExport implements FromView
{
    protected $startDate, $endDate, $statuses;

    public function __construct($startDate, $endDate, $statuses)
    {

        $this->startDate = Carbon::parse($startDate);
        $this->endDate = Carbon::parse($endDate);
        $this->statuses = $statuses;
    }

    /**
     * @return view
     */
    public function view(): view
    {
        $products = OrderProduct::with([
            'product',
            'order'
        ])->whereHas('order', function ($q) {
            $q->whereBetween('requested_time',
                [$this->startDate->format('Y-m-d 00:00:00'), $this->endDate->format('Y-m-d 23:59:59')])
              ->whereIn('status', $this->statuses);
        })->groupBy('order_id', 'product_id')->select('product_id', 'order_id',
            DB::raw('sum(price) as price'))->get();

        $day = ($this->startDate == $this->endDate) ? $this->startDate->format('l') : '';
        $startDate = $this->startDate->format('Y/m/d');
        $endDate = $this->endDate->format('Y/m/d');

//        if (auth()->user()->id === 1) {
//            dd($day,
//                $startDate,
//                $endDate,
//                $products);
//        }

        return view('report::admin.export.datatables.product-orders', compact('products', 'day', 'startDate', 'endDate'));
    }
}
