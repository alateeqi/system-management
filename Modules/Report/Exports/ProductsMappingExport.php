<?php

namespace Modules\Report\Exports;


use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Modules\Product\Entities\Price;

//class ProductsExportMapping implements FromCollection, WithMapping, WithHeadings
class ProductsMappingExport implements FromView
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
//        return Product::with('categories')->get();
        return Price::all();

//        Taxonomy::categories()->active()->orderBy('order')->whereHas('products')->with([
//            'products',
//            'products.prices' => function ($q) {
//                $q->where('deleted_at', null);
//            }
//        ])->get();
    }

    public function map($product): array
    {

        if ($product->product != null)
            return [
                $product->product->id,
                $product->product->categories->implode('title', ', '),
                $product->product->translateOrNew('ar')->title,
                $product->product->translateOrNew('en')->title,
                $product->unite_name,
                $product->price,
//                $product->product->content,
//                $product->product->preparation,
//                Carbon::parse($product->product->created_at)->toFormattedDateString()
            ];
        return [];

    }

    public function headings(): array
    {
        return [
            '#',
            'Categories',
            'Title AR',
            'Title EN',
            'type',
            'Price',
//            'Content',
//            'Preparation',
//            'Created At'
        ];
    }

    public function view(): View
    {

        $products = Price::orderBy('id', 'desc')->get();
        $rows = [];
        $products->map(function ($product) use (&$rows) {
            if ($product->product != null)
                $rows[] = [
                    'id' => $product->product->id,
                    'category' => $product->product->categories->implode('title', ', '),
                    'title_ar' => $product->product->translateOrNew('ar')->title,
                    'title_en' => $product->product->translateOrNew('en')->title,
                    'type' => $product->unite_name,
                    'price' => $product->price,
//                $product->product->content,
//                $product->product->preparation,
//                Carbon::parse($product->product->created_at)->toFormattedDateString()
                ];

        });

        return view('report::admin.export.datatables.products-map', compact('rows'));
    }
}
