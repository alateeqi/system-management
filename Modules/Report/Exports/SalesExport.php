<?php

namespace Modules\Report\Exports;

use Modules\Order\Entities\Order;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SalesExport implements FromView
{
    protected $startDate, $endDate, $statuses;

    public function __construct($startDate, $endDate, $statuses)
    {

        $this->startDate = Carbon::parse($startDate);
        $this->endDate = Carbon::parse($endDate);
        $this->statuses = $statuses;
    }


    /**
     * @return View
     */
    public function view(): view
    {
        $orders = Order::with([
            'products',
            'address',
            'customer',
            'paymentMethod',
            'gate'
        ])->whereBetween('requested_time', [$this->startDate->format('Y-m-d 00:00:00'), $this->endDate->format('Y-m-d 23:59:59')])
                       ->whereIn('status', $this->statuses)
                       ->get();

        $day = ($this->startDate == $this->endDate) ? $this->startDate->format('l') : '';
        $startDate = $this->startDate->format('Y/m/d');
        $endDate = $this->endDate->format('Y/m/d');

        return view('report::admin.export.datatables.sales', compact('orders', 'day', 'startDate', 'endDate'));
    }
}
