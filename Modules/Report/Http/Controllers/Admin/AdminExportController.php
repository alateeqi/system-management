<?php

namespace Modules\Report\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Report\Interfaces\Admin\AdminExcelInterface;
use Modules\Report\Jobs\ExportLargDataToExcel;

class AdminExportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:reports-read'])->only('index');
        $this->middleware(['permission:reports-create'])->only('create');
        $this->middleware(['permission:reports-update'])->only('edit');
        $this->middleware(['permission:reports-delete'])->only('destroy');
    }

    public function index()
    {
        return view('report::admin.export.index');
    }

    public function export(Request $request)
    {

        try {
            $module = $request->input('selected_report');
            $moduleClass = "Modules\Report\Exports\\" . ucfirst($module) . "Export";
//            if (ucfirst($module) . "Export" == 'CustomersMappingExport') {
//                $fileName = 'CustomersExport' . Carbon::now()->toDateString() . '.xlsx';
//                (new $moduleClass)->queue($fileName)->chain([
//                    new ExportLargDataToExcel($request->user(), $fileName)
//                ]);
//                return response()->json(['message' => 'Export started!']);
//
//            } else {

                $view = new $moduleClass($request->startDate, $request->endDate, $request->statuses);


                Excel::store($view, 'public/' . $module . '.xlsx');
                if (ucfirst($module)  == 'Customers' ||ucfirst($module) =="CustomersMapping" ) {
                    $button = '<a class="btn btn-info btn-sm" target="_blank" href="' . config('app.url') . Storage::url($module . '.xlsx') . '">Export to excel</a>' . '<button type="button" class="btn mx-1 my-2 btn-secondary btn-sm print-report">Print</button>' . $view->view()->render() . $view->data()->links('vendor.pagination.post-url', ['paginator' => $view->data()]);

                } else
                    $button = '<a class="btn btn-info btn-sm" target="_blank" href="' . config('app.url') . Storage::url($module . '.xlsx') . '">Export to excel</a>' . '<button type="button" class="btn mx-1 my-2 btn-secondary btn-sm print-report">Print</button>' . $view->view()->render();

                return response()->json(['message' => $button]);
//            }

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
//        $request->validate([
//            'selected_report' => 'required',
//            'filter_data' => 'required',
//        ]);

//        $request->merge(['filter_data' => json_decode($request->filter_data, true)]);


//        $view = new $moduleClass($request);

    }
}
