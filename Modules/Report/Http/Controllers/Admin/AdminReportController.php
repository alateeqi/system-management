<?php

namespace Modules\Report\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Notifications\SendInvoiceUrlSms;
use Illuminate\Http\Request;
use Modules\Report\Http\Requests\Admin\SendMessage;
use Modules\Report\Interfaces\Admin\AdminReportInterface;
use function Aws\map;

class AdminReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:reports-read'])->only('index');
        $this->middleware(['permission:reports-create'])->only('create');
        $this->middleware(['permission:reports-update'])->only('edit');
        $this->middleware(['permission:reports-delete'])->only('destroy');
    }

    public function index(AdminReportInterface $interface)
    {

        $data = $interface->index();
        $products = $data['products'];
        $employees = $data['employees'];
        $cities = $data['cities'];
        $paymentMethodsIds = $data['paymentMethodsIds'];
        $paymentMethods = $data['paymentMethods'];
        $gates = $data['gates'];
        $gatesIds = $data['gatesIds'];
        $accounts = $data['accounts'];
        return view('report::admin.report.index',
            compact('products', 'employees', 'cities', 'paymentMethods', 'gates', 'accounts', 'paymentMethodsIds', 'gatesIds'));
    }

    public function show(Request $request)
    {
        $request->validate([
            'selected_report' => 'required',
//            'filter_data' => 'required',
        ]);

        $request->merge(['filter_data' => $request]);
        $module = $request->input('selected_report');
        $moduleClass = "Modules\Report\Reports\\" . ucfirst($module);
        $filter_data = $request->filter_data;
//        dd($request->request);
       
        return $moduleClass::get($filter_data);
    }

    public function send_message(SendMessage $request, AdminReportInterface $interface)
    {
        $interface->sendMessages($request->validated());
    }
}
