<?php

namespace Modules\Report\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SendMessage extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'phones' => ['required','array'],
            'phones.*' => ['numeric'],
            'message' =>  ['required','string'],
        ];
    }
}
