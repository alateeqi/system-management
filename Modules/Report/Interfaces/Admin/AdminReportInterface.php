<?php

namespace Modules\Report\Interfaces\Admin;

interface AdminReportInterface
{
    public function index();
    public function sendMessages($data);
}
