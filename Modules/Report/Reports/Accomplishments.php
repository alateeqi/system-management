<?php

namespace Modules\Report\Reports;



use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Modules\Auth\Entities\User;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;

class Accomplishments
{
    static public function get($filter_data)
    {
        if ( ! $filter_data['products']) {
            $filter_data['products'] = Product::all();
        }

        if ( ! $filter_data['employees']) {
            $filter_data['employees'] = User::all()->pluck('id');
        }

        $accomplishments = Accomplishment::whereBetween('date',
            [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d')
            ])
                                         ->whereIn('user_id', collect($filter_data['employees']));

        $frequency = $filter_data['frequency'];

        $accomplishments = $accomplishments->get();
        $accomplishmentsGrouped = $accomplishments->groupBy([
            function ($val) use ($frequency, $filter_data) {
                return Carbon::parse($val->{$filter_data['date']})->format($frequency);
            },
            function ($item) {
                return $item->user_id;
            },
            function ($item) {
                return $item->price->unite_name;
            }
        ]);

        dd($accomplishmentsGrouped);

        $chartData = [
            ['dates']
        ];

        if ( ! $filter_data['employees']) {
            $filter_data['employees'] = User::all()->toArray();
        }

        foreach ($filter_data['employees'] as $employee) {
            $chartData[0][] = $employee['name'];
        }

        switch ($frequency) {
            case 'Y/m/d':
                $intervalVal = '1 day';
                break;
            case 'W':
                $intervalVal = '1 week';
                break;
            case 'm':
                $intervalVal = '1 month';
                break;
            case 'Y':
                $intervalVal = '1 year';
                break;
            case 'H':
                $intervalVal = '1 hour';
                break;
        }

        $interval = DateInterval::createFromDateString($intervalVal);
        if ($intervalVal === '1 hour') {
            $period = new DatePeriod(Carbon::parse('2020-01-01 00:00:00'), $interval,
                Carbon::parse('2020-01-01 23:59:59'));
        } else {
            $period = new DatePeriod(Carbon::parse($filter_data['startDate'])->setHours(0)->setMinutes(0)->setSeconds(0),
                $interval,
                Carbon::parse($filter_data['endDate'])->setHours(23)->setMinutes(59)->setSeconds(59));
        }


        foreach ($period as $date) {
            $date = $date->format($frequency);
            $column = [$date];
            foreach ($filter_data['employees'] as $employee) {

                if (isset($accomplishmentsGrouped[$date][$employee['id']])) {
                    $column[] = $accomplishmentsGrouped[$date][$employee['id']]->sum('total_price');
                } else {
                    $column[] = 0;
                }
            }

            $chartData[] = $column;
        }

        $type = "ColumnChart";
        $title = 'Employees Sales Report';

        $statusesCount['total'] = $orders->count();
        $statusesCount['totalSales'] = $orders->sum('total_price');
        $statusesCount['others'] = $orders->whereIn('status',
            [Order::STATUS_NAME['new'], Order::STATUS_NAME['updated']])->count();
        $statusesCount['delivered'] = $orders->where('status', Order::STATUS_NAME['delivered'])->count();
        $statusesCount['proved'] = $orders->where('status', Order::STATUS_NAME['proved'])->count();
        $statusesCount['canceled'] = $orders->where('status', Order::STATUS_NAME['canceled'])->count();
        $statusesCount['canceled'] = $orders->where('status', Order::STATUS_NAME['canceled'])->count();
        $canceled_by_user = $orders->where('status', Order::STATUS_NAME['canceled'])->whereIn('editor_id',
            collect($filter_data['employees'])->pluck('id'));
        $statusesCount['canceled_by_user_count'] = $canceled_by_user->count();
        $statusesCount['canceled_by_user_sum'] = $canceled_by_user->sum('total_price');

        return view('report::admin.report.reports.employees',
            compact('type', 'chartData', 'title', 'statusesCount', 'totalSum', 'totalCount', 'orders',
                'filter_data'));
    }
}
