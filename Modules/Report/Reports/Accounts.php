<?php

namespace Modules\Report\Reports;


use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Transaction\Entities\Transaction;

class Accounts
{
    static public function get($filter_data)
    {
        if (empty($filter_data['account'])) {
            return '<div class="callout callout-danger"><h5>Please select the account!</h5></div>';
        }

        $account = Taxonomy::whereId($filter_data['account'])->first();
        $filter_data['account'] = [
            'id' => $filter_data['account'],
            'title' => $account->title
        ];

        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());

        $transactions = Transaction::whereHas('record', function ($query) use ($filter_data) {
            $query->where('status', 1);
            $query->whereBetween('date', [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ]);
        })
            ->whereIn('account_id', $relatedAccounts)->get();

        $frequency = $filter_data['frequency'];

        $transactionsGrouped = $transactions->groupBy(function ($val) use ($frequency, $filter_data) {
            return Carbon::parse($val->record->date)->format($frequency);
        });

        $chartData = [
            ['dates', 'Credit', 'Debt']
        ];

        switch ($frequency) {
            case 'Y/m/d':
                $intervalVal = '1 day';
                break;
            case 'W':
                $intervalVal = '1 week';
                break;
            case 'm':
                $intervalVal = '1 month';
                break;
            case 'Y':
                $intervalVal = '1 year';
                break;
            case 'H':
                $intervalVal = '1 hour';
                break;
        }

        $interval = DateInterval::createFromDateString($intervalVal);
        if ($intervalVal === '1 hour') {
            $period = new DatePeriod(Carbon::parse('2020-01-01 00:00:00'), $interval,
                Carbon::parse('2020-01-01 23:59:59'));
        } else {
            $period = new DatePeriod(Carbon::parse($filter_data['startDate'])->setHours(0)->setMinutes(0)->setSeconds(0),
                $interval,
                Carbon::parse($filter_data['endDate'])->setHours(23)->setMinutes(59)->setSeconds(59));
        }


        foreach ($period as $date) {
            $date = $date->format($frequency);
            $column = [$date];
            if (isset($transactionsGrouped[$date])) {
                $column[] = $transactionsGrouped[$date]->sum('credit_amount');
                $column[] = $transactionsGrouped[$date]->sum('debt_amount');
            } else {
                $column[] = 0;
                $column[] = 0;
            }
            $chartData[] = $column;
        }

        $type = "ColumnChart";
        $title = 'Account Report';

        return view('report::admin.report.reports.accounts',
            compact('type', 'chartData', 'title', 'filter_data', 'transactionsGrouped'));
    }
}
