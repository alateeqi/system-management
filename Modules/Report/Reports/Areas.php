<?php

namespace Modules\Report\Reports;


use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Product;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Repositories\Admin\City\AdminCityRepository;

class Areas
{
    static public function get($filter_data)
    {
//        if ( ! $filter_data['products']) {
//            $filter_data['products'] = Product::all();
//        }


        $orders = Order::with('city')->whereBetween($filter_data['date'],
            [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ])->whereIn('status', $filter_data['statuses']);

        $totalSum = $orders->sum('total_price');
        $totalCount = $orders->count();

        if ($filter_data['employees']) {
            $orders = $orders->whereIn('user_id', $filter_data['employees']);
        }

        if ($filter_data['cities']) {
            $orders = $orders->whereIn('city_id', $filter_data['cities']);
        }

        if ($filter_data['products']) {
            $orders = $orders->whereHas('products', function ($orderProduct) use ($filter_data) {
                $orderProduct->whereIn('product_id', $filter_data['products']);
            });
        }

        if ($filter_data['paymentMethods']) {
            $orders = $orders->whereIn('payment_method_id', $filter_data['paymentMethods']);
        }

        if ($filter_data['gates']) {
            $orders = $orders->whereIn('gate_id', $filter_data['gates']);
        }

        $frequency = $filter_data['frequency'];

        $orders = $orders->get();

        $ordersGrouped = $orders->groupBy([
            function ($val) use ($frequency, $filter_data) {
                return Carbon::parse($val->{$filter_data['date']})->format($frequency);
            },
            function ($item) {
                return $item->city_id;
            }
        ]);

        $first = ['Dates'];
        $cities_collect = [];
        if (!$filter_data['cities']) {
            $filter_data['cities'] = AdminCityRepository::allCities();
            foreach ($filter_data['cities'] as $city) {
                $first[] = $city['title'];
            }
        } else {
            foreach ($filter_data['cities'] as $city) {
                $obj_city = AdminCityRepository::findStatic($city);
                array_push($cities_collect, $obj_city);
                $first[] = $first[] = $obj_city->title;
            }
            $filter_data['cities'] = $cities_collect;
        }

        $chartData = [$first];

        switch ($frequency) {
            case 'Y/m/d':
                $intervalVal = '1 day';
                break;
            case 'W':
                $intervalVal = '1 week';
                break;
            case 'm':
                $intervalVal = '1 month';
                break;
            case 'Y':
                $intervalVal = '1 year';
                break;
            case 'H':
                $intervalVal = '1 hour';
                break;
        }

        $interval = DateInterval::createFromDateString($intervalVal);
        if ($intervalVal === '1 hour') {
            $period = new DatePeriod(Carbon::parse('2020-01-01 00:00:00'), $interval,
                Carbon::parse('2020-01-01 23:59:59'));
        } else {
            $period = new DatePeriod(Carbon::parse($filter_data['startDate'])->setHours(0)->setMinutes(0)->setSeconds(0),
                $interval,
                Carbon::parse($filter_data['endDate'])->setHours(23)->setMinutes(59)->setSeconds(59));
        }

        foreach ($period as $date) {
            $date = $date->format($frequency);
            $column = [$date];
            foreach ($filter_data['cities'] as $city) {
                if (isset($ordersGrouped[$date][$city['id']])) {
                    $column[] = $ordersGrouped[$date][$city['id']]->count();
                } else {
                    $column[] = 0;
                }
            }
            $chartData[] = $column;
        }

        $type = "ColumnChart";
        $title = 'Sales Report';

        $statusesCount['total'] = $orders->count();
        $statusesCount['totalSales'] = $orders->sum('total_price');
        $statusesCount['others'] = $orders->whereIn('status',
            [Order::STATUS_NAME['new'], Order::STATUS_NAME['updated']])->count();
        $statusesCount['delivered'] = $orders->where('status', Order::STATUS_NAME['delivered'])->count();
        $statusesCount['proved'] = $orders->where('status', Order::STATUS_NAME['proved'])->count();
        $statusesCount['canceled'] = $orders->where('status', Order::STATUS_NAME['canceled'])->count();
        $users = [];
        if ($filter_data['employees']) {
            foreach ($orders->map(function ($order) {
                return $order->user;
            })->unique('id') as $user) {

                $users[] = [
                    'id' => $user->id,
                    'name' => $user->name
                ];
            }
            $filter_data['employees'] = $users;
        }

        return view('report::admin.report.reports.areas',
            compact('type', 'chartData', 'title', 'statusesCount', 'totalSum', 'totalCount', 'orders',
                'filter_data'));
    }
}
