<?php

namespace Modules\Report\Reports;



use Carbon\Carbon;
use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;

class CustomersBest
{
    static public function get($filter_data)
    {
        $customers = Customer::whereHas('orders', function ($orders) use ($filter_data) {
            $orders->whereBetween('created_at', [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ]);
            $orders->where('status', '!=', Order::STATUS_NAME['canceled']);
        })->get();

        $customersByOrderCount = $customers->groupBy(function ($customer) {
            return $customer->orders()->count();
        })->sortByDesc(function ($customers, $key) {
            return $key;
        })->take(3);

        $customersByOrderValue = $customers->sortByDesc(function ($customer) {
            return $customer->orders()->sum('total_price');
        })->take(3);

        $customersByCanceledOrders = Customer::without('addresses')->whereHas('orders',
            function ($orders) use ($filter_data) {
                $orders->whereBetween('created_at', [
                    Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                    Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                ]);
            })->get()
                                             ->sortByDesc(function ($customer) {
                                                 return $customer->orders()->where('status',
                                                     Order::STATUS_NAME['canceled'])->count();
                                             })
                                             ->take(3);

        $title = 'Best Customers Report';
        return view('report::admin.report.reports.customers-best',
            compact('title', 'filter_data', 'customersByOrderCount',
                'customersByOrderValue', 'customersByCanceledOrders'
            ));
    }
}
