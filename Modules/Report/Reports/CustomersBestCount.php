<?php

namespace Modules\Report\Reports;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\Order;

class CustomersBestCount
{
    /**
     * @param $filter_data
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    static public function get($filter_data)
    {

        $orders = Order::select('customers.phone','customers.name', 'orders.customer_id', DB::raw('count(orders.id) as total'),DB::raw('sum(total_price)as total_price'))
            ->join('customers', 'customers.id', 'orders.customer_id')
            ->whereBetween('orders.created_at', [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ])->where('orders.status', '!=', Order::STATUS_NAME['canceled'])
            ->havingRaw("total BETWEEN  " . $filter_data['orderCount'].' AND  '.$filter_data['to_orderCount'])
            ->orderBy(DB::raw('total'), 'DESC')
            ->groupBy('customers.phone','customers.name','orders.customer_id')
            ->get();


        $title = 'Best Customers Report By Order Count';

        return view('report::admin.report.reports.customers-best-count', compact('title', 'filter_data', 'orders'));
    }
}
