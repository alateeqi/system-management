<?php

namespace Modules\Report\Reports;



use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;

class CustomersCount
{
    static public function get($filter_data)
    {
        $customers = Customer::whereHas('orders', function ($orders) use ($filter_data) {
            $orders->whereBetween('created_at', [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ]);
            $orders->where('status', '!=', Order::STATUS_NAME['canceled']);
        })->get();

        $newCustomers = $customers->whereBetween('created_at', [
            Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
            Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
        ]);

        $newCustomersHaveOneOrder = $newCustomers->filter(function ($customer) {
            return $customer->orders()->count() === 1;
        })->count();

        $newCustomersHaveMoreThanOneOrder = $newCustomers->filter(function ($customer) {
            return $customer->orders()->count() > 1;
        })->count();

        $newCustomersCount = $newCustomers->count();
        $newCustomersGrouped = $newCustomers->groupBy([
            function ($val) use ($filter_data) {
                return Carbon::parse($val->created_at)->format($filter_data['frequency']);
            }
        ]);

        $activeCustomers = $customers->whereNotBetween('created_at', [
            Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
            Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
        ]);

        $activeCustomersCount = $activeCustomers->count();
        $activeCustomersGrouped = $activeCustomers->groupBy([
            function ($activeCustomer) use ($filter_data) {
                return Carbon::parse($activeCustomer->orders()->orderBy('created_at')->first()->created_at)->format($filter_data['frequency']);
            }
        ]);

        $first = ['Dates', 'New Customers', 'Active Customers'];
        $chartData = [$first];

        switch ($filter_data['frequency']) {
            case 'Y/m/d':
                $intervalVal = '1 day';
                break;
            case 'W':
                $intervalVal = '1 week';
                break;
            case 'm':
                $intervalVal = '1 month';
                break;
            case 'Y':
                $intervalVal = '1 year';
                break;
            case 'H':
                $intervalVal = '1 hour';
                break;
        }

        $interval = DateInterval::createFromDateString($intervalVal);
        if ($intervalVal === '1 hour') {
            $period = new DatePeriod(Carbon::parse('2020-01-01 00:00:00'), $interval,
                Carbon::parse('2020-01-01 23:59:59'));
        } else {
            $period = new DatePeriod(Carbon::parse($filter_data['startDate'])->setHours(0)->setMinutes(0)->setSeconds(0),
                $interval,
                Carbon::parse($filter_data['endDate'])->setHours(23)->setMinutes(59)->setSeconds(59));
        }

        foreach ($period as $date) {
            $date = $date->format($filter_data['frequency']);
            $column = [$date];
            if (isset($newCustomersGrouped[$date])) {
                $column[] = $newCustomersGrouped[$date]->count();
            } else {
                $column[] = 0;
            }

            if (isset($activeCustomersGrouped[$date])) {
                $column[] = $activeCustomersGrouped[$date]->count();
            } else {
                $column[] = 0;
            }
            $chartData[] = $column;
        }

        $type = "ColumnChart";
        $title = 'Customers Report';

        return view('report::admin.report.reports.customers-count',
            compact('type', 'chartData', 'title', 'filter_data',
                'newCustomersCount', 'activeCustomersCount', 'newCustomersHaveOneOrder',
                'newCustomersHaveMoreThanOneOrder'));
    }
}
