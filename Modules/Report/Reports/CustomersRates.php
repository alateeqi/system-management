<?php

namespace Modules\Report\Reports;



use Carbon\Carbon;
use Modules\Customer\Entities\Customer;
use Modules\Order\Entities\Order;

class CustomersRates
{
    static public function get($filter_data)
    {
        $customers = Customer::whereHas('orders', function ($orders) use ($filter_data) {
            $orders->where('status', '!=', Order::STATUS_NAME['canceled']);
        })->orderBy('id','desc')->paginate(30);

        $customersActivity = $customers->sortByDesc(function ($customer) {
            $diffDays = Carbon::parse($customer->created_at)->diffInDays(Carbon::now());
            if ($diffDays > 10) {
                return $customer->orders()->count() * $customer->orders()->sum('total_price') / $diffDays;
            }

            return 0;
        });

        $title = 'Customers Rates Report';

        return view('report::admin.report.reports.customers-rates',
            compact('title', 'filter_data', 'customersActivity','customers'
            ));
    }
}
