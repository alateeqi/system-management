<?php

namespace Modules\Report\Reports;



use Carbon\Carbon;
use Modules\Order\Entities\Order;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Transaction\Entities\Transaction;

class General
{
    static public function get($filter_data)
    {

        $orders = Order::whereHas('products')->whereBetween('requested_time',
            [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ])
                       ->whereNotIn('status', [Order::STATUS_NAME['new'], Order::STATUS_NAME['updated']])
                       ->get();

        $doneOrders = $orders->whereNotIn('status', [Order::STATUS_NAME['canceled']]);
        $data['orders'] = $orders;
        $data['sales'] = $doneOrders->sum('total_price');
        $data['doneOrders'] = $doneOrders;
        $data['canceledSales'] = $orders->where('status', Order::STATUS_NAME['canceled'])->sum('total_price');
        $data['delivery'] = $doneOrders->sum('delivery_price');
        $data['freeOrders'] = $doneOrders->where('total_price', 0)->sum(function ($order) {
            return $order->products_price;
        });
        $data['freeOrdersDelivery'] = $doneOrders->where('total_price', 0)->sum(function ($order) {
            return $order->delivery_price;
        });
        $data['discounts'] = $doneOrders
            ->filter(function ($item) {
                return $item->discount != 0 || $item->discount_percentage != 0;
            })
            ->sum(function ($order) {
                return ($order->products_price + $order->delivery_price) - $order->total_price;
            });


        $accountId = 526;

        $account = Taxonomy::whereId($accountId)->first();

        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['deliveryCommission'] = Transaction::without(['account'])
                                                 ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                                 ->whereBetween('records.date', [
                                                     Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                                     Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                                 ])
                                                 ->where('records.status', 1)
                                                 ->whereIn('transactions.account_id', $relatedAccounts)
                                                 ->sum('debt_amount');

        // ============== //
        $accountId = 134;
        $account = Taxonomy::whereId($accountId)->first();
        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['expenses'] = Transaction::without(['account'])
                                       ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                       ->whereBetween('records.date', [
                                           Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                           Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                       ])
                                       ->where('records.status', 1)
                                       ->whereIn('transactions.account_id', $relatedAccounts)
                                       ->sum('debt_amount');
        // ============== //
        $accountId = 163;
        $account = Taxonomy::whereId($accountId)->first();
        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['productionExpenses'] = Transaction::without(['account'])
                                                 ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                                 ->whereBetween('records.date', [
                                                     Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                                     Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                                 ])
                                                 ->where('records.status', 1)
                                                 ->whereIn('transactions.account_id', $relatedAccounts)
                                                 ->sum('debt_amount');
        // ============== //
        // ============== //
        $accountId = 479;
        $account = Taxonomy::whereId($accountId)->first();
        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['salaries'] = Transaction::without(['account'])
                                       ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                       ->whereBetween('records.date', [
                                           Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                           Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                       ])
                                       ->where('records.status', 1)
                                       ->whereIn('transactions.account_id', $relatedAccounts)
                                       ->sum('debt_amount');

        $accountId = 723;
        $account = Taxonomy::whereId($accountId)->first();
        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['knet'] = Transaction::without(['account'])
                                       ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                       ->whereBetween('records.date', [
                                           Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                           Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                       ])
                                       ->where('records.status', 1)
                                       ->whereIn('transactions.account_id', $relatedAccounts)
                                       ->sum('debt_amount');

        // ============== //
        $accountId = 480;
        $account = Taxonomy::whereId($accountId)->first();
        $relatedAccounts = array_merge([$account->id], $account->descendants->pluck('id')->toArray());
        $data['rents'] = Transaction::without(['account'])
                                    ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                    ->whereBetween('records.date', [
                                        Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                        Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                    ])
                                    ->where('records.status', 1)
                                    ->whereIn('transactions.account_id', $relatedAccounts)
                                    ->sum('debt_amount');

        $tags = Taxonomy::accountTags()->get();
        $data['tags'] = [];
        foreach ($tags as $tag) {

            $data['tags'][] = [
                'title' => $tag->title,
                'sum' => Transaction::without(['account'])
                                    ->leftJoin('records', 'transactions.record_id', '=', 'records.id')
                                    ->whereBetween('records.date', [
                                        Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                                        Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                                    ])
                                    ->where('records.status', 1)
                                    ->whereHas('tags', function ($query) use ($tag) {
                                        $query->where('tag_id', $tag->id);
                                    })
                                    ->sum('debt_amount')
            ];
        }

        // ============== //

        $title = 'General Report';

        return view('report::admin.report.reports.general',
            compact('title', 'orders', 'filter_data', 'data'));
    }
}
