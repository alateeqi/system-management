<?php

namespace Modules\Report\Reports;


use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderProduct;
use Modules\Product\Entities\Product;
use Modules\Product\Repositories\Admin\AdminProductRepository;

class Products
{
    static public function get($filter_data)
    {
//        if ( ! $filter_data['products']) {
//            $filter_data['products'] = Product::all();
//        }

        $initial = Order::whereHas('products')->whereBetween($filter_data['date'],
            [
                Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
            ]);

        $totalSum = $initial->selectRaw('SUM(total_price - (delivery_price + discount)) as sum');
        $totalSum = $totalSum->first()->sum;
        $totalCount = $initial->count();

        $orderProduct = OrderProduct::with([
            'product',
            'order'
        ])->whereHas('order', function ($q) use ($filter_data) {
            $q->whereBetween($filter_data['date'],
                [
                    Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),
                    Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')
                ])
                ->whereIn('status', $filter_data['statuses']);
            if ($filter_data['employees']) {
                $q->whereIn('user_id', $filter_data['employees']);
            }
            if ($filter_data['cities']) {
                $q->whereIn('city_id', $filter_data['cities']);
            }
            if ($filter_data['paymentMethods']) {
                $q->whereIn('payment_method_id', $filter_data['paymentMethods']);
            }
            if ($filter_data['gates']) {
                $q->whereIn('gate_id', $filter_data['gates']);
            }
        });
        if ($filter_data['products']) {
            $orderProduct = $orderProduct->whereIn('product_id', $filter_data['products']);
        }


        $frequency = $filter_data['frequency'];

        $orderProduct = $orderProduct->get();
        $orderProductGrouped = $orderProduct->groupBy([
            function ($val) use ($frequency, $filter_data) {
                return Carbon::parse($val->order->{$filter_data['date']})->format($frequency);
            },
            function ($item) {
                return $item->product_id;
            },
            function ($item) {
                return $item->order_id;
            }
        ]);

        $first = ['dates'];
        $products_collect = [];
        if (!$filter_data['products']) {
            $filter_data['products'] = AdminProductRepository::allProduct();
            foreach ($filter_data['products'] as $product) {
                $first[] = $product['title'];
            }
        } else {
            foreach ($filter_data['products'] as $product) {
                $obj_product = AdminProductRepository::findStatic($product);
                array_push($products_collect, $obj_product);
                $first[] = $obj_product->title;
            }
            $filter_data['products'] = $products_collect;
        }

//        foreach ($filter_data['products'] as $product) {
//            $first[] = $product['title'];
//        }
        $chartData = [$first];

        switch ($frequency) {
            case 'Y/m/d':
                $intervalVal = '1 day';
                break;
            case 'W':
                $intervalVal = '1 week';
                break;
            case 'm':
                $intervalVal = '1 month';
                break;
            case 'Y':
                $intervalVal = '1 year';
                break;
            case 'H':
                $intervalVal = '1 hour';
                break;
        }
        $interval = DateInterval::createFromDateString($intervalVal);
        if ($intervalVal === '1 hour') {
            $period = new DatePeriod(Carbon::parse('2020-01-01 00:00:00'), $interval,
                Carbon::parse('2020-01-01 23:59:59'));
        } else {
            $period = new DatePeriod(Carbon::parse($filter_data['startDate'])->setHours(0)->setMinutes(0)->setSeconds(0),
                $interval,
                Carbon::parse($filter_data['endDate'])->setHours(23)->setMinutes(59)->setSeconds(59));
        }
        foreach ($period as $date) {
            $date = $date->format($frequency);
            $column = [$date];
            foreach ($filter_data['products'] as $product) {
                if (isset($orderProductGrouped[$date][$product['id']])) {
                    $column[] = $orderProductGrouped[$date][$product['id']]->count();
                } else {
                    $column[] = 0;
                }
            }
            $chartData[] = $column;
        }

        $type = "ColumnChart";
        $title = 'Products Report';

        $statusesCount['total'] = $orderProduct->groupBy('order_id')->count();
        $statusesCount['totalSales'] = $orderProduct->groupBy('order_id')->sum(function ($item) {
            return $item[0]->order->total_price - ($item[0]->order->delivery_price + $item[0]->order->discount);
        });
        $statusesCount['delivered'] = $orderProduct->where('order.status',
            Order::STATUS_NAME['delivered'])->groupBy('order_id')->count();
        $statusesCount['canceled'] = $orderProduct->where('order.status',
            Order::STATUS_NAME['canceled'])->groupBy('order_id')->count();
        $statusesCount['proved'] = $orderProduct->where('order.status',
            Order::STATUS_NAME['proved'])->groupBy('order_id')->count();
        $statusesCount['others'] = $orderProduct->whereIn('order.status',
            [Order::STATUS_NAME['new'], Order::STATUS_NAME['updated']])->groupBy('order_id')->count();
        $users = [];
        if ($filter_data['employees']) {
            foreach ($orderProduct->map(function ($order) {
                return $order->order->user;
            })->unique('id') as $user) {

                $users[] = [
                    'id' => $user->id,
                    'name' => $user->name
                ];
            }
            $filter_data['employees'] = $users;
        }
        return view('report::admin.report.reports.products',
            compact('type', 'chartData', 'title', 'statusesCount', 'totalSum', 'totalCount', 'orderProduct', 'filter_data'));
    }
}
