<?php

namespace Modules\Report\Repositories\Admin;


use App\Notifications\SendMessageSms;
use Modules\Auth\Entities\User;
use Modules\Customer\Repositories\Admin\AdminCustomerRepository;
use Modules\Product\Repositories\Admin\AdminProductRepository;
use Modules\Report\Interfaces\Admin\AdminReportInterface;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Repositories\Admin\City\AdminCityRepository;
use Modules\Taxonomy\Repositories\Admin\Gate\AdminGateRepository;
use Modules\Taxonomy\Repositories\Admin\Payment\AdminPaymentRepository;
use Modules\User\Repositories\Admin\AdminUserRepository;

class AdminReportRepository implements AdminReportInterface
{

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return [
            'products' => AdminProductRepository::allProduct(),
            'employees' => (new AdminUserRepository($this->user))->allUser(),
            'cities' => AdminCityRepository::allCities(),
            'paymentMethods' => AdminPaymentRepository::activePaymentMethods(),
            'gates' => AdminGateRepository::active_gates(),
            'accounts' => Taxonomy::accounts()->get(),
            'gatesIds' => collect(AdminGateRepository::gates())->map(function ($item) {
                return $item->id;
            }),
            'paymentMethodsIds' => collect(AdminPaymentRepository::activePaymentMethods())->map(function ($item) {
                return $item->id;
            }),
        ];
//        Taxonomy::accounts()->with([
//            'translation' => function ($q) {
//                return $q->select('title', 'taxonomy_id');
//            }
//        ])->defaultOrder()->get()

    }

    public function sendMessages($data)
    {
        for ($i = 0; $i < count($data['phones']); $i++) {
            $customer = (new AdminCustomerRepository($this->user))->findByPhone($data['phones'][$i]);
            $customer->notify(new SendMessageSms($data['message'],$data['phones'][$i]));
        }

    }
}
