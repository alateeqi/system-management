<?php 

return [
    'excel_reports' => 'تصدير اكسل',
    'from'=>'من ',
    'to'=>'الى',
    'status'=>'الحالة',
    'new'=>'الجديد',
    'products_report'=>'تقرير المنتجات',
    'products_details'=>'تفاصيل المنتجات',
    'employee_report'=>'تقرير الموظيفن',
    'date_report'=>'تقرير التاريخ',
    'day_report'=>'تقرير اليوم',
    'areas_report'=>'تقرير المناطق',
    'sales_report'=>'تقرير المبيعات',
    'Sales_24_hours'=>'المبيعات خلال ٢٤ ساعة',
    'name'=>'الاسم ',
]

?>