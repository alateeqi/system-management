<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Logo</th>
			<th>Areas report</th>
			<th>Day: {{ $day }}</th>
			<th>From {{ $startDate }} To {{ $endDate }}</th>
		</tr>
		<tr>
			<th colspan="2">Orders Count: {{ $ordersCount = $orders->count() }}</th>
			<th colspan="2">Sum of Orders: {{ $ordersSum = $orders->sum('total_price') }} </th>
		</tr>
		</thead>
		<thead>
		<tr>
			<th>N</th>
			<th>The area</th>
			<th>Number of invoices</th>
			<th>Percentage of invoices</th>
			<th>Value of total sales</th>
			<th>Percentage of total sales</th>
		</tr>
		</thead>
		<tbody>
		@php($i = 1)
		@php($ordersCount = $orders->count())
		@foreach($orders->groupBy('city_id')->sortByDesc(function($order){ return $order->count(); }) as $city_id => $order)
			@if(empty($city_id))
				@foreach($order->groupBy('gate_id') as $pickUpOrder)
					<tr>
						<td>{{ $i++ }}</td>
						<td>{{ $pickUpOrder->first()->gate->title }}</td>
						<td>{{ $pickUpOrder->count() }}</td>
						<td>{{ number_format(($pickUpOrder->count() / $ordersCount) * 100, 2) }}%</td>
						<td>{{ $pickUpOrder->sum('total_price') }} </td>
						<td>{{ number_format(($pickUpOrder->sum('total_price') / $ordersSum) * 100, 2) }}%</td>
					</tr>
				@endforeach
			@else
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $city_id ? $order->first()->city->title : 'Pick Up' }}</td>
					<td>{{ $order->count() }}</td>
					<td>{{ number_format(($order->count() / $ordersCount) * 100, 2) }}%</td>
					<td>{{ $order->sum('total_price') }} </td>
					<td>{{ number_format(($order->sum('total_price') / $ordersSum) * 100, 2) }}%</td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>
</div>