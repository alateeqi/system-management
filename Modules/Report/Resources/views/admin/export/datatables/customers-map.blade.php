<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Name</th>
            <th>City</th>
            <th>street</th>
            <th>block</th>
            <th>house</th>
            <th>avenue</th>
            <th>flat</th>
            <th>floor</th>
            <th>notes</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>

            @foreach($customers as $customer)
                    @foreach($customer->addresses as $address)
                        <tr>
                            <td>{{ $address->id }}</td>
                            <td>{{ $customer->phone}}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->name }}</td>
                            <td>{{ $address->city->title }}</td>
                            <td>{{ $address->street }}</td>
                            <td>{{ $address->block }}</td>
                            <td>{{ $address->house }}</td>
                            <td>{{ $address->avenue }}</td>
                            <td>{{ $address->flat }}</td>
                            <td>{{ $address->floor }}</td>
                            <td>{{ $address->notes }}</td>
                            <td>{{ $address->created_at }}</td>
                        </tr>
                    @endforeach

            @endforeach


        </tbody>
    </table>
</div>
