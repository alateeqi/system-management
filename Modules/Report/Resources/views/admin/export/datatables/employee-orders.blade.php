<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Logo</th>
			<th>Employee report</th>
			<th>Day: {{ $day }}</th>
			<th>From {{ $startDate }} To {{ $endDate }}</th>
		</tr>
		<tr>
			<th colspan="2">Orders Count: {{ $ordersCount = $orders->count() }}</th>
			<th>Sum of orders: {{ $ordersSum = $orders->sum('total_price') }} </th>
		</tr>
		</thead>
		<thead>
		<tr>
			<th>N</th>
			<th>Employee</th>
			<th>Number of invoices</th>
			<th>Percentage of invoices</th>
			<th>Value of total price</th>
			<th>Percentage of total price</th>
		</tr>
		</thead>
		<tbody>
		@php($i = 1)
		@foreach($orders->groupBy('user_id')->sortByDesc(function($order){ return $order->count(); }) as $order)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $order->first()->user->name }}</td>
				<td>{{ $order->count() }}</td>
				<td>{{ number_format(($order->count() / $ordersCount) * 100, 2) }}%</td>
				<td>{{ $order->sum('total_price') }} </td>
				<td>{{ number_format(($order->sum('total_price') / $ordersSum) * 100, 2) }}%</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>