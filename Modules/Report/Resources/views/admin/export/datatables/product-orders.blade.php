<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Logo</th>
			<th>Products report</th>
			<th>Day: {{ $day }}</th>
			<th>From {{ $startDate }} To {{ $endDate }}</th>
		</tr>
		<tr>
			<th colspan="2">Orders Count: {{ $ordersCount = $products->groupBy('order_id')->count() }}</th>
			<th colspan="2">Sum of orders: {{ $ordersSum = $products->sum('price') }} </th>
			<th colspan="2">Products Count: {{ $productsCount = $products->groupBy('product_id')->count() }}</th>
		</tr>
		</thead>
		<thead>
		<tr>
			<th>N</th>
			<th>ID</th>
			<th>Product</th>
			<th>Number of invoices</th>
			<th>Percentage of invoices</th>
			<th>Percentage of products</th>
			<th>Value of total price</th>
			<th>Percentage of total price</th>
		</tr>
		</thead>
		<tbody>
		@php($i = 1)
		@foreach($products->groupBy('product_id')->sortByDesc(function($product){ return $product->count(); }) as $product)
			<tr>
				<td>{{ $i++ }}</td>
				<td>P{{ $product->first()->product->id }}</td>
				<td>{{ $product->first()->product->title }}</td>
				<td>{{ $product->count() }}</td>
				<td>{{ number_format(($product->count() / $ordersCount) * 100, 2) }}%</td>
				<td>{{ number_format(($product->count() / $productsCount) * 100, 2) }}%</td>
				<td>{{ $product->sum('price') }} </td>
				<td>{{ number_format(($product->sum('price') / $ordersSum) * 100, 2) }}%</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>