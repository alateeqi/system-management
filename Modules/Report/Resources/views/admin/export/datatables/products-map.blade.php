<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Categories</th>
            <th>Title AR</th>
            <th>Title EN</th>
            <th>type</th>
            <th>Price</th>

        </tr>
        </thead>
        <tbody>
        @foreach($rows as $product)


            <tr>
                <td>{{ $product['id'] }}</td>
                <td>{{ $product['category']}}</td>
                <td>{{ $product['title_ar'] }}</td>
                <td>{{ $product['title_en']}}</td>
                <td>{{ $product['type'] }}</td>
                <td>{{ $product['price'] }}</td>

            </tr>


        @endforeach

        </tbody>
    </table>

</div>

