<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Logo</th>
			<th>Date report</th>
			<th>Day: {{ $day }}</th>
			<th>From {{ $startDate }} To {{ $endDate }}</th>
		</tr>
		<tr>
			<th colspan="2">Orders Count: {{ $ordersCount = $orders->count() }}</th>
			<th colspan="2">Sum of orders: {{ $ordersSum = $orders->sum('total_price') }} </th>
		</tr>
		</thead>
		<thead>
		<tr>
			<th>N</th>
			<th>Hour</th>
			<th>Number of invoices</th>
			<th>Percentage of total invoices count</th>
			<th>Total sales</th>
			<th>Percentage of total sales</th>
		</tr>
		</thead>
		<tbody>
		@php($i = 1)
		@foreach($orders->groupBy(function ($val) {
					return Carbon\Carbon::parse($val->requested_time)->format('H');
				})->sortByDesc(function($order){ return $order->count(); }) as $order)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $order->first()->requested_time->format('H') }}</td>
				<td>{{ $order->count() }}</td>
				<td>{{ number_format(($order->count() / $ordersCount) * 100, 2) }}%</td>
				<td>{{ $order->sum('total_price') }} </td>
				<td>{{ number_format(($order->sum('total_price') / $ordersSum) * 100, 2) }}%</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>