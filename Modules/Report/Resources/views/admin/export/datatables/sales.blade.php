<div class="table-responsive">
	<table class="table table-striped">
		<thead>
		<tr>
			<th colspan="2">Logo</th>
			<th colspan="2">Sales report</th>
			<th>Day: {{ $day }}</th>
			<th colspan="4">From {{ $startDate }} To {{ $endDate }}</th>
		</tr>
		</thead>
		<thead>
		<tr>
			<th>N</th>
			<th>Employee</th>
			<th>Phone</th>
			<th>City</th>
			<th>Order#</th>
			<th>Date</th>
			<th>Invoice Time</th>
			<th>Product Price</th>
			<th>Delivery</th>
			<th>Discount</th>
			<th>Discount percentage</th>
			<th>Total Price</th>
			<th>Gate</th>
			<th>Payment Mode</th>
			<th>Status</th>
			<th>Canceled Due</th>
			<th>Receipt no</th>
			<th>Notes</th>
		</tr>
		</thead>
		<tbody>
		@php($i = 1)
		@php($totalProductPrice = 0)
		@foreach($orders as $order)
			@php($productPrice = $order->products->sum('price'))
			@php($totalProductPrice += $productPrice)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $order->user->name }}</td>
				<td>{{ $order->customer->phone }}</td>
				<td>{{ optional($order->city)->title }}</td>
				<td>#{{ $order->id }}</td>
				<td>{{ $order->created_at->format('Y/m/d H:i') }}</td>
				<td>{{ $order->requested_time->format('Y/m/d H:i') }}</td>
				<td>{{ $productPrice }} </td>
				<td>{{ $order->delivery_price }} </td>
				<td>{{ $order->discount }} </td>
				<td>{{ $order->discount_percentage }} %</td>
				<td>{{ $order->total_price }} </td>
				<td>{{ $order->gate->title }}</td>
				<td>{{ $order->paymentMethod->title }}</td>
				<td>{{ $order->status_name }}</td>
				<td>{{ $order->canceled_due }}</td>
				<td>{{ $order->receipt }}</td>
				<td>{{ $order->notes }}</td>
			</tr>
		@endforeach
		<tr>
			<td>Total</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>{{ $totalProductPrice }} </td>
			<td>{{ $orders->sum('delivery_price') }} </td>
			<td>{{ $orders->sum('discount') }} </td>
			<td></td>
			<td>{{ $orders->sum('total_price') }} </td>
			<td></td>
			<td></td>
		</tr>
		</tbody>
	</table>
	<table class="table table-striped">
		<thead>
		<tr>
			<th colspan="5">Payment Methods</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>#</td>
			<td>Orders Count</td>
			<td>percentage of all Orders</td>
			<td>Total sales</td>
			<td>percentage of total sales</td>
		</tr>
		@foreach($orders->groupBy('payment_method_id') as $paymentMethodOrders)
			<tr>
				<td>{{ $paymentMethodOrders->first()->paymentMethod->title }}</td>
				<td>{{ $paymentMethodOrders->count() }}</td>
				<td>{{ number_format(($paymentMethodOrders->count() / $orders->count()) * 100, 2) }}%</td>
				<td>{{ $paymentMethodOrders->sum('total_price') }} </td>
				<td>{{ number_format(($paymentMethodOrders->sum('total_price') / $orders->sum('total_price')) * 100, 2) }}
					%
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
		<tr>
			<th colspan="5">Gates</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>#</td>
			<td>Orders Count</td>
			<td>percentage of all Orders</td>
			<td>Total sales</td>
			<td>percentage of total sales</td>
		</tr>
		@foreach($orders->groupBy('gate_id') as $gateOrders)
			<tr>
				<td>{{ $gateOrders->first()->gate->title }}</td>
				<td>{{ $gateOrders->count() }}</td>
				<td>{{ number_format(($gateOrders->count() / $orders->count()) * 100, 2) }}%</td>
				<td>{{ $gateOrders->sum('total_price') }} </td>
				<td>{{ number_format(($gateOrders->sum('total_price') / $orders->sum('total_price')) * 100, 2) }}
					%
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
		<tr>
			<th colspan="5">Order Statuses</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>#</td>
			<td>Orders Count</td>
			<td>percentage of all Orders</td>
			<td>Total sales</td>
			<td>percentage of total sales</td>
		</tr>
		@foreach($orders->groupBy('status') as $statusOrders)
			<tr>
				<td>{{ $statusOrders->first()->statusName }}</td>
				<td>{{ $statusOrders->count() }}</td>
				<td>{{ number_format(($statusOrders->count() / $orders->count()) * 100, 2) }}%</td>
				<td>{{ $statusOrders->sum('total_price') }} </td>
				<td>{{ number_format(($statusOrders->sum('total_price') / $orders->sum('total_price')) * 100, 2) }}%
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>