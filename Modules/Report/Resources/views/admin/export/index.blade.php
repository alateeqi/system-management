@extends('layouts.dashboard.app')
@section('title')
    Export Excel
@endsection

@section('content')
    <!-- Main content -->
    <section class="page-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-12">


                    <div class="card">
                        <div class="card-header">

                            @lang('report::site.excel_reports')

                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form-group" method="POST" action="{{ route('admin.export.show') }}"
                                  id="form_filter_export">
                                <div class="row">
                                    <div class="col-4 ">
                                        <h5>@lang('report::site.from')</h5>
                                        <input id="date_from" type="text" class="form-control date-datepicker"
                                               name="startDate"
                                               value="{{ old('startDate',now()->startOfMonth()->toDateString()) }}">
                                    </div>

                                    <div class="col-4 ">
                                        <h5>@lang('report::site.to')</h5>
                                        <input id="date_to" type="text" name="endDate"
                                               class="form-control date-datepicker"
                                               value="{{ old('endDate',now()->endOfMonth()->toDateString()) }}">
                                    </div>


                                </div>
                                <br/>
                                <div class="row">
                                    <h5>@lang('report::site.status')</h5>
                                    <br/>
                                    <br/>

                                    @for ($i = 0; $i < count(\Modules\Order\Entities\Order::STATUSES); $i++)
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" name="statuses[]" type="checkbox"
                                                       id="{{ \Modules\Order\Entities\Order::STATUSES[$i]['name'] }}"
                                                       checked
                                                       value="{{ $i }}">
                                                <label class="form-check-label "
                                                       for="{{ \Modules\Order\Entities\Order::STATUSES[$i]['name'] }}">{{ \Modules\Order\Entities\Order::STATUSES[$i]['name'] }}</label>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <hr/>
                                <br/>
                                <div class="row d-flex align-items-stretch">
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report0" type="radio" class="material"
                                                   value="ProductSales">
                                            <label for="report0"
                                                   class="material">@lang('report::site.products_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report1" type="radio" class="material"
                                                   value="ProductsMapping">
                                            <label for="report1"
                                                   class="material">@lang('report::site.products_details')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report2" type="radio" class="material"
                                                   value="EmployeeSales">
                                            <label for="report2"
                                                   class="material">@lang('report::site.employee_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report3" type="radio" class="material"
                                                   value="DateSales">
                                            <label for="report3"
                                                   class="material"> @lang('report::site.date_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report4" type="radio" class="material"
                                                   value="DaySales">
                                            <label for="report4"
                                                   class="material"> @lang('report::site.day_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report5" type="radio" class="material"
                                                   value="AreasSales">
                                            <label for="report5"
                                                   class="material"> @lang('report::site.areas_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report6" type="radio"
                                                   class="material" value="CustomersMapping">
                                            <label for="report6"
                                                   class="material">Address Of Customers
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report7" type="radio"
                                                   class="material" value="Customers">
                                            <label for="report7"
                                                   class="material">Customers
                                                report</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report8" type="radio" class="material"
                                                   value="Sales">
                                            <label for="report8"
                                                   class="material">@lang('report::site.sales_report')</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report9" type="radio" class="material"
                                                   value="SalesBy24HoursForRequestedTime">
                                            <label for="report9" class="material"> Sales By 24 Hours<br>(Requested Time)</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="inputGroup">
                                            <input name="selected_report" id="report10" type="radio" class="material"
                                                   value="SalesBy24HoursForCreatedTime">
                                            <label for="report10" class="material">Sales By 24 Hours<br>(Created
                                                Time)</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-4 ">
                                        <button type="submit" class="btn btn-primary  form-control">

                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div id="data-response">

                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('js')
    <script>
        $('#form_filter_export').on('submit', function (e) {
            e.preventDefault()
            $('#data-response').empty()

            $.post('{{ route('admin.export.show') }}', $('#form_filter_export').serialize(), function (data,
                                                                                                       status) {

                $('#data-response').append(data.message)

            });
        })
        $(document).on('click', '.print-report', function () {
            $('.table-responsive').printThis();
        });
        function post_url(page) {
            $('#data-response').empty()
            $.post('{{ route('admin.export.show') }}?page='+page,
                ($('#form_filter_export').serialize()),
                function (data, status) {
                    $("#data-response").html(data.message);

                });
        }
    </script>
@endpush
