@extends('layouts.dashboard.app')
@section('title')
    Reports
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12 ">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h2>
                                Reports
                            </h2>
                        </div>
                        <div class="card-body">
                            <form  id="form-submit">


                                <div class="row">
                                    <div class="col-md-6">
                                        <label><h6>Select Report</h6></label>
                                        <select name="selected_report" class="form-control"
                                                id="selected_report">
                                            <option value="General">General</option>
                                            <option value="Sales">Sales</option>
                                            <option value="Products">Products</option>
                                            <option value="ProductsQuantity">ProductsQuantity</option>
                                            <option value="Areas">Areas</option>
                                            <option value="Employees">Employees</option>
                                            <option value="Accounts">Accounts</option>
                                            <option value="CustomersCount">Customers Count</option>
                                            <option value="CustomersBest">Best Customers</option>
                                            <option value="CustomersRates">Customers Rates</option>
                                            <option value="CustomersBestCount">Best Customers By Orders Count</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label><h6>@lang('report::site.from')</h6></label>
                                                    <input class="date-datepicker result form-control"
                                                           type="text"
                                                           name="startDate"
                                                           value="{{Carbon\Carbon::now()->subDays(4)->toDateString()}}"
                                                           placeholder="From Date"
                                                           id="filter-data-startDate">
                                                </div>


                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label><h5>@lang('report::site.to')</h5></label>
                                                    <input class="date-datepicker result form-control"
                                                           type="text"
                                                           name="endDate"
                                                           value="{{Carbon\Carbon::now()->toDateString()}}"
                                                           id="filter-data-endDate"
                                                           placeholder="To Date">
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                                <div class="group-form mb-2">

                                </div>
                                <div id="content-non-customers-rates">
                                    <div class="form-group mb-2">

                                        {{--                                    <div id="content-non-general">--}}
                                        {{--                                        <button onclick="setDatesToday()"--}}
                                        {{--                                                id="today-button"--}}
                                        {{--                                                class="btn btn-sm btn-secondary mb-2">--}}
                                        {{--                                            Today--}}
                                        {{--                                        </button>--}}
                                        {{--                                        <button onclick="setDatesYesterday()"--}}
                                        {{--                                                id="yesterday-button"--}}
                                        {{--                                                class="btn btn-sm btn-secondary mb-2">--}}
                                        {{--                                            Yesterday--}}
                                        {{--                                        </button>--}}
                                        {{--                                    </div>--}}
                                        <div id="content-non-general-customers-best-count">
                                            <label>
                                                <h6>
                                                    Frequency
                                                </h6>
                                            </label>
                                            <div class="row">
                                                <div class="inputGroup col-md-2 col-sm-6 col-xs-6 ">
                                                    <input name="frequency" id="frequencyY/m/d" type="radio"
                                                           class="material"
                                                           value="Y/m/d"
                                                           checked>
                                                    <label for="frequencyY/m/d" class="material">Daily</label>
                                                </div>

                                                <div class="inputGroup col-md-2 col-sm-6 col-xs-6">
                                                    <input name="frequency" id="frequencyW" type="radio"
                                                           class="material"
                                                           value="W">
                                                    <label for="frequencyW" class="material">Weekly</label>
                                                </div>

                                                <div class="inputGroup col-md-2 col-sm-6 col-xs-6">
                                                    <input name="frequency" id="frequencym" type="radio"
                                                           class="material"
                                                           value="m">
                                                    <label for="frequencym" class="material">Monthly</label>
                                                </div>

                                                <div class="inputGroup col-md-2 col-sm-6 col-xs-6">
                                                    <input name="frequency" id="frequencyY"
                                                           type="radio" class="material"
                                                           value="Y">
                                                    <label for="frequencyY" class="material">Yearly</label>
                                                </div>
                                                <div class="inputGroup col-md-2 col-sm-6 col-xs-6">
                                                    <input name="frequency" id="frequencyH" type="radio"
                                                           class="material"
                                                           value="H">
                                                    <label for="frequencyH" class="material">Hourly</label>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="content-customers-best-count">
                                        <div class="col-md-6">
                                            <label>Who has more than order</label>
                                            <select name="orderCount" class="form-control"
                                                    id="orderCount">

                                                @for ($i = 0; $i < 100; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>to who has less than order</label>
                                            <select name="to_orderCount"
                                                class="form-control"
                                                    id="to_orderCount">

                                                @for ($i = 0; $i < 100; $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>

                                    <div id="content-non-customers-best-count">
                                        <div id="content-non-account-customers-count-customers-best-General">
                                            <label>
                                                <h6>
                                                    Filter By
                                                </h6></label>
                                            <div class="row">

                                                <div class="inputGroup col-md-5">
                                                    <input name="date" id="requested_time" type="radio"
                                                           class="material"
                                                           checked
                                                           value="requested_time">
                                                    <label for="requested_time" class="material"> Request Time</label>
                                                </div>
                                                <div class=" col-md-1"></div>
                                                <div class="inputGroup col-md-5">
                                                    <input name="date" id="created_at" type="radio"
                                                           class="material"
                                                           value="created_at">
                                                    <label for="created_at" class="material">Created Time</label>
                                                </div>


                                            </div>
                                            <div class="form-group mb-2">
                                                <label>
                                                    <h6>
                                                        Statuses
                                                    </h6></label>
                                                <div class="row">
                                                    {{--                                                <div class="inputGroup col-2">--}}
                                                    {{--                                                    <div class="checked">--}}
                                                    {{--                                                        <input name="statuses" id="statuses" type="checkbox"--}}
                                                    {{--                                                               class="material"--}}
                                                    {{--                                                               value="[0,1,2,3,4]">--}}
                                                    {{--                                                        <label for="statuses" class="material"> All Statuses</label>--}}
                                                    {{--                                                    </div>--}}

                                                    {{--                                                </div>--}}

                                                    <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                                        <div class="checked">
                                                            <input name="statuses[]" id="statusesNew" type="checkbox"
                                                                   class="material"
                                                                   value="0"
                                                                   checked>
                                                            <label for="statusesNew" class="material"> New</label>
                                                        </div>

                                                    </div>
                                                    <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                                        <div class="checked">
                                                            <input name="statuses[]" id="statusesUpdated"
                                                                   type="checkbox"
                                                                   class="material"
                                                                   checked
                                                                   value="3">
                                                            <label for="statusesUpdated" class="material">
                                                                Updated</label>
                                                        </div>

                                                    </div>
                                                    <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                                        <div class="checked">
                                                            <input name="statuses[]" id="statusesDelivered"
                                                                   type="checkbox"
                                                                   class="material"
                                                                   checked
                                                                   value="1">
                                                            <label for="statusesDelivered" class="material">
                                                                Delivered</label>
                                                        </div>

                                                    </div>
                                                    <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                                        <div class="checked">
                                                            <input name="statuses[]" id="statusesCanceled"
                                                                   type="checkbox"
                                                                   class="material"

                                                                   value="4">
                                                            <label for="statusesCanceled" class="material">
                                                                Canceled</label>
                                                        </div>

                                                    </div>
                                                    <div class="inputGroup col-md-2 col-sm-2 col-xs-2">
                                                        <div class="checked">
                                                            <input name="statuses[]" id="statusesProved" type="checkbox"
                                                                   class="material"
                                                                   checked
                                                                   value="2">
                                                            <label for="statusesProved" class="material"> Proved</label>
                                                        </div>

                                                    </div>


                                                </div>
                                            </div>
                                            <div class="form-group mb-2">
                                                <label>
                                                    <h6>
                                                        Payment Methods
                                                    </h6>
                                                </label>
                                                <div class="row">
                                                    {{--                                                <div class="inputGroup col-2">--}}
                                                    {{--                                                    <div class="checked">--}}
                                                    {{--                                                        <input name="paymentMethod" id="paymentMethodsIds"--}}
                                                    {{--                                                               type="checkbox"--}}
                                                    {{--                                                               class="material"--}}
                                                    {{--                                                               value="{{json_encode($paymentMethodsIds)}}">--}}
                                                    {{--                                                        <label for="paymentMethodsIds" class="material"> All Payment--}}
                                                    {{--                                                            Methods</label>--}}
                                                    {{--                                                    </div>--}}

                                                    {{--                                                </div>--}}
                                                    @foreach($paymentMethods as $payment)
                                                        <div class="inputGroup col-md-2 col-sm-2 col-xs-2 ">
                                                            <div class="checked">
                                                                <input name="paymentMethods[]"
                                                                       id="paymentMethod{{$payment->id}}"
                                                                       type="checkbox"
                                                                       class="material"
                                                                       checked
                                                                       value="{{$payment->id}}">
                                                                <label for="paymentMethod{{$payment->id}}"
                                                                       class="material"> {{$payment->title}}</label>
                                                            </div>

                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>
                                            <div class="form-group mb-2">
                                                <label>
                                                    <h6>
                                                        Gates
                                                    </h6></label>
                                                <div class="row">
                                                    {{--                                                <div class="inputGroup col-2">--}}
                                                    {{--                                                    <div class="checked">--}}
                                                    {{--                                                        <input name="gate" id="$gatesIds" type="checkbox"--}}
                                                    {{--                                                               class="material"--}}
                                                    {{--                                                               value="{{json_encode($gatesIds)}}">--}}
                                                    {{--                                                        <label for="$gatesIds" class="material"> All Gates</label>--}}
                                                    {{--                                                    </div>--}}

                                                    {{--                                                </div>--}}

                                                    @foreach($gates as $gate)
                                                        <div class="inputGroup col-md-2 col-sm-2 col-xs-2 ">
                                                            <div class="checked">
                                                                <input name="gates[]" id="gate{{$gate->id}}"
                                                                       type="checkbox"
                                                                       class="material"
                                                                       checked
                                                                       value="{{$gate->id}}">
                                                                <label for="gate{{$gate->id}}"
                                                                       class="material"> {{$gate->title}}</label>
                                                            </div>

                                                        </div>
                                                    @endforeach

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group mb-2">
                                                        <label>
                                                            <h6>
                                                                Products
                                                            </h6></label>
                                                        <select class="multiple-select" name="products[]" multiple>
                                                            @foreach($products as $product)
                                                                <option
                                                                    value="{{$product->id}}">{{$product->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-2">
                                                        <label>
                                                            <h6>
                                                                Employees
                                                            </h6></label>
                                                        <select class="multiple-select" name="employees[]" multiple>
                                                            @foreach($employees as $index=>$employee)
                                                                <option value="{{$index}}">{{$employee}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-2">
                                                        <label>
                                                            <h6>
                                                                Cities
                                                            </h6></label>
                                                        <select class="multiple-select" name="cities[]" multiple>
                                                            @foreach($cities as $city)
                                                                <option value="{{$city->id}}">{{$city->title}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div id="content-account">
                                            <label><h6>
                                                    Account
                                                </h6></label>
                                            <select class="form-control" name="account">
                                                @foreach($accounts as $account)
                                                    <option value="{{$account->id}}">{{$account->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-sm btn-primary" id="submit-button">Show</button>
                                    <button class="btn btn-sm btn-primary print-btn" id="print-button" >Print
                                    </button>
                                    <button class="btn btn-success" onclick="ExportToExcel()">export to excel</button>

                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="col-md-12 ">

                        <div class="response-message"  ></div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('js')
    <script>
        $(function () {

            $('#content-non-general').hide()
            $('#content-customers-best-count').hide()
            $('#content-non-general-customers-best-count').hide()
            $('#content-non-customers-best-count').hide()
            $('#content-non-account-customers-count-customers-best-General').hide()
            $('#content-account').hide()

            $('#selected_report').on('change', function () {

                if ($('#selected_report').val() !== 'CustomersRates')
                    $('#content-non-customers-rates').show()


                if ($('#selected_report').val() === 'Accounts') {
                    $('#content-non-account-customers-count-customers-best-General').hide()

                    $('#content-non-customers-best-count').show()
                    $('#content-account').show()
                    // != General
                    $('#content-non-general').show()
                    //!= CustomersBestCount
                    $('#content-customers-best-count').hide()
                    return;
                }
                if ($('#selected_report').val() === 'CustomersCount') {
                    $('#content-non-account-customers-count-customers-best-General').hide()
                    $('#content-account').hide()
                    // != General
                    $('#content-non-general').show()
                    //!= CustomersBestCount
                    $('#content-customers-best-count').hide()
                    return;
                }
                if ($('#selected_report').val() === 'CustomersBest') {
                    $('#content-non-account-customers-count-customers-best-General').hide()
                    $('#content-account').hide()
                    // != General
                    $('#content-non-general').show()
                    //!= CustomersBestCount
                    $('#content-customers-best-count').hide()
                    return;
                }
                if ($('#selected_report').val() === 'General') {
                    $('#content-non-general-customers-best-count').hide()
                    $('#content-non-general').hide()

                    //!= CustomersBestCount
                    $('#content-customers-best-count').hide()

                    $('#content-non-customers-best-count').hide()
                    $('#content-non-account-customers-count-customers-best-General').hide()
                    $('#content-account').hide()
                    return;
                }
                if ($('#selected_report').val() === 'CustomersBestCount') {
                    $('#content-non-general-customers-best-count').hide()
                    $('#content-non-customers-best-count').hide()
                    //!= CustomersBestCount
                    $('#content-customers-best-count').show()

                    $('#content-non-account-customers-count-customers-best-General').hide()
                    $('#content-account').hide()

                    // != General
                    $('#content-non-general').show()

                    return;
                }

                if ($('#selected_report').val() === 'CustomersRates') {
                    $('#content-non-customers-rates').hide()
                    return;
                }


                if ($('#selected_report').val() !== 'General' && $('#selected_report').val() !== 'CustomersBestCount' && $('#selected_report').val() !== 'Accounts' && $('#selected_report').val() !== 'CustomersBest') {
                    $('#content-non-general-customers-best-count').show()
                    //!= CustomersBestCount
                    $('#content-customers-best-count').hide()
                    $('#content-non-customers-best-count').show()
                    $('#content-non-account-customers-count-customers-best-General').show()
                    $('#content-account').hide()
                    return;
                }


            })

            $('#submit-button').on('click', function (e) {
                e.preventDefault()

                $(".response-message").empty();
                $.post('/admin/reports/show',
                    ($('#form-submit').serialize()),
                    function (data) {
                        $(".response-message").html(data);

                    });
            })
            $('#print-button').on('click',function (e){
                e.preventDefault()
                $('.report-content').printThis();
            })
        })

        function post_url(page) {
            $(".response-message").empty();
            $.post('/admin/reports/show?page='+page,
                ($('#form-submit').serialize()),
                function (data) {
                    $(".response-message").html(data);

                });
        }

        function setDatesToday() {
            $('#filter-data-startDate').val('{{\Carbon\Carbon::today()->toDateString()}}');
            $('#filter-data-endDate').val('{{\Carbon\Carbon::today()->toDateString()}}');
            $('#today-button').addClass('btn-success')
            $('#yesterday-button').removeClass('btn-success')
        }

        function setDatesYesterday() {
            $('#filter-data-startDate').val('{{\Carbon\Carbon::yesterday()->toDateString()}}');
            $('#filter-data-endDate').val('{{\Carbon\Carbon::yesterday()->toDateString()}}');
            $('#yesterday-button').addClass('btn-success')
            $('#today-button').removeClass('btn-success')
        }


        function ExportToExcel() {
            var htmltable = document.getElementById('best-customer-count');
            var html = htmltable.outerHTML;
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
        }
    </script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endpush



<!-- Main content -->
