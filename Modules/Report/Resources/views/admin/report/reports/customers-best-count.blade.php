<div class="report-content">
    <h2>{{ $title }}</h2>
    <div class="mb-2">
        <b>Dates:</b>
        {{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
        - {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
    </div>

    @if($orders)
        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                data-bs-target="#exampleVerticallycenteredModal">إرسال رسالة SMS
        </button>
        <div class="card card-primary">
            <div class="card-body">
                <h3 class="my-3">
                    Best customers By Orders Count
                </h3>

                <div class="table-responsive">
                    <table class="table table-bordered" id="best-customer-count">
                        <thead>
                        <tr>
                            <th>Order Count</th>
                            <th>Customer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $phones = '';
                        @endphp
                        @foreach($orders as $order)
                            @php
                                $phones .= $order->phone.',';
                            @endphp
                            <tr>
                                <td>{{ $order->total }}</td>
                                <td>
                                    <div>
                                        <div><b>Phone:</b> {{ $order->phone }}</div>
                                        <div><b>Name:</b> {{ $order->name }}</div>
                                        <div>
                                            <b>Total Sales:</b> {{$order->total_price }} KWD
                                            {{--												<b>Total Sales:</b> {{ $order->customer->orders->whereBetween('created_at', [Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d 00:00:00'),Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d 23:59:59')])->where('status', '!=', \Modules\Order\Entities\Order::STATUS_NAME['canceled'])->sum('total_price') }} KWD--}}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
            <form method="POST" class="form-control" action="{{route('admin.send.messages')}}">
                @csrf
                <input type="hidden" name="phones[]" value="{{$phones}}">
{{--                <button type="button" class="btn btn-primary" data-bs-toggle="modal"--}}
{{--                        data-bs-target="#exampleVerticallycenteredModal">إرسال رسالة SMS--}}
{{--                </button>--}}
                <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" style="display: none;"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    نص الرسالة
                                </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <textarea name="message" class="form-control"></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">
                                    إرسال الرسالة
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @else
        <div class="callout callout-danger"><h5>No Results!</h5>
            <p>There is no one order matches selected dates</p></div>
    @endif
</div>
