<div class="report-content">
	<h2>{{ $title }}</h2>
	<div class="mb-2">
		<b>Dates:</b>
		{{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
		- {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
	</div>
	@if($customersByOrderValue)
		<div class="card card-primary">
			<div class="card-body">
				<h3 class="my-3">
					Best 3 customers By Orders Value
				</h3>

				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Customer</th>
							<th>Orders Value</th>
						</tr>
						</thead>
						<tbody>
						@foreach($customersByOrderValue as $customer)
							<tr>
								<td>
									<div><b>Phone:</b> {{ $customer->phone }}</div>
									<div><b>Name:</b> {{ $customer->name }}</div>
									<div><b>Orders Count:</b> {{ $customer->orders()->count() }}</div>
								</td>
								<td>{{ $customer->orders()->sum('total_price') }} KWD</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>

				<h3 class="my-3">
					Best 3 customers By Orders Count
				</h3>

				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Order Count</th>
							<th>Customer</th>
						</tr>
						</thead>
						<tbody>
						@foreach($customersByOrderCount as $orderCount => $customers)
							<tr>
								<td>{{ $orderCount }}</td>
								<td>
									@if($customers->count() > 3)
										{{ $customers->count() }} Customers
									@else
										@foreach($customers as $customer)
											<div>
												<div><b>Phone:</b> {{ $customer->phone }}</div>
												<div><b>Name:</b> {{ $customer->name }}</div>
												<div><b>Total Sales:</b> {{ $customer->orders->sum('total_price') }} KWD
												</div>

											</div>
										@endforeach
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>

				<h3 class="my-3">
					Top 3 customers By Canceled Orders
				</h3>

				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Canceled Order Count</th>
							<th>Customer</th>
						</tr>
						</thead>
						<tbody>
						@foreach($customersByCanceledOrders as $customer)
							@if($customer->orders()->where('status', \Modules\Order\Entities\Order::STATUS_NAME['canceled'])->count())
								<tr>
									<td>{{ $customer->orders()->where('status', \Modules\Order\Entities\Order::STATUS_NAME['canceled'])->count() }}</td>
									<td>
										<div>
											<div><b>Phone:</b> {{ $customer->phone }}</div>
											<div><b>Name:</b> {{ $customer->name }}</div>
											<div><b>Total Sales:</b> {{ $customer->orders->sum('total_price') }} KWD
											</div>
											<div><b>Total Orders Count:</b> {{ $customer->orders()->count() }}</div>
										</div>
									</td>
								</tr>
							@endif
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@else
		<div class="callout callout-danger"><h5>No Results!</h5>
			<p>There is no one order matches selected dates</p></div>
	@endif
</div>
