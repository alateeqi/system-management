<div class="report-content">
    <h2>{{ $title }}</h2>
    <div class="mb-2">
        <b>Dates:</b>
        {{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
        - {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
    </div>
    @if($customersActivity)
        <div class="card card-primary">
            <div class="card-body">
                <h3 class="my-3">
                    General Customers Rates
                </h3>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Activity Rate</th>
                            <th>Customer</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customersActivity as $customer)
                            <tr>
                                @php($diffDays = Carbon\Carbon::parse($customer->created_at)->diffInDays(Carbon\Carbon::now()))
                                <td>{{ $diffDays ? number_format($customer->orders()->count() * $customer->orders()->sum('total_price') / $diffDays , 2) : 0 }}</td>
                                <td>
                                    <div>
                                        <div><b>Phone:</b> {{ $customer->phone }}</div>
                                        <div><b>Name:</b> {{ $customer->name }}</div>
                                        <div><b>Created
                                                At</b> {{ Carbon\Carbon::parse($customer->created_at)->format('Y-m-d') }}
                                        </div>
                                        <div><b>Total Orders Sales:</b> {{ $customer->orders()->sum('total_price') }}
                                            KWD
                                        </div>
                                        <div><b>Total Orders Count:</b> {{ $customer->orders()->count() }}</div>
                                        <div><b>Last Order
                                                At:</b> {{ Carbon\Carbon::parse($customer->orders->last()->created_at)->format('Y-m-d') }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $customers->links('vendor.pagination.post-url', ['paginator' => $customers]) }}

            </div>
        </div>
    @else
        <div class="callout callout-danger"><h5>No Results!</h5>
            <p>There is no one order matches selected dates</p></div>
    @endif
</div>
