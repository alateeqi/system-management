<?php


use Illuminate\Support\Facades\Route;


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            Route::get('reports/index', [\Modules\Report\Http\Controllers\Admin\AdminReportController::class, 'index'])->name('reports.index');
            Route::get('reports/export', [\Modules\Report\Http\Controllers\Admin\AdminExportController::class, 'index'])->name('admin.export.index');

            Route::post('reports/show', [\Modules\Report\Http\Controllers\Admin\AdminReportController::class, 'show'])->name('reports.show');
            Route::post('reports/export/show', [\Modules\Report\Http\Controllers\Admin\AdminExportController::class, 'export'])->name('admin.export.show');
            Route::post('reports/send-message', [\Modules\Report\Http\Controllers\Admin\AdminReportController::class, 'send_message'])->name('admin.send.messages');
        });

    });
