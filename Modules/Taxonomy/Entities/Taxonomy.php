<?php

namespace  Modules\Taxonomy\Entities;


use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Product\Entities\Product;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class Taxonomy extends Model implements HasMedia
{
    
    use Translatable, InteractsWithMedia,  RecordsActivity, SoftDeletes,NodeTrait,HasFactory;
    
    const TYPE_CATEGORY = [
        'no' => 1,
        'images' => 1,
        'excerpt' => 'excerpt',
        'content' => 'content',
        'nested' => 'nested',
    ];

    const TYPE_CITY = [
        'no' => 2,
        'excerpt' => 'Delivery Price',
    ];

    const TYPE_PAYMENT_METHOD = [
        'no' => 3,
    ];

    const TYPE_TODO_LABEL = [
        'no' => 4,
    ];

    const TYPE_ACCOUNT = [
        'no' => 5,
        'nested' => 'nested',
        'excerpt' => 'Account No',
    ];

    const TYPE_TRANSACTION_TYPE = [
        'no' => 6,
    ];

    const TYPE_ADVANCED_OPTIONS = [
        'no' => 7,
        'excerpt' => 'Value',
    ];

    const TYPE_ACCOUNT_TAG = [
        'no' => 8,
    ];

    const TYPE_PAGE = [
        'no' => 9,
        'images' => 0,
        'excerpt' => 'excerpt',
        'content' => 'content',
    ];

    const TYPE_CONTACT = [
        'no' => 10,
        'images' => 0,
        'excerpt' => 'icon',
        'nested' => 'nested',
    ];

    const TYPE_SLIDER = [
        'no' => 11,
        'images' => 1,
        'excerpt' => 'link',
        'nested' => 'nested',
        'delete' => 'delete'
    ];

    const TYPE_TRANSLATION = [
        'no' => 12,
        'excerpt' => 'translation',
    ];

    const TYPE_META = [
        'no' => 13,
        'title' => 'Meta key',
        'excerpt' => 'Value',
    ];

    const TYPE_GATE = [
        'no' => 14,
    ];

    const TYPE_INSTAGRAM_SLIDER = [
        'no' => 15,
        'images' => 1,
        'delete' => 'delete'
    ];

    protected $translatedAttributes = ['title', 'excerpt', 'content', 'slug'];
    protected $fillable = ['title', 'excerpt', 'content', 'type', 'slug'];
    protected $appends = ['options', 'image', 'full_image'];

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }

        self::savingActivity();

        static::creating(function ($model) {
            $model->user_id = request()->user()->id;
        });

        static::updating(function ($model) {
            $model->editor_id = request()->user()->id;
        });
    }


    public function scopeCategories($query)
    {
        return $query->where('type', '=', self::TYPE_CATEGORY['no']);
    }

    public function scopeCities($query)
    {
        return $query->where('type', '=', self::TYPE_CITY['no']);
    }

    public function scopePaymentMethods($query)
    {
        return $query->where('type', '=', self::TYPE_PAYMENT_METHOD['no']);
    }

    public function scopeGates($query)
    {
        return $query->where('type', '=', self::TYPE_GATE['no']);
    }

    public function scopeTodoLabels($query)
    {
        return $query->where('type', '=', self::TYPE_TODO_LABEL['no']);
    }

    public function scopeAccounts($query)
    {
        return $query->where('type', '=', self::TYPE_ACCOUNT['no']);
    }

    public function scopeTansactionTypes($query)
    {
        return $query->where('type', '=', self::TYPE_TRANSACTION_TYPE['no']);
    }

    public function scopeAccountTags($query)
    {
        return $query->where('type', '=', self::TYPE_ACCOUNT_TAG['no']);
    }

    public function scopePages($query)
    {
        return $query->where('type', '=', self::TYPE_PAGE['no']);
    }

    public function scopeContacts($query)
    {
        return $query->where('type', '=', self::TYPE_CONTACT['no']);
    }

    public function scopeSliders($query)
    {
        return $query->where('type', '=', self::TYPE_SLIDER['no']);
    }

    public function scopeWebsiteTranslations($query)
    {
        return $query->where('type', '=', self::TYPE_TRANSLATION['no']);
    }

    public function scopeMetas($query)
    {
        return $query->where('type', '=', self::TYPE_META['no']);
    }

    public function scopeInstagramSliders($query)
    {
        return $query->where('type', '=', self::TYPE_INSTAGRAM_SLIDER['no']);
    }

    public function hasChildren()
    {
        return $this->children()->count() > 0;
    }

    public static function getCorrectType($type = null)
    {
        if (strtolower($type) === 'category') {
            return self::TYPE_CATEGORY;
        } elseif (strtolower($type) === 'city') {
            return self::TYPE_CITY;
        } elseif (strtolower($type) === 'payment method') {
            return self::TYPE_PAYMENT_METHOD;
        } elseif (strtolower($type) === 'todo label') {
            return self::TYPE_TODO_LABEL;
        } elseif (strtolower($type) === 'account') {
            return self::TYPE_ACCOUNT;
        } elseif (strtolower($type) === 'transaction type') {
            return self::TYPE_TRANSACTION_TYPE;
        } elseif (strtolower($type) === 'advanced options') {
            return self::TYPE_ADVANCED_OPTIONS;
        } elseif (strtolower($type) === 'account tags') {
            return self::TYPE_ACCOUNT_TAG;
        } elseif (strtolower($type) === 'pages') {
            return self::TYPE_PAGE;
        } elseif (strtolower($type) === 'contacts') {
            return self::TYPE_CONTACT;
        } elseif (strtolower($type) === 'sliders') {
            return self::TYPE_SLIDER;
        } elseif (strtolower($type) === 'translations') {
            return self::TYPE_TRANSLATION;
        } elseif (strtolower($type) === 'metas') {
            return self::TYPE_META;
        } elseif (strtolower($type) === 'gate') {
            return self::TYPE_GATE;
        } elseif (strtolower($type) === 'instagram sliders') {
            return self::TYPE_INSTAGRAM_SLIDER;
        } else {
            return self::TYPE_CATEGORY;
        }
    }

    /**
     * @param null $type
     *
     * @return string
     */
    public static function getCorrectTypeName($type = null)
    {
        if ( ! is_null($type) && ! is_numeric($type)) {
            $type = Taxonomy::getCorrectType($type);
        }
        if ($type === self::TYPE_CATEGORY['no']) {
            return 'Category'; /*trans('strings.Category')*/
        } elseif ($type === self::TYPE_CITY['no']) {
            return 'City';
        } elseif ($type === self::TYPE_PAYMENT_METHOD['no']) {
            return 'Payment Method';
        } elseif ($type === self::TYPE_TODO_LABEL['no']) {
            return 'Todo Label';
        } elseif ($type === self::TYPE_ACCOUNT['no']) {
            return 'Account';
        } elseif ($type === self::TYPE_TRANSACTION_TYPE['no']) {
            return 'Transaction Type';
        } elseif ($type === self::TYPE_ADVANCED_OPTIONS['no']) {
            return 'Advanced Options';
        } elseif ($type === self::TYPE_ACCOUNT_TAG['no']) {
            return 'Account Tags';
        } elseif ($type === self::TYPE_PAGE['no']) {
            return 'Pages';
        } elseif ($type === self::TYPE_CONTACT['no']) {
            return 'Contacts';
        } elseif ($type === self::TYPE_SLIDER['no']) {
            return 'Sliders';
        } elseif ($type === self::TYPE_TRANSLATION['no']) {
            return 'Translations';
        } elseif ($type === self::TYPE_META['no']) {
            return 'Metas';
        } elseif ($type === self::TYPE_GATE['no']) {
            return 'Gate';
        } elseif ($type === self::TYPE_INSTAGRAM_SLIDER['no']) {
            return 'Instagram Sliders';
        } else {
            return 'Category';
        }
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('full')
            ->performOnCollections('images');

        $this->addMediaConversion('medium')
            ->width(800)
            ->height(450)
            ->performOnCollections('images');

        $this->addMediaConversion('thumbnail')
            ->width(400)
            ->height(225)
            ->performOnCollections('images');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category', 'category_id', 'product_id');
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'transaction_tag', 'tag_id', 'transaction_id');
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'thumbnail');
    }

    public function getFullImageAttribute()
    {
        return $this->getFirstMediaUrl('images', 'full');
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function getOptionsAttribute()
    {
        switch ($this->type) {
            case 1:
                return $this::TYPE_CATEGORY;
                break;
            case 2:
                return $this::TYPE_CITY;
                break;
            case 3:
                return $this::TYPE_PAYMENT_METHOD;
                break;
            case 4:
                return $this::TYPE_TODO_LABEL;
                break;
            case 5:
                return $this::TYPE_ACCOUNT;
                break;
            case 6:
                return $this::TYPE_TRANSACTION_TYPE;
                break;
            case 7:
                return $this::TYPE_ADVANCED_OPTIONS;
                break;
            case 8:
                return $this::TYPE_ACCOUNT_TAG;
                break;
            case 9:
                return $this::TYPE_PAGE;
                break;
            case 10:
                return $this::TYPE_CONTACT;
                break;
            case 11:
                return $this::TYPE_SLIDER;
                break;
            case 12:
                return $this::TYPE_TRANSLATION;
                break;
            case 13:
                return $this::TYPE_META;
            case 14:
                return $this::TYPE_GATE;
                break;
            case 15:
                return $this::TYPE_INSTAGRAM_SLIDER;
                break;
        }
        
    }


}
