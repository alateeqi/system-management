<?php

namespace Modules\Taxonomy\Entities;


use App\Traits\RecordsActivity;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class TaxonomyTranslation extends Model
{
    use Sluggable, RecordsActivity;

    public $timestamps = false;
    protected $fillable = ['title', 'excerpt', 'slug'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return ['slug' => ['source' => 'title']];
    }

    protected static function boot()
    {
        parent::boot();
        if (auth()->guest()) {
            return true;
        }

        self::savingActivity();
    }
}
