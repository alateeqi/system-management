<?php

namespace Modules\Taxonomy\Http\Controllers\Admin;


use Illuminate\Routing\Controller;
use Modules\Taxonomy\Http\Requests\Admin\ToggleStatus;
use Modules\Taxonomy\Interfaces\Admin\AppTaxonomyInterface;



class AppTaxonomyController extends Controller
{
    public function toggleStatus (ToggleStatus $request , AppTaxonomyInterface $interface){
                
        return response()->json([
            'data' =>$interface->toggleStatus($request->validated())
        ]);
    }
}