<?php

namespace Modules\Taxonomy\Http\Controllers\Admin\Category;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Routing\Controller;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Http\Requests\Admin\Category\Store;
use Modules\Taxonomy\Http\Requests\Admin\Category\Update;
use Modules\Taxonomy\Interfaces\Admin\Category\AdminCategoryInterface;
use Modules\Taxonomy\Repositories\Guest\Category\GustCategoryRepository;

class AdminCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:settings-read'])->only('index');
        $this->middleware(['permission:settings-create'])->only('create');
        $this->middleware(['permission:settings-update'])->only('edit');
        $this->middleware(['permission:settings-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminCategoryInterface $interface)
    {

        // $categories = Taxonomy::categories()->when($request->search, function($query) use ($request) {
        //     return $query->where('title-en','like', '%' . $request->search . '%' );
        // })->latest()->Paginate();

    $categories = $interface->index($request);
        return view('taxonomy::admin.categories.list',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Request $request,AdminCategoryInterface $interface)
    {


        $categories = collect($interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });
        
        
        return view('taxonomy::admin.categories.create',  compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store( Store $request, AdminCategoryInterface $interface)
    {
    
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('categries.create')->withErrors($exception->getMessage());
        }

    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminCategoryInterface $interface,$category_id)
    {
        $category = $interface->find($category_id);
        
        $categories = collect($interface->categories())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });

            
        return view('taxonomy::admin.categories.edit',compact('categories','category'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminCategoryInterface $interface)
    {
        
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }


    public function destroy(Taxonomy $taxonomy, AdminCategoryInterface $interface )
    {

            try {
            DB::beginTransaction();
            $category=$interface->destroy($taxonomy);
            
            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('categories.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('categries.index')->withErrors($exception->getMessage());
        }
        
    }
}
