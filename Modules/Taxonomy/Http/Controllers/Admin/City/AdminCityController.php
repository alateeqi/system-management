<?php

namespace Modules\Taxonomy\Http\Controllers\Admin\City;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Http\Requests\Admin\City\Store;
use Modules\Taxonomy\Http\Requests\Admin\City\Update;
use Modules\Taxonomy\Interfaces\Admin\City\AdminCityInterface;
use Modules\Taxonomy\Repositories\Admin\City\AdminCityRepository;

class AdminCityController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:settings-read'])->only('index');
        $this->middleware(['permission:settings-create'])->only('create');
        $this->middleware(['permission:settings-update'])->only('edit');
        $this->middleware(['permission:settings-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request,AdminCityInterface $interface)
    {
        $cities = $interface->index($request);
        $status = Taxonomy::getCorrectType($request->input('status'));
        return view('taxonomy::admin.cities.list', compact('cities', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(AdminCityInterface $interface)
    {
        return view('taxonomy::admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(AdminCityInterface $interface, Store $request)
    {

        try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('cities.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->route('cities.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminCityInterface $interface, $city_id)

    {
        $city = $interface->find($city_id);
        return view('taxonomy::admin.cities.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminCityInterface $interface)
    {

        try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('cities.index')->withSuccess('Success');
        } catch (\Exception $exception) {

            DB::rollBack();

            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminCityInterface $interface, $city_id)
    {

        try {
            DB::beginTransaction();

            $city = $interface->destroy($city_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('cities.index');
        } catch (\Exception $exception) {

            DB::rollBack();

            return redirect()->route('cities.index')->withErrors($exception->getMessage());
        }
    }

    public function allCities()
    {
        return response()->json([
            'data' => AdminCityRepository::allCities()
        ]);
    }
}
