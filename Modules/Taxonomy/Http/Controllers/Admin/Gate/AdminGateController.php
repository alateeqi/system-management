<?php

namespace Modules\Taxonomy\Http\Controllers\Admin\Gate;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Http\Requests\Admin\Gate\Store;
use Modules\Taxonomy\Http\Requests\Admin\Gate\Update;
use Modules\Taxonomy\Interfaces\Admin\Gate\AdminGateInterface;

class AdminGateController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:settings-read'])->only('index');
        $this->middleware(['permission:settings-create'])->only('create');
        $this->middleware(['permission:settings-update'])->only('edit');
        $this->middleware(['permission:settings-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request , AdminGateInterface $interface)
    {
    $gates = $interface->index($request);

        return view('taxonomy::admin.gates.list',compact('gates'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {

        return view('taxonomy::admin.gates.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminGateInterface $interface)
    {
        try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('gates.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->route('gates.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminGateInterface $interface, $gate_id)
    {
        $gate = $interface->find($gate_id);
        return view('taxonomy::admin.gates.edit',compact('gate'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminGateInterface $interface)
    {
        try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('gates.index');
        } catch (\Exception $exception) {

            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminGateInterface $interface ,  $gate_id)
    {
            try {
            DB::beginTransaction();

            $payment=$interface->destroy($gate_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            
            return redirect()->route('gates.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('gates.index')->withErrors($exception->getMessage());
        }
        
    }
}
