<?php

namespace Modules\Taxonomy\Http\Controllers\Admin\Payment;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Http\Requests\Admin\Payment\Store;
use Modules\Taxonomy\Interfaces\Admin\Payment\AdminPaymentsInterface;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Http\Requests\Admin\Payment\Update;

class AdminPaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:settings-read'])->only('index');
        $this->middleware(['permission:settings-create'])->only('create');
        $this->middleware(['permission:settings-update'])->only('edit');
        $this->middleware(['permission:settings-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminPaymentsInterface $interface)
    {
    $payments = $interface->index($request);

        return view('taxonomy::admin.payment.list',compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('taxonomy::admin.payment.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request , AdminPaymentsInterface $interface)
    {
        
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('payment.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('payment.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminPaymentsInterface $interface,$payment_id)
    {
        $payment = $interface->find($payment_id);
        
        return view('taxonomy::admin.payment.edit',compact('payment'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request,AdminPaymentsInterface $interface  )
    {
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('payment.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */

    public function destroy( AdminPaymentsInterface $interface , $payment_id)
    {
            try {
            DB::beginTransaction();

            $payment=$interface->destroy($payment_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            
            return redirect()->route('payment.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('payment.index')->withErrors($exception->getMessage());
        }
        
    }
}
