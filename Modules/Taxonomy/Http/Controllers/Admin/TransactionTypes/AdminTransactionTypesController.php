<?php

namespace Modules\Taxonomy\Http\Controllers\Admin\TransactionTypes;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Interfaces\Admin\TransactionTypes\AdminTransactionTypesInterface;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Http\Requests\Admin\TransactionType\Store;
use Modules\Taxonomy\Http\Requests\Admin\TransactionType\Update;

class AdminTransactionTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:settings-read'])->only('index');
        $this->middleware(['permission:settings-create'])->only('create');
        $this->middleware(['permission:settings-update'])->only('edit');
        $this->middleware(['permission:settings-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminTransactionTypesInterface $interface)
    {
    $transactiontypes = $interface->index($request);

        return view('taxonomy::admin.transactiontypes.list',compact('transactiontypes'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('taxonomy::admin.transactiontypes.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request , AdminTransactionTypesInterface $interface)
    {

            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('transactiontypes.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('transactiontypes.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminTransactionTypesInterface $interface, $transaction_id)
    {
        $transaction = $interface->find($transaction_id);
        return view('taxonomy::admin.transactiontypes.edit',compact('transaction'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */

    public function update(Update $request, AdminTransactionTypesInterface $interface)
    {
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('transactiontypes.index');
        } catch (\Exception $exception) {            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminTransactionTypesInterface $interface , Taxonomy $taype_id)
    {
        
            try {
            DB::beginTransaction();

            $interface->destroy($taype_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('transactiontypes.index');
        } catch (\Exception $exception) {            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }
}
