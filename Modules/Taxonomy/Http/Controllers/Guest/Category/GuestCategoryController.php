<?php

namespace Modules\Taxonomy\Http\Controllers\Guest\Category;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Taxonomy\Interfaces\Guest\Category\GuestCategoryInterface;

class GuestCategoryController extends Controller
{
    public function getCategory(GuestCategoryInterface $interface, $slug)
    {
        $data = $interface->findBySlug($slug);
        return $this->respond([
            'categories' => $data['categories'],
            'category' => $data['category'],
        ]);
    }
}
