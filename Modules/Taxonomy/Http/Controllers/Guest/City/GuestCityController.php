<?php

namespace Modules\Taxonomy\Http\Controllers\Guest\City;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Taxonomy\Http\Requests\Guest\City\Search;
use Modules\Taxonomy\Interfaces\Guest\City\GuestCityInterface;


class GuestCityController extends Controller
{
    public function cities(Search $request,GuestCityInterface $interface)
    {
        return $interface->cities($request->validated());
    }
}
