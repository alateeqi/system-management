<?php

namespace Modules\Taxonomy\Http\Controllers\Guest\Page;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Taxonomy\Interfaces\Guest\GuestSiteInterface;
use Modules\Taxonomy\Interfaces\Guest\Page\GuestPageInterface;

class GuestPageController extends Controller
{
    public function page(GuestPageInterface $interface, $slug)
    {
        return $interface->page($slug);
    }
}
