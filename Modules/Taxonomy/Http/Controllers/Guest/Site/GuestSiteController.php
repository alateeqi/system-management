<?php

namespace Modules\Taxonomy\Http\Controllers\Guest\Site;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Taxonomy\Interfaces\Guest\GuestSiteInterface;

class GuestSiteController extends Controller
{
    public function siteStatus(GuestSiteInterface $site_interface)
    {
        return $site_interface->siteStatus();
    }
}
