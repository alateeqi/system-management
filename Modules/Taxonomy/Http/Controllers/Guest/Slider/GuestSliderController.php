<?php

namespace Modules\Taxonomy\Http\Controllers\Guest\Slider;

use Modules\Auth\Http\Controllers\Client\Controller;
use Modules\Taxonomy\Interfaces\Guest\Slider\GuestSliderInterface;

class GuestSliderController extends Controller
{

    public function instagramSliders(GuestSliderInterface $slider_interface)
    {
        return $slider_interface->instagramSliders();
    }
}
