<?php

namespace Modules\Taxonomy\Http\Controllers\Website\Metas;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Interfaces\Website\Metas\AdminMetasInterface;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Http\Requests\Admin\Website\Metas\Store as MetasStore;
use Modules\Taxonomy\Http\Requests\Admin\Website\Metas\Update;

class AdminMetasController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:website-read'])->only('index');
        $this->middleware(['permission:website-create'])->only('create');
        $this->middleware(['permission:website-update'])->only('edit');
        $this->middleware(['permission:website-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminMetasInterface $interface)
    {
    $metas = $interface->index($request);

        return view('taxonomy::website.metas.list',compact('metas'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('taxonomy::website.metas.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(MetasStore $request ,AdminMetasInterface $interface)
    {
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('metas.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('metas.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminMetasInterface $interface, $meta_id)
    {
        $meta = $interface->find($meta_id);
        return view('taxonomy::website.metas.edit',compact('meta'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminMetasInterface $interface)
    {
        
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('metas.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminMetasInterface $interface,Taxonomy $meta_id)
    {
            try {
            DB::beginTransaction();
            
            $interface->destroy($meta_id);
            
            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('metas.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('metas.index')->withErrors($exception->getMessage());
        }
    }
}
