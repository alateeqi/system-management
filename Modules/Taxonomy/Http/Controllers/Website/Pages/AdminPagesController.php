<?php

namespace Modules\Taxonomy\Http\Controllers\Website\Pages;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Http\Requests\Admin\Website\Pages\Store;
use Modules\Taxonomy\Http\Requests\Admin\Website\Pages\Update;
use Modules\Taxonomy\Interfaces\Website\Pages\AdminPagesInterface;

class AdminPagesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:website-read'])->only('index');
        $this->middleware(['permission:website-create'])->only('create');
        $this->middleware(['permission:website-update'])->only('edit');
        $this->middleware(['permission:website-delete'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminPagesInterface $interface)
    {
    $pages = $interface->index($request);

        return view('taxonomy::website.pages.list',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        
        return view('taxonomy::website.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminPagesInterface $interface)
    {
        
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('pages.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('pages.create')->withErrors($exception->getMessage());
        }        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminPagesInterface $interface,$page_id)
    {
            $page = $interface->find($page_id);
        return view('taxonomy::website.pages.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminPagesInterface $interface)
    {
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('pages.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy( AdminPagesInterface $interface , $page_id)
    {
            try {
            DB::beginTransaction();

            $page=$interface->destroy($page_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('pages.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('pages.index')->withErrors($exception->getMessage());
        }
        
    }
}
