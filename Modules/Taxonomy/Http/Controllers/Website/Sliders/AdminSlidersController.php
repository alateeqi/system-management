<?php

namespace Modules\Taxonomy\Http\Controllers\Website\Sliders;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Interfaces\Website\Sliders\AdminSlidersInterface;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Http\Requests\Admin\Website\Sliders\Store;
use Modules\Taxonomy\Http\Requests\Admin\Website\Sliders\Update;

class AdminSlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:website-read'])->only('index');
        $this->middleware(['permission:website-create'])->only('create');
        $this->middleware(['permission:website-update'])->only('edit');
        $this->middleware(['permission:website-delete'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request ,AdminSlidersInterface $interface)
    {
    $sliders = $interface->index($request);

        return view('taxonomy::website.sliders.list',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(AdminSlidersInterface $interface ,Request $request)
    {
        $sliders = collect($interface->sliders())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });
        
        
        return view('taxonomy::website.sliders.create',  compact('sliders'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminSlidersInterface $interface)
    {
        
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('sliders.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('sliders.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminSlidersInterface $interface , $slider_id)
    {
    $slider = $interface->find($slider_id);
        
        $sliders = collect($interface->sliders())->map(function ($value) {
            return [
                'id' => $value->id,
                'title' => $value->title
            ];
        });

        return view('taxonomy::website.sliders.edit',compact('sliders','slider'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminSlidersInterface $interface)
    {

            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('sliders.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminSlidersInterface $interface,$slider_id )
    {

            try {
            DB::beginTransaction();

            $interface->destroy($slider_id);

            DB::commit();
            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('sliders.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('sliders.index')->withErrors($exception->getMessage());
        }
    }
}
