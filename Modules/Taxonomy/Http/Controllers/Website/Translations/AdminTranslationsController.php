<?php

namespace Modules\Taxonomy\Http\Controllers\Website\Translations;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Http\Requests\Admin\Website\Translations\Store;
use Modules\Taxonomy\Interfaces\Website\Translations\AdminTranslationsInterface;
use Illuminate\Support\Facades\DB;
use Modules\Taxonomy\Http\Requests\Admin\Website\Translations\Update;

class AdminTranslationsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:website-read'])->only('index');
        $this->middleware(['permission:website-create'])->only('create');
        $this->middleware(['permission:website-update'])->only('edit');
        $this->middleware(['permission:website-delete'])->only('destroy');
    }
    
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request, AdminTranslationsInterface $interface)
    {
    $translations = $interface->index( $request);

        return view('taxonomy::website.translations.list',compact('translations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('taxonomy::website.translations.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Store $request ,AdminTranslationsInterface $interface)
    {
            try {
            DB::beginTransaction();

            $interface->store($request->validated());

            DB::commit();
            session()->flash('success', __('site.added_successfully'));
            return redirect()->route('translations.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('translations.create')->withErrors($exception->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('taxonomy::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(AdminTranslationsInterface $interface ,$translation_id)
    {
        $translation = $interface->find($translation_id);
        return view('taxonomy::website.translations.edit',compact('translation'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Update $request, AdminTranslationsInterface $interface)
    {
            try {
            DB::beginTransaction();

            $interface->update($request->validated());

            DB::commit();
            session()->flash('success', __('site.updated_successfully'));
            return redirect()->route('translations.index');
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(AdminTranslationsInterface $interface ,$translation_id)
    {
            try {
            DB::beginTransaction();

            $translation=$interface->destroy($translation_id);

            DB::commit();

            session()->flash('success', __('site.deleted_successfully'));
            return redirect()->route('translations.index');
            
        } catch (\Exception $exception) {
            
            DB::rollBack();
            return redirect()->route('translations.index')->withErrors($exception->getMessage());
        }
    }
}
