<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Taxonomy\Repositories\Admin\Category\AdminCategoryRepository;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
            'content-ar' => ['nullable',],
            'content-en' => ['nullable', ''],
            'excerpt-ar' => ['nullable', 'max:255'],
            'excerpt-en' => ['nullable', 'max:255'],
            'categories' => ['nullable'],
            'parent_id' => ['nullable'],
            // 'image' => ['required', 'string'],
            'categories.*' => ['in:' .AdminCategoryRepository::categoriesIDs()],
        ];
    }
}
