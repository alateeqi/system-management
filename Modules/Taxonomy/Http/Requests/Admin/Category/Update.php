<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Taxonomy\Repositories\Admin\Category\AdminCategoryRepository;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'category_id'=>['required','exists:taxonomies,id'],
            'parent_id' =>['nullable'],
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
            'content-ar' => ['', ''],
            'content-en' => ['', ''],
            'excerpt-ar' => ['', 'max:255'],
            'excerpt-en' => ['', 'max:255'],
            'images_old_deleted' => ['nullable'],
            'categories' => [''],
            // 'image' => ['nullable'],
            'categories.*' => ['in:' .AdminCategoryRepository::categoriesIDs()],
        ];
    }
}
