<?php

namespace Modules\Taxonomy\Http\Requests\Admin\City;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'city_id'=>['required','exists:taxonomies,id'],
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
            'delivery_price' => ['required'],

        ];
    }
}
