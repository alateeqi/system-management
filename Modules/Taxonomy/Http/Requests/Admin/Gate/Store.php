<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Gate;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
        ];
    }
}
