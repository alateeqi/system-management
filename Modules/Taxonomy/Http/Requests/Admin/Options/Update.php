<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Options;

use Illuminate\Foundation\Http\FormRequest;


class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'option_id'=>['required','exists:taxonomies,id'],

            'title-ar' => ['required', 'max:255'],
            'title-en' => ['required', 'max:255'],
            'excerpt-ar' => ['nullable', 'max:255'],
            'excerpt-en' => ['nullable', 'max:255'],
            'content-en' => ['nullable', 'max:255'],
            'content-ar' => ['nullable', 'max:255'],

        ];
    }
}
