<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Payment;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'payment_id'=>['required','exists:taxonomies,id'],
            'title-ar' => ['required', 'max:255'],
            'title-en' => ['required', 'max:255'],
        ];
    }
}
