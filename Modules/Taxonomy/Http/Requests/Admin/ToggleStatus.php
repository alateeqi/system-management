<?php

namespace Modules\Taxonomy\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ToggleStatus extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        
        return [
            'taxonomy_id' => ['required', 'numeric', 'exists:taxonomies,id'],
        ];
    }
}
