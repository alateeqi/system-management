<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Website\Pages;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
            'page_id'=>['required','exists:taxonomies,id'],

            'title-ar' => ['required', 'max:255'],
            'title-en' => ['required', 'max:255'],
            'content-ar' => ['', ''],
            'content-en' => ['', ''],
            'excerpt-ar' => ['', 'max:255'],
            'excerpt-en' => ['', 'max:255'],
            // 'images_old_deleted' => ['nullable'],
        ];
    }
}
