<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Website\Sliders;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Taxonomy\Repositories\Website\Sliders\AdminSliderRepository;

class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title-en' => ['required', 'max:255'],
            'title-ar' => ['required', 'max:255'],
            'content-ar' => ['', ''],
            'content-en' => ['', ''],
            'excerpt-ar' => ['', 'max:255'],
            'excerpt-en' => ['', 'max:255'],
            'categories' => [''],
            'parent_id' => ['nullable'],
            'categories.*' => ['in:' .AdminSliderRepository::slidersIDs()],
        ];
    }
}
