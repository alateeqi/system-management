<?php

namespace Modules\Taxonomy\Http\Requests\Admin\Website\Sliders;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Taxonomy\Repositories\Website\Sliders\AdminSliderRepository;

class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'slider_id'=>['required','exists:taxonomies,id'],
            'parent_id' =>['nullable'],
            'title-ar' => ['required', 'max:255'],
            'title-en' => ['required', 'max:255'],
            'excerpt-ar' => ['', 'max:255'],
            'excerpt-en' => ['', 'max:255'],
            'categories' => [''],
            'images_old_deleted' => ['nullable'],
            'categories.*' => ['in:' .AdminSliderRepository::slidersIDs()],
        ];
    }
}
