<?php

namespace Modules\Taxonomy\Http\Requests\Guest\City;

use Illuminate\Foundation\Http\FormRequest;

class Search extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'q' => ['nullable','string']
        ];
    }
}
