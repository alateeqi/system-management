<?php

namespace Modules\Taxonomy\Interfaces\Admin;

interface AppTaxonomyInterface

{
    public function toggleStatus($data);
}
