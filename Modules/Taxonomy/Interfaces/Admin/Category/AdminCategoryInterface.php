<?php

namespace Modules\Taxonomy\Interfaces\Admin\Category;

use Illuminate\Http\Request;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Http\Requests\Admin\Category\Update;


interface AdminCategoryInterface

{
    public function index(Request $request);

    public function categories();

    public function find($id);

    public static function categoriesIDs();

    public function store($data);

    public function update($data);

    public function destroy(Taxonomy $taxonomy);

    public function allCategoriesWithProducts();

}
