<?php

namespace Modules\Taxonomy\Interfaces\Admin\City;

use Illuminate\Http\Request;

interface AdminCityInterface

{
    public function index(Request $request);

    public function store($data);

    public function update($data);

    public function find($id);

    public function destroy($id);

    public static function allCities();
}
