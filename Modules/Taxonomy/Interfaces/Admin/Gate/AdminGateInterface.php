<?php

namespace Modules\Taxonomy\Interfaces\Admin\Gate;
use Illuminate\Http\Request;
interface AdminGateInterface

{
    public function index(Request $request);

    public function store($data);

    public function find($id);

    public function update($data);

    public static function gates();
    
    public function destroy($id);

    public static function active_gates();
}
