<?php

namespace Modules\Taxonomy\Interfaces\Admin\Options;
use Illuminate\Http\Request;
use Modules\Taxonomy\Entities\Taxonomy;
interface AdminOptionsInterface

{
public function index(Request $request);

    public function store($data);

    public function find($id);
    
    public function update($data);

    public function destroy(Taxonomy $option_id);
}
