<?php

namespace Modules\Taxonomy\Interfaces\Admin\Payment;
use Illuminate\Http\Request;
interface AdminPaymentsInterface

{
    public function index(Request $request);

    public function store($data);

    public function update($data);

    public function find($id);

    public function destroy($id);

    public static function paymentMethods();

    public static function activePaymentMethods();
}
