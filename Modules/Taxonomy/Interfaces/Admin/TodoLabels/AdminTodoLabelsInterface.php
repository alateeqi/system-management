<?php

namespace Modules\Taxonomy\Interfaces\Admin\TodoLabels;

interface AdminTodoLabelsInterface

{
    public function index($paginate = 15);
    
    public function store($data);
}
