<?php

namespace Modules\Taxonomy\Interfaces\Admin\TransactionTypes;

use Modules\Taxonomy\Entities\Taxonomy;
use Illuminate\Http\Request;
interface AdminTransactionTypesInterface

{
    public function index(Request $request);    
    public function store($data);
    public function find($id);
    public function update($data);
    public function destroy(Taxonomy $id);
}
