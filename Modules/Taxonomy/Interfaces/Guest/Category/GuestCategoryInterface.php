<?php

namespace Modules\Taxonomy\Interfaces\Guest\Category;

interface GuestCategoryInterface
{
    public function categories();

    public function findBySlug($slug);
}
