<?php

namespace Modules\Taxonomy\Interfaces\Guest\City;

interface GuestCityInterface
{
    public function cities($data);
}
