<?php

namespace Modules\Taxonomy\Interfaces\Guest;

interface GuestSiteInterface
{
    public function siteStatus();
}
