<?php

namespace Modules\Taxonomy\Interfaces\Guest\Page;

interface GuestPageInterface
{
    public function page($slug);

}
