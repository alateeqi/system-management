<?php

namespace Modules\Taxonomy\Interfaces\Guest\Slider;

interface GuestSliderInterface
{
    public function sliders();

    public function instagramSliders();
}
