<?php

namespace Modules\Taxonomy\Interfaces\Website\Contacts;
use Illuminate\Http\Request;
interface AdminContactsInterface

{
    public function index(Request $request);
    
    public function store($data);

    public function find($id);

    public function update($data);
    
    public function destroy($id);
}
