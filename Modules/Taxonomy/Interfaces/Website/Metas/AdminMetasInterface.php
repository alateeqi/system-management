<?php

namespace Modules\Taxonomy\Interfaces\Website\Metas;
use Illuminate\Http\Request;
use Modules\Taxonomy\Entities\Taxonomy;

interface AdminMetasInterface

{
    public function index(Request $request);
    
    public function store($data);
    public function update($data);
    public function destroy(Taxonomy $meta_id);
}
