<?php

namespace Modules\Taxonomy\Interfaces\Website\Pages;

use Illuminate\Http\Request;

interface AdminPagesInterface


{
    public function index(Request $request);
    
    public function store($data);

    public function find($id);

    public function update($data);
    
    public function destroy($id);
}
