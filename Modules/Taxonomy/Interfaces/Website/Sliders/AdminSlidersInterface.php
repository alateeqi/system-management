<?php

namespace Modules\Taxonomy\Interfaces\Website\Sliders;
use Illuminate\Http\Request;
interface AdminSlidersInterface

{
    public function index(Request $request);
    
    public function sliders();
    
    public function store($data);

    public function find($id);
    public function update($data);
    
    public function destroy($id);
}
