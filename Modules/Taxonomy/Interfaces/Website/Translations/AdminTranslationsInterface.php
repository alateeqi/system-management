<?php

namespace Modules\Taxonomy\Interfaces\Website\Translations;
use Illuminate\Http\Request;
interface AdminTranslationsInterface

{
    public function index(Request $request);
    
    public function store($data);

    public function find($id);

    public function update($data);

    public function destroy($id);
}
