<?php

namespace Modules\Taxonomy\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Modules\Taxonomy\Interfaces\Admin\AppTaxonomyInterface;
use Modules\Taxonomy\Interfaces\Admin\Category\AdminCategoryInterface;
use Modules\Taxonomy\Interfaces\Admin\City\AdminCityInterface;
use Modules\Taxonomy\Interfaces\Admin\Gate\AdminGateInterface;
use Modules\Taxonomy\Interfaces\Admin\Options\AdminOptionsInterface;
use Modules\Taxonomy\Interfaces\Admin\Payment\AdminPaymentsInterface;
use Modules\Taxonomy\Interfaces\Admin\TodoLabels\AdminTodoLabelsInterface;
use Modules\Taxonomy\Interfaces\Admin\TransactionTypes\AdminTransactionTypesInterface;
use Modules\Taxonomy\Interfaces\Guest\Category\GuestCategoryInterface;
use Modules\Taxonomy\Interfaces\Guest\City\GuestCityInterface;
use Modules\Taxonomy\Interfaces\Guest\GuestSiteInterface;
use Modules\Taxonomy\Interfaces\Guest\Page\GuestPageInterface;
use Modules\Taxonomy\Interfaces\Guest\Slider\GuestSliderInterface;
use Modules\Taxonomy\Interfaces\Website\Contacts\AdminContactsInterface;
use Modules\Taxonomy\Interfaces\Website\Metas\AdminMetasInterface;
use Modules\Taxonomy\Interfaces\Website\Pages\AdminPagesInterface;
use Modules\Taxonomy\Interfaces\Website\Sliders\AdminSlidersInterface;
use Modules\Taxonomy\Interfaces\Website\Translations\AdminTranslationsInterface;
use Modules\Taxonomy\Repositories\Admin\AppTaxonomyRepository;
use Modules\Taxonomy\Repositories\Admin\Category\AdminCategoryRepository;
use Modules\Taxonomy\Repositories\Admin\City\AdminCityRepository;
use Modules\Taxonomy\Repositories\Admin\Gate\AdminGateRepository;
use Modules\Taxonomy\Repositories\Admin\Options\AdminOptionRepository;
use Modules\Taxonomy\Repositories\Admin\Payment\AdminPaymentRepository;
use Modules\Taxonomy\Repositories\Admin\TodoLabels\AdminTodoLabelsRepository;
use Modules\Taxonomy\Repositories\Admin\TransactionTypes\AdminTransactionTypesRepository;
use Modules\Taxonomy\Repositories\Guest\Category\GustCategoryRepository;
use Modules\Taxonomy\Repositories\Guest\City\GuestCityRepository;
use Modules\Taxonomy\Repositories\Guest\Page\GuestPageRepository;
use Modules\Taxonomy\Repositories\Guest\Site\GuestSiteRepository;
use Modules\Taxonomy\Repositories\Guest\Slider\GustSliderRepository;
use Modules\Taxonomy\Repositories\Website\Contacts\AdminContactRepository;
use Modules\Taxonomy\Repositories\Website\Metas\AdminMetaRepository;
use Modules\Taxonomy\Repositories\Website\Pages\AdminPagesRepository;
use Modules\Taxonomy\Repositories\Website\Sliders\AdminSliderRepository;
use Modules\Taxonomy\Repositories\Website\Translations\AdminTranslationRepository;


class TaxonomyServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Taxonomy';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'taxonomy';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->singleton(AdminCategoryInterface::class, function ($app) {
            return new AdminCategoryRepository(Auth::user());
        });
        $this->app->singleton(AdminCityInterface::class, function ($app) {
            return new AdminCityRepository(Auth::user());
        });
        $this->app->singleton(AdminPaymentsInterface::class, function ($app) {
            return new AdminPaymentRepository(Auth::user());
        });
        $this->app->singleton(AdminGateInterface::class, function ($app) {
            return new AdminGateRepository(Auth::user());
        });

        $this->app->singleton(AdminOptionsInterface::class, function ($app) {
            return new AdminOptionRepository(Auth::user());
        });

        $this->app->singleton(AdminTodoLabelsInterface::class, function ($app) {
            return new AdminTodoLabelsRepository(Auth::user());
        });

        $this->app->singleton(AdminTransactionTypesInterface::class, function ($app) {
            return new AdminTransactionTypesRepository(Auth::user());
        });

        $this->app->singleton(AdminPagesInterface::class, function ($app) {
            return new AdminPagesRepository(Auth::user());
        });

        $this->app->singleton(AdminSlidersInterface::class, function ($app) {
            return new AdminSliderRepository(Auth::user());
        });
        $this->app->singleton(AdminContactsInterface::class, function ($app) {
            return new AdminContactRepository(Auth::user());
        });
        $this->app->singleton(AdminTranslationsInterface::class, function ($app) {
            return new AdminTranslationRepository(Auth::user());
        });

        $this->app->singleton(AdminMetasInterface::class, function ($app) {
            return new AdminMetaRepository(Auth::user());
        });

        $this->app->singleton(AppTaxonomyInterface::class, function ($app) {
            return new AppTaxonomyRepository(Auth::user());
        });

        $this->app->singleton(GuestSliderInterface::class, function ($app) {
            return new GustSliderRepository();
        });

        $this->app->singleton(GuestCategoryInterface::class, function ($app) {
            return new GustCategoryRepository();
        });
        $this->app->singleton(GuestSiteInterface::class, function ($app) {
            return new GuestSiteRepository();
        });
        $this->app->singleton(GuestPageInterface::class, function ($app) {
            return new GuestPageRepository();
        });
        $this->app->singleton(GuestCityInterface::class, function ($app) {
            return new GuestCityRepository();
        });

    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);
        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
