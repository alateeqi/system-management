<?php

namespace Modules\Taxonomy\Repositories\Admin;

use Illuminate\Support\Facades\Cache;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Admin\AppTaxonomyInterface;

class AppTaxonomyRepository implements AppTaxonomyInterface
{

    public function toggleStatus($data)
    {
        $this->clearCache();
        $model = Taxonomy::find($data['taxonomy_id']);
        $model->status = !$model->status;
        $model->save();

        return $model;

    }

    private function clearCache()
    {
        if (Cache::has('all_categories_with_products'))
            Cache::forget('all_categories_with_products');
        if (Cache::has('all_categories'))
            Cache::forget('all_categories');
        if (Cache::has('all_cities'))
            Cache::forget('all_cities');
        if (Cache::has('all_gates'))
            Cache::forget('all_gates');
        if (Cache::has('active_gates'))
            Cache::forget('active_gates');
        if (Cache::has('all_payment_methods'))
            Cache::forget('all_payment_methods');
        if (Cache::has('active_payment_methods'))
            Cache::forget('active_payment_methods');
    }
}
