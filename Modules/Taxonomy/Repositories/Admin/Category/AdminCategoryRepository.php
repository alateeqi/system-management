<?php

namespace Modules\Taxonomy\Repositories\Admin\Category;

use Illuminate\Support\Facades\Cache;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Admin\Category\AdminCategoryInterface;
use Illuminate\Http\Request;
class AdminCategoryRepository implements AdminCategoryInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        
        $categories =Taxonomy::categories()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        return $categories;

        // $categories = Taxonomy::categories()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
        //     // dd($categories);

        // return $categories;

    }

    private function clearCache()
    {
        if (Cache::has('all_categories_with_products'))
            Cache::forget('all_categories_with_products');
        if (Cache::has('all_categories'))
            Cache::forget('all_categories');

    }

    public function store($data)
    {
        $this->clearCache();
        $new_category = new Taxonomy();
        $new_category->type = 1;

        $new_category->parent_id = $data['parent_id'];
        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_category->translateOrNew($key)->title = $data['title-' . $key];
            $new_category->translateOrNew($key)->excerpt = $data['excerpt-' . $key];
            // $new_category ->translateOrNew($key)->content = $data['content-' . $key];

        }

        $new_category->save();
        $new_category->addMedia(request()->file('image'))->toMediaCollection('images');

        return $new_category;
    }


    public function categories()
    {
        $categories = Cache::remember('all_categories', 60 * 60, function () {
            return Taxonomy::categories()
                ->whereNotIn('id', [570, 573])
                ->active()
                ->get();
        });


        return $categories;
    }

    public static function categoriesIDs()
    {
        $categories = Taxonomy::categories()
            ->whereNotIn('id', [570, 573])
            ->active()
            ->pluck('id');
        return $categories;
    }


    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_CATEGORY['no'])->first();
    }

    public function update($data)
    {
        $this->clearCache();
        $category = $this->find($data['category_id']);
        $category->parent_id = $data['parent_id'];
        $category->translateOrNew('ar')->title = $data['title-ar'];
        $category->translateOrNew('en')->title = $data['title-en'];
        $category->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        $category->translateOrNew('en')->excerpt = $data['excerpt-en'];
        // $new_category ->categories()->sync($data['categories']);
        $category->save();

        if (isset($data['images_old_deleted'])) {
            for ($i = 0; $i < count($data['images_old_deleted']); $i++) {
                $category->deleteMedia($data['images_old_deleted'][$i]);
            }
        }
        if (isset($data['image'])) {
            foreach ($data['image'] as $file) {

                $category->addMedia(request()->file('image'))->toMediaCollection('images');
            }
        }
        
        return $category;

    }

    public function destroy(Taxonomy $taxonomy)
    {

        $taxonomy->delete();

    }

    public function allCategoriesWithProducts()
    {

        $categories = Cache::remember('all_categories_with_products', 60 * 60, function () {
            return Taxonomy::categories()->active()->orderBy('order')->whereHas('products')->with([
                'products',
                'products.prices' => function ($q) {
                    $q->where('deleted_at', null);
                }
            ])->get();
        });
        return $categories;
    }
}
