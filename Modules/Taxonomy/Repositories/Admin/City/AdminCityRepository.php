<?php

namespace Modules\Taxonomy\Repositories\Admin\City;

use Illuminate\Support\Facades\Cache;
use Modules\Taxonomy\Entities\Taxonomy;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Auth\Entities\User;
use Illuminate\Http\Request;
use Modules\Taxonomy\Interfaces\Admin\City\AdminCityInterface;

class AdminCityRepository implements AdminCityInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public static function allCities()
    {
        $cities = Cache::remember('all_cities', 60 * 60, function () {
            return Taxonomy::cities()->get();
        });
        return $cities;
    }

    private function clearCache()
    {
        if (Cache::has('all_cities'))
            Cache::forget('all_cities');

    }

    public function index(Request $request)
    {
        $cities =Taxonomy::cities()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        return $cities;
        // $cities = Taxonomy::cities()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
        // return $cities;

    }

    public function store($data)
    {
        $this->clearCache();
        $new_Cty = new Taxonomy();
        $new_Cty->type = 2;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_Cty->translateOrNew($key)->title = $data['title-' . $key];
            $new_Cty->translateOrNew($key)->excerpt = $data['delivery_price'];

        }
        $new_Cty->save();

        // $new_category ->categories()->sync($data['categories']);

        return $new_Cty;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_CITY['no'])->first();
    }
 public static function findStatic($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_CITY['no'])->first();
    }

    public function update($data)
    {
        $this->clearCache();
        $city = $this->find($data['city_id']);
        $city->translateOrNew('ar')->title = $data['title-ar'];
        $city->translateOrNew('en')->title = $data['title-en'];
        $city->translateOrNew()->excerpt = $data['delivery_price'];
        // $new_category ->categories()->sync($data['categories']);
        $city->save();
        return $city;

    }

    public function destroy($id)
    {
        $this->clearCache();
        $cities = $this->find($id);
        $cities->delete();
    }

}
