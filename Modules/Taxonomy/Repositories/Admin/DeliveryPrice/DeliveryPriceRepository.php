<?php

namespace Modules\Taxonomy\Repositories\Admin\DeliveryPrice;


use Modules\Taxonomy\Entities\Taxonomy;

class DeliveryPriceRepository
{
    public static function websiteDeliveryPrice()
    {
        return Taxonomy::where('id', 726)->first();
    }

    public static function systemDeliveryPrice()
    {
        return Taxonomy::where('id', 496)->first();
    }
    public static function sunOrMonDeliveryPrice()
    {
        return Taxonomy::active()->where('id', 725)->first();
    }
}
