<?php

namespace Modules\Taxonomy\Repositories\Admin\Gate;

use Illuminate\Support\Facades\Cache;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Admin\Gate\AdminGateInterface;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Illuminate\Http\Request;
class AdminGateRepository implements AdminGateInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    private function clearCache()
    {
        if (Cache::has('all_gates'))
            Cache::forget('all_gates');
        if (Cache::has('active_gates'))
            Cache::forget('active_gates');

    }

    public static function gates()
    {
        $gates = Cache::remember('all_gates', 60 * 60, function () {
            return Taxonomy::gates()->get();
        });
        return $gates;
    }

    public static function active_gates()
    {
        $active_gates = Cache::remember('active_gates', 60 * 60, function () {
            return Taxonomy::gates()->active()->get();
        });
        return $active_gates;
    }

    public function index(Request $request)
    {
        $gates =Taxonomy::gates()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        
        // $gates = Taxonomy::gates()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);

        return $gates;

    }

    public function store($data)
    {
        $this->clearCache();
        $new_gates = new Taxonomy();
        $new_gates->type = 14;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_gates->translateOrNew($key)->title = $data['title-' . $key];

        }
        $new_gates->save();

        // $new_category ->categories()->sync($data['categories']);

        return $new_gates;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_GATE['no'])->first();
    }

    public function update($data)
    {
        $this->clearCache();
        {
            $gate = $this->find($data['gate_id']);

            $gate->translateOrNew('ar')->title = $data['title-ar'];
            $gate->translateOrNew('en')->title = $data['title-en'];
            $gate->save();

            return $gate;

        }

    }

    public function destroy($id)
    {
        
        $gate = $this->find($id);

        $gate->delete();
    }
}
