<?php

namespace Modules\Taxonomy\Repositories\Admin\Options;

use Illuminate\Http\Request;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Interfaces\Admin\Options\AdminOptionsInterface;

class AdminOptionRepository implements AdminOptionsInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $options = Taxonomy::where('type', Taxonomy::TYPE_ADVANCED_OPTIONS['no'])->when($request->search, function ($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%');
        })->latest()->Paginate(10);
        // $options = Taxonomy::where('type', Taxonomy::TYPE_ADVANCED_OPTIONS['no'])
        //     ->orderBy('order')->paginate($paginate);
        return $options;
    }

    public function store($data)
    {
        $new_options = new Taxonomy();
        $new_options->type = 7;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_options->translateOrNew($key)->title = $data['title-' . $key];
            $new_options->translateOrNew($key)->excerpt = $data['excerpt-' . $key];


        }
        $new_options->save();


        return $new_options;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_ADVANCED_OPTIONS['no'])->first();
    }

    public function update($data)
    {

        $option = $this->find($data['option_id']);

        if (isset($data['title-ar'])) {
            $option->translateOrNew('ar')->excerpt = $data['title-ar'];
        }
        if (isset($data['title-en'])) {
            $option->translateOrNew('en')->excerpt = $data['title-en'];
        }
        if (isset($data['excerpt-ar'])) {
            $option->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        }
        if (isset($data['excerpt-en'])) {
            $option->translateOrNew('en')->excerpt = $data['excerpt-en'];
        }
        $option->translateOrNew('ar')->content = $data['content-ar'];

//        $option->translateOrNew('en')->content = $data['content-en'];
        $option->save();
        return $option;
    }

    function destroy(Taxonomy $option_id)
    {
        $option_id->delete();
    }

    public static function checkOption($id)
    {
        return self::find($id)->status == 1;
    }


    public static function isEndWeek()
    {
        return date('D') == 'Sat' || date('D') == 'Fri';
    }

    public static function isMiddleWeek()
    {
        return date('D') == 'Sun' || date('D') == 'Mon' || date('D') == 'Tue' || date('D') == 'Wed' || date('D') == 'Thu';
    }

    public static function pointOfWeek()
    {
        if (self::isMiddleWeek()) {
            if (self::checkOption(745))
                return self::find(745);
            else return null;
        } else if (self::isEndWeek()) {
            if (self::checkOption(746))
                return self::find(746);

            else return null;
        } else
            return null;

    }

}
