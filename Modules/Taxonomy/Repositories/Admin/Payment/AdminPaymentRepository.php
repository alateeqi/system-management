<?php

namespace Modules\Taxonomy\Repositories\Admin\Payment;

use Illuminate\Support\Facades\Cache;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Interfaces\Admin\Payment\AdminPaymentsInterface;
use Illuminate\Http\Request;
class AdminPaymentRepository implements AdminPaymentsInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    private function clearCache()
    {
        if (Cache::has('all_payment_methods'))
            Cache::forget('all_payment_methods');
        if (Cache::has('active_payment_methods'))
            Cache::forget('active_payment_methods');

    }

    public static function paymentMethods()
    {
        $payment_methods = Cache::remember('all_payment_methods', 60 * 60, function () {
            return Taxonomy::paymentMethods()->get();
        });
        return $payment_methods;


    }

    public static function activePaymentMethods()
    {
        $active_payment_methods = Cache::remember('active_payment_methods', 60 * 60, function () {
            return Taxonomy::paymentMethods()->active()->get();
        });
        return $active_payment_methods;


    }

    public function index(Request $request)
    {
        $payments =Taxonomy::paymentMethods()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        return $payments;

        // $payments = Taxonomy::paymentMethods()
        //     ->orderBy('order')->paginate($paginate);
        // return $payments;
    }

    public function store($data)
    {
        $this->clearCache();
        $new_payment = new Taxonomy();
        $new_payment->type = 3;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_payment->translateOrNew($key)->title = $data['title-' . $key];

        }
        $new_payment->save();
        

        // $new_category ->categories()->sync($data['categories']);

        return $new_payment;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_PAYMENT_METHOD['no'])->first();
    }

    public function update($data)
    {
        $this->clearCache();
        $payment = $this->find($data['payment_id']);
        $payment->translateOrNew('ar')->title = $data['title-ar'];
        $payment->translateOrNew('en')->title = $data['title-en'];
        $payment->save();
        return $payment;

    }



    public function destroy($id)
    {

        $payment = $this->find($id);

        $payment->delete();

    }
}
