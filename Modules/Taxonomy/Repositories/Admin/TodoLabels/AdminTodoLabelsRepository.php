<?php

namespace Modules\Taxonomy\Repositories\Admin\TodoLabels;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Admin\TodoLabels\AdminTodoLabelsInterface;

class AdminTodoLabelsRepository implements AdminTodoLabelsInterface  
{
    private $user;
    public function __construct(User $user)
    {
    $this->user=$user;   
    }
public function index($paginate = 30 ){

        

        $todolabels = Taxonomy::todolabels()
            // id 570 for Carrot Co
            // id 573 for Plates 
            ->whereNotIn('id', [570, 573])->active()->orderBy('order')->paginate($paginate);
            
            return  $todolabels;

}

    public function store($data){

    }  
}
