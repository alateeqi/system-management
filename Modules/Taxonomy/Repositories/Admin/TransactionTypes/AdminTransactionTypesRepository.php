<?php

namespace Modules\Taxonomy\Repositories\Admin\TransactionTypes;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Admin\TransactionTypes\AdminTransactionTypesInterface;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Illuminate\Http\Request;

class AdminTransactionTypesRepository implements AdminTransactionTypesInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $transactiontypes =Taxonomy::TansactionTypes()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        // $transactiontypes = Taxonomy::TansactionTypes()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->active()->orderBy('order')->paginate($paginate);

        return $transactiontypes;

    }

    public function store($data)
    {
        $new_transactiontypes = new Taxonomy();
        $new_transactiontypes->type = 6;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_transactiontypes->translateOrNew($key)->title = $data['title-' . $key];

        }
        $new_transactiontypes->save();

        // $new_category ->categories()->sync($data['categories']);

        return $new_transactiontypes;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_TRANSACTION_TYPE['no'])->first();
    }

    public function update($data)
    {
        $transaction = $this->find($data['transaction_id']);

        $transaction->translateOrNew('ar')->title = $data['title-ar'];
        $transaction->translateOrNew('en')->title = $data['title-en'];
        $transaction->save();
        return $transaction;
    }

    public function destroy(Taxonomy $taype_id)
    {
        $taype_id->delete();
    }
}
