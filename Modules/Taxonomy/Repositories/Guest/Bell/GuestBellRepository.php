<?php

namespace Modules\Taxonomy\Repositories\Guest\Bell;

use Modules\Taxonomy\Entities\Taxonomy;

class GuestBellRepository
{
    public static function onBell()
    {
        $bell = Taxonomy::where('id', 687)->firstOrFail();
        $bell->status = 1;
        $bell->save();
        return $bell;
    }
}
