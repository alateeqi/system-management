<?php

namespace Modules\Taxonomy\Repositories\Guest\Category;

use Modules\Product\Repositories\Guest\GuestProductRepository;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Interfaces\Guest\Category\GuestCategoryInterface;


class GustCategoryRepository implements GuestCategoryInterface
{

    public function categories()
    {
        return Taxonomy::categories()->active()->whereNotIn('id', [570, 573])->orderBy('order')->get();

    }

    public function findBySlug($slug)
    {

        if ($slug != 'all') {

            $category = Taxonomy::categories()
                // id 570 for Carrot Co
                // id 573 for Plates
                ->whereNotIn('id', [570, 573])
                ->with([
                    'products' => function ($p) {
                        $p->where('status', '=', 1);
                    }
                ])
                ->whereHas('translations',
                    function ($query) use ($slug) {
                        $query->where('slug', $slug);
                    })->firstOrFail();
        } else {
            $category = (object)[
                'title' => 'All Categories',
                'products' => (new GuestProductRepository())->productsAllCategories()
            ];
        }

        $categories = Taxonomy::categories()
            // id 570 for Carrot Co
            // id 573 for Plates
            ->whereNotIn('id', [570, 573])
            ->active()->orderBy('order')->get();

        return [
            'categories' => $categories,
            'category' => $category,
        ];
    }
}
