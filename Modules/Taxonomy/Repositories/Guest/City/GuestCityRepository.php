<?php

namespace Modules\Taxonomy\Repositories\Guest\City;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Interfaces\Guest\City\GuestCityInterface;

class GuestCityRepository implements GuestCityInterface
{

    public function cities($data)
    {
        $taxonomies = Taxonomy::cities();

        if ($data['q']) {
            $taxonomies = $taxonomies->whereHas('translations', function ($query) use ($data) {
                $query->where('title', 'like', '%' . $data['q'] . '%');
            });
        }

        return $taxonomies->limit(10)->get();
    }
}
