<?php

namespace Modules\Taxonomy\Repositories\Guest\DeliveryPrice;

use Modules\Taxonomy\Entities\Taxonomy;

class GuestDeliveryPriceRepository
{
    public static function websiteDeliveryPrice()
    {
        return Taxonomy::where('id', 726)->first();
    }

    public static function systemDeliveryPrice()
    {
        return Taxonomy::where('id', 496)->first();
    }
    public static function isSunOrMon()
    {
        return Taxonomy::active()->where('id', 725)->first();
    }
}
