<?php

namespace Modules\Taxonomy\Repositories\Guest\Gate;

use Modules\Taxonomy\Entities\Taxonomy;

class GuestGateRepository
{
    public static function website()
    {
        return Taxonomy::gates()->whereHas('translations', function ($query) {
            $query->where('title', 'like', '%Website%');
        })->firstOrFail();
    }
}
