<?php

namespace Modules\Taxonomy\Repositories\Guest\Page;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Interfaces\Guest\Page\GuestPageInterface;

class GuestPageRepository implements GuestPageInterface
{
    public function page($slug)
    {
        return Taxonomy::pages()->whereHas('translations', function ($query) use ($slug) {
            $query->where('slug', $slug)->orWhere('title', $slug);
        })->firstOrFail();
    }
}
