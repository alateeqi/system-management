<?php

namespace Modules\Taxonomy\Repositories\Guest\PaymentMethod;

use Modules\Taxonomy\Entities\Taxonomy;

class GuestPaymentMethodRepository
{
    public static function Bank()
    {
      return  Taxonomy::paymentMethods()->whereHas('translations', function ($query) {
            $query->where('title', 'like', '%Bank%');
        })->firstOrFail();
    }
}
