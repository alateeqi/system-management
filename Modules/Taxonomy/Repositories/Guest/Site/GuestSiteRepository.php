<?php

namespace Modules\Taxonomy\Repositories\Guest\Site;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Interfaces\Guest\GuestSiteInterface;

class GuestSiteRepository implements GuestSiteInterface
{

    public function siteStatus()
    {

        $siteStatus = Taxonomy::find(689);

        if ($siteStatus->status) {
            return 1;
        }

        return 0;
    }
}
