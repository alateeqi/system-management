<?php

namespace Modules\Taxonomy\Repositories\Guest\Slider;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Taxonomy\Interfaces\Guest\Slider\GuestSliderInterface;


class GustSliderRepository implements GuestSliderInterface
{

    public function sliders()
    {
        return Taxonomy::sliders()->active()->orderBy('order')->get();
    }

    public function instagramSliders()
    {
        return Taxonomy::instagramSliders()->where('status', 1)->get();
    }
}
