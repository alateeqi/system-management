<?php

namespace Modules\Taxonomy\Repositories\Website\Contacts;

use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Illuminate\Http\Request;
use Modules\Taxonomy\Interfaces\Website\Contacts\AdminContactsInterface;


class AdminContactRepository implements AdminContactsInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {

        $contacts =Taxonomy::contacts()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        // $contacts = Taxonomy::contacts()->orderBy('order')->paginate($paginate);

        return $contacts;

    }

    public function store($data)
    {
        $new_contact = new Taxonomy();
        $new_contact->type = 10;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_contact->translateOrNew($key)->title = $data['title-' . $key];
            $new_contact->translateOrNew($key)->excerpt = $data['excerpt-' . $key];


        }
        $new_contact->save();

        // $new_category ->categories()->sync($data['categories']);

        return $new_contact;
    }

    public function find($id)
    {

        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_CONTACT['no'])->first();
    }

    public function update($data)
    {

        $contact = $this->find($data['contact_id']);

        $contact->translateOrNew('ar')->title = $data['title-ar'];
        $contact->translateOrNew('en')->title = $data['title-en'];

        $contact->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        $contact->translateOrNew('en')->excerpt = $data['excerpt-en'];

        $contact->save();

        return $contact;
    }

    public function destroy($id)
    {
        $contact = $this->find($id);
        $contact->delete();

    }

}
