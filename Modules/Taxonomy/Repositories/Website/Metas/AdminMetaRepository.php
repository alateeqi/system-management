<?php

namespace Modules\Taxonomy\Repositories\Website\Metas;
use Illuminate\Support\Facades\Cache;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Illuminate\Http\Request;
use Modules\Taxonomy\Interfaces\Website\Metas\AdminMetasInterface;

class AdminMetaRepository implements AdminMetasInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $metas =Taxonomy::metas()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);

        // $metas = Taxonomy::metas()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->active()->orderBy('order')->paginate($paginate);
        return $metas;

    }
    private function clearCache()
    {
        if (Cache::has('all_metas'))
            Cache::forget('all_metas');
        if (Cache::has('all_metas'))
            Cache::forget('all_metas');

    }

    public function store($data)
    {
        $this->clearCache();
        $new_metas = new Taxonomy();
        $new_metas->type = 13;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_metas->translateOrNew($key)->title = $data['title-' . $key];
            $new_metas->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
            $new_metas->translateOrNew('en')->excerpt = $data['excerpt-en'];
        }
        $new_metas->save();

        return $new_metas;
    }
        public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_META['no'])->first();
    }
    public function update($data)
    {
        $this->clearCache();
        $metas = $this->find($data['meta_id']);
        $metas->translateOrNew('ar')->title = $data['title-ar'];
        $metas->translateOrNew('en')->title = $data['title-en'];
        $metas->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        $metas->translateOrNew('en')->excerpt = $data['excerpt-en'];
        $metas->save();
        return $metas;
    }

    function destroy(Taxonomy $meta_id)
    {
        $meta_id->delete();
    }

}
