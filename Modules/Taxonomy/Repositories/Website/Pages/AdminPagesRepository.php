<?php

namespace Modules\Taxonomy\Repositories\Website\Pages;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Taxonomy\Interfaces\Website\Pages\AdminPagesInterface;
use Illuminate\Http\Request;


class AdminPagesRepository implements AdminPagesInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


public function index(Request $request)
    {
    $pages =Taxonomy::pages()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);

        // $pages = Taxonomy::pages()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates 
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
            
            return  $pages;


    // public function index($)
    // {


    //     $pages = Taxonomy::pages()
    //         // id 570 for Carrot Co
    //         // id 573 for Plates
    //         ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);


    //     return $pages;

    // }
    }
    public function store($data)
    {
        $new_page = new Taxonomy();
        $new_page->type = 9;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_page->translateOrNew($key)->title = $data['title-' . $key];
            $new_page->translateOrNew($key)->excerpt = $data['excerpt-' . $key];
            // $new_page->translateOrNew($key)->content = $data['content-' . $key];

        }
        $new_page->save();

        // $new_page->addMedia(request()->file('image'))->toMediaCollection('images');

        // $new_category ->categories()->sync($data['categories']);

        return $new_page;
    }

    public function find($id)
    {

        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_PAGE['no'])->first();
    }

    public function update($data)
    {
        $page = $this->find($data['page_id']);

        $page->translateOrNew('ar')->title = $data['title-ar'];
        $page->translateOrNew('en')->title = $data['title-en'];

        $page->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        $page->translateOrNew('en')->excerpt = $data['excerpt-en'];
        // $new_category ->categories()->sync($data['categories']);
        $page->save();

        // if (isset($data['images_old_deleted'])) {
        //     for ($i = 0; $i < count($data['images_old_deleted']); $i++) {
        //         $page->deleteMedia($data['images_old_deleted'][$i]);
        //     }

        // }

        // $page->addMedia(request()->file('image'))->toMediaCollection('images');
        return $page;

    }

    public function destroy($id)
    {

        $page = $this->find($id);

        $page->delete();

    }
}
