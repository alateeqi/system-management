<?php

namespace Modules\Taxonomy\Repositories\Website\Sliders;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Modules\Taxonomy\Interfaces\Website\Sliders\AdminSlidersInterface;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Illuminate\Http\Request;


class AdminSliderRepository implements AdminSlidersInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
    
    $sliders =Taxonomy::sliders()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(5);
        // $sliders = Taxonomy::sliders()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates 
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
            return  $sliders;

    }
    // public function index($paginate = 30)
    // {

    //     $sliders = Taxonomy::sliders()
    //         // id 570 for Carrot Co
    //         // id 573 for Plates
    //         ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
    //     return $sliders;


    // }

    public function store($data)
    {

        $new_slider = new Taxonomy();
        $new_slider->type = 11;

        $new_slider->parent_id = $data['parent_id'];

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_slider->translateOrNew($key)->title = $data['title-' . $key];
            $new_slider->translateOrNew($key)->excerpt = $data['excerpt-' . $key];
            // $new_category ->translateOrNew($key)->content = $data['content-' . $key];

        }
        $new_slider->save();

        $new_slider->addMedia(request()->file('image'))->toMediaCollection('images');

        // $new_category ->categories()->sync($data['categories']);

        return $new_slider;
    }

    public function sliders()
    {
        //  ->pluck('title','id')
        $sliders = Taxonomy::sliders()
            ->whereNotIn('id', [570, 573])
            ->active()
            ->get();

        return $sliders;
    }

    public static function slidersIDs()
    {
        $sliders = Taxonomy::sliders()
            ->whereNotIn('id', [570, 573])
            ->active()
            ->pluck('id');
        return $sliders;
    }

    public function find($id)
    {
        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_SLIDER['no'])->first();
    }

    public function update($data)
    {
        $slider = $this->find($data['slider_id']);
        $slider->parent_id = $data['parent_id'];
        $slider->translateOrNew('ar')->title = $data['title-ar'];
        $slider->translateOrNew('en')->title = $data['title-en'];

        $slider->translateOrNew('ar')->excerpt = $data['excerpt-ar'];
        $slider->translateOrNew('en')->excerpt = $data['excerpt-en'];


        $slider->save();


        if (isset($data['images_old_deleted'])) {
            for ($i = 0; $i < count($data['images_old_deleted']); $i++) {
                $slider->deleteMedia($data['images_old_deleted'][$i]);
            }

        }
        if (isset($data['image'])) {
            foreach ($data['image'] as $file) {

                $slider->addMedia(request()->file('image'))->toMediaCollection('images');
            }
        }
        
        return $slider;
    }

    public function destroy($id)
    {
        $slider = $this->find($id);
        $slider->delete();
    }
}
