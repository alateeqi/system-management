<?php

namespace Modules\Taxonomy\Repositories\Website\Translations;

use Modules\Taxonomy\Entities\Taxonomy;
use Modules\Auth\Entities\User;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\LaravelLocalization;

use Modules\Taxonomy\Interfaces\Website\Translations\AdminTranslationsInterface;

class AdminTranslationRepository implements AdminTranslationsInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $translations =Taxonomy::WebsiteTranslations()->when($request->search, function($query) use ($request) {
            return $query->whereTranslationLike('title', '%' . $request->search . '%' );
        })->latest()->Paginate(10);
        // $translations = Taxonomy::WebsiteTranslations()
        //     // id 570 for Carrot Co
        //     // id 573 for Plates
        //     ->whereNotIn('id', [570, 573])->orderBy('order')->paginate($paginate);
        return $translations;
    }

    public function store($data)
    {
        $new_translation = new Taxonomy();
        $new_translation->type = 12;

        foreach ((new LaravelLocalization())->getSupportedLanguagesKeys() as $key) {

            $new_translation->translateOrNew($key)->title = $data['title-' . $key];
            $new_translation->translateOrNew($key)->excerpt = $data['excerpt-' . $key];


        }
        $new_translation->save();

        // $new_category ->categories()->sync($data['categories']);

        return $new_translation;
    }

    public function find($id)
    {

        return Taxonomy::where('id', $id)->where('type', Taxonomy::TYPE_TRANSLATION['no'])->first();

    }

    public function update($data)
    {
        $translation = $this->find($data['translation_id']);
        $translation->translateOrNew('en')->title = $data['title-en'];
        $translation->translateOrNew('ar')->title = $data['title-ar'];
        $translation->save();
        return $translation;
    }

    public function destroy($id)
    {
        $translation = $this->find($id);
        $translation->delete();
    }
}
