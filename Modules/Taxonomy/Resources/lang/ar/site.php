<?php 


return [
    
    'dashboard'=> 'الرئيسية',
    'name-ar' => 'الاسم باللغة العربية',
    'name-en'=> 'الاسم باللغة الانجليزية',
    'title' => 'النص',
    'value-ar'=> 'القيمة باللغة العربية',
    'value-en'=> 'القيمة باللغة الانجليزية',
    'add' => 'أضف ',
    'edit' => 'تعديل',
    'delete'=> 'حذف',
    'search'=> 'بحث',
    'permissions' => 'الصلاحيات',
    'create' => 'اضافة',
    'read' => 'عرض',
    'update'=> 'تعديل',
    'status' => 'الحالة',
    'confirm_the_deletion'=> 'هل انت متاكد من عملية الحذف ؟',
    'cancel' => 'الغاء',
    

    'add_admin'=>'اضافة مشرف جديد',
    'Enter_first_name'=> 'ادخل الاسم الاول ',
    'Enter_last_name'=> 'ادخل الاسم الثاني ',
    'password_confirmation' => 'تاكيد كلمة المرور' ,

    'added_successfully' => 'تم اضافة البيانات بنجاح',
    'updated_successfully' => 'تم تعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',



    'not_data_found'=>'لا يوجد اي سجلات',
    'no_records'=>'لا يوجد اي سجلات',
    'users' => 'الموظفون',
    'first_name' => 'الاسم الاول',
    'last_name' => 'الاسم الاخير',
    'email' => 'البريد الالكتروني',
    'password' => 'كلمة المرور',
    'action' => 'العمليات ',


    'categories'=> 'الاقسام',
    'all_categories'=> 'كل الاقسام',
    'add_category'=> 'اضف قسم',
    'edit_category'=>'تعديل القسم',
    'name' => 'الاسم',
    'products_count' => 'عدد المنتجات',
    'related_products' => 'المنتجات المرتبطة',

        'name-ar' => 'الاسم باللغة العربية',

        'name-en' => 'الاسم باللغة الانجليزية',
        'description-en'=> 'الوصف  بالانجليزية ',
        'description-ar' =>'الوصف بالعربي',


        
    'products' => 'المنتجات',
    'sale_price'=> 'سعر البيع',
    'purchase_price'=> 'سعر التكلفة',
    'add_products' => 'اضف منتج',
    'stock' => 'المخزن',
    'image'=> 'الصورة',
    'add_image' => 'اضف صورة',
    'profit_percent' => 'المكسب',
    'description' => 'الوصف',
    'edit_product' => 'تعديل المنتج',


    'clients' => 'العملاء',
    'add_client' => 'أضف عميل ',
    'phone' => 'الرقم ',
    'address' => 'العنوان',
    'edit_client' => 'تعديل العميل ',
    'add_order'=> 'أضف طلب',
    'orders'=>'الطلبات',
    'price'=>'السعر',
    'product'=>'المنتج',
    'quantiy'=> 'الكمية',
    'total' => 'المجموع',
    'country'=> 'الدولة',
    'select_city' => 'اخترالمنطقة',
    'city' => 'المنطقة',
    'notes'=>'ملاحظة',
    'block' => 'القطعة',
    'house'=> 'المنزل',
    'street' => 'الشارع',
    'floor'=> 'الدور',
    'time'=> 'الوقت',
    'Addresses'=> 'العناوين',
    'add_address' => 'اضف عنوان',
    'select_client'=> 'اختيار العميل',
    'Settings'=> 'الاعدادات',
    'cities'=> 'المدن',
    'delivery_price'=>'سعر التوصيل',
    'add_new' => 'أضف جديد',
    'add_city' => 'اضف مدينة',
    

    'todolabel' => 'كل التصنيفات',
    'transactiontypes'=> 'انواع المعملات',
    'website' => 'موقع الويب',
    
    'Contact_Information' => ' معلومات الاتصال',
    

    'metas' => 'الاهداف',

    'select_category' => 'حدد القسم',

    'Payment_Methods' => 'طرق الدفع',
    'Add_Payment_Methods' => 'اضف طريقة دفع',
    'edit_payment_methods' => 'تعديل طريقة دفع',

    'sales_gate' => 'بوابة البيع',
    'add_sales_gate' => ' اضف بوابة البيع ',
    'edit_sales_gate'=> 'تعديل بوابة البيع',


    'value' => 'القيمة',

    'advanced_options' => 'خيارات متقدمة',
    'add_advanced_ptions' => 'add advanced ptions',
    'edit_advanced_ptions' => 'edit advanced ptions',


    'pages' => 'الصفحات',
    'add_pages' => 'اضافة الصفحات  ',
    'edit_pages' => 'تعديل الصفحات  ',

    'sliders' => 'Sliders',
    'add_sliders' => 'Add Slider',
    'edit_sliders' => 'Edit Slider',

    'websitetranslations' => 'الترجمات',
    'translation' => 'الترجمة',
    'add_translation' => 'أضف ترجمة',
    'edit_translation' => 'تعديل  ترجمة',
    'title-ar'=> 'النص بالعربي',
    'title-en'=> 'النص بالانجليزي',
    'translation-ar' => 'الترجمة بالعربي',
    'translation-en' => 'الترجمة بالانجليزي',
    'select_category'=>'حدد قسم',
    'orders'=>'الطلبات',
    'edit_city'=>'تعديل المدينة',
    'add_transaction_types'=>'Add Transaction Types',




];