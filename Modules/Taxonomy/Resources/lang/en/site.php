<?php


return [

'image'=> 'image',    
'products' =>'Products',
'edit' => 'Edit',
'delete' => 'delete',
'Settings'=> 'Settings',
'dashboard' =>'Dashboard',
'categories' => 'categories',
'add_category' => 'Add Category',
'name'=>'Name',
'search'=> 'search',
'all_categories'=> 'All Gategories ',
'edit_category'=>'Edit Category',
'products_count' => 'Products Count ',
'related_products' => 'Related Products',
'action' => 'Action',
'confirm_the_deletion'=> 'Confirm The Deletion',
'cancel'=>'Cancel',
'add_image'=>'add image',



'cities'=> 'Cities',
'delivery_price'=>'Delivery Price',
'add_city' => 'Add City',
'add_new' => 'Add New',
'add' => 'Add' ,

'Payment_Methods' => 'Payment Methods',

'todolabel' => 'todolabel ',
'transactiontypes'=> 'Transaction Types ',
'website' => 'Website',

'Contact_Information' => ' Contact Information ',

'metas' => 'Metas',
'status' => 'status',

'Add_Payment_Methods' => 'Add Payment Methods',

'name-ar' => 'name in arabic',
'description-ar'=> 'description in arabic',
'name-en' => 'name in eniglish',
'description-en'=> 'description in eniglish ',

'sales_gate' => 'Sales Gate ',
'add_sales_gate' => 'Add Sales Gate',
'edit_sales_gate' => 'edit Sales Gate',

'value-ar' => 'Value in eniglish',
'value-en' => 'Value arabic',

'add_advanced_ptions'=>'Add Advanced Options',
'edit_advanced_ptions'=>'Edit Advanced Options',
'advanced_options' => 'Advanced Options ',
'value'=>'value',

'pages' => 'Pages',
'add_pages' => 'Add Pages',
'edit_pages' => 'Edit Pages',


'sliders' => 'Sliders',
'add_sliders' => 'Add Slider',
'edit_sliders' => 'Edit Slider',


'translation' => 'translation',
'websitetranslations' => 'Website Translations',
'add_translation' => 'Add Translation ',
'edit_translation' => 'Edit Translation',
'title-ar'=> 'النص بالعربي',
'title-en'=> 'النص بالانجليزي',
'translation-ar' => 'الترجمة بالعربي',
'translation-en' => 'الترجمة بالانجليزي',
'not_data_found'=> 'not data found',
'select_category'=>'Select Category',
'orders'=>'Orders',
'edit_city'=>'Edit City',
'edit_payment_methods'=>'Edit Payment Methods',
'add_transaction_types'=>'add transaction types',

];