@extends('layouts.dashboard.app')


@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.add_category')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('categories.index') }}">@lang('taxonomy::site.categories')
                            </a></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->

            <!--EN FOR  page-header-->
            <div class="row">

                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('categries.store') }}" id="form-category" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-22 text-info m-1"><i class="lni lni-grid-alt"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.add_category')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('taxonomy::site.categories')</label>
                                            <select name="parent_id" class="single-select select2-hidden-accessible"
                                                    data-select2-id="1" tabindex="-1" aria-hidden="true"
                                                    data-placeholder="Choose anything">
                                                <option selected=""
                                                        value="">@lang('taxonomy::site.select_category')</option>
                                                @for ($category = 0; $category < count($categories); $category++)
                                                    <option
                                                        value="{{ $categories[$category]['id'] }}">{{ $categories[$category]['title'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title" placeholder="@lang('taxonomy::site.name-ar')">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitleEN"
                                                   class="form-label">@lang('taxonomy::site.name-en')</label>
                                            <input class="form-control" name="title-en" id="title-en" rows="3"
                                                   placeholder="@lang('taxonomy::site.name-en')">

                                        </div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentAR" class="form-label">category Content
                                                    AR</label>
                                                <textarea class="form-control" name="excerpt-ar"
                                                          id="inputCategoryContentAR" rows="3"
                                                          placeholder="Enter category content Arabic"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentEN" class="form-label">category Content
                                                    EN</label><textarea class="form-control" name="excerpt-en"
                                                                        id="inputcategoryContentEN" rows="3"
                                                                        placeholder="Enter category content English"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row mb-3">
                                    <label for="image" class="form-label">@lang('taxonomy::site.add_image')</label>
                                    <div class="col-sm-9">
                                        <div class="card-body">
                                            <input id="image" type="file" class="form-control image" name="image"
                                                   id="fancy-file-upload">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="image" class="col-sm-3 col-form-label"></label>
                                    <img class="col-sm-3  img-thumbnail image-preview" src="" alt="">
                                </div>
                                {{-- <div class="hidden-input-images"></div> --}}
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button id="submitform" type="submit"
                                                class="btn btn-primary">@lang('taxonomy::site.add_category')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @push('js')
        <script>
            const submitform = = document.getElementById("submitform");
            const image = = document.getElementById("image");

            submitform.addEvenrListener("click", uploadCanvasImage);

            function uploadCanvasImage() {
                const base64 = image.toDataURL().split(",")[1];
                const body = {
                    "generated_at": new Date().toISOString(),
                    "png": base64
                    "jpg": base64

                };
            }
        </script>

        <script>
            $(".image").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.image-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        </script>


        <script>
            $('.single-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            $('.multiple-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        </script>

    @endpush
@endsection
