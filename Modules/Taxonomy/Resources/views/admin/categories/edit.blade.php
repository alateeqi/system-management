@extends('layouts.dashboard.app')

@push('css')
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet"/>
    <link href="{{ URL::asset('assets/plugins/select2/css/select2-bootstrap4.css') }}" rel="stylesheet"/>

    <style>
        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show {
            width: 100px;
            height: 100px;
            position: relative;
            overflow: hidden;
            margin-bottom: 1em;
            float: left;
            border-radius: 12px;
            box-shadow: 0 0 4px 0 #888;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show button.btn-danger {
            position: absolute;
            top: 3px;
            right: 3px;
            width: 20px;
            height: 20px;
            border-radius: 15px;
            font-size: 10px;
            line-height: 1.42;
            padding: 2px 0;
            text-align: center;
            z-index: 3;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show .imageuploadify-details-show {
            position: absolute;
            top: 0;
            padding-top: 20px;
            width: 100%;
            height: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            background: rgba(255, 255, 255, .5);
            z-index: 2;
            opacity: 1;
        }

    </style>
@endpush
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.add_category')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('categories.index') }}">@lang('taxonomy::site.categories')
                                </a></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->

            <div class="row">

                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('categries.update') }}" method="POST" enctype="multipart/form-data"
                          id="form-category">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-22 text-info m-1"><i class="lni lni-grid-alt"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.edit_category')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">@lang('taxonomy::site.categories ')</label>
                                            <select name="parent_id" class="single-select select2-hidden-accessible"
                                                    data-select2-id="1" tabindex="-1" aria-hidden="true"
                                                    data-placeholder="Choose anything">
                                                <option selected=""
                                                        value=""> @lang('taxonomy::site.select_category')</option>
                                                @for ($cat = 0; $cat < count($categories); $cat++)
                                                    <option value="{{ $categories[$cat]['id'] }}"
                                                        {{ $category->parent_id == $categories[$cat]['id'] ? 'selected' : '' }}>
                                                        {{ $categories[$cat]['title'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">

                                        <div class="mb-3">
                                            <label for="inputCategoryTitle"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title"
                                                   value="{{ $category->translate('ar')->title }}">

                                            <input class="form-control" type="hidden" name="category_id"
                                                   value="{{ $category->id }}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitleEN"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" value="{{ $category->translate('en')->title }}"
                                                   name="title-en" id="title-en" rows="3">
                                        </div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentAR" class="form-label">category excerpt
                                                    AR</label>
                                                <textarea class="form-control" name="excerpt-ar"
                                                          id="inputCategoryContentAR" rows="3"
                                                          placeholder="Enter category content Arabic">{{ $category->translate('ar')->excerpt }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentEN" class="form-label">category excerpt
                                                    EN</label>
                                                <textarea class="form-control" name="excerpt-en"
                                                          id="inputcategoryContentEN" rows="3"
                                                          placeholder="Enter category content English">{{ $category->translate('en')->excerpt }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="image" class="form-label">@lang('taxonomy::site.add_image')</label>
                                    <div class="col-sm-9">
                                        <div class="card-body">
                                            <input id="image-uploadify-show" type="file" class="form-control image"
                                                   name="image" id="fancy-file-upload" src=""
                                                   value="{{ $category->image }}">

                                        </div>
                                    </div>
                                </div>
                                <div class="hidden-input-images"></div>
                                <div class="row mb-3">
                                    <label for="image" class="col-sm-3 col-form-label"></label>
                                    <img class="col-sm-3  img-thumbnail image-preview" src="" alt="">
                                </div>

                                <div class="imageuploadify-show row  g-2 justify-content-center mt-3">
                                    <div class="imageuploadify-images-list-show text-center">
                                        <br/><br/>
                                        <div id="old_images">
                                            @php($image_ids = [])
                                            @foreach ($category->getMedia('images') as $image)
                                                @php(array_push($image_ids, $image->id))
                                                <div class="col imageuploadify-container-show" style="margin: 3px;"
                                                     id="image_old{{ $image->id }}">
                                                    <button type="button" class="btn btn-danger bx bx-x"
                                                            onclick="remove_old_image('{{ $image->id }}')"></button>
                                                    <div class="imageuploadify-details--show">
                                                        <img src="{{ $image->getUrl('full') }}"
                                                             data-id="{{ $image->id }}" width="100">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button id="submit-form" type="submit"
                                                class="btn btn-info btn-primary">@lang('taxonomy::site.edit_category')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>





    @push('js')
        <script>

        </script>
        <script>
            const submitform = = document.getElementById("submit-form");
            const image = = document.getElementById("image");

            submitform.addEvenrListener("click", uploadCanvasImage);

            function uploadCanvasImage() {
                const base64 = image.toDataURL().split(",")[1];
                const body = {
                    "generated_at": new Date().toISOString(),
                    "png": base64 "jpg": base64

                };
            }
        </script>

        <script>
            $(".image").change(function () {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.image-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            })
            var images_list = [];
            $(document).ready(function () {
                $('#image-uploadify-show').imageuploadify();

                $('.submit-form').on('click', function (e) {
                    e.preventDefault();
                    $('.imageuploadify-container img').each(function () {
                        $('.hidden-input-images').append(
                            '<input type="hidden" name="image" value="' + $(this).attr('src') + '">'
                        )
                    });

                    for (i = 0; i < images_list.length; i++) {
                        $('#form-category').append('<input type="hidden" name="images_old_deleted[]" value="' +
                            images_list[i] + '">')
                    }
                    document.getElementById('form-category').submit();
                });
            })

            function remove_old_image(id) {
                $('#image_old' + id).remove();
                images_list.push(id)
            }
        </script>


        <script>
            $('.single-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            $('.multiple-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        </script>
    @endpush
@endsection
