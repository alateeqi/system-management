@extends('layouts.dashboard.app')

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.edit_sales_gate')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('gates.index') }}">@lang('taxonomy::site.sales_gate')
                                </a></li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->

            <div class="row">

                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('gates.update') }}" method="POST">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 m-2 text-info"><i class="lni lni-cloud-network"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.edit_sales_gate')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                    </div>
                                    <div class="row mb-3">

                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">category
                                                ContentAR</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title" value="{{$gate->translate('ar')->title}}">
                                            <input class="form-control" type="hidden" name="gate_id"
                                                   value="{{$gate->id}}">

                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitleEN" class="form-label">category Content
                                                EN</label>
                                            <input class="form-control" value="{{$gate->translate('en')->title}}"
                                                   name="title-en" id="title-en" rows="3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-grid">
                                    <button type="submit"
                                            class="btn btn-primary">@lang('taxonomy::site.edit')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    </div>
    @push('js')
        <script>
            $('.single-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            $('.multiple-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        </script>

    @endpush
@endsection
