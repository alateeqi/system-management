@extends('layouts.dashboard.app')


@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.add_new')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <a
                                    href="{{ route('options.index')}}">@lang('taxonomy::site.advanced_options')
                            </a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>

            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">

                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('options.update') }}" method="POST">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 mx-2 text-info pt-2"><i
                                                class="fadeIn animated bx bx-columns"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.add_new')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">

                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputOptionTitle"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title" value="{{$option->translate('ar')->title}}">
                                            <input class="form-control" type="hidden" name="option_id"
                                                   value="{{$option->id}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputOptionTitleEN"
                                                   class="form-label">@lang('taxonomy::site.name-en')</label>
                                            <input class="form-control" name="title-en" id="title-en" rows="3"
                                                   value="{{$option->translate('en')->title}}"></div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputOptionValueAR"
                                                       class="form-label">@lang('taxonomy::site.value-ar')</label>
                                                <textarea class="form-control" name="excerpt-ar" id="inputOptionValueAR"
                                                          rows="3">{{$option->translate('ar')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputOptionValueEN"
                                                       class="form-label">@lang('taxonomy::site.value-en')</label>
                                                <textarea class="form-control" name="excerpt-en" id="inputOptionValueEN"
                                                          rows="3">{{$option->translate('en')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-12">
                                        @if($option->id == 746||$option->id == 745)
                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <label for="inputOptioncontentAR" class="form-label"> min count
                                                        order </label>
                                                    <input class="form-control"
                                                           name="content-ar"
                                                           id="inputOptioncontentAR"
                                                           type="number"
                                                           value="{{$option->translate('ar')->content}}">
                                                </div>
                                            </div>
                                          @endif
                                        {{--                                        <div class="col-6">--}}
                                         {{--                                            <div class="mb-3">--}}
                                        {{--                                                <label for="inputOptioncontentEN" class="form-label">min count order</label>--}}
                                        {{--                                                <input class="form-control"--}}
                                        {{--                                                          name="content-en"--}}
                                        {{--                                                       type="number"--}}
                                        {{--                                                          id="inputOptioncontentEN"--}}
                                        {{--                                                       value="{{$option->translate('en')->content}}"--}}
                                        {{--                                                       >--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit"
                                                class="btn btn-info">@lang('taxonomy::site.edit')</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </div>
    </div>
@endsection






