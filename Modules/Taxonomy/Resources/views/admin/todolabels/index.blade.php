@extends('layouts.dashboard.app')
@section('title')
    @lang('taxonomy::site.todolabel')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.todolabel')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="#"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>

            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-22 text-primary pt-2"> <i class="lni lni-menu"></i></div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('taxonomy::site.todolabel') <small></small></h5>

                    <form action="" method="GET">
                        <div class="row">

                            <div class="col-md-6">
                                <input type="search" name="search" class="form-control mt-2 mx-3  radius-30 form-control "
                                    placeholder="@lang('taxonomy::site.search')" value="">
                            </div>

                            <div class="col-md-6">
                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                    href="#">@lang('taxonomy::site.add_city')<i class="lni lni-circle-plus mx-1"></i></a>
                            </div>

                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($todolabels->count() > 0)
                        <table class="table mb-0 table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">@lang('taxonomy::site.name')</th>


                                    <th scope="col">@lang('taxonomy::site.action')</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($todolabels as $index => $todolabel)
                                    <tr>
                                        <th scope="row">{{ $index + 1 }}</th>

                                        <td>{{ $todolabel->title }}</td>




                                        <td>

                                            <a class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                                data-bs-toggle="dropdown"
                                                aria-expanded="false">@lang('taxonomy::site.action')</a>
                                            <ul class="dropdown-menu" style="">

                                                <li class="dropdown-item font-18 text-primary">
                                                    <i class="lni lni-cog"></i>
                                                    <a class="text-dark" href="#">@lang('taxonomy::site.edit')</a>
                                                </li>


                                                <li class="dropdown-item font-18">

                                                    <i class="bx bxs-trash " style="color: #ff6b6b;"></i>


                                                </li>
                                        </td>

                                    </tr>

                                    <!-- users-delete -->

                                    <form action="#" method="POST" style="display:inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1"
                                            aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @lang('site.confirm_the_deletion')
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">@lang('site.cancel')</button>
                                                        <button type="submit"
                                                            class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                @endforeach
                            </tbody>

                            <div class="mt-3">
								{{-- {{$cities = DB::table('taxonomies')->simplePaginate(5);}} --}}
                            </div>

                        @else
                            <h2>@lang('taxonomy::site.not_data_found')</h2>
                    @endif
                    </table>
                </div>
            </div>
        </div>
    </div>





@endsection
