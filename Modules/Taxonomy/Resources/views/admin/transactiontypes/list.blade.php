@extends('layouts.dashboard.app')
@section('title')
    @lang('taxonomy::site.transactiontypes')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.transactiontypes')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-22 text-primary pt-2"><i class="lni lni-menu"></i></div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('taxonomy::site.transactiontypes') <small></small>
                    </h5>
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="search" name="search"
                                       class="form-control mt-2 mx-3  radius-30 form-control "
                                       placeholder="@lang('taxonomy::site.search')" value="">
                            </div>
                            <div class="col-md-6">
                                @if (auth()->user()->hasPermission('settings-create'))
                                    <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                       href="{{ route('transactiontypes.create') }}">@lang('taxonomy::site.add_new')<i
                                            class="lni lni-circle-plus mx-1"></i></a>
                                @else
                                    <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30 disabled"
                                       href="#">@lang('user::site.add_new')<i class="lni lni-circle-plus mx-1 "></i></a>
                                @endif
                            </div>
                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($transactiontypes->count() > 0)
                        <table class="table mb-0 table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">@lang('taxonomy::site.name')</th>
                                <th scope="col">@lang('taxonomy::site.action')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($transactiontypes as $index => $transactiontype)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $transactiontype->title }}</td>
                                    <td>
                                        @if (auth()->user()->hasPermission('settings-update'))
                                            <a class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                               data-bs-toggle="dropdown"
                                               aria-expanded="false">@lang('taxonomy::site.action')</a>
                                        @else
                                            <a class="btn btn-primary dropdown-toggle btn-sm disabled"
                                               href="#">@lang('user::site.action')</a>
                                        @endif

                                        <ul class="dropdown-menu" style="">
                                            @if (auth()->user()->hasPermission('settings-update'))
                                                <li class="dropdown-item font-18 text-primary">
                                                    <i class="lni lni-cog"></i>
                                                    <a class="text-dark"
                                                       href="{{ route('transactiontypes.edit', $transactiontype->id) }}">@lang('taxonomy::site.edit')</a>
                                                </li>
                                            @endif
                                            @if (auth()->user()->hasPermission('settings-delete'))
                                                <li class="dropdown-item font-18">
                                                    <i class="bx bxs-trash " style="color: #ff6b6b;"></i>
                                                    <a href="#" class="text-dark " data-bs-toggle="modal"
                                                       data-bs-target="#exampleVerticallycenteredModal{{ $transactiontype->id }}">@lang('taxonomy::site.delete')</a>
                                                </li>
                                        @endif
                                    </td>
                                    <td class="font-30">
                                        @if (auth()->user()->hasPermission('settings-update'))
                                            <div class="form-check form-switch">
                                                <input class=" form-check-input" id="flexSwitchCheckChecked"
                                                       type="checkbox"
                                                       onchange="toggle_status({{ $transactiontype->id }})"
                                                    {{ $transactiontype->status === 1 ? 'checked= "" ' : '' }}>
                                            </div>
                                            {{-- @include('taxonomy::admin.status', [ --}}
                                            {{-- 'item' => $transactiontype, --}}
                                            {{-- ]) --}}
                                        @else
                                            <div class="form-check form-switch">
                                                <a class=" form-check-input disabled" id="flexSwitchCheckChecked"
                                                   type="checkbox">
                                                </a>
                                            </div>
                                        @endif
                                    </td>

                                </tr>
                                <!-- delete -->
                                <div class="modal fade"
                                     id="exampleVerticallycenteredModal{{ $transactiontype->id }}" tabindex="-1">
                                    <form action="{{route('transactiontypes.delete',$transactiontype->id)}}"
                                          method="POST" style="display:inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    @lang('taxonomy::site.confirm_the_deletion')
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">@lang('taxonomy::site.cancel')</button>
                                                    <button type="submit"
                                                            class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="mt-3 mb-3 mx-3">
                            {{ $transactiontypes->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    @else
                        <h2>@lang('taxonomy::site.not_data_found')</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>





@endsection
