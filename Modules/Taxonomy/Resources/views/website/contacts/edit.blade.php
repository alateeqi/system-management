@extends('layouts.dashboard.app')


@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.edit')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('contacts.index') }}">@lang('taxonomy::site.Contact_Information')
                                </a></li>

                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">
                {{-- @include('partials._errors') --}}
                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('contacts.update')}}" method="POST">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 m-2 text-info"><i class="lni lni-agenda"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.edit')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputContactTitle"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   value="{{$contact->translate('ar')->title}}">
                                            <input class="form-control" type="hidden" name="contact_id"
                                                   value="{{$contact->id}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputContactTitle"
                                                   class="form-label">@lang('taxonomy::site.name-en')</label>
                                            <input class="form-control" name="title-en" id="title-en" rows="3"
                                                   value="{{$contact->translate('en')->title}}"></div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputContactContentAR"
                                                       class="form-label">@lang('taxonomy::site.description-ar')</label>
                                                <textarea class="form-control" name="excerpt-ar"
                                                          id="inputCategoryContentAR"
                                                          rows="3">{{$contact->translate('ar')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputContactContentEN"
                                                       class="form-label">@lang('taxonomy::site.description-en')</label>
                                                <textarea class="form-control" name="excerpt-en"
                                                          id="inputcategoryContentEN"
                                                          rows="3">{{$contact->translate('en')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn btn-primary">@lang('taxonomy::site.edit')</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
