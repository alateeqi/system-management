@extends('layouts.dashboard.app')
@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.add_sliders')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('sliders.index') }}">@lang('taxonomy::site.sliders')
                                </a></li>

                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">
                {{-- @include('partials._errors') --}}
                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('sliders.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 mx-2 mt-2 text-info"><i class="lni lni-image"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.add_sliders')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">Select2 Multiple Control</label>
                                            <select name="parent_id" class="single-select select2-hidden-accessible"
                                                    data-select2-id="1" tabindex="-1" aria-hidden="true"
                                                    data-placeholder="Choose anything">
                                                @for ($slider = 0; $slider < count($sliders); $slider++)
                                                    <option value="{{ $sliders[$slider]['id'] }}">
                                                        {{ $sliders[$slider]['title'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputsliderTitle"
                                                   class="form-label">@lang('taxonomy::site.name-ar')</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title" placeholder="@lang('taxonomy::site.name-ar')">

                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputsliderTitleEN"
                                                   class="form-label">@lang('taxonomy::site.name-en')</label>
                                            <input class="form-control" name="title-en" id="title-en" rows="3"
                                                   placeholder="@lang('taxonomy::site.name-en')">

                                        </div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputSliderContentAR" class="form-label">link ar"</label>
                                                <textarea class="form-control" name="excerpt-ar"
                                                          id="inputCategoryContentAR" rows="3"
                                                          placeholder="@lang('taxonomy::site.description-ar')"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputSliderContentEN" class="form-label">link en</label>
                                                <textarea class="form-control" name="excerpt-en"
                                                          id="inputSliderContentEN" rows="3"
                                                          placeholder="@lang('taxonomy::site.description-en')"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="image" class="form-label">@lang('taxonomy::site.add_image')</label>
                                    <div class="col-sm-9">
                                        <div class="card-body">
                                            <input id="image" type="file" class="form-control image" name="image"
                                                   id="fancy-file-upload">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="image" class="col-sm-3 col-form-label"></label>
                                    <img class="col-sm-3  img-thumbnail image-preview" src="" alt="">
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit"
                                                class="btn btn-primary">@lang('taxonomy::site.add_sliders')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @push('js')
        <script>
            const submitform = = document.getElementById("submitform");
            const image = = document.getElementById("image");

            submitform.addEvenrListener("click", uploadCanvasImage);

            function uploadCanvasImage() {
                const base64 = image.toDataURL().split(",")[1];
                const body = {
                    "generated_at": new Date().toISOString(),
                    "png": base64
                    "jpg": base64

                };
            }


        </script>
        <script>
            $('.single-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            $('.multiple-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        </script>
    @endpush

@endsection
