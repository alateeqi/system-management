@extends('layouts.dashboard.app')

@push('css')
    <style>
        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show {
            width: 100px;
            height: 100px;
            position: relative;
            overflow: hidden;
            margin-bottom: 1em;
            float: left;
            border-radius: 12px;
            box-shadow: 0 0 4px 0 #888;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show button.btn-danger {
            position: absolute;
            top: 3px;
            right: 3px;
            width: 20px;
            height: 20px;
            border-radius: 15px;
            font-size: 10px;
            line-height: 1.42;
            padding: 2px 0;
            text-align: center;
            z-index: 3;
        }

        .imageuploadify-show .imageuploadify-images-list-show .imageuploadify-container-show .imageuploadify-details-show {
            position: absolute;
            top: 0;
            padding-top: 20px;
            width: 100%;
            height: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            background: rgba(255, 255, 255, .5);
            z-index: 2;
            opacity: 1;
        }

    </style>
@endpush

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.edit_sliders')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a
                                    href="{{ route('sliders.index') }}">@lang('taxonomy::site.sliders')
                                </a></li>

                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">
                {{-- @include('partials._errors') --}}
                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('sliders.update') }}" method="POST" id="form-slider"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 mx-2 mt-2 text-info"><i class="lni lni-image"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.edit_sliders')</h5>
                                    </div>
                                    <hr/>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">Select2 Multiple Control</label>
                                            <select class="single-select select2-hidden-accessible" name="parent_id"
                                                    data-placeholder="Choose anything" multiple="multiple">
                                                @for ($slid = 0; $slid < count($sliders); $slid++)
                                                    {{-- <option value="{{ $sliders[$slid]['id'] }}">{{ $sliders[$slid]['title'] }}</option> --}}
                                                    <option
                                                        value="{{ $sliders[$slid]['id'] }}" {{$slider->parent_id == $sliders[$slid]['id']  ? 'selected' : '' }}>{{ $sliders[$slid]['title'] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">category Content
                                                AR</label>
                                            <input class="form-control" name="title-ar" id="title-ar" rows="3"
                                                   data-form-type="title" value="{{$slider->translate('ar')->title}}">
                                            <input class="form-control" name="slider_id" id="slider_id" rows="3"
                                                   type="hidden" value="{{$slider->id}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitleEN" class="form-label">category Content
                                                EN</label>
                                            <input class="form-control" name="title-en" id="title-en"
                                                   rows="3" value="{{$slider->translate('en')->title}}">
                                        </div>
                                    </div>
                                    <div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentAR" class="form-label">link ar</label>
                                                <textarea class="form-control" name="excerpt-ar"
                                                          id="inputCategoryContentAR"
                                                          rows="3">{{$slider->translate('ar')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputCategoryContentEN" class="form-label">link en</label>
                                                <textarea class="form-control" name="excerpt-en"
                                                          id="inputcategoryContentEN"
                                                          rows="3">{{$slider->translate('en')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="image" class="form-label">@lang('taxonomy::site.add_image')</label>
                                    <div class="col-sm-9">
                                        <div class="card-body">
                                            <input id="image-uploadify-show" type="file" class="form-control image"
                                                   name="image" id="fancy-file-upload" src=""
                                                   value="{{ $slider->image }}">

                                        </div>
                                    </div>
                                </div>
                                <div class="hidden-input-images"></div>
                                <div class="row mb-3">
                                    <label for="image" class="col-sm-3 col-form-label"></label>
                                    <img class="col-sm-3  img-thumbnail image-preview" src="" alt="">
                                </div>

                                <div class="imageuploadify-show row  g-2 justify-content-center mt-3">
                                    <div class="imageuploadify-images-list-show text-center">
                                        <br/><br/>
                                        <div id="old_images">
                                            @php($image_ids = [])
                                            @foreach ($slider->getMedia('images') as $image)
                                                @php(array_push($image_ids, $image->id))
                                                <div class="col imageuploadify-container-show" style="margin: 3px;"
                                                     id="image_old{{ $image->id }}">
                                                    <button type="button" class="btn btn-danger bx bx-x"
                                                            onclick="remove_old_image('{{ $image->id }}')"></button>
                                                    <div class="imageuploadify-details--show">
                                                        <img src="{{ $image->getUrl('full') }}"
                                                             data-id="{{ $image->id }}" width="100">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button id="submit-form" type="submit"
                                                class="btn btn-primary">@lang('taxonomy::site.edit_sliders')</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



    @push('js')
        <script>

            const submitform = = document.getElementById("submit-form");
            const image = = document.getElementById("image");

            submitform.addEvenrListener("click", uploadCanvasImage);

            function uploadCanvasImage() {
                const base64 = image.toDataURL().split(",")[1];
                const body = {
                    "generated_at": new Date().toISOString(),
                    "png": base64
                    "jpg": base64

                };
            }
        </script>

        <script>
            $(".image").change(function () {

                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.image-preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            })

            var images_list = [];
            $(document).ready(function () {
                $('#image-uploadify-show').imageuploadify();

                $('#submit-form').on('click', function (e) {
                    e.preventDefault();
                    $('.imageuploadify-container img').each(function () {
                        $('.hidden-input-images').append(
                            '<input type="hidden" name="image" value="' + $(this).attr('src') + '">'
                        )
                    });

                    for (i = 0; i < images_list.length; i++) {
                        $('#form-slider').append('<input type="hidden" name="images_old_deleted[]" value="' +
                            images_list[i] + '">')
                    }
                    document.getElementById('form-slider').submit();
                });
            })

            function remove_old_image(id) {
                $('#image_old' + id).remove();
                images_list.push(id)
            }
        </script>

        <script>
            $('.single-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
            $('.multiple-select').select2({
                theme: 'bootstrap4',
                width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                placeholder: $(this).data('placeholder'),
                allowClear: Boolean($(this).data('allow-clear')),
            });
        </script>



    @endpush
@endsection

