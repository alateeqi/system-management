@extends('layouts.dashboard.app')
@section('title')
    @lang('taxonomy::site.sliders')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.sliders')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div class="font-30 mx-2 mt-2 text-primary"><i class="lni lni-image"></i></div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('taxonomy::site.sliders') <small></small></h5>
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="search" name="search"
                                       class="form-control mt-2 mx-3  radius-30 form-control "
                                       placeholder="@lang('taxonomy::site.search')" value="{{request()->search}}">
                            </div>

                            <div class="col-md-6">
                                @if (auth()->user()->hasPermission('website-create'))
                                    <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                       href="{{ route('sliders.create') }}">@lang('taxonomy::site.add_sliders')<i
                                            class="lni lni-circle-plus mx-1"></i></a>
                                @else
                                    <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30 disabled"
                                       href="#">@lang('user::site.add_new')<i class="lni lni-circle-plus mx-1 "></i></a>
                                @endif
                            </div>

                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($sliders->count() > 0)
                        <div class="table-responsive">
                            <table id="example2" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">@lang('taxonomy::site.name')</th>
                                    <th scope="col">@lang('taxonomy::site.image')</th>
                                    <th scope="col">@lang('taxonomy::site.action')</th>
                                    <th scope="col">@lang('taxonomy::site.status')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($sliders as $index => $slider)
                                    <tr>
                                        <th scope="row">{{ $index + 1 }}</th>
                                        <td>{{ $slider->title }}</td>
                                        <td><img class="w-25"
                                                 src="{{ $slider->getFirstMediaUrl('images', 'thumbnail') }}"></td>
                                        <td>
                                            @if (auth()->user()->hasPermission('website-update'))
                                                <a class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                                   data-bs-toggle="dropdown"
                                                   aria-expanded="false">@lang('taxonomy::site.action')</a>
                                            @else
                                                <a class="btn btn-primary dropdown-toggle btn-sm disabled"
                                                   href="#">@lang('user::site.action')</a>
                                            @endif
                                            <ul class="dropdown-menu" style="">
                                                @if (auth()->user()->hasPermission('website-update'))
                                                    <li class="dropdown-item font-18 text-primary">
                                                        <i class="lni lni-cog"></i>
                                                        <a class="text-dark"
                                                           href="{{ route('sliders.edit', $slider->id) }}">@lang('taxonomy::site.edit')</a>
                                                    </li>
                                                @endif
                                                @if (auth()->user()->hasPermission('website-delete'))
                                                    <li class="dropdown-item font-18">
                                                        <i class="bx bxs-trash " style="color: #ff6b6b;"></i>
                                                        <a href="#" class="text-dark " data-bs-toggle="modal"
                                                           data-bs-target="#exampleVerticallycenteredModal">@lang('taxonomy::site.delete')</a>
                                                    </li>
                                            @endif
                                        </td>

                                        <td class="font-30">
                                            @if (auth()->user()->hasPermission('website-update'))
                                                {{--                                                @include('taxonomy::admin.status',['item' => $slider])--}}
                                                <div class="form-check form-switch">
                                                    <input class=" form-check-input" id="flexSwitchCheckChecked"
                                                           type="checkbox"
                                                           onchange="toggle_status({{$slider->id}})" {{$slider->status === 1 ? 'checked= "" ' : ''}}>
                                                </div>
                                            @else
                                                <div class="form-check form-switch">
                                                    <a class=" form-check-input disabled" id="flexSwitchCheckChecked"
                                                       type="checkbox"></a>
                                                </div>
                                            @endif
                                        </td>

                                    </tr>


                                    <!-- slider-delete -->
                                    <div class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1"
                                         aria-hidden="true">
                                        <form action="{{ route('sliders.delete', $slider->id) }}" method="POST"
                                              style="display:inline-block;">
                                            @csrf
                                            @method('DELETE')
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">@lang('taxonomy::site.delete')</h5>
                                                        <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @lang('taxonomy::site.confirm_the_deletion')
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">@lang('taxonomy::site.cancel')</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">@lang('taxonomy::site.delete')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="mt-3 mb-3 mx-3">
                                {{ $sliders->links('vendor.pagination.bootstrap-4') }}
                                {{-- {{ $categories = DB::table('taxonomies')->simplePaginate(10) }} --}}
                            </div>
                            @else
                                <h2>@lang('taxonomy::site.not_data_found')</h2>
                            @endif

                        </div>
                </div>
            </div>
        </div>





@endsection
