@extends('layouts.dashboard.app')


@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('taxonomy::site.edit_translation')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('translations.index') }}">@lang('taxonomy::site.websitetranslations')</li></a>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="row">
                
                <div class="col-xl-10 mx-auto">
                    <form action="{{ route('translations.update') }}" method="POST">
                        @csrf
                        <div class="card border-top border-0 border-4 border-info">
                            <div class="card-body">
                                <div class="border p-4 rounded">
                                    <div class="card-title d-flex align-items-center">
                                        <div class="font-30 m-2 text-info"> <i class="lni lni-italic"></i></div>
                                        <h5 class="mb-0 text-info">@lang('taxonomy::site.edit_translation')</h5>
                                    </div>
                                    <hr/>
                                    <div class="row mb-3">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle"class="form-label">@lang('taxonomy::site.title-ar')</label>
                                            <input class="form-control" name="title-ar"id="title-ar" rows="3" data-form-type="title" value="{{$translation->translate('ar')->title}}">
                                            <input type="hidden" class="form-control" name="translation_id"id="translation_id" value="{{$translation->id}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="inputCategoryTitle" class="form-label">@lang('taxonomy::site.title-en')</label>
                                            <input class="form-control"  name="title-en"id="title-en" rows="3" value="{{$translation->translate('en')->title}}">

                                        </div>
                                    </div>
									<div class="row col-12">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputTranslationContentAR" class="form-label">@lang('taxonomy::site.translation-ar')</label>
                                                <textarea class="form-control"name="excerpt-ar"id="inputCategoryContentAR"rows="3">{{$translation->translate('ar')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="inputTranslationContentAR" class="form-label">@lang('taxonomy::site.translation-en')</label>
                                                <textarea class="form-control"name="excerpt-en"id="inputcategoryContentEN"rows="3">{{$translation->translate('ar')->excerpt}}</textarea>
                                            </div>
                                        </div>
                                    </div>		
                                </div>

                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit"class="btn btn-primary">@lang('taxonomy::site.edit')</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
