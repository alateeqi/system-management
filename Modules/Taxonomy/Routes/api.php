<?php

Route::get('instagram-sliders', 'Guest\Slider\GuestSliderController@instagramSliders');
Route::get('get-site-status', 'Guest\Site\GuestSiteController@siteStatus');
Route::get('pages/{slug}', 'Guest\Page\GuestPageController@page');
Route::get('category/{slug}', 'Guest\Category\GuestCategoryController@getCategory');
Route::get('cities', 'Guest\City\GuestCityController@cities');
