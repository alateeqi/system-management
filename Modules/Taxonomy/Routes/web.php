<?php


use Illuminate\Support\Facades\Route;
use Modules\Taxonomy\Http\Controllers\Admin\AppTaxonomyController;
use Modules\Taxonomy\Http\Controllers\Admin\Category\AdminCategoryController;
use Modules\Taxonomy\Http\Controllers\Admin\City\AdminCityController;
use Modules\Taxonomy\Http\Controllers\Admin\Gate\AdminGateController;
use Modules\Taxonomy\Http\Controllers\Admin\Options\AdminOptionsController;
use Modules\Taxonomy\Http\Controllers\Admin\Payment\AdminPaymentController;
use Modules\Taxonomy\Http\Controllers\Admin\TodoLabels\AdminTodoLabelsController;
use Modules\Taxonomy\Http\Controllers\Admin\TransactionTypes\AdminTransactionTypesController;
use Modules\Taxonomy\Http\Controllers\Website\Contacts\AdminContactsController;
use Modules\Taxonomy\Http\Controllers\Website\Metas\AdminMetasController;
use Modules\Taxonomy\Http\Controllers\Website\Pages\AdminPagesController;
use Modules\Taxonomy\Http\Controllers\Website\Sliders\AdminSlidersController;
use Modules\Taxonomy\Http\Controllers\Website\Translations\AdminTranslationsController;


Route::group(
    [
            'prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ],
    function () {
        Route::prefix('admin')->middleware(['auth'])->group(function () {

            Route::post('/taxonomy/toggle-status/', [AppTaxonomyController::class, 'toggleStatus'])->name('toggle-status');

            //category routes
            Route::get('/category', [AdminCategoryController::class, 'index'])->name('categories.index');
            Route::get('/categpries/create', [AdminCategoryController::class, 'create'])->name('categries.create');
            Route::post('/categpries/store', [AdminCategoryController::class, 'store'])->name('categries.store');
            Route::get('/categpries/edit/{category_id}', [AdminCategoryController::class, 'edit'])->name('categries.edit');
            Route::post('/categries/update', [AdminCategoryController::class, 'update'])->name('categries.update');
            Route::delete('/categpries/delete/{taxonomy}', [AdminCategoryController::class, 'destroy'])->name('categries.delete');


            // cities routes
            Route::get('/cities', [AdminCityController::class, 'index'])->name('cities.index');
            Route::get('/cities/create', [AdminCityController::class, 'create'])->name('cities.create');
            Route::post('/cities/store', [AdminCityController::class, 'store'])->name('cities.store');
            Route::get('/cities/edit/{city_id}', [AdminCityController::class, 'edit'])->name('cities.edit');
            Route::post('/cities/update', [AdminCityController::class, 'update'])->name('cities.update');
            Route::delete('/cities/delete/{city_id}', [AdminCityController::class, 'destroy'])->name('cities.delete');
            Route::get('/cities/all-cities', [AdminCityController::class, 'allCities'])->name('admin.cities.all_cities');

            // payment routes
            Route::get('/payment', [AdminPaymentController::class, 'index'])->name('payment.index');
            Route::get('/payment/create', [AdminPaymentController::class, 'create'])->name('payment.create');
            Route::post('/payment/store', [AdminPaymentController::class, 'store'])->name('payment.store');
            Route::get('/payment/edit/{payment_id}', [AdminPaymentController::class, 'edit'])->name('payment.edit');
            Route::post('/payment/update', [AdminPaymentController::class, 'update'])->name('payment.update');
            Route::delete('/payment/delete/{payment_id}', [AdminPaymentController::class, 'destroy'])->name('payment.delete');

            // gates routes
            Route::get('/gates', [AdminGateController::class, 'index'])->name('gates.index');
            Route::get('/gates/create', [AdminGateController::class, 'create'])->name('gates.create');
            Route::post('/gates/store', [AdminGateController::class, 'store'])->name('gates.store');
            Route::get('/gates/edit/{gate_id}', [AdminGateController::class, 'edit'])->name('gates.edit');
            Route::post('/gates/update', [AdminGateController::class, 'update'])->name('gates.update');
            Route::delete('/gates/delete/{gate_id}', [AdminGateController::class, 'destroy'])->name('gates.delete');

            // option routes
            Route::get('/advanced_options', [AdminOptionsController::class, 'index'])->name('options.index');
            Route::get('/advanced_options/create', [AdminOptionsController::class, 'create'])->name('options.create');
            Route::post('/advanced_options/store', [AdminOptionsController::class, 'store'])->name('options.store');
            Route::get('/advanced_options/edit/{option_id}', [AdminOptionsController::class, 'edit'])->name('options.edit');
            Route::post('/advanced_options/update', [AdminOptionsController::class, 'update'])->name('options.update');
            Route::delete('/advanced_options/delete/{option_id}', [AdminOptionsController::class, 'destroy'])->name('options.delete');

            // todolabels routes
            Route::get('/AodoLabels', [AdminTodoLabelsController::class, 'index'])->name('todolabels');

            // transactiontypes routes
            Route::get('/transactiontypes', [AdminTransactionTypesController::class, 'index'])->name('transactiontypes.index');
            Route::get('/transactiontypes/create', [AdminTransactionTypesController::class, 'create'])->name('transactiontypes.create');
            Route::post('/transactiontypes/store', [AdminTransactionTypesController::class, 'store'])->name('transactiontypes.store');
            Route::get('/transactiontypes/edit/{transaction_id}', [AdminTransactionTypesController::class, 'edit'])->name('transactiontypes.edit');
            Route::post('/transactiontypes/update', [AdminTransactionTypesController::class, 'update'])->name('transactiontypes.update');
            Route::delete('/transactiontypes/delete/{taype_id}', [AdminTransactionTypesController::class, 'destroy'])->name('transactiontypes.delete');

            // pages routes
            Route::get('/pages', [AdminPagesController::class, 'index'])->name('pages.index');
            Route::get('/pages/create', [AdminPagesController::class, 'create'])->name('pages.create');
            Route::post('/pages/store', [AdminPagesController::class, 'store'])->name('pages.store');
            Route::get('/pages/edit/{page_id}', [AdminPagesController::class, 'edit'])->name('pages.edit');
            Route::post('/pages/update', [AdminPagesController::class, 'update'])->name('pages.update');
            Route::delete('/pages/delete/{page_id}', [AdminPagesController::class, 'destroy'])->name('pages.delete');

            // sliders routes
            Route::get('/sliders', [AdminSlidersController::class, 'index'])->name('sliders.index');
            Route::get('/sliders/create', [AdminSlidersController::class, 'create'])->name('sliders.create');
            Route::post('/sliders/store', [AdminSlidersController::class, 'store'])->name('sliders.store');
            Route::get('/sliders/edit/{slider_id}', [AdminSlidersController::class, 'edit'])->name('sliders.edit');
            Route::post('/sliders/update', [AdminSlidersController::class, 'update'])->name('sliders.update');
            Route::delete('/sliders/delete/{slider_id}', [AdminSlidersController::class, 'destroy'])->name('sliders.delete');

            // contacts routes
            Route::get('/contacts', [AdminContactsController::class, 'index'])->name('contacts.index');
            Route::get('/contacts/create', [AdminContactsController::class, 'create'])->name('contacts.create');
            Route::post('/contacts/store', [AdminContactsController::class, 'store'])->name('contacts.store');
            Route::get('/contacts/edit/{contact_id}', [AdminContactsController::class, 'edit'])->name('contacts.edit');
            Route::post('/contacts/update', [AdminContactsController::class, 'update'])->name('contacts.update');
            Route::delete('/contacts/delete/{contact_id}', [AdminContactsController::class, 'destroy'])->name('contacts.delete');

            // translations routes
            Route::get('/translations', [AdminTranslationsController::class, 'index'])->name('translations.index');
            Route::get('/translations/create', [AdminTranslationsController::class, 'create'])->name('translations.create');
            Route::post('/translations/store', [AdminTranslationsController::class, 'store'])->name('translations.store');
            Route::get('/translations/edit/{translation_id}', [AdminTranslationsController::class, 'edit'])->name('translations.edit');
            Route::post('/translations/update', [AdminTranslationsController::class, 'update'])->name('translations.update');
            Route::delete('/translations/delete/{translation_id}', [AdminTranslationsController::class, 'destroy'])->name('translations.delete');

            // Metas routes
            Route::get('/metas', [AdminMetasController::class, 'index'])->name('metas.index');
            Route::get('/metas/create', [AdminMetasController::class, 'create'])->name('metas.create');
            Route::post('/metas/store', [AdminMetasController::class, 'store'])->name('metas.store');
            Route::get('/metas/edit/{meta_id}', [AdminMetasController::class,'edit'])->name('metas.edit');
            Route::post('/metas/update', [AdminMetasController::class, 'update'])->name('metas.update');
            Route::delete('/metas/delete/{meta_id}', [AdminMetasController::class, 'destroy'])->name('metas.delete');



        });
    }
);
