<?php

namespace Modules\Transaction\Entities;

use App\Traits\RecordsActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Record\Entities\Record;
use Modules\Taxonomy\Entities\Taxonomy;

class Transaction extends Model
{
    use SoftDeletes, RecordsActivity;

    protected $fillable = [
        'record_id',
        'account_id',
        'title',
        'credit_amount',
        'debt_amount',
    ];

    protected $with = [
        'account'
    ];

    protected static function boot()
    {
        parent::boot();

        if (auth()->guest()) {
            return true;
        }

        self::savingActivity();
    }

    public function record(){
        return $this->belongsTo(Record::class);
    }

    public function account(){
        return $this->belongsTo(Taxonomy::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Taxonomy::class, 'transaction_tag', 'transaction_id', 'tag_id');
    }
}
