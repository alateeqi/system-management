<?php

namespace Modules\User\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;


class Store extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],

            'email' => ['required', 'max:255'],

            'password' => ['required'],

            'phone' => ['nullable'],

            'salary' => ['nullable', 'max:255'],

            'permissions' => ['nullable'],

            'role' => ['required', 'max:255'],

            'birth_date'=>['nullable','date'],

            'time_start'=>['nullable',],

            'time_end'=>['nullable'],

            'start_date_employment'=>['nullable','date'],

            'end_date_employment'=>['nullable','date'],

        ];
    }
}
