<?php

namespace Modules\User\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;


class Update extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'users_id' => ['required'],
            'name' => ['required', 'max:255'],
            'email' => ['required', 'max:255'],
            'password' => ['nullable'],
            'phone' => ['nullable'],
            'salary' => ['nullable', 'max:255'],
            'permissions' => ['nullable'],
            'role' => ['required', 'max:255'],
            'birth_date'=>['nullable','date'],

            'time_start'=>['nullable',],

            'time_end'=>['nullable'],
            
            'start_date_employment'=>['nullable','date'],

            'end_date_employment'=>['nullable','date'],
        ];
    }
}
