<?php

namespace Modules\User\Interfaces\Admin;
use Illuminate\Http\Request;
interface AdminUserInterface

{
    public function index(Request $request);

    public function store($data);
    public function update($data);

    public function allUser();
}
