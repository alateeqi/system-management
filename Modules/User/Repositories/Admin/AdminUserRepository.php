<?php

namespace Modules\User\Repositories\Admin;

use Modules\Auth\Entities\User;
use Modules\User\Interfaces\Admin\AdminUserInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Hash;

class AdminUserRepository implements AdminUserInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index(Request $request)
    {
        $users =User::when($request->search, function($query) use ($request) {
            return $query->where('name','like', '%' . $request->search . '%' );
        })->latest()->Paginate(10);

        // $users = User::paginate($paginate);

        return $users;

    }

    public function store($data)
    {

        $new_user = new User();
        $new_user->name = $data['name'];
        $new_user->email = $data['email'];
        $new_user->phone = $data['phone'];
        $new_user->salary = $data['salary'];
        $new_user->role = $data['role'];
        $new_user->birth_date = $data['birth_date'];
        $new_user->time_start = $data['time_start'];
        $new_user->time_end = $data['time_end'];
        $new_user->start_date_employment = $data['start_date_employment'];
        $new_user->end_date_employment = $data['end_date_employment'];
        $new_user->password= Hash::make($data['password']);
        $new_user->save();
        $new_user->attachRole('admin');
        $new_user->syncPermissions($data['permissions']);

        
        return $new_user;
    }

    public function update($data)
    {
        $user = User::findOrFail($data['users_id']);
        // $new_user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->salary = $data['salary'];
        $user->role = $data['role'];
        $user->birth_date = $data['birth_date'];
        $user->time_start = $data['time_start'];
        $user->time_end = $data['time_end'];
        $user->start_date_employment = $data['start_date_employment'];
        $user->end_date_employment = $data['end_date_employment'];
        $user->password = Hash::make($data['password']);
        $user->save();
        $user->syncPermissions($data['permissions']);
        
        return $user;
    }

    public function allUser()
    {
        return User::orderBy('id', 'desc')->pluck('name', 'id');
    }

}


