@extends('layouts.dashboard.app')
@section('title')
    @lang('user::site.users')
@endsection

@push('css')
    <style>
        .icon-font {
            font-size: 2px !important;
        }
        .form-font {
            font-size: 17px;
        }
    </style>
@endpush

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('user::site.users')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <div class="card">
                <div class="card-body p-3">
                    <h5 class="card-title">@lang('user::site.add_user')</h5>
                    <hr/>

                    <form class="mt-4" action="{{ route('users.store') }}" id="form-recipe" method="POST"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="border border-3 p-2 rounded">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-3">
                                                <label for="inputRecipestTitle"
                                                    class="form-label">@lang('user::site.name')</label>
                                                <input type="text" name="name" class="form-control form-font"
                                                    id="inputRecipestTitle" placeholder="@lang('user::site.name')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="inputRecipesName"
                                                    class="form-label">@lang('user::site.phone') </label>
                                                <input name="phone" type="number" class="form-control form-font"
                                                    id="inputRecipesName" placeholder="@lang('user::site.phone')">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="inputRecipesQuantity"
                                                    class="form-label">@lang('user::site.email')</label>
                                                <input type="email" name="email" class="form-control form-font"
                                                    id="inputRecipesQuantity" placeholder="@lang('user::site.email')">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-3">
                                                <label for="inputRecipestTitle"
                                                    class="form-label">@lang('user::site.password')</label>
                                                <input type="password" name="password" class="form-control form-font"
                                                    id="inputRecipestTitle" placeholder="@lang('user::site.password')">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="inputRecipesName"class="form-label">@lang('user::site.salary') </label>
                                                <input name="salary" type="text" class="form-control form-font"
                                                    id="inputRecipesName" placeholder="@lang('user::site.salary')">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="exampleDataList"
                                                class="form-label">@lang('user::site.role')</label>
                                            <input class="form-control" list="datalistOptions" id="exampleDataList" name="role" placeholder="Roles">
                                            <datalist id="datalistOptions">
                                                <option value="Admin">
                                                <option value="Accountant">
                                                <option  value="Call center">
                                                <option value="Delivery">
                                                <option value="Kitchen">
                                                <option value="Storekeeper">
                                            </datalist>
                                        </div>

                                        <div class="row mb-3 form-font">
                                            <label for="password_confirmation" class="form-label">
                                                @lang('user::site.role')</label>
                                            <div class="col-sm-12 ">
                                                <div class="row mb-3">
                                                    @php
                                                        $models = ['products', 'settings', 'clients', 'orders', 'website','recipes','users','reports','coupons'];
                                                        $maps = ['create', 'read', 'update', 'delete'];
                                                    @endphp

                                                    <ul class="nav nav-tabs nav-primary" role="tablist">
                                                        @foreach ($models as $index => $model)
                                                            <li class=" nav-item  {{ $index == 0 ? 'active' : '' }}"
                                                                role="presentation"><a class="nav-link"
                                                                    data-bs-toggle="tab" href="#{{ $model }}"
                                                                    role="tab" aria-selected="true">
                                                                    <div class="d-flex align-items-center">
                                                                        <div class="tab-title">@lang('user::site.'. $model)
                                                                        </div>
                                                                    </div>
                                                                </a></li>
                                                        @endforeach
                                                    </ul>
                                                    {{-- end of nav --}}
                                                    <div class="tab-content py-3 ">
                                                        @foreach ($models as $index => $model)
                                                            <div class="tab-pane fade  {{ $index == 0 ? 'active' : '' }} "
                                                                id="{{ $model }}" role="tabpanel">
                                                                @foreach ($maps as $map)
                                                                    <div class="d-inline p-2"><input class="form-check-input m-1" type="checkbox"
                                                                            id="gridCheck4" name="permissions[]"
                                                                            value="{{ $model . '-' . $map }}"><label
                                                                            class="form-check-label"
                                                                            for="gridCheck4">@lang('user::site.' . $map)</label>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                {{-- end of tab --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="border border-3 p-2 rounded table-responsive">
                                    <div class="row g-3">

                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('user::site.start_time')</label>
                                                <input class="result form-control" type="time" id="time" name="time_start"placeholder="Date Picker...">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('user::site.end_time')</label>
                                                <input class="result form-control time" type="time" id="time" name="time_end"
                                                    placeholder="Date Picker...">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('user::site.start_date_employment')</label>
                                                <input class="result form-control date " type="date" 
                                                name="start_date_employment"
                                                    placeholder="Date Picker...">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('user::site.end_date_employment')</label>
                                                <input class="result form-control date" type="date" 
                                                name="end_date_employment"
                                                    placeholder="Date Picker...">
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label class="form-label">@lang('user::site.birth_date') </label>
                                                <input class="result form-control datepicker" type="date" id="date" 
                                                name="birth_date"
                                                placeholder="Date Picker...">
                                            </div>
                                        </div>
                                        
                                        <div class="col-12">
                                            <div class="d-grid">
                                                <button type="submit" class="btn btn-primary"
                                                    id="submit-form">@lang('user::site.add_user')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    {{--  --}}

    <script>
        $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: true
            }),
            $('.timepicker').pickatime()
    </script>
    <script>
        $(function() {
            $('#date-time').bootstrapMaterialDatePicker({
                format: 'YYYY-MM-DD HH:mm'
            });
            $('#date').bootstrapMaterialDatePicker({
                time: false
            });
            $('.date').bootstrapMaterialDatePicker({
                time: false
            });
            $('#time').bootstrapMaterialDatePicker({
                date: false,
                format: 'HH:mm'
            });
            $('.time').bootstrapMaterialDatePicker({
                date: false,
                format: 'HH:mm'
            });
        });
    </script>
@endpush
