@extends('layouts.dashboard.app')
@section('title')
    @lang('user::site.users')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="page-content">
            <!-- page-header-->
            <!--breadcrumb-->
            <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
                <div class="breadcrumb-title pe-3">@lang('user::site.users')</div>
                <div class="ps-3">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 p-0">
                            <li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!--end breadcrumb-->
            <!--EN FOR  page-header-->
            <div class="card border-top border-0 border-4 border-primary table-responsive">
                <div class="card-title d-flex align-items-center">
                    <div><i class="bx bxs-user me-1 font-22 text-primary pt-2"></i>
                    </div>
                    <h5 class="mb-0  font-12 text-primary pt-2">@lang('user::site.users') <small></small>
                    </h5>

                    <form action="" method="GET">
                        <div class="row">
                            
                            <div class="col-md-6">
                                <input type="search" name="search" class="form-control mt-2 mx-3  radius-30 form-control "
                                    placeholder="@lang('user::site.search')" value="{{ request()->search }}">
                            </div>

                            <div class="col-md-6">
                                @if (auth()->user()->hasPermission('users-create'))
                                <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30"
                                    href="{{route('users.create')}}">@lang('user::site.add_new')<i class="lni lni-circle-plus mx-1"></i></a>
                                    @else
                                    <a class="p-2 btn btn-outline-primary  btn-sm  mt-2 mx-2  radius-30 disabled"
                                        href="#">@lang('site.add')<i class="lni lni-circle-plus mx-1"></i></a>
                                @endif
                            </div>

                        </div>
                    </form>
                    {{-- end of form --}}
                </div>

                <div class="card-body ">
                    @if ($users->count() > 0)
                        <div class="table-responsive">
                            <table id="example2" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">@lang('user::site.name')</th>

                                        <th scope="col">@lang('user::site.email')</th>
                                        <th scope="col">@lang('user::site.phone')</th>
                                        <th scope="col">@lang('user::site.role')</th>
                                        <th scope="col">@lang('user::site.status')</th>
                                        <th scope="col">@lang('user::site.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $index => $user)
                                        <tr>
                                            <th scope="row">{{ $index + 1 }}</th>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>{{ $user->role }}</td>

                                            <td class="font-30">
{{--                                                @include('taxonomy::admin.status',['item' => $user])--}}
                                            </td>
                                            <td>
                                                @if (auth()->user()->hasPermission('users-update'))
                                                <a class="btn btn-primary dropdown-toggle btn-sm" type="button"
                                                    data-bs-toggle="dropdown" aria-expanded="false">تعديل</a>
                                                    @else
                                                <a href="#" class="btn btn-primary dropdown-toggle btn-sm disabled"
                                                    type="button" data-bs-toggle="dropdown" aria-expanded="false">تعديل</a>
                                                @endif

                                                <ul class="dropdown-menu" style="">
                                                    @if (auth()->user()->hasPermission('users-update'))
                                                    <li class="dropdown-item font-18 text-primary">
                                                        <i class="lni lni-cog"></i>
                                                        <a class="text-dark" href="{{ route('users.edit', $user->id) }}">@lang('user::site.edit')</a>
                                                    </li>
                                                    @endif
                                                    @if (auth()->user()->hasPermission('users-delete'))
                                                    <li class="dropdown-item font-18">
                                                        <i class="bx bxs-trash " style="color: #ff6b6b;"></i>
                                                        <a href="#" class="text-dark " data-bs-toggle="modal"
                                                        data-bs-target="#exampleVerticallycenteredModal{{$user->id}}">@lang('user::site.delete')</a>
                                                    </li>
                                                    @endif
                                                </ul>

                                            </td>
                                        </tr>

                                        <!-- users-delete -->
                                        <div class="modal fade" id="exampleVerticallycenteredModal{{$user->id}}" tabindex="-1"aria-hidden="true">
                                        <form action="{{route('users.delete',$user->id)}}" method="POST" style="display:inline-block;">
                                            @csrf
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">@lang('user::site.delete')</h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @lang('user::site.confirm_the_deletion')
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">@lang('user::site.cancel')</button>
                                                            <button type="submit"
                                                                class="btn btn-primary">@lang('user::site.delete')</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                </form>
                                            </div>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="mt-3 mb-3 mx-3">
                                {{ $users->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        @else
                            <h2>@lang('taxonomy::site.not_data_found')</h2>
                    @endif
                </div>
            </div>
        </div>
    </div>





@endsection
