<?php

namespace App\Composer;

use Modules\Order\Repositories\Admin\AdminOrderRepository;

class LayoutComposer
{
    public function compose($view)
    {
        $view->with([
            'unreadWebsiteOrders' => count((new AdminOrderRepository(auth()->user()))->unreadWebsiteOrders()['unreadWebsiteOrders']),
        ]);
    }
}
