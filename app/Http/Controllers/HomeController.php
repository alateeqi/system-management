<?php

namespace App\Http\Controllers;


use Modules\Home\Interfaces\Admin\AdminHomeInterface;
use Modules\Order\Interfaces\AdminOrderInterface;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(AdminHomeInterface $interface)
    {
        $statistics = $interface->statistics();
        return view('home', compact('statistics'));
    }


}
