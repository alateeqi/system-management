<?php

namespace App\Http\Middleware;

use Closure;

class IsAjaxMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->ajax()){
            $request->session()->put('fullUrl', $request->fullUrl());
            return redirect()->route('admin');
        }

        return $next($request);
    }
}
