<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @param array                     $roles
     *
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if ( ! auth()->check()) {
            if (str_is('*/admin*', $request->url())) {
                return response()->view('admin.login');
            }

            return response()->view('auth.login');
        } elseif ( ! in_array($request->user()->role, $roles)) {
            return redirect('/');
        }

        return $next($request);
    }
}
