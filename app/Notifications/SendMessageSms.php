<?php

namespace App\Notifications;

use App\Channels\kwtsms;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Http;

class SendMessageSms extends Notification
{
    private $message;
    private $phone;

    /**
     * Create a new notification instance.
     *
     * @param $message
     * @param $phone
     */
    public function __construct($message, $phone)
    {
        $this->phone = $phone;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [kwtsms::class];
    }

    public function toSms($notifiable)
    {

        $request = 'https://www.kwtsms.com/API/send/?username=bakednco&password=5231033&sender=KWT-MESSAGE&mobile=965';
        $request .= $this->phone;

        $request .= '&lang=1&message=';
        $request .= $this->message;

        $response = Http::get($request);

        \Log::info('SMS $response  ' .$this->phone.': '. $response);
    }
}
