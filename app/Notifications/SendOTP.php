<?php

namespace App\Notifications;

use App\Channels\kwtsms;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Http;


/**
 * @property  password
 */
class SendOTP extends Notification
{
    private $password;
    private $phone;

    /**
     * Create a new notification instance.
     *
     * @param $password
     * @param $phone
     */
    public function __construct($password, $phone)
    {
        $this->phone = $phone;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [kwtsms::class];
    }

    public function toSms($notifiable)
    {
        $request = 'https://www.kwtsms.com/API/send/?username=bakednco&password=5231033&sender=KWT-MESSAGE&mobile=965';
        $request .= $this->phone;
        $request .= '&lang=1&message=';
        $request .= "Your+bakednco.com+login+information+is+\nphone:+$this->phone+\npassword:+$this->password";

        $response = Http::get($request);
        \Log::info('SMS $response: '. $response);
    }
}
