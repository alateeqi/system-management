<?php

namespace App\Traits;


use Spatie\Activitylog\Models\Activity;

trait RecordsActivity
{
    protected static function savingActivity()
    {
        static::updating(
            function ($record) {
                if ($record->isDirty()) {
                    $data = [];
                    foreach ($record->getDirty() as $key => $value) {
                        $data[$key] = [
                            'old' => $record->getOriginal($key),
                            'new' => $value
                        ];
                    }
                    $record->recordActivity('updated', $data);
                }
            }
        );

        static::deleting(
            function ($record) {
                $data = [];
                foreach ($record->getAttributes() as $key => $value) {
                    $data[$key] = [
                        'old' => '',
                        'new' => $value
                    ];
                }
                $record->recordActivity('deleted', $data);
            }
        );

        static::created(
            function ($record) {
                $data = [];
                foreach ($record->getAttributes() as $key => $value) {
                    $data[$key] = [
                        'old' => '',
                        'new' => $value
                    ];
                }
                $record->recordActivity('created', $data);
            }
        );
    }

    protected function recordActivity($event, $data)
    {
        $causer_type = (new \ReflectionClass(auth()->user()))->getName();
        $this->activity()
            ->create([
                'causer_id' => auth()->id(),
                'causer_type' => $causer_type,
                'description' => $this->getActivityType($event),
                'log_name' => 'users activities',
                'properties' => [
                    'data' => $data
                ]
            ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    protected function getActivityType($event)
    {
        $type = strtolower((new \ReflectionClass($this))->getShortName());

        return "{$type} {$event}";
    }
}
