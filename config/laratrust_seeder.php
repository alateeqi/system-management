<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [

        'super_admin' => [

            'products'=> 'c,r,u,d',
            'settings'=> 'c,r,u,d',
            'clients'=>  'c,r,u,d',
            'orders'=>   'c,r,u,d',
            'website'=>  'c,r,u,d',
            'recipes'=>  'c,r,u,d',
            'users'=>    'c,r,u,d',
            'reports'=>  'c,r,u,d',
            'coupons'=>  'c,r,u,d',
            
        ],
        
        'admin' =>[]

    ],




    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete',

    ]
];
