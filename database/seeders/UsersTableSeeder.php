<?php

namespace Database\Seeders;
use Modules\Auth\Entities\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $user = User::create([
            'name' => 'mostafa Azizi test',
            'email' => 'mostafa35@gmail.com',
            'password' => bcrypt('123456'),
        ]);
        $user->attachRole('super_admin');
    }
}






