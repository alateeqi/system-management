<?php

return [
    'today_orders' => 'Today Orders',
    'today_website_order' => 'Today Website Order',
    'sales_today' => 'Sales today',
    'New Customer' => 'New Customer',
    'monthly_sales' => 'Monthly Sales',
    'sales_for_last_7_days' => 'Sales For Last 7 Days',
    'best_sales_products_for_last_7_days' => 'Best Sales Products For Last 7 Days',
    'added_successfully' => 'Added Successfully',
    'updated_successfully' => 'Updated Successfully',
    'deleted_successfully' => 'Deleted Successfully',
    'value' => 'value',
    'orders' => 'orders',
    'targets_points' => 'Targets & Points',
    'coupons_gifts' => 'Coupons & Gifts',
    'coupons' => 'Coupons',
    'add_coupon' => '+ Add Coupon',
    'gifts' => 'Gifts',
    'gift_winners' => 'Gift Winners',
    'reports' => 'Reports',
    'export_excel' => 'Export Excel',
    'add_order' => 'Add Order',
    'activity_logs' => 'Activity Logs',
    'logout' => 'Logout',
    'occasions' => 'Occasions',
    'add_occasion' => '+ Add Occasion',
    'user_points' => 'User Points',
    'save' => 'Save',
    'add_new' => 'Add New',
]

?>
