@extends('layouts.dashboard.app')
@section('title')
    @lang('taxonomy::site.sales_gate')
@endsection

@section('content')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">

                <div class="col-12 col-md-3">

                    <example-component :paymentmethods="{{json_encode($paymentMethods)}}"
                                       :accounts='{{json_encode($accounts)}}'
                                       :products="{{json_encode($products)}}"
                                       :employees="{{json_encode($employees)}}"
                                       :cities="{{json_encode($cities)}}"
                                       :gates="{{json_encode($gates)}}"
                                       :gatesids="{{json_encode($gatesIds)}}"
                                       :paymentMethodsids="{{json_encode($paymentMethodsIds)}}"
                                       :csrftoken="{{json_encode(csrf_token())}}"
                                       :lng="{{json_encode(app()->getLocale())}}"
                    ></example-component>
                </div>
                <div class="col-12 col-md-9">

                    <div class="response-message"></div>
                </div>
            </div>
        </div>

    </div>





    <!-- Main content -->



@endsection

@push('js')

<script>
    $(function (e) {

        $('#menu').metisMenu();
        $(".toggle-icon").click(function () {
            if ($(".wrapper").hasClass("toggled")) {
                // unpin sidebar when hovered
                $(".wrapper").removeClass("toggled");
                $(".sidebar-wrapper").unbind("hover");
            } else {
                $(".wrapper").addClass("toggled");
                $(".sidebar-wrapper").hover(function () {
                    $(".wrapper").addClass("sidebar-hovered");
                }, function () {
                    $(".wrapper").removeClass("sidebar-hovered");
                })
            }
        });
        $(".mobile-toggle-menu").on("click", function () {
            $(".wrapper").addClass("toggled");
        });
    })
</script>
    <script src="{{mix('js/app.js')}}"></script>
    <script type="text/javascript">
        function ExportToExcel(){
            var htmltable= document.getElementById('best-customer-count');
            var html = htmltable.outerHTML;
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(html));
        }
    </script>
@endpush



<!-- Main content -->
