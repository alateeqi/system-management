<div class="report-content">
    <h2>{{ $title }}</h2>
    <div class="mb-2">
        <b>Dates:</b>
        {{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
        - {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
    </div>

    <div class="mb-2">
        <b>Account:</b>
        {{ $filter_data['account']['title'] }}
    </div>


    @if($transactionsGrouped->count())

        <div class="card card-primary">
            <div class="card-body">

                <section class="content mb-4" id="chart-account" xmlns:v-on="http://www.w3.org/1999/xhtml">
                    <chart
                        :type="type"
                        :data="chartData"
                    ></chart>
                </section>

            </div>
        </div>
    @else
        <div class="callout callout-danger"><h5>No Results!</h5>
            <p>There is no one order matches selected dates</p></div>
    @endif

</div>

    <script>
        new Vue({
        el: '#chart-account',
        data: {
        type: '{{ $type }}',
        chartData: @JSON($chartData)
    },
    });
</script>
