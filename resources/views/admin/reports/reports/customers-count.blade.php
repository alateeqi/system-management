
<div class="report-content">
	<h2>{{ $title }}</h2>
	<div class="mb-2">
		<b>Dates:</b>
		{{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
		- {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
	</div>
	@if($activeCustomersCount)
		<table cellpadding="5px" style="width: 100%; table-layout: fixed;">
			<tr>
				<td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">All Customers</p>
                                    <h5 class="mb-0">   {{ $newCustomersCount + $activeCustomersCount }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

				</td>
				<td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">New Customers Count</p>
                                    <h5 class="mb-0">     {{ $newCustomersCount }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

				</td>
				<td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Active Customers Count</p>
                                    <h5 class="mb-0">     	{{ $activeCustomersCount }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

				</td>
			</tr>
		</table>
		<table cellpadding="5px" style="width: 100%; table-layout: fixed;">
			<tr>
				<td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">New Customers Have One Order</p>
                                    <h5 class="mb-0">   {{ $newCustomersHaveOneOrder }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

				</td>
				<td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">New Customers Have More Than One Order</p>
                                    <h5 class="mb-0">    {{ $newCustomersHaveMoreThanOneOrder }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

				</td>
			</tr>
		</table>

		<div class="card card-primary">
			<div class="card-body">
                <div class="card card-primary">
                    <div class="card-body">

                        <div class="content mb-4" id="chart-account" xmlns:v-on="http://www.w3.org/1999/xhtml">
                            <chart
                                :type="type"
                                :data="chartData"
                            ></chart>
                        </div>
                    </div>
                </div>
			</div>
		</div>

	@else
		<div class="callout callout-danger"><h5>No Results!</h5>
			<p>There is no one order matches selected dates</p></div>
	@endif
</div>
<script>
    new Vue({
        el: '#chart-account',
        data: {
            type: '{{ $type }}',
            chartData: @JSON($chartData)
        },
    });
</script>
