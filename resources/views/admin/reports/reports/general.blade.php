
    <div class=" report-content">
        <h2>{{ $title }}</h2>
        <div class="mb-2">
            <b>Dates:</b>
            {{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
            - {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
        </div>
        <div class="card card-primary">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered text-right" dir="rtl">
                        <tbody>
                        <tr>
                            <td width="300px">إجمالي المبيعات</td>
                            <td class="text-success">{{ $data['orders']->sum('products_price') }}
                                KWD
                            </td>
                        </tr>
                        <tr>
                            <td>إجمالي مصروفات التوصيل</td>
                            <td>{{ $data['orders']->sum('delivery_price') }} KWD</td>
                        </tr>
                        <tr>
                            <td>إجمالي مصروفات التوصيل - من دون الطلبات الملغات</td>
                            <td>{{ $data['doneOrders']->sum('delivery_price') }} KWD</td>
                        </tr>
                        @foreach($data['doneOrders']->groupBy('payment_method_id') as $items)
                            <tr>
                                <td>إجمالي مبيعات ({{ $items[0]->paymentMethod->title }})</td>
                                <td>{{ $items->sum('products_price') }}
                                    KWD
                                </td>
                            </tr>
                            <tr>
                                <td>إجمالي مصروفات التوصيل ({{ $items[0]->paymentMethod->title }})</td>
                                <td>{{ $items->sum('delivery_price') }} KWD</td>
                            </tr>
                        @endforeach

                        @foreach($data['doneOrders']->groupBy('gate_id') as $items)
                            <tr>
                                <td>إجمالي مبيعات ({{ $items[0]->gate->title }})</td>
                                <td>{{ $items->sum('products_price') }}
                                    KWD
                                </td>
                            </tr>
                            <tr>
                                <td>إجمالي مصروفات التوصيل ({{ $items[0]->gate->title }})</td>
                                <td>{{ $items->sum('delivery_price') }} KWD</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td>مصروفات الإنتاج</td>
                            <td>{{ $data['productionExpenses'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>اجمالي الفواتير الملغاه</td>
                            <td>{{ $data['canceledSales'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>قيمة الفواتير المجانية</td>
                            <td>{{ $data['freeOrders'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>قيمة توصيل الفواتير المجانية</td>
                            <td>{{ $data['freeOrdersDelivery'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>اجمالي خصومات الفواتير</td>
                            <td>{{ $data['discounts'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>الإيجارات</td>
                            <td>{{ $data['rents'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>الرواتب</td>
                            <td>{{ $data['salaries'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>رسوم طلبات الكينت</td>
                            <td>{{ $data['knet'] }} KWD</td>
                        </tr>

                        @foreach($data['tags'] as $tag)

                            <tr>
                                <td>{{ $tag['title'] }}</td>
                                <td>{{ $tag['sum'] }} KWD</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td>اجمالي المصروفات</td>
                            <td class="text-danger font-weight-bold">{{ $data['expenses'] }} KWD</td>
                        </tr>

                        <tr>
                            <td>صافي الايراد القابل للتوزيع</td>
                            @php($profit = $data['orders']->sum('products_price') - $data['expenses'])
                            <td class="text-success font-weight-bold">{{ $profit }} KWD</td>
                        </tr>

                        <tr>
                            <td class="font-weight-bold text-center" colspan="2">توزيع الأرباح الشهرية</td>
                        </tr>
                        @php($amountSaved = $profit / 5)

                        <tr>
                            <td>السيد طلال 50%</td>
                            <td class="text-success font-weight-bold">{{ ($profit - $amountSaved) / 2 }} KWD</td>
                        </tr>

                        <tr>
                            <td>الآنسة نوره 50%</td>
                            <td class="text-success font-weight-bold">{{ ($profit - $amountSaved) / 2 }} KWD</td>
                        </tr>

                        <tr>
                            <td>Amount saved 20%</td>
                            <td class="text-success font-weight-bold">{{ $amountSaved }} KWD</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

