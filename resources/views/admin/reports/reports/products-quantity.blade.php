<!-- Main content -->
<div class="report-content">
	<h2>{{ $title }}</h2>
	<div class="mb-2">
		<b>Dates:</b>
		{{ Carbon\Carbon::parse($filter_data['startDate'])->format('Y-m-d') }}
		- {{ Carbon\Carbon::parse($filter_data['endDate'])->format('Y-m-d') }}
	</div>
	<div class="mb-2">
		<b>Statuses:</b>
		@foreach($filter_data['statuses'] as $status)
			<span class="badge {{ \Modules\Order\Entities\Order::STATUSES[$status]['class'] }}">{{ \Modules\Order\Entities\Order::STATUSES[$status]['name'] }}</span>
		@endforeach
	</div>
	<div class="mb-2">
		<b>Employees:</b>
		@if(!count($filter_data['employees']))
			All employees
		@else
			@foreach($filter_data['employees'] as $employee)
				<span class="badge bg-secondary">{{ $employee['name'] }}</span>
			@endforeach
		@endif
	</div>

	@if($orderProduct->count())
        <table cellpadding="5px" style="width: 100%; table-layout: fixed;">
            <tr>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Filtred Orders Count</p>
                                    <h5 class="mb-0"> {{ $statusesCount['total'] }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-trending-up font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">All Orders Count</p>
                                    <h5 class="mb-0"> {{ $totalCount }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-archive font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Percentage</p>
                                    <h5 class="mb-0">   {{ number_format($statusesCount['total'] * 100 / $totalCount, 2) }}
                                        %</h5>
                                </div>
                                <div class="ms-auto"><i class=" font-50"> %</i>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

                </td>
            </tr>
            <tr>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Filtred Orders Sales</p>
                                    <h5 class="mb-0">    {{ round($statusesCount['totalSales']) }} KWD</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-data font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

                    <!-- /.info-box -->
                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">All Orders Sales</p>
                                    <h5 class="mb-0">{{ round($totalSum) }} KWD</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-dollar font-50"></i>
                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Percentage</p>
                                    <h5 class="mb-0">         {{ number_format($statusesCount['totalSales'] * 100 / $totalSum, 2) }}
                                        %</h5>
                                </div>
                                <div class="ms-auto"><i class=" font-50"> %</i>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>


                </td>
            </tr>
        </table>
        <table cellpadding="5px" style="width: 100%; table-layout: fixed;">
            <tr>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">New & Updated</p>
                                    <h5 class="mb-0">        {{ $statusesCount['others'] }}</h5>
                                </div>
                                <div class="ms-auto"><i class=" font-50"> R</i>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Delivered</p>
                                    <h5 class="mb-0">   {{ $statusesCount['delivered'] }}</h5>
                                </div>
                                <div class="ms-auto">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-truck ">
                                        <rect x="1" y="3" width="15" height="13"></rect>
                                        <polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon>
                                        <circle cx="5.5" cy="18.5" r="2.5"></circle>
                                        <circle cx="18.5" cy="18.5" r="2.5"></circle>
                                    </svg>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Proved</p>
                                    <h5 class="mb-0"> {{ $statusesCount['proved'] }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-check font-50"></i>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>

                </td>
                <td>
                    <div class="card radius-10 overflow-hidden">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <p class="mb-0  font-18">Canceled</p>
                                    <h5 class="mb-0"> {{ $statusesCount['canceled'] }}</h5>
                                </div>
                                <div class="ms-auto"><i class="bx bx-x font-50"></i>

                                </div>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                </td>
            </tr>
        </table>
		<div class="card card-primary">
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Product</th>
							<th>Quantity</th>
						</tr>
						</thead>
						<tbody>
						@foreach($orderProduct->groupBy('product_id')->sortByDesc(function($product){ return $product->count(); }) as $productOrders)
							@foreach($productOrders->groupBy('price_id') as $productPrice)
								<tr>
									<td>{{ $productOrders->first()->product->title }}</td>
									<td>{{ $productPrice->sum('quantity') }} x {{ $productPrice->first()->priceList->unite_name }}</td>
								</tr>
							@endforeach
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@else
		<div class="callout callout-danger"><h5>No Results!</h5>
			<p>There is no one order matches selected dates</p></div>
	@endif
</div>


