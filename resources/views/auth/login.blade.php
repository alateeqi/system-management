<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
<link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
<link href="{{URL::asset('assets/css/app.css')}}" rel="stylesheet">

<div class="authentication-header"></div>
<div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-0">
    <div class="container-fluid">
        <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
            <div class="col mx-auto">
                <div class="mb-4 text-center">
                    {{-- logo --}}
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="p-4 rounded">
                            <div class="text-center">
                                <h3 class="">Sign in</h3>
                            </div>

                            <div class="login-separater text-center mb-4"><span>{{ __('login')}}</span>
                                <hr/>
                            </div>
                            <div class="form-body">
                                <form class="row g-3" method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="col-12">
                                        <label for="email" class="form-label">{{ __('E-Mail Address')}}</label>

                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}" required autocomplete="email"
                                               autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                        @enderror
                                    </div>

                                    <div class="col-12">
                                        <label for="password" class="form-label">Enter Password</label>
                                        <div class="input-group" id="show_hide_password">
                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password" required autocomplete="current-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>
                                    </div>
                                    {{--                                        <div class="col-md-6">--}}
                                    {{--                                            <div class="form-check form-switch">--}}
                                    {{--                                                <input class="form-check-input" type="checkbox"--}}
                                    {{--                                                       id="flexSwitchCheckChecked" checked>--}}
                                    {{--                                                <label class="form-check-label"--}}
                                    {{--                                                       for="flexSwitchCheckChecked">{{ __('Remember Me') }}</label>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    											<div class="col-md-6 text-end">	<a href="authentication-forgot-password.html">Forgot Password ?</a>--}}
                                    {{--                                </div>--}}
                                    <div class="col-12">
                                        <div class="d-grid">
                                            <button type="submit" class="btn btn-primary"><i
                                                    class="bx bxs-lock-open"></i>{{ __('تسجيل الدخول') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
