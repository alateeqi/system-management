@extends('layouts.dashboard.app')

@section('title')
    Dashboard Bakednco
@endsection

@push('css')
<style>
    b{
        font-size: 16px;
    }
</style>
@endpush
@section('content')
    <!--start page wrapper -->
    <div class="page-wrapper">
        <div class="page-content">

            <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">

                <div class="col font-11">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1">
                                    <b> <p class="mb-0">@lang('site.today_orders')</p></b>
                                    <hr />
                                    <h6 class="font-weight-bold">{{ $statistics['countOfOrdersToday'] }}
                                        {{-- <small class="text-success font-13">(+40%)</small> --}}
                                    </h6>
                                    {{-- <p class="text-success mb-0 font-13">Analytics for today</p> --}}
                                </div>
                                <div class="widgets-icons bg-gradient-kyoto text-white"><i class='bx bxs-cube'></i>
                                </div>

                            </div>
                            {{-- <div class="" id="chart4"></div> --}}
                        </div>
                    </div>
                </div>
                <div class="col font-11">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1">
                                    <b><p class="mb-0">@lang('site.today_website_order')</p></b>
                                    <hr />
                                    {{-- <small class="text-success font-13">(+40%)</small> --}}
                                    <h6 class="font-weight-bold">{{ $statistics['countOfWebsiteOrderCount'] }}
                                    </h6>
                                    {{-- <p class="text-success mb-0 font-13">Analytics for today</p> --}}
                                </div>
                                <div class="widgets-icons bg-gradient-cosmic text-white"><i class='bx bx-cart'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col font-11">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1">
                                    <b><p class="mb-0">@lang('site.sales_today')</p></b>
                                    <hr />
                                    <h6 class="font-weight-bold">{{ $statistics['todaySales'] }} KWD
                                        {{-- <small class="text-success font-13">(+40%)</small> --}}
                                    </h6>
                                    {{-- <p class="text-success mb-0 font-13">Analytics for today</p> --}}
                                </div>
                                <div class="widgets-icons bg-gradient-moonlit text-white"><i class='bx bx-wallet'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col font-11">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1">
                                    <b><p class="mb-0">@lang('site.New Customer')</p></b>
                                    <hr />
                                    <h6 class="font-weight-bold">{{ count($statistics['customerToday']) }}
                                        {{-- <small class="text-success font-13">(+40%)</small> --}}
                                    </h6>
                                    {{-- <p class="text-success mb-0 font-13">Analytics for today</p> --}}
                                </div>
                                <div class="widgets-icons bg-gradient-burning text-white"><i class='bx bx-group'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col font-11">
                    <div class="card radius-10">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div class="flex-grow-1">
                                    <b><p class="mb-0">@lang('site.monthly_sales')</p></b>
                                    <hr />

                                    <h6 class="font-weight-bold">{{ $statistics['monthlySales'] }} KWD
                                        {{-- <small class="text-success font-13">(+40%)</small> --}}
                                    </h6>
                                    {{-- <p class="text-success mb-0 font-13">Analytics for today</p> --}}
                                </div>
                                <div class="widgets-icons bg-gradient-lush text-white"><i class='bx bx-wallet'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--end row-->


            <div class="row">
                <div class="col-12  d-flex">
                    <div class="card radius-10 w-100">
                        <div class="card-body">
                            <div class="" id="sales-last-7-days"></div>
                        </div>
                    </div>
                </div>

            </div>
            <!--end row-->


            {{-- {{dd($statistics['dates_last_7_days_orders'])}} --}}


            <div class="row">
                {{-- <div class="col-12 col-xl-4 d-flex"> --}}
                {{-- <div class="card radius-10 w-100"> --}}
                {{-- <div class="card-body"> --}}
                {{-- <div class="d-flex align-items-center"> --}}
                {{-- <div> --}}
                {{-- <h5 class="mb-0">New Customers</h5> --}}
                {{-- </div> --}}
                {{-- <div class="font-22 ms-auto"><i class='bx bx-dots-horizontal-rounded'></i> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list p-3 mb-3"> --}}
                {{-- <div --}}
                {{-- class="customers-list-item d-flex align-items-center border-top border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-1.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Emy Jackson</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">emy_jac@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-2.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Martin Hughes</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">martin.hug@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="assets/images/avatars/avatar-3.png" class="rounded-circle" width="46" --}}
                {{-- height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Laura Madison</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">laura_01@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-4.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Shoan Stephen</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">s.stephen@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-5.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Keate Medona</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">Keate@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-6.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Paul Benn</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">pauly.b@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-7.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Winslet Maya</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">winslet_02@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="assets/images/avatars/avatar-8.png" class="rounded-circle" width="46" --}}
                {{-- height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Bruno Bernard</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">bruno.b@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-9.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Merlyn Dona</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">merlyn.d@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i --}}
                {{-- class='bx bx-dots-vertical-rounded'></i></a> --}}
                {{-- </div> --}}
                {{-- </div> --}}
                {{-- <div class="customers-list-item d-flex align-items-center border-bottom p-2 cursor-pointer"> --}}
                {{-- <div class=""> --}}
                {{-- <img src="{{URL::asset('assets/images/avatars/avatar-10.png')}}" --}}
                {{-- class="rounded-circle" width="46" height="46" alt=""/> --}}
                {{-- </div> --}}
                {{-- <div class="ms-2"> --}}
                {{-- <h6 class="mb-1 font-14">Alister Campel</h6> --}}
                {{-- <p class="mb-0 font-13 text-secondary">alister_42@xyz.com</p> --}}
                {{-- </div> --}}
                {{-- <div class="list-inline d-flex customers-contacts ms-auto"><a href="javascript:;" --}}
                {{-- class="list-inline-item"><i --}}
                {{-- class='bx bxs-envelope'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i class='bx bxs-phone'></i></a> --}}
                {{-- <a href="javascript:;" class="list-inline-item"><i
                    class='bx bx-dots-vertical-rounded'></i></a>
                </div>
            </div>
        </div>
    </div>
{{-- </div> --}}


                <div class="col-12 col-xl-4 d-flex">
                    <div class="card radius-10 w-100 ">
                        <div class="card-body">
                            <div class="d-flex align-items-center">
                                <div>
                                    <h6 class="mb-1">@lang('site.best_sales_products_for_last_7_days')</h6>
                                </div>
                                <div class="font-22 ms-auto"><i class="bx bx-dots-horizontal-rounded"></i>
                                </div>
                            </div>
                        </div>

                        <div class="product-list p-3 mb-3">
                            @foreach ($statistics['bestProducts'] as $product)
                                <a href="{{ route('products.show', $product->first()[0]->product_id) }}">
                                    <div class="d-flex align-items-center py-3 border-bottom cursor-pointer">



                                        <div class="product-img me-2">
                                            <img src="{{ $product->first()[0]->product->image }}" alt="product img">
                                        </div>

                                        <div class="">
                                            <h6 class="mb-0 font-14">{{ $product->first()[0]->product->title }}</h6>
                                            <p class="mb-0">
                                                {{ $product->count(function ($item) {
                                                    return $item[0]->order;
                                                }) }}
                                                Sales</p>
                                        </div>
                                        <div class="ms-auto">
                                            <h6 class="mb-0">
                                                {{ $product->sum(function ($item) {
                                                    return $item[0]->order->total_price - ($item[0]->order->delivery_price + $item[0]->order->discount);
                                                }) }}
                                                KWD</h6>
                                        </div>

                                    </div>
                                </a>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <!--end row-->


        </div>
    </div>
    <!--end page wrapper -->
    <!--start overlay-->
    <div class="overlay toggle-icon"></div>
    <!--end overlay-->
    <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
    <!--End Back To Top Button-->
    @include('layouts.dashboard.footer')
@endsection
@push('js')
    <script>
        Highcharts.chart('sales-last-7-days', {
            chart: {
                type: 'area',
                height: 350,
                styledMode: true
            },
            credits: {
                enabled: false
            },
            accessibility: {
                //description: 'Image description: An area chart compares the nuclear stockpiles of the USA and the USSR/Russia between 1945 and 2017. The number of nuclear weapons is plotted on the Y-axis and the years on the X-axis. The chart is interactive, and the year-on-year stockpile levels can be traced for each country. The US has a stockpile of 6 nuclear weapons at the dawn of the nuclear age in 1945. This number has gradually increased to 369 by 1950 when the USSR enters the arms race with 6 weapons. At this point, the US starts to rapidly build its stockpile culminating in 32,040 warheads by 1966 compared to the USSR’s 7,089. From this peak in 1966, the US stockpile gradually decreases as the USSR’s stockpile expands. By 1978 the USSR has closed the nuclear gap at 25,393. The USSR stockpile continues to grow until it reaches a peak of 45,000 in 1986 compared to the US arsenal of 24,401. From 1986, the nuclear stockpiles of both countries start to fall. By 2000, the numbers have fallen to 10,577 and 21,000 for the US and Russia, respectively. The decreases continue until 2017 at which point the US holds 4,018 weapons compared to Russia’s 4,500.'
            },
            title: {
                text: 'Sales For Last 7 Days'
            },

            xAxis: {
                allowDecimals: false,
                type: 'date',
                labels: {
                    formatter: function() {
                        var index = this.value
                        return @json($statistics['dates_last_7_days_orders'])[index]; // clean, unformatted number for year
                    }
                },
                accessibility: {
                    rangeDescription: 'Range: 1940 to 2017.'
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function() {
                        return this.value + 'KWD';
                    }
                }
            },
            tooltip: {
                pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
            },
            // plotOptions: {
            //     area: {
            //         pointStart:1940 ,
            //         marker: {
            //             enabled: false,
            //             symbol: 'circle',
            //             radius: 2,
            //             states: {
            //                 hover: {
            //                     enabled: true
            //                 }
            //             }
            //         }
            //     }
            // },
            series: [{
                    name: 'Sales',
                    data: @json($statistics['total_last_7_days_orders'])
                }
                // , {
                //     name: 'Traffic',
                //     data: [null, null, null, null, null, null, null, null, null, null,
                //         5, 25, 50, 120, 150, 200, 426, 660, 869, 1060,
                //         1605, 2471, 3322, 4238, 5221, 6129, 7089, 8339, 9399, 10538,
                //         11643, 13092, 14478, 15915, 17385, 19055, 21205, 23044, 25393, 27935,
                //         30062, 32049, 33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000,
                //         37000, 35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                //         21000, 20000, 19000, 18000, 18000, 17000, 16000, 15537, 14162, 12787,
                //         12600, 11400, 5500, 4512, 4502, 4502, 4500, 4500
                //     ]
                // }
            ]
        });
    </script>
@endpush
