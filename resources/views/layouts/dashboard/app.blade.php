<!DOCTYPE html>
<html dir="{{LaravelLocalization::getCurrentLocaleDirection()}}">
<head>
    <title> @yield('title','bakednco') </title>
    <meta charset="UTF-8 ">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
{{--    <meta name="Description" content="Bootstrap Responsive Admin Web Dashboard HTML5 Template">--}}
{{--    <meta name="Author" content="Spruko Technologies Private Limited">--}}
{{--    <meta name="Keywords"content="admin,admin dashboard,admin dashboard template,admin panel template,admin template,admin theme,bootstrap 4 admin template,bootstrap 4 dashboard,bootstrap admin,bootstrap admin dashboard,bootstrap admin panel,bootstrap admin template,bootstrap admin theme,bootstrap dashboard,bootstrap form template,bootstrap panel,bootstrap ui kit,dashboard bootstrap 4,dashboard design,dashboard html,dashboard template,dashboard ui kit,envato templates,flat ui,html,html and css templates,html dashboard template,html5,jquery html,premium,premium quality,sidebar bootstrap 4,template admin bootstrap 4"/>--}}

    <link href="{{ URL::asset('assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css')}}" rel="stylesheet"/>

    {{-- Internal  Notify css --}}
		<link href="{{ URL::asset('assets/plugins/notifications/css/notifIt.css') }}" rel="stylesheet" />
		<link href="{{ URL::asset('assets/plugins/Drag-And-Drop/dist/imageuploadify.min.css')}}" rel="stylesheet" />



		<!--Internal  Notify js -->
    <script src="{{ URL::asset('assets/plugins/notifications/js/notifIt.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/notifications/js/notifit-custom.js') }}"></script>



    @include('layouts.dashboard.head')


</head>

<body class="main-body app sidebar-mini">

<!-- main-content -->
<div class="wrapper" id="app">
    <div class="container mt-5">
        <div class="row">
            {{-- <div class="col-3"></div> --}}
            <div class="col-9">
                @include('partials._errors')
            </div>
        </div>
    </div>



    @include('partials._session')
    @include('layouts.dashboard.main-header')
{{--    @include('layouts.dashboard.main-sidebar',['url'=>str_replace('http','https',request()->url())])--}}
    @include('layouts.dashboard.main-sidebar',['url'=>str_replace('http','http',request()->url())])

    @yield('content')
    @include('layouts.dashboard.sidebar')
    @include('layouts.dashboard.footer')

</div>
<div id="loading-show" >
    <div id="text-overlay" style="display:none;">
        ... جاري التحميل
    </div>
</div>
<a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
@include('layouts.dashboard.footer-scripts')
</body>



</html>
