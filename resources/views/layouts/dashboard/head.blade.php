@if(app()->getLocale() == 'ar')

    <!--favicon-->
    <link rel="icon" href="{{URL::asset('assetsrtl/images/favicon-32x32.png')}}" type="image/png"/>
    <!--plugins-->
    <link href="{{URL::asset('assetsrtl/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/highcharts/css/highcharts.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet"/>
    <!-- loader-->
    {{--    <link href="{{URL::asset('assetsrtl/css/pace.min.css')}}" rel="stylesheet"/>--}}
    {{--    <script src="{{URL::asset('assetsrtl/js/pace.min.js')}}"></script>--}}
    <!-- Bootstrap CSS -->
    <link href="{{URL::asset('assetsrtl/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assetsrtl/css/bootstrap-extended.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assetsrtl/css/app.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assetsrtl/css/icons.css')}}" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="{{URL::asset('assetsrtl/css/dark-theme.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('assetsrtl/css/semi-dark.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('assetsrtl/css/header-colors.css')}}"/>
    <link href="{{URL::asset('assetsrtl/plugins/datatable/css/dataTables.bootstrap5.min.css')}}" rel="stylesheet"/>
    {{--  --}}
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{URL::asset('assetsrtl/plugins/datetimepicker/css/classic.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/datetimepicker/css/classic.time.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assetsrtl/plugins/datetimepicker/css/classic.date.css')}}" rel="stylesheet"/>
    <link rel="stylesheet"
          href="{{URL::asset('assetsrtl/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css')}}">

    {{-- form WIZARD --}}
    <link href="{{URL::asset('assetsrtl/plugins/smart-wizard/css/smart_wizard_all.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="  {{URL::asset('assetsrtl/plugins/select2/css/select2.min.css')}}" rel="stylesheet"/>
    <link href=" {{URL::asset('assetsrtl/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet"/>
    <!--plugins-->
    <link
        href="{{URL::asset('assetsrtl/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css')}}"
        rel="stylesheet"/>


@else

    <!--favicon-->
    <link rel="icon" href="{{URL::asset('assets/images/favicon-32x32.png')}}" type="image/png"/>
    <!--plugins-->
    <link href="{{URL::asset('assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/highcharts/css/highcharts.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet"/>
    <!-- loader-->
    {{--    <link href="{{URL::asset('assets/css/pace.min.css')}}" rel="stylesheet"/>--}}
    {{--    <script src="{{URL::asset('assets/js/pace.min.js')}}"></script>--}}
    <!-- Bootstrap CSS -->
    <link href="{{URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/bootstrap-extended.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="{{URL::asset('assets/css/app.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/css/icons.css')}}" rel="stylesheet">
    <!-- Theme Style CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/css/dark-theme.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('assets/css/semi-dark.css')}}"/>
    <link rel="stylesheet" href="{{URL::asset('assets/css/header-colors.css')}}"/>
    <link href="{{URL::asset('assets/plugins/datatable/css/dataTables.bootstrap5.min.css')}}" rel="stylesheet"/>
    {{--  --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{URL::asset('assets/plugins/datetimepicker/css/classic.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/datetimepicker/css/classic.time.css')}}" rel="stylesheet"/>
    <link href="{{URL::asset('assets/plugins/datetimepicker/css/classic.date.css')}}" rel="stylesheet"/>
    <link rel="stylesheet"
          href="{{URL::asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css')}}">
    {{-- form WIZARD --}}
    <link href="{{URL::asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="  {{URL::asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet"/>
    <link href=" {{URL::asset('assets/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet"/>
    <link
        href="{{URL::asset('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.min.css')}}"
        rel="stylesheet"/>
    {{-- form WIZARD --}}
    <link href="{{URL::asset('assets/plugins/smart-wizard/css/smart_wizard_all.min.css')}}" rel="stylesheet"
          type="text/css"/>









@endif

<link rel="stylesheet" href="{{URL::asset('assets/plugins/notifications/css/lobibox.min.css')}}"/>
<link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    .inputGroup {
        border-radius: 10px;
        background-color: #f0f0f0;
        display: block;
        margin: 10px 5px;
        position: relative;
        padding-left: 0px;
        padding-right: 0px;

    }

    .inputGroup input {
        left: auto !important;
        opacity: 1 !important;
        width: 32px;
        height: 32px;
        -webkit-box-ordinal-group: 2;
        order: 1;
        z-index: 2;
        position: absolute;
        right: 10px;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        cursor: pointer;
        visibility: hidden;
    }

    .inputGroup label {
        border-radius: 10px;
        height: auto !important;
        line-height: inherit !important;
        font-weight: inherit !important;
        padding: 12px 20px;
        width: 100%;
        display: block;
        text-align: left;
        color: #3c454c;
        cursor: pointer;
        position: relative;
        z-index: 2;
        -webkit-transition: color 200ms ease-in;
        transition: color 200ms ease-in;
        overflow: hidden;
    }

    .inputGroup label:before {
        width: 10px;
        height: 10px;
        border-radius: 50%;
        content: "";
        background-color: #1976d2;
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%) scale3d(1, 1, 1);
        transform: translate(-50%, -50%) scale3d(1, 1, 1);
        -webkit-transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);
        transition: all 300ms cubic-bezier(0.4, 0, 0.2, 1);
        opacity: 0;
        z-index: -1;
    }


    .inputGroup label:after {
        width: 32px;
        height: 32px;
        content: "";
        border: 2px solid transparent;
        background-color: #fff;
        background-image: url("data:image/svg+xml,%3Csvg width='32' height='32' viewBox='0 0 32 32' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M5.414 11L4 12.414l5.414 5.414L20.828 6.414 19.414 5l-10 10z' fill='%23fff' fill-rule='nonzero'/%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: 2px 3px;
        border-radius: 50%;
        z-index: 2;
        position: absolute;
        right: 10px;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        cursor: pointer;
        -webkit-transition: all 200ms ease-in;
        transition: all 200ms ease-in;
    }

    .inputGroup input:checked ~ label {
        color: #fff;
    }

    .inputGroup input:checked ~ label:before {
        -webkit-transform: translate(-50%, -50%) scale3d(56, 56, 1);
        transform: translate(-50%, -50%) scale3d(56, 56, 1);
        opacity: 1;
    }

    .inputGroup input:checked ~ label:after {
        background-color: #54e0c7;
        border-color: #54e0c7;
    }

    .inputGroup .checked label:before {

        border-radius: 15%;

    }

    .inputGroup .checked label:after {
        border-radius: 15%;
    }

</style>
@stack('css')

