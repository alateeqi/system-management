<!--start header -->
<header>
    <div class="topbar d-flex align-items-center">
        <nav class="navbar navbar-expand">
            <div class="mobile-toggle-menu"><i class='bx bx-menu'></i>
            </div>
            <div class="top-menu-left d-none d-lg-block">

            </div>
            <div class="search-bar flex-grow-1">

            </div>
            <div class="top-menu ms-auto">
                <ul class="navbar-nav align-items-center">
                    <li class="nav-item bell-li dropdown dropdown-large" @click="stopBeep(); bell = false ">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="{{route('admin.orders.website')}}">

                           @if ($unreadWebsiteOrders>0)
                                <span class="alert-count">{{$unreadWebsiteOrders}}</span>
                           @endif
                            <i class='bx bx-bell'></i>
                        </a>

                    </li>


                    <li class="nav-item dropdown dropdown-large">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret position-relative" href="#"
                           role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="lni lni-world"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end w-25">
                            <div class="flex-grow-1">
                                @foreach (LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"
                                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </li>


                </ul>
            </div>
            <div class="user-box dropdown">
                <a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret" href="#"
                   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="{{ URL::asset('assets/images/logo-icon.png') }}" class="user-img"
                         alt="user avatar">
                    <div class="user-info ps-3">
                        <p class="user-name mb-0">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                        </p>
                        {{-- <p class="designattion mb-0">Web Designer</p> --}}
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-menu-end">
{{--                    <li><a class="dropdown-item" href="javascript:;"><i--}}
{{--                                class="bx bx-user"></i><span>Profile</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="dropdown-item" href="javascript:;"><i--}}
{{--                                class="bx bx-cog"></i><span>Settings</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="dropdown-item" href="javascript:;"><i--}}
{{--                                class='bx bx-home-circle'></i><span>Dashboard</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="dropdown-item" href="javascript:;"><i--}}
{{--                                class='bx bx-dollar-circle'></i><span>Earnings</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a class="dropdown-item" href="javascript:;"><i--}}
{{--                                class='bx bx-download'></i><span>Downloads</span></a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <div class="dropdown-divider mb-0"></div>--}}
{{--                    </li>--}}
                    <li><a class="dropdown-item" href="{{ route('logout') }} "
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                class='bx bx-log-out-circle'></i><span>Logout</span></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!--end header -->
