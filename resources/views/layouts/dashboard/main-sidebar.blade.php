<!--sidebar wrapper -->


<style>
    .sidebar-wrapper a {
        font-size: 20px !important;
    }
</style>

<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{URL::asset('assets/images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Baked&CO</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-first-page'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu " id="menu">
        <li class="">
            <a calss href="{{route('home')}}">
                <div class="parent-icon"><i class='bx bx-home'></i></div>
                <div class="menu-title ">{{trans('taxonomy::site.dashboard')}}</div>
            </a>
        </li>

        @if (auth()->user()->hasPermission('products-read'))
            <li class=" {{$url ==route('products.index')||$url==route('products.create')?'mm-active':''}}">
                <a href="{{ route('products.index')}}">
                    <div class="parent-icon"><i class="fadeIn animated bx bx-store-alt"></i></div>
                    <div class="menu-title">@lang('taxonomy::site.products')</div>
                </a>
            </li>
        @endif

        @if (auth()->user()->hasPermission('orders-read'))

            <li class=" {{$url ==route('admin.orders.index')||$url==route('admin.orders.create')?'mm-active':''}}">
                <a href="{{ route('admin.orders.index')}}">
                    <div class="parent-icon"><i class="bx bx-package "></i></div>
                    <div class="menu-title">@lang('site.orders')</div>
                </a>
            </li>


        @endif
        @if (auth()->user()->hasPermission('orders-create'))

            <li class=" {{$url ==route('admin.orders.create')||$url==route('admin.orders.create')?'mm-active':''}}">
                <a href="{{ route('admin.orders.create')}}">
                    <div class="parent-icon"><i class="bx bxs-plus-square"></i></div>
                    <div class="menu-title"> @lang('site.add_order')</div>
                </a>
            </li>

        @endif


        <li class=" {{$url ==route('admin.coupons.index')||$url==route('admin.coupons.create')||$url==route('admin.gifts.index')||$url==route('admin.gifts-winners.index')?'mm-active':''}}">
            @if (auth()->user()->hasPermission('coupons-read'))
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bxs-coupon "></i>
                    </div>
                    <div class="menu-title">
                        @lang('site.coupons_gifts')
                    </div>
                </a>
            @endif
            <ul>
                @if (auth()->user()->hasPermission('coupons-read'))

                    <li class="{{$url ==route('admin.coupons.index')?'mm-active':''}}"><a
                            href="{{ route('admin.coupons.index')}}">
                            <i class="bx bxs-discount "></i> @lang('site.coupons')
                        </a>
                    </li>
                @endif
                @if (auth()->user()->hasPermission('coupons-create'))

                    <li class="{{$url ==route('admin.coupons.create')?'mm-active':''}}"><a
                            href="{{ route('admin.coupons.create')}}">
                            @lang('site.add_coupon')</a></li>
                @endif
                @if (auth()->user()->hasPermission('coupons-create'))

                    <li class="{{$url ==route('admin.gifts.index')?'mm-active':''}}"><a
                            href="{{ route('admin.gifts.index')}}">
                            <i class="bx bxs-gift "></i> @lang('site.gifts')</a></li>
                    <li class="{{$url ==route('admin.gifts-winners.index')?'mm-active':''}}"><a
                            href="{{ route('admin.gifts-winners.index')}}">
                            <i class="bx bxs-group "></i> @lang('site.gift_winners')</a></li>
                @endif


            </ul>
        </li>


        @if (auth()->user()->hasPermission('reports-read'))
            <li class=" {{$url ==route('reports.index')?'mm-active':''}}">
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bx-chart "></i>
                    </div>
                    <div class="menu-title">
                        @lang('site.reports')
                    </div>
                </a>
                <ul>
                    <li class=" {{$url ==route('reports.index')?'mm-active':''}}"><a href="{{ route('reports.index')}}"><i
                                class="lni lni-pie-chart"></i>
                            @lang('site.reports')</a></li>
                    <li class=" {{$url ==route('admin.export.index')?'mm-active':''}}"><a
                            href="{{ route('admin.export.index')}}"><i
                                class="bx bxs-file-export"></i>
                            @lang('site.export_excel')</a></li>

                </ul>
            </li>
        @endif
        @if (auth()->user()->hasPermission('recipes-read'))
            <li class="{{$url ==route('recipes.index')?'mm-active':''}}"><a href="{{ route('recipes.index')}}">
                    <div class="parent-icon"><i class="lni lni-service"></i></div>
                    <div class="menu-title">@lang('recipes::site.recipes')</div>
                </a></li>
        @endif
        @if (auth()->user()->hasPermission('users-read'))
            <li class=" {{$url ==route('customers.index')?'mm-active':''}}">
                <a href="{{ route('customers.index')}}">
                    <div class="parent-icon"><i class="fadeIn animated lni lni-users"></i></div>
                    <div class="menu-title">@lang('customer::site.customers')</div>
                </a>
            </li>
            <li class=" {{$url ==route('users.index')?'mm-active':''}}">
                <a href="{{ route('users.index')}}">
                    <div class="parent-icon"><i class="fadeIn animated bx bx-briefcase"></i></div>
                    <div class="menu-title">@lang('user::site.users')</div>
                </a>
            </li>

            <li class=" {{$url ==route('admin.points.index')||$url==route('admin.points.create')||$url==route('admin.points.users')?'mm-active':''}}">

                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bx-target-lock "></i>
                    </div>
                    <div class="menu-title">
                        @lang('site.targets_points')
                    </div>
                </a>

                <ul>


                    <li class="{{$url ==route('admin.points.index')?'mm-active':''}}"><a
                            href="{{ route('admin.points.index')}}">
                            <i class="bx bx-target-lock "></i> @lang('site.occasions')
                        </a>
                    </li>


                    <li class="{{$url ==route('admin.points.create')?'mm-active':''}}"><a
                            href="{{ route('admin.points.create')}}">
                            @lang('site.add_occasion')</a></li>

                    <li class="{{$url ==route('admin.points.users')?'mm-active':''}}"><a
                            href="{{ route('admin.points.users')}}">
                            <i class="bx bxs-group "></i> @lang('site.user_points')</a></li>


                </ul>
            </li>



        @endif


        @if (auth()->user()->hasPermission('reports-read'))


            <li class=" {{$url ==route('logs.index')?'mm-active':''}}">
                <a href="{{ route('logs.index')}}">
                    <div class="parent-icon"><i class="fadeIn animated bx bx-run"></i></div>
                    <div class="menu-title">@lang('site.activity_logs')</div>
                </a>
            </li>
        @endif
        @if (auth()->user()->hasPermission('website-read'))
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="lni lni-world"></i>
                    </div>
                    <div class="menu-title">@lang('taxonomy::site.website')</div>
                </a>
                <ul>

                    <li class="{{$url ==route('pages.index')?'mm-active':''}}"><a href="{{ route('pages.index')}}"><i
                                class="lni lni-remove-file"></i>@lang('taxonomy::site.pages')
                        </a></li>

                    <li class="{{$url ==route('sliders.index')?'mm-active':''}}"><a
                            href="{{ route('sliders.index')}}"><i
                                class="lni lni-image"></i>@lang('taxonomy::site.sliders')
                        </a></li>

                    <li class="{{$url ==route('contacts.index')?'mm-active':''}}"><a
                            href="{{ route('contacts.index')}}"><i
                                class="lni lni-agenda"></i>@lang('taxonomy::site.Contact_Information')</a></li>

                    <li class="{{$url ==route('translations.index')?'mm-active':''}}"><a
                            href="{{ route('translations.index')}}"><i
                                class="lni lni-italic"></i>@lang('taxonomy::site.websitetranslations')</a></li>

                    <li class="{{$url ==route('metas.index')?'mm-active':''}}"><a href="{{ route('metas.index')}}"><i
                                class="lni lni-target"></i>@lang('taxonomy::site.metas')</a></li>
                </ul>
            </li>
        @endif
        @if (auth()->user()->hasPermission('settings-read'))
            <li>
                <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class="bx bx-cog bx-spin"></i>
                    </div>
                    <div class="menu-title">@lang('taxonomy::site.Settings')</div>
                </a>

                <ul>

                    @if (auth()->user()->hasPermission('settings-read'))

                        <li class="{{$url ==route('products.setting')?'mm-active':''}}"><a
                                href="{{ route('products.setting')}}"><i
                                    class="lni lni-producthunt"></i>
                                Product Setting </a></li>
                        <li class="{{$url ==route('categories.index')?'mm-active':''}}"><a
                                href="{{ route('categories.index')}}"><i
                                    class="lni lni-grid-alt"></i>@lang('taxonomy::site.categories')</a></li>

                        <li class="{{$url ==route('cities.index')?'mm-active':''}}"><a
                                href="{{ route('cities.index')}}"><i
                                    class="lni lni-apartment"></i>@lang('taxonomy::site.cities')</a></li>

                        <li class="{{$url ==route('payment.index')?'mm-active':''}}"><a
                                href="{{ route('payment.index')}}"><i
                                    class="lni lni-credit-cards"></i>@lang('taxonomy::site.Payment_Methods')</a></li>

                        <li class="{{$url ==route('gates.index')?'mm-active':''}}"><a
                                href="{{ route('gates.index')}}"><i
                                    class="lni lni-cloud-network"></i>@lang('taxonomy::site.sales_gate')</a></li>

                        <li class="{{$url ==route('options.index')?'mm-active':''}}"><a
                                href="{{ route('options.index')}}"><i
                                    class="fadeIn animated bx bx-columns"></i>@lang('taxonomy::site.advanced_options')
                            </a></li>



                        {{-- <li class="{{$url ==route('transactiontypes.index')?'mm-active':''}}"><a
                                href="{{ route('transactiontypes.index')}}"><i
                                    class="lni lni-menu"></i>@lang('taxonomy::site.transactiontypes')</a></li> --}}


                    @endif
                </ul>

            </li>


        @endif
        <li class=" {{$url ==route('logout')?'mm-active':''}}">
            <a href="{{ route('logout')}}"
            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <div class="parent-icon"><i
                        class='bx bx-log-out-circle'></i></div>
                <div class="menu-title">@lang('site.logout')</div>

            </a>
        </li>


    </ul>
    <!--end navigation-->
</div>
<!--end sidebar wrapper -->


