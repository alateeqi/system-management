@if (isset($errors)&&count($errors)>0)
    <div class="alert alert-danger border-0 bg-danger alert-dismissible fade show  mt-5">
        <div class="d-flex align-items-center">
            <div class="font-35 text-white"><i class="bx bxs-message-square-x"></i>
            </div>
            <div class="ms-5">
                @foreach ($errors->all() as $error)
                    <div class=" ml-4 mr-4 text-white">{{$error}}</div>
                @endforeach

            </div>
        </div>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif


{{-- @if ($errors->any())

    <div class="alert alert-danger">

        @foreach ($errors->all() as $error)

            <p>{{$error}}</p>
            
        @endforeach

    </div>
    
@endif --}}
